# README #

### What is this repository for? ###
An open-source version of a research project that demonstrates 
a novel approach to malware detection in a large-scale networks (5 million Symantec clients)

Research paper: Exploiting Latent Attack Semantics for Intelligent Malware Detection 
Available: https://arxiv.org/abs/1708.01864


### How do I get set up? ###
Requirements:
- Intellij Idea
- Apache Spark
- XGBoost
- Symantec dataset (please email mikhail.kazdagli@utexas.edu)

### Who do I talk to? ###
Mikhail Kazdagli mikhail.kazdagli@utexas.edu