package edu.ut.austin.Utils;

import edu.ut.austin.DataInputOutput.GraphDataLoader;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.GraphClasses.Node;
import edu.ut.austin.Introspection.DetectionSnapshot;
import edu.ut.austin.Introspection.NBD_State;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.MutableTriple;
import org.apache.ivy.core.module.descriptor.Configuration;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 2/24/2017.
 */
public class Utils {
    public static Double convertUnixTimeStampToDays(Double unix_time_stamp) {
        if (unix_time_stamp == null)
            return null;
        else
            return (double) TimeUnit.SECONDS.toHours(unix_time_stamp.longValue()) / 24;
    }

    public static Double convertUnixTimeStampToDays(long unix_time_stamp) {
        return convertUnixTimeStampToDays((double) unix_time_stamp);
    }

    public static void dumpCollectionToCSV(Collection data, String file_name) {
        try (
            Writer writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(file_name), "ascii"))
        ) {
            for (Object obj : data) {
                writer.write(obj + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void dumpSuspiciousURLs(Map<String, MutableTriple<Double, Integer, Integer>> malicious_urls,
                                          Map<String, MutableTriple<Double, Integer, Integer>> benign_urls, String file_name) {
        System.err.println("<<< DISABLE >>>: dumpSuspiciousURLs()");
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        Map<String, MutableTriple<Double, Integer, Integer>> urls = new LinkedHashMap<>();
        urls.putAll(malicious_urls);
        urls.putAll(benign_urls);
        System.out.format("# of malicious urls: %d   # of benign urls: %d\n", malicious_urls.size(), benign_urls.size());

        try (
                OutputStreamWriter stream_writer = new FileWriter("data/" + file_name);
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (Map.Entry<String, MutableTriple<Double, Integer, Integer>> entry: urls.entrySet()) {
                String str = String.format("%.6f,%d,%d,%s\n", entry.getValue().getLeft(),
                        entry.getValue().getMiddle(), entry.getValue().getRight(), entry.getKey());
                writer.write(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static Set<String> loadMalwareHashes() {
        String file_name = "data/hash_labels.csv";
        Set<String> malware_hashes = new HashSet<>();

        try (
                BufferedReader br = new BufferedReader(new FileReader(file_name))
        ) {
            String line;
            while ((line = br.readLine()) != null)
                malware_hashes.add(line);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!malware_hashes.isEmpty())
            return malware_hashes;

        GraphDataLoader loader = new GraphDataLoader();
        malware_hashes = loader.getFileInfoMapWithoutMapping().values().parallelStream()
                .filter(file_info -> !file_info.is_benign).map(file_info -> file_info.sha2).collect(Collectors.toSet());

        try (
                OutputStreamWriter stream_writer = new FileWriter(file_name);
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (String line : malware_hashes)
                writer.write(line + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return malware_hashes;
    }

    // dumps distribution of nbd based on infected portions
    public static void dumpNBD_Stat(Collection<DetectionSnapshot> snapshots) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        // <percentage of malware in a nbd, number of benign nodes>
        Map<Double, Integer> distribution = snapshots.stream().flatMap(snapshot -> snapshot.nbd_states.values().stream()).parallel()
                .filter(NBD_State::isMalicious).map(nbd -> new ImmutablePair<>(nbd.getUniqueBenignFiles_GroundTruth(0).size(), nbd.getUniqueMaliciousFiles_GroundTruth(0).size()))
                .collect(Collectors.groupingByConcurrent(pair -> (double) pair.getRight() / (pair.getLeft() + pair.getRight()),
                        Collectors.mapping(pair -> pair.getLeft(), Collectors.summingInt(Integer::valueOf))))
                .entrySet().parallelStream().sorted(Collections.reverseOrder(Map.Entry.comparingByKey()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        try (
                OutputStreamWriter stream_writer = new FileWriter(parameters.out_folder + "/nbd_distribution_stat.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (Map.Entry<Double, Integer> entry : distribution.entrySet()) {
                writer.write(entry.getKey() + "," + entry.getValue() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    // dumps nbds' Shape FVs in a format suitable for loading with pandas dataframe API
    public static void dumpNBD_ShapeFVs(Collection<DetectionSnapshot> snapshots, double threshold) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        int nbd_counter = 0;

        try (
                OutputStreamWriter stream_writer = new FileWriter(parameters.out_folder + "/nbds_fvs.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            String header = "label," + IntStream.range(0, parameters.GD_pca_dim_count * parameters.GD_bin_count)
                    .mapToObj(i -> Integer.toString(i)).collect(Collectors.joining(","));
            writer.write(header + "\n");

            for (DetectionSnapshot snapshot : snapshots) {
                for (NBD_State nbd : snapshot.nbd_states.values()) {
                    int label = nbd.mal_ratio < threshold ? 0 : 1;
                    String fv_str = nbd.getFV_String(null);
                    if (fv_str != null) {
                        writer.write(label + "," + fv_str + "\n");
                        nbd_counter ++;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        global_parameters.logger.info("Dumped " + nbd_counter + " NBD FVs");
    }

    public static void dumpDomainNames(Map<Long, DefaultDirectedGraphWrapper> data) {
        System.err.println("<<< DISABLE >>>: dumpDomainNames()");
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        Set<String> domains = data.values().parallelStream().flatMap(graph ->
                graph.edgeSet().stream()).map(edge -> edge.domain).collect(Collectors.toSet());

        try (
                OutputStreamWriter stream_writer = new FileWriter(parameters.out_folder + "/domain_names.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (String domain : domains)
                writer.write(domain + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        Set<String> urls = data.values().parallelStream().flatMap(graph ->
                graph.edgeSet().stream()).map(edge -> edge.url).collect(Collectors.toSet());

        try (
                OutputStreamWriter stream_writer = new FileWriter(parameters.out_folder + "/url_names.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (String url : urls)
                writer.write(url + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static void dumpURL_File_Associations(Map<Long, DefaultDirectedGraphWrapper> data) {
        System.err.println("<<< DISABLE >>>: dumpURL_File_Associations()");
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        Map<String, Set<String>> benign_map = data.values().parallelStream()
                .flatMap(graph -> graph.vertexSet().stream()).filter(Node::isBenign).flatMap(node -> {
                    return node.incoming_url_edges.stream().map(url -> new ImmutablePair<>(url, node.sha2_id));
        }).collect(Collectors.groupingByConcurrent(ImmutablePair::getLeft,
                Collectors.mapping(ImmutablePair::getRight, Collectors.toSet())));

        Map<String, Set<String>> malicious_map = data.values().parallelStream()
                .flatMap(graph -> graph.vertexSet().stream()).filter(Node::isMalicious).flatMap(node -> {
                    return node.incoming_url_edges.stream().map(url -> new ImmutablePair<>(url, node.sha2_id));
        }).collect(Collectors.groupingByConcurrent(ImmutablePair::getLeft,
                Collectors.mapping(ImmutablePair::getRight, Collectors.toSet())));

        try (
                OutputStreamWriter stream_writer = new FileWriter("data/url_benign_file_associations.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (Map.Entry<String, Set<String>> entry : benign_map.entrySet())
                writer.write(entry.getKey() + ","
                        + entry.getValue().stream().collect(Collectors.joining(",")) + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        try (
                OutputStreamWriter stream_writer = new FileWriter("data/url_malicious_file_associations.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (Map.Entry<String, Set<String>> entry : malicious_map.entrySet())
                writer.write(entry.getKey() + ","
                        + entry.getValue().stream().collect(Collectors.joining(",")) + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /* Distribution of lifespans of malicious urls.
    *  For each url we compute the difference between the timestamp of the first served malware sample
    *  and the timestamp of last malware sample. Then we dump the distribution of those intervals.
    * */
    public static void compromised_url_lifetime_distribution(Map<Long, DefaultDirectedGraphWrapper> data) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        Map<String, MutablePair<AtomicLong, AtomicLong>> map = new ConcurrentHashMap<>();

        data.values().parallelStream().flatMap(graph -> graph.vertexSet().stream())
                .filter(Node::isMalicious).forEach(node -> {
            node.incoming_url_edges.forEach(url -> {
                map.putIfAbsent(url, new MutablePair<>(new AtomicLong(node.time_stamp), new AtomicLong(node.time_stamp)));
                MutablePair<AtomicLong, AtomicLong> pair = map.get(url);
                pair.getLeft().getAndUpdate(value -> node.time_stamp < value ? node.time_stamp : value);
                pair.getRight().getAndUpdate(value -> node.time_stamp > value ? node.time_stamp : value);
            });
        });

        List<String> results = map.entrySet().parallelStream().map(entry ->
                entry.getKey() + "," + convertUnixTimeStampToDays(entry.getValue().getRight().get() - entry.getValue().getLeft().get())).collect(Collectors.toList());
        dumpCollectionToCSV(results, parameters.out_folder + "/compromised_url_lifetime_distribution.csv");
    }

    public static String encodeCollectionForPandasDataFrame(Collection<String> collection) {
        if (collection.isEmpty())
            return "[]";
        else
            return "\"[\\\'" + collection.stream().collect(Collectors.joining("\\\',\\\'")) + "\\\']\"";
    }

    public static String getPid() {
        byte array[] = new byte[256];
        InputStream is = null;
        try {
            is = new FileInputStream("/proc/self/stat");
            is.read(array);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < array.length; i++) {
            if ((array[i] < '0') || (array[i] > '9')) {
                return new String(array, 0, i);
            }
        }
        return "-1";
    }

    public static void dumpRandomlySelectedNBDs(Map<Long, DefaultDirectedGraphWrapper> data) {
        String line;
        String file_path = ConfigurationParameters.getInstance().out_folder + File.separator + "nbd_sizes.csv";
        String file_path_dump = ConfigurationParameters.getInstance().out_folder + File.separator + "nbd_random_selection_malware_ratio.csv";
        List<Integer> nbd_sizes = new LinkedList<>();
        List<Double> nbd_infection_rates = new LinkedList<>();
        ConcurrentHashMap<String, Node> map = new ConcurrentHashMap<>();

        try (
                BufferedReader br = new BufferedReader(new FileReader(file_path))
        ){
            while ((line = br.readLine()) != null)
                nbd_sizes.add(Integer.parseInt(line));
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }

        data.values().parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).forEach(node -> {
                    map.putIfAbsent(node.sha2_id, node);
        });

        ArrayList<Node> nodes = new ArrayList<>(map.values());

        for (int nbd_size : nbd_sizes) {
            Supplier<Stream<Node>> supplier = () ->
                    new Random().ints(nbd_size, 0, nodes.size()).mapToObj(nodes::get);
            long benign_count = supplier.get().filter(Node::isBenign).count();
            long malicious_count = supplier.get().filter(Node::isMalicious).count();
            double ratio = (double) malicious_count / (malicious_count + benign_count);
            nbd_infection_rates.add(ratio);
        }
        Utils.dumpCollectionToCSV(nbd_infection_rates, file_path_dump);
        System.exit(0);
    }
}
