package edu.ut.austin.Utils;

import org.nd4j.linalg.api.ndarray.INDArray;

/**
 * Created by Mikhail on 3/14/2017.
 */
@FunctionalInterface
public interface Distance {
    double computeDistance(INDArray vec1, INDArray vec2);
}
