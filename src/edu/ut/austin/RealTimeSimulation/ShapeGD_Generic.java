package edu.ut.austin.RealTimeSimulation;

import edu.ut.austin.DataInputOutput.GraphDataLoader;
import edu.ut.austin.DataInputOutput.ShapeGD_DataLoader;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.ML_Models.TrainShapeGD;

import java.util.Set;

/**
 * Created by Mikhail on 5/8/2017.
 */
public class ShapeGD_Generic {
    private ShapeGD_Spark shape_gd_spark = null;
    private ClusterSensitiveShapeGD cluster_sensitive_shape_gd_spark = null;
    public boolean is_invalid;

    public ShapeGD_Generic(boolean load_shape_gd_only) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        switch (parameters.shape_gd_type) {
            case GlobalGD:
                shape_gd_spark = ShapeGD_DataLoader.loadShapeGD_Spark(load_shape_gd_only);
                break;

            case PerNBD_GD:
                cluster_sensitive_shape_gd_spark = ClusterSensitiveShapeGD.load();
                break;
        }
        is_invalid = (shape_gd_spark == null) && (cluster_sensitive_shape_gd_spark == null);
    }

    public ShapeGD getClusterSensitiveShapeGD(int cluster_id) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        switch (parameters.shape_gd_type) {
            case GlobalGD:
                return shape_gd_spark;

            case PerNBD_GD:
                return cluster_sensitive_shape_gd_spark.getClusterSensitiveShapeGD(cluster_id);
        }
        return null;
    }

    public void trainShapeGD(GraphDataLoader loader) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        switch (parameters.shape_gd_type) {
            case GlobalGD:
                shape_gd_spark = TrainShapeGD.trainGlobalShapeGD();
                break;

            case PerNBD_GD:
                cluster_sensitive_shape_gd_spark = TrainShapeGD.trainMultipleConditionalShapeGDs(loader);
                break;
        }
    }

    public double[] getDimWeights() {
        return getClusterSensitiveShapeGD(0).getDimWeights();
    }

    public double[][] getRefHist() {
        return getClusterSensitiveShapeGD(0).getRefHist();
    }

    public Set<String> getLD_FP_ListSha256() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        switch (parameters.shape_gd_type) {
            case GlobalGD:
                return shape_gd_spark.getLD_FP_ListSha256();

            case PerNBD_GD:
                return cluster_sensitive_shape_gd_spark.getLD_FP_ListSha256();
        }
        return null;
    }

    public Set<String> getLD_TP_ListSha256() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        switch (parameters.shape_gd_type) {
            case GlobalGD:
                return shape_gd_spark.getLD_TP_ListSha256();

            case PerNBD_GD:
                return cluster_sensitive_shape_gd_spark.getLD_TP_ListSha256();
        }
        return null;
    }
}
