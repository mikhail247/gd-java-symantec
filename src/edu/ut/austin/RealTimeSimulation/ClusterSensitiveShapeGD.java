package edu.ut.austin.RealTimeSimulation;

import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import org.nustaq.serialization.FSTConfiguration;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Mikhail on 5/8/2017.
 */
// thread-safe class
public class ClusterSensitiveShapeGD implements Serializable {
    // <cluster_id, shape_gd>
    private ConcurrentSkipListMap<Integer, ShapeGD> shape_gd_map;

    public ClusterSensitiveShapeGD() {
        shape_gd_map = new ConcurrentSkipListMap<>();
    }

    public void addShapeGD(int cluster_id, ShapeGD shape_gd) {
        shape_gd_map.put(cluster_id, shape_gd);
    }

    public ShapeGD getClusterSensitiveShapeGD(int cluster_id) {
        return shape_gd_map.get(cluster_id);
    }

    public void save() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        global_parameters.logger.info("Serializing ShapeGD_Spark object");
        FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();
        byte serialized_object[] = conf.asByteArray(this);

        try (
                OutputStream file = new FileOutputStream(parameters.shape_gd_spark_file_name);
                OutputStream output = new BufferedOutputStream(file);
        ) {
            output.write(serialized_object);
        } catch (IOException ex) {
            global_parameters.logger.log(Level.SEVERE, "Cannot serialize IntrospectionModule", ex);
            System.exit(-1);
        }
    }

    public static ClusterSensitiveShapeGD load() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();
        Logger logger = GlobalParameters.getInstance().detection_logger;

        try {
            byte serialized_object[] = Files.readAllBytes(Paths.get(parameters.shape_gd_spark_file_name));
            ClusterSensitiveShapeGD shape_gd = (ClusterSensitiveShapeGD) conf.asObject(serialized_object);
            logger.info("ShapeGD_Spark Object loaded");
            return shape_gd;
        } catch (IOException e) {
            System.err.println("Cannot deserialize ShapeGD_Spark Object.");
            logger.log(Level.SEVERE, "Cannot deserialize ShapeGD_Spark Object.", e);
        }
        return null;
    }

    public Set<String> getLD_FP_ListSha256() {
        return null;
    }

    public Set<String> getLD_TP_ListSha256() {
        return null;
    }
}
