package edu.ut.austin.RealTimeSimulation;

import edu.ut.austin.DataInputOutput.GraphDataLoader;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.GraphClasses.Edge;
import edu.ut.austin.GraphClasses.Node;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jgrapht.graph.DirectedSubgraph;
import org.nustaq.serialization.FSTConfiguration;
import org.nustaq.serialization.serializers.FSTCollectionSerializer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 2/1/2017.
 */
public class RealTimeSimulation {
    private final long max_time_stamp;
    private long current_time;
    // <machine_id, <root, IG graph>>
    private Map<Long, Map<String, DefaultDirectedGraphWrapper>> unmodifiable_data;
    private ArrayList<ConcurrentHashMap<Long, DefaultDirectedGraphWrapper>> data;
    private GraphDataLoader loader;
    // <current time, sliced graphs>
    private LinkedBlockingDeque<Pair<Long, Collection<DefaultDirectedGraphWrapper>>> graph_buffer;
    private Thread data_producer;

    public RealTimeSimulation(GraphDataLoader loader) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        this.max_time_stamp = loader.getLastTimeStamp();
        this.unmodifiable_data = loader.getUnmodifiableData();
        this.loader = loader;
        this.graph_buffer = new LinkedBlockingDeque<>(100);
        initDataStorage();

        if (parameters.rebuild_graph_cache != null && parameters.rebuild_graph_cache) {
            try {
                FileUtils.deleteDirectory(new File(parameters.graph_cache_folder));
                Files.createDirectories(Paths.get(parameters.graph_cache_folder));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        data_producer = null;
        if (parameters.engine_thread_count <= 1) {
            data_producer = new Thread(this::runRealTimeDataProducer);
            data_producer.setPriority(Thread.MAX_PRIORITY);
            data_producer.start();
        }
    }

    private void initDataStorage() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        long start_time = loader.getFirstTimeStamp() - 1;
        this.current_time = start_time;

        int size = (int) ((max_time_stamp + parameters.time_window_size - start_time)/parameters.shift_rolling_time_window);
        data = new ArrayList<>(size);
        ArrayList<ConcurrentHashMap<Long, DefaultDirectedGraphWrapper>>  data_t = new ArrayList<>(size);
        IntStream.range(0, size).forEach(i -> data_t.add(new ConcurrentHashMap<>()));
        IntStream.range(0, size).forEach(i -> data.add(new ConcurrentHashMap<>()));
        global_parameters.logger.info("Initializing RealTimeSimulation Object (size: " + size + "/" + unmodifiable_data.size() + ")");


        unmodifiable_data.entrySet().parallelStream().flatMap(entry -> entry.getValue().values().stream()).forEach(graph -> {
            int start_index = (int) ((graph.getMinTimeStamp() - start_time)/parameters.shift_rolling_time_window);
            int end_index = (int) ((graph.getMaxTimeStamp() - start_time)/parameters.shift_rolling_time_window);

            for (int i = start_index; i <= end_index; i ++)
                data_t.get(i).put(graph.machine_id, graph);
        });

        int win_size = (int) (parameters.time_window_size / parameters.shift_rolling_time_window);
        if (parameters.time_window_size % parameters.shift_rolling_time_window != 0)
                throw new RuntimeException("time_window_size must be a multiple of shift_rolling_time_window");

        IntStream.range(0, data_t.size()).forEach(i -> { // must be a sequential stream
            int win_start_index = Math.max(0, i - (win_size - 1));
            for (int j = i; j >= win_start_index; j --)
                data.get(i).putAll(data_t.get(j));
        });
        global_parameters.logger.info("RealTimeSimulation Object initialized");
    }

    public int getEpochCount() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        return (int) ((max_time_stamp + parameters.time_window_size
                - loader.getFirstTimeStamp() + 1) / parameters.shift_rolling_time_window + 1);
    }

    public synchronized Pair<Long, Collection<DefaultDirectedGraphWrapper>> getRealTimeDataSpeculative() {
        Collection<DefaultDirectedGraphWrapper> sliced_graphs;
        Pair<Long, Collection<DefaultDirectedGraphWrapper>> queue_head;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        current_time += parameters.shift_rolling_time_window;

        sliced_graphs = loadSlicedGraphs(current_time);
        while (sliced_graphs == null) {

            System.out.println("Slicing Graphs: getRealTimeDataSpeculative()");
            if (max_time_stamp < current_time - parameters.time_window_size) {
                try {
                    data_producer.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
                return null;
            }

            while ((queue_head = graph_buffer.pollFirst()) == null) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (queue_head.getLeft() == current_time) {
                sliced_graphs = queue_head.getRight();
                break;
            }
            if (queue_head.getLeft() > current_time) {
                global_parameters.logger.log(Level.SEVERE, "Slicing data: missing timestamp");
                System.exit(-1);
            }
        }

        long completed = 100 * (current_time - loader.getFirstTimeStamp()) / (loader.getLastTimeStamp() + parameters.time_window_size - loader.getFirstTimeStamp());
        Date current_date = new Date(current_time * 1000);
        System.out.format("Slicing Graphs in parallel: Current virtual date: %s\t(%s) Completed: %d %%\n",
                current_date, loader.getDatasetTimeScale(), completed);

        saveSlicedGraphs(sliced_graphs, current_time);
        return new ImmutablePair<>(current_time, sliced_graphs);
    }

    private Collection<DefaultDirectedGraphWrapper> loadSlicedGraphs(long time_stamp) {
        Collection<DefaultDirectedGraphWrapper> sliced_graphs = null;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();
        Date current_date = new Date(time_stamp*1000);

        if (parameters.rebuild_graph_cache == null)
            return null;

        try {
            byte serialized_object[] = Files.readAllBytes(Paths.get(parameters.graph_cache_folder + File.separator + time_stamp));
            sliced_graphs = (Collection<DefaultDirectedGraphWrapper>) conf.asObject(serialized_object);
        } catch (IOException ex) {
            global_parameters.logger.log(Level.SEVERE, "Cannot deserialize sliced graphs: " + current_date);
        }
        return sliced_graphs;
    }

    private void saveSlicedGraphs(Collection<DefaultDirectedGraphWrapper> sliced_graphs, long time_stamp) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        Date current_date = new Date(time_stamp*1000);

        if (parameters.rebuild_graph_cache == null)
            return;

        Set<String> existing_files = Stream.of(new File(parameters.graph_cache_folder).listFiles())
                .map(File::getName).collect(Collectors.toSet());

        if (existing_files.contains(Long.toString(time_stamp))) {
            //global_parameters.logger.info("Graph found in cache. timestamp: " + current_date);
            return;
        }

        FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();
        conf.setShareReferences(true);
        conf.registerSerializer(HashSet.class, new FSTCollectionSerializer(),true);
        global_parameters.logger.info("Saving sliced graphs. timestamp: " + current_date);

        try (
                OutputStream file = new FileOutputStream(parameters.graph_cache_folder + File.separator + time_stamp);
                OutputStream output = new BufferedOutputStream(file);
        ) {
            byte serialized_object[] = conf.asByteArray(sliced_graphs);
            output.write(serialized_object);
            System.out.println("Saved sliced graphs. timestamp: " + current_date);
        } catch (Exception ex) {
            global_parameters.logger.log(Level.SEVERE, "Cannot Serialize sliced graphs " + current_date, ex);
            ex.printStackTrace();
        }
    }

    private void runRealTimeDataProducer() {
        Set<String> existing_files = null;
        Collection<DefaultDirectedGraphWrapper> sliced_data;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        long start_time = loader.getFirstTimeStamp() - 1;
        long current_time = start_time + parameters.shift_rolling_time_window;
        long max_time_stamp = loader.getLastTimeStamp();

        if (parameters.rebuild_graph_cache != null)
            existing_files = Stream.of(new File(parameters.graph_cache_folder).listFiles())
                    .map(File::getName).collect(Collectors.toSet());

        while (max_time_stamp >= current_time - parameters.time_window_size) {
            if (parameters.rebuild_graph_cache != null && !parameters.rebuild_graph_cache) {
                Date current_date = new Date(current_time * 1000);
                if (existing_files.contains(Long.toString(current_time))) {
                    current_time += parameters.shift_rolling_time_window;
                    continue;
                }
            }

            int current_time_index = (int) ((current_time - start_time) / parameters.shift_rolling_time_window) - 1;
            Map<Long, DefaultDirectedGraphWrapper> map = new HashMap<>(data.get(current_time_index));
            int win_size = (int) (parameters.time_window_size / parameters.shift_rolling_time_window);
            assert (parameters.time_window_size % parameters.shift_rolling_time_window == 0);
            int win_start_index = Math.max(0, current_time_index - (win_size - 1));

            for (int i = win_start_index; i < current_time_index; i++)
                map.putAll(data.get(i));

            final long final_current_time = current_time;
            sliced_data = map.entrySet().parallelStream().map(entry -> {
                long machine_id = entry.getKey();
                long start_time_slicing = final_current_time - parameters.time_window_size;
                if (entry.getValue().isGraphSliceable(start_time_slicing, final_current_time)) {
                    DefaultDirectedGraphWrapper graph = (DefaultDirectedGraphWrapper) entry.getValue().clone();
                    graph.sliceGraph(start_time_slicing, final_current_time);
                    if (!graph.isGraphEmpty())
                        return graph;
                }
                return null;
            }).filter(Objects::nonNull).collect(Collectors.toList());

            try {
                graph_buffer.putLast(new ImmutablePair<>(current_time, sliced_data));
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(-1);
            }
            current_time += parameters.shift_rolling_time_window;
        }
    }

    public Pair<Long, Collection<DefaultDirectedGraphWrapper>> getRealTimeData(long time_stamp, int thread_id) {
        Collection<DefaultDirectedGraphWrapper> sliced_graphs;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        long start_time = loader.getFirstTimeStamp() - 1;
//        current_time += parameters.shift_rolling_time_window;
        int current_time_index = (int) ((time_stamp - start_time)/parameters.shift_rolling_time_window) - 1;

        if (max_time_stamp < time_stamp - parameters.time_window_size)
            return null;

        Collection<DefaultDirectedGraphWrapper> map = new LinkedList<>(data.get(current_time_index).values());
        int win_size = (int) (parameters.time_window_size / parameters.shift_rolling_time_window);
        if (parameters.time_window_size % parameters.shift_rolling_time_window != 0)
            throw new RuntimeException("time_window_size must be a multiple of shift_rolling_time_window");
        int win_start_index = Math.max(0, current_time_index - (win_size - 1));

        for (int i = win_start_index; i < current_time_index; i ++)
            map.addAll(data.get(i).values());

        sliced_graphs = sliceDataMap(time_stamp, map);
        return new ImmutablePair<>(time_stamp, sliced_graphs);
    }

    public Collection<DefaultDirectedGraphWrapper> getRealTimeData(int thread_id) {
        Collection<DefaultDirectedGraphWrapper> sliced_data;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        long start_time = loader.getFirstTimeStamp() - 1;
        current_time += parameters.shift_rolling_time_window;
        int current_time_index = (int) ((current_time - start_time)/parameters.shift_rolling_time_window) - 1;

        if (max_time_stamp < current_time - parameters.time_window_size)
            return null;

        Collection<DefaultDirectedGraphWrapper> map = new LinkedList<>(data.get(current_time_index).values());
        int win_size = (int) (parameters.time_window_size / parameters.shift_rolling_time_window);
        assert(parameters.time_window_size % parameters.shift_rolling_time_window == 0);
        int win_start_index = Math.max(0, current_time_index - (win_size - 1));

        for (int i = win_start_index; i < current_time_index; i ++)
            map.addAll(data.get(i).values());

        sliced_data = sliceDataMap(this.current_time, map);
        return sliced_data;
    }

    private Collection<DefaultDirectedGraphWrapper> sliceDataMap(long time_stamp, Collection<DefaultDirectedGraphWrapper> map) {
        Collection<DefaultDirectedGraphWrapper> sliced_data;
        int vcpu_count =  GlobalParameters.getInstance().executor_size;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        ConcurrentLinkedDeque<DefaultDirectedGraphWrapper> job_queue = new ConcurrentLinkedDeque<>(map);

        class ParallelDataSupplier implements Callable<Map<Long, DefaultDirectedGraphWrapper>> {
            private int thread_id;
            private long start_time, end_time;

            private ParallelDataSupplier(int thread_id, long start_time, long end_time) {
                this.thread_id = thread_id;
                this.start_time = start_time;
                this.end_time = end_time;
            }

            @Override
            public Map<Long, DefaultDirectedGraphWrapper> call() throws Exception {
                Map<Long, DefaultDirectedGraphWrapper> sliced_data = new HashMap<>();

                while(!job_queue.isEmpty()) {
                    DefaultDirectedGraphWrapper graph = thread_id % 2 == 0 ? job_queue.pollFirst() : job_queue.pollLast();
                    if (graph == null)
                        break;
                    unmodifiable_data.get(graph.machine_id).values().stream().filter(graph_t -> graph_t.isGraphSliceable(start_time, end_time))
                            .forEach(graph_t -> {
                                DefaultDirectedGraphWrapper graph_for_slicing = (DefaultDirectedGraphWrapper) graph_t.clone();
                                graph_for_slicing.sliceGraph(start_time, end_time);
                                if (!graph_for_slicing.isGraphEmpty())
                                    sliced_data.put(graph.machine_id, graph_for_slicing);
                    });
                }
                return sliced_data;
            }
        }

        long start_time = time_stamp - ConfigurationParameters.getInstance().time_window_size;
        // equal size batch partitioning is not optimal
        sliced_data = map.parallelStream().map(graph -> {
                if (graph.isGraphSliceable(start_time, time_stamp)) {
                    DefaultDirectedGraphWrapper graph_t = (DefaultDirectedGraphWrapper) graph.clone();
//                    DirectedSubgraph<Node, Edge> graph_t = new DirectedSubgraph<>(graph);
                    graph_t.sliceGraph(start_time, time_stamp);
//                    if (graph_t.vertexSet().stream().anyMatch(node -> graph_t.edgesOf(node).isEmpty()))
//                        System.out.println('x');

                    if (!graph_t.isGraphEmpty())
                        return graph_t;
                }
                return null;
        }).filter(Objects::nonNull).collect(Collectors.toList());

        System.out.println("Graphs sliced");
        return Collections.unmodifiableCollection(sliced_data);
    }

    public long getCurrentTime() {
        return current_time;
    }
}
