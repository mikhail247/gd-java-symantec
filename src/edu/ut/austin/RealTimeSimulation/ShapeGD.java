package edu.ut.austin.RealTimeSimulation;

import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.ML_Models.TrainML_Models;
import edu.ut.austin.NBD.Cluster;
import ml.dmlc.xgboost4j.java.Booster;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * Created by Mikhail on 3/30/2017.
 */
public abstract class ShapeGD implements Serializable {
    // contains only FPs and TPs, should be loaded from parquet
    // <sha256, FV>
    protected Map<String, List<int[]>> data;

    public int dims, bin_count;
    public double[][] edges;
    public double ref_hist[][];
    public double dim_weights[];
    public TreeMap<Integer, double[]> thresholds;

    public abstract Triple<Boolean, Double, double[][]> processNBD(long current_virtual_time, int nbd_index, int trial,
                                                                   Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>> nbd_structure, Cluster cluster );

    public double[][] getEdges() {
        return edges;
    }

    double[][] buildNormalizedHistogramFromVeatureVectors(List<Integer> fv_indexes, ShapeGD_Matlab.Data data) {
        int fvs_count = data.fvs.length;
        double hist[][] = new double[dims][bin_count];

        for (int ind : fv_indexes) {
            ind %= fvs_count;
            double fv[] = data.fvs[ind];;

            for (int dim = 0; dim < dims; dim ++) {
                int bin_index = getBinIndex(fv[dim], dim);
                hist[dim][bin_index]++;
            }
        }
        return hist;
    }

    // returns bucket indices
    // hashes contains sha2 hashes
    public List<int[]> getBucketVectorsFromSpark(Map<String, List<int[]>> data, Collection<String> hash_set) {
        Map<String, Long> hash_map = hash_set.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        return getBucketVectorsFromSpark(data, hash_map);
    }

    // returns bucket indices
    // accepts sha256 hashes of the files whose FVs are in the data map
    public List<int[]> getBucketVectorsFromSpark(Map<String, List<int[]>> data, Map<String, Long> hashes) {
        List<int[]> fvs = new LinkedList<>();

        for (Map.Entry<String, Long> entry : hashes.entrySet()) {
            List<int[]> fv = data.get(entry.getKey());
            if (fv == null) continue; // data contains only FPs, thus not all hashes can be found in it
            LongStream.range(0, entry.getValue()).forEach(i -> fvs.addAll(fv));
        }
        return fvs;
    }

    double[][] buildNormalizedHistogramFromVeatureVectors(List<double[]> data) {
        double hist[][] = new double[dims][bin_count];

        for (double fv[] : data) {
            for (int dim = 0; dim < dims; dim ++) {
                int bin_index = getBinIndex(fv[dim], dim);
                hist[dim][bin_index]++;
            }
        }

        // normalize ref_hist
        for (int i = 0; i < dims ; i++) {
            double sum = 0;
            for (int j = 0; j < bin_count; j++)
                sum += hist[i][j];
            for (int j = 0; j < bin_count; j++)
                hist[i][j] /= sum;
        }
        return hist;
    }

    public double[][] buildNormalizedHistogramFromBucketIndices(List<int[]> data) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        double hist[][] = new double[dims][bin_count];

        for (int bucket_vector[] : data) {
            for (int dim = 0; dim < dims; dim ++) {
                int bin_index = bucket_vector[dim];
                hist[dim][bin_index]++;
            }
        }

        // normalize ref_hist
        for (int i = 0; i < dims ; i++) {
            double sum = 0;
            for (int j = 0; j < bin_count; j++)
                sum += hist[i][j];
            if (sum < parameters.epsilon)
                continue;
            for (int j = 0; j < bin_count; j++)
                hist[i][j] /= sum;
        }
        return hist;
    }

    private int getBinIndex(double value, int dim) {
        int i;

        for (i = 0; i < edges[dim].length; i ++) {
            if (value <= edges[dim][i])
                return i;
        }
        return i;
    }

    public double computeWassersteinDistance(double hist[][]) {
        double distance[] = new double[dims];

        for (int dim = 0; dim < dims; dim ++) {
            double pairwise_distance[] = new double[bin_count+1];
            for (int i = 0; i < bin_count; i ++)
                pairwise_distance[i+1] = pairwise_distance[i] + hist[dim][i] - ref_hist[dim][i];

            for (int i = 0; i < bin_count; i ++)
                distance[dim] += Math.abs(pairwise_distance[i]);
        }

        double dist = 0;
        for (int i = 0; i < dims; i ++)
            dist += distance[i]* dim_weights[i];
        return dist;
    }

    @Deprecated
    double[][] mergeAndNormalizeHistograms(double hist_1[][], double hist_2[][]) {
        double hist[][] = new double[dims][bin_count];

        for (int i = 0; i < dims; i ++) {
            for (int j = 0; j < bin_count; j ++)
                hist[i][j] = hist_1[i][j] + hist_2[i][j];
        }

        for (int dim = 0; dim < dims; dim ++) {
            int sum = 0;
            for (int i = 0; i < bin_count; i ++)
                sum += hist[dim][i];
            if (sum != 0) {
                for (int i = 0; i < bin_count; i++)
                    hist[dim][i] /= sum;
            }
        }
        return hist;
    }

    public Map<String, List<int[]>> getData() {
        return data;
    }

    public double[] getDimWeights() {
        return dim_weights;
    }

    public double[][] getRefHist() {
        return ref_hist;
    }
}
