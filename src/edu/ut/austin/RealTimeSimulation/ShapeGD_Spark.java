package edu.ut.austin.RealTimeSimulation;

import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.Introspection.IntrospectionModule;
import edu.ut.austin.NBD.Cluster;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.nustaq.serialization.FSTConfiguration;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

/**
 * Created by Mikhail on 3/30/2017.
 */
public class ShapeGD_Spark extends ShapeGD {
    // LDs' FPs and LDs' TPs
    private ArrayList<String> benignware_hash_list, malware_hash_list;
    // <virtual_time, <nbd_hash, prediction>>
    private transient Map<String, Double> xgboost_predictor;

    public ShapeGD_Spark(double edges[][], double ref_hist[][], double dim_weights[], TreeMap<Integer, double[]> thresholds) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        this.dims = parameters.GD_pca_dim_count;
        this.bin_count = parameters.GD_bin_count;
        this.edges = edges;
        this.ref_hist = ref_hist;
        this.data = null;
        this.dim_weights = dim_weights;
        this.thresholds = thresholds;
        this.benignware_hash_list = null;
        this.malware_hash_list = null;
    }

    public double[] getDimWeights() {
        return dim_weights;
    }

    public double[][] getRefHist() {
        return ref_hist;
    }

    // Currently used to build VT-level FVs, other output is set to default values
    // <machine_id, <benign file hashes, malicious file hashes>>
    public Triple<Boolean, Double, double[][]> processNBD(long current_virtual_time, int nbd_index,
                                                          int trial, Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>> nbd_structure, Cluster cluster ) {
        Map<String, Long> assigned_benignware_hashes, assigned_malware_hashes;

        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        int benign_file_count = nbd_structure.entrySet().stream().mapToInt(entry -> entry.getValue().getLeft().size()).sum();
        int malicious_file_count = nbd_structure.entrySet().stream().mapToInt(entry -> entry.getValue().getRight().size()).sum();

        if (benign_file_count + malicious_file_count < parameters.GD_fvs_per_nbd_count)
            return null;

        if (parameters.use_static_hash_mapping) {
            assigned_benignware_hashes = nbd_structure.values().stream()
                    .flatMap(pair -> pair.getLeft().stream()).map(file_info -> file_info.redirect.sha2)
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

            assigned_malware_hashes = nbd_structure.values().stream()
                    .flatMap(pair -> pair.getRight().stream()).map(file_info -> file_info.redirect.sha2)
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        } else {
            Random random = new Random(12345);
            assigned_benignware_hashes = IntStream.range(0, benign_file_count)
                    .map(i -> random.nextInt(benignware_hash_list.size())).mapToObj(index -> benignware_hash_list.get(index))
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            assigned_malware_hashes = IntStream.range(0, malicious_file_count)
                    .map(i -> random.nextInt(malware_hash_list.size())).mapToObj(index -> malware_hash_list.get(index))
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        }

        List<int[]> benign_dataset = getBucketVectorsFromSpark(data, assigned_benignware_hashes);
        List<int[]> malicious_dataset = getBucketVectorsFromSpark(data, assigned_malware_hashes);

        List<int[]> dataset = new LinkedList<>(benign_dataset);
        dataset.addAll(malicious_dataset);

        if(dataset.isEmpty())
            return null;

        double hist[][] = buildNormalizedHistogramFromBucketIndices(dataset);
        double score = computeWassersteinDistance(hist);
        double thresholds_vector[] = {0}; // to cover the case when we use POST_PROCESSING_XGBOOST and gd does not have the corresponding threshold vector

        Map.Entry<Integer, double[]> entry;
        if (parameters.use_adaptive_shape_gd)
            entry = thresholds.floorEntry(benign_file_count + malicious_file_count);
        else
            entry = thresholds.floorEntry(parameters.GD_fvs_per_nbd_count);

        // a quick fix for the case when a corresponding threshold vector is absent
        if (entry != null || parameters.shape_gd_implementation != ConfigurationParameters.ShapeGDImplementation.POST_PROCESSING_XGBOOST) {
            thresholds_vector = entry.getValue();
        }

        double upper_threshold, lower_threshold;
        if (parameters.threshold_percentile < 100) {
            upper_threshold = (new Percentile()).evaluate(thresholds_vector, parameters.threshold_percentile);
            lower_threshold = (new Percentile()).evaluate(thresholds_vector, 100 - parameters.threshold_percentile);
        } else {
            double scaling_coefficient = parameters.threshold_percentile / 100;
            DoubleSummaryStatistics stat = Arrays.stream(thresholds_vector).summaryStatistics();
            upper_threshold = stat.getMax() * scaling_coefficient;
            lower_threshold = stat.getMin() / scaling_coefficient;
        }
        boolean cluster_label = score >= upper_threshold || score <= lower_threshold;

        // use an IDEAL approximation to achieve the best possible results
        cluster_label = (double) malicious_file_count/(benign_file_count + malicious_file_count) >= parameters.enable_ideal_shape_gd;
        return  new ImmutableTriple<>(true, -1.0, hist);
    }

    // returns bucket indices
    @Deprecated
    public static List<int[]> getBucketVectorsFromSpark(Dataset<Row> data, Map<String, Long> hashes) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        int dims = parameters.GD_pca_dim_count;
        ArrayList<String> col_names = IntStream.range(0, dims)
                .mapToObj(i -> "pca_feature_bucket_" + i).collect(Collectors.toCollection(ArrayList::new));

        JavaRDD<int[]> dataset = data.filter("LD > 0.5").toJavaRDD().flatMap(new FlatMapFunction<Row, int[]>() {
            @Override
            public Iterator<int[]> call(Row row) throws Exception {
                List<int[]> ret = new LinkedList<>();
                String sha256 = row.<String>getAs("sha256");
                Long value = hashes.get(sha256);
                if (value != null) {
                    int fv[] = new int[dims];
                    for (int i = 0; i < dims; i++)
                        fv[i] = row.<Double>getAs(col_names.get(i)).intValue();
                    LongStream.range(0, value).forEach(i -> {ret.add(fv);});
                }
                return ret.iterator();
            }
        });
        return dataset.collect();
    }

    public static List<double[]> getFeatureVectorsFromSpark(Dataset<Row> data, Map<String, Long> hashes) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        double decision_threshold = 0.5;
        if (parameters.LF_implementation_type != ConfigurationParameters.LD_ImplementationType.SPARK_RF)
            decision_threshold = parameters.LD_threshold;

        JavaRDD<double[]> dataset = data.filter("LD > " + decision_threshold).toJavaRDD().flatMap(new FlatMapFunction<Row, double[]>() {
            @Override
            public Iterator<double[]> call(Row row) throws Exception {
                List<double[]> ret = new LinkedList<>();
                String sha256 = row.<String>getAs("sha256");
                Long value = hashes.get(sha256);
                if (value != null) {
                    for (int i = 0; i < value; i++)
                        ret.add(row.<org.apache.spark.ml.linalg.Vector>getAs("pca_features").copy().toArray());
                }
                return ret.iterator();
            }
        });
        return dataset.collect();
    }

    public void initData(Dataset<Row> data) {
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        ArrayList<String> col_names = IntStream.range(0, dims)
                .mapToObj(i -> "pca_feature_bucket_" + i).collect(Collectors.toCollection(ArrayList::new));

        this.data = data.collectAsList().parallelStream().collect(Collectors.groupingBy(
                row -> row.<String>getAs("sha256"), Collectors.mapping(row -> {
                    int fv[] = new int[dims];
                    for (int i = 0; i < dims; i++)
                        fv[i] = row.<Double>getAs(col_names.get(i)).intValue();
                    return fv;
                }, Collectors.toList())
        ));

        this.benignware_hash_list = data.filter("label == 0").select("sha256").distinct()
                .collectAsList().parallelStream().map(row -> row.<String>getAs("sha256")).collect(Collectors.toCollection(ArrayList::new));
        this.malware_hash_list = data.filter("label == 1").select("sha256").distinct()
                .collectAsList().parallelStream().map(row -> row.<String>getAs("sha256")).collect(Collectors.toCollection(ArrayList::new));
    }

    public Set<String> getLD_FP_ListSha256() {
        return Collections.unmodifiableSet(new HashSet<>(benignware_hash_list));
    }

    public Set<String> getLD_TP_ListSha256() {
        return Collections.unmodifiableSet(new HashSet<>(malware_hash_list));
    }


    public void setXGBoostPredictor(Map<String, Double> xgboost_predictor) {
        this.xgboost_predictor = xgboost_predictor;
    }

    public void save() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        global_parameters.logger.info("Serializing ShapeGD_Spark object");
        FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();
        byte serialized_object[] = conf.asByteArray(this);

        try (
                OutputStream file = new FileOutputStream(parameters.shape_gd_spark_file_name);
                OutputStream output = new BufferedOutputStream(file);
        ) {
            output.write(serialized_object);
        } catch (IOException ex) {
            global_parameters.logger.log(Level.SEVERE, "Cannot serialize IntrospectionModule", ex);
            System.exit(-1);
        }
    }

    public static ShapeGD_Spark load() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        ShapeGD_Spark shape_gd_spark = null;
        FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();
        Logger logger = GlobalParameters.getInstance().detection_logger;

        try {
            byte serialized_object[] = Files.readAllBytes(Paths.get(parameters.shape_gd_spark_file_name));
            shape_gd_spark = (ShapeGD_Spark) conf.asObject(serialized_object);
            logger.info("ShapeGD_Spark Object loaded");
            assert(parameters.shape_gd_implementation != ConfigurationParameters.ShapeGDImplementation.THRESHOLD
                    || (parameters.shape_gd_implementation == ConfigurationParameters.ShapeGDImplementation.THRESHOLD
                    && shape_gd_spark.thresholds.firstEntry().getKey() <= parameters.GD_fvs_per_nbd_count));
        } catch (IOException e) {
            System.err.println("Cannot deserialize ShapeGD_Spark Object.");
            logger.log(Level.SEVERE, "Cannot deserialize ShapeGD_Spark Object.", e);
        }

        String line;
        String file_name = "data/nbd_classification_fp_tp_" + parameters.data_file_count + ".csv";
        Map<String, Double> xgboost_predictor = new ConcurrentHashMap<>();

        try (
                BufferedReader br = new BufferedReader(new FileReader(file_name));
        ){
            while ((line = br.readLine()) != null) {
                String array[] = line.split(",");
                double prediction = Double.parseDouble(array[1]);
                xgboost_predictor.put(array[0], prediction);
            }
        } catch (Exception e) {
            e.printStackTrace();
            global_parameters.logger.log(Level.SEVERE, file_name + " not found");
        }
        shape_gd_spark.setXGBoostPredictor(xgboost_predictor);
        return shape_gd_spark;
    }
}
