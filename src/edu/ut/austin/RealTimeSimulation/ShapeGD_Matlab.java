package edu.ut.austin.RealTimeSimulation;

import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.Introspection.IntrospectionModule;
import edu.ut.austin.NBD.Cluster;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import java.util.*;

/**
 * Created by Mikhail on 2/8/2017.
 */
public class ShapeGD_Matlab extends ShapeGD {
    public static class Data {
        public boolean fv_indices[];
        public double fvs[][];

        public Data(boolean fv_indices[], double fvs[][]) {
            this.fv_indices = fv_indices;
            this.fvs = fvs;
        }
    }

    private double pca_mean[];
    private double pca_basis[][];
    private Data benignware, malware;

    // Creating a ShapeGD object from Matlab's output
    public ShapeGD_Matlab(double ref_hist[][], double edges[][], double dim_weight[], TreeMap<Integer, double[]> thresholds,
                          double pca_mean[], double pca_basis[][], Data benignware, Data malware) {
        this.dims = dim_weight.length;
        this.bin_count = edges[0].length+1;
        this.ref_hist = ref_hist;
        this.edges = edges;
        this.dim_weights = dim_weight;
        this.thresholds = thresholds;
        this.pca_mean = pca_mean;
        this.pca_basis = pca_basis;
        this.benignware = benignware;
        this.malware = malware;
    }

    public INDArray getPCA_Mean() {
        return Nd4j.create(pca_mean);
    }

    public INDArray getPCA_Basis() {
        return Nd4j.create(pca_basis);
    }

    @Deprecated
    public Triple<Boolean, Double, double[][]> processNBD(long current_virtual_time, int nbd_index,
                                                          int trial, Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>> nbd_structure, Cluster cluster) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        List<Integer> benign_indices = new LinkedList<>();
        List<Integer> malicious_indices = new LinkedList<>();
        Map<Long, Pair<List<Integer>, List<Integer>>> fv_assignment = new HashMap<>();
        Random random = new Random(12345);
        int benign_file_count = nbd_structure.entrySet().stream().mapToInt(entry -> entry.getValue().getLeft().size()).sum();
        int malicious_file_count = nbd_structure.entrySet().stream().mapToInt(entry -> entry.getValue().getRight().size()).sum();

        if (parameters.enable_ideal_shape_gd != null) {
            boolean label = (double) malicious_file_count/benign_file_count >= parameters.enable_ideal_shape_gd;
            return new ImmutableTriple<>(label, Double.POSITIVE_INFINITY, null);
        }

        int benign_fv_count = parameters.benign_trace_length * benign_file_count;
        int malicious_fv_count = parameters.malicious_trace_length * malicious_file_count;

        for (Map.Entry<Long, Pair<Set<FileInfo>, Set<FileInfo>>> entry : nbd_structure.entrySet()) {
            int nbd_benign_fv_count = parameters.benign_trace_length * entry.getValue().getLeft().size();
            int nbd_malicious_fv_count = parameters.malicious_trace_length * entry.getValue().getRight().size();

            List<Integer> nbd_benign_fv_assignment = new LinkedList<>();
            List<Integer> nbd_malicious_fv_assignment = new LinkedList<>();

            for (int i = 0; i < nbd_benign_fv_count; i ++) {
                int ind = random.nextInt(benignware.fv_indices.length);
                if (benignware.fv_indices[ind])
                    nbd_benign_fv_assignment.add(ind);
            }

            for (int i = 0; i < nbd_malicious_fv_count; i ++) {
                int ind = random.nextInt(malware.fv_indices.length);
                if (malware.fv_indices[ind])
                    nbd_malicious_fv_assignment.add(ind);
            }
            benign_indices.addAll(nbd_benign_fv_assignment);
            malicious_indices.addAll(nbd_malicious_fv_assignment);
            fv_assignment.put(entry.getKey(), new ImmutablePair<>(nbd_benign_fv_assignment, nbd_malicious_fv_assignment));
        }

        if (benign_indices.size() + malicious_indices.size()
                < parameters.LD_FP_rate*parameters.min_fv_count_for_detection)
            return null;

        double benign_hist[][] = buildNormalizedHistogramFromVeatureVectors(benign_indices, benignware);
        double malicious_hist[][] = buildNormalizedHistogramFromVeatureVectors(malicious_indices, malware);
        double hist[][] = mergeAndNormalizeHistograms(benign_hist, malicious_hist);
        double score = computeWassersteinDistance(hist);
        double thresholds_vector[] = thresholds.floorEntry(benign_file_count + malicious_file_count).getValue();
        double threshold = (new Percentile()).evaluate(thresholds_vector, parameters.threshold_percentile);
        double upper_threshold = (new Percentile()).evaluate(thresholds_vector, parameters.threshold_percentile);
        double lower_threshold = (new Percentile()).evaluate(thresholds_vector, 100 - parameters.threshold_percentile);
        return  new ImmutableTriple<>(score >= upper_threshold || score <= lower_threshold, score, null);
    }
}
