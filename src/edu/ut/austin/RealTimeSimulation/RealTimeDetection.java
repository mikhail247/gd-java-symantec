package edu.ut.austin.RealTimeSimulation;

import com.mathworks.engine.EngineException;
import com.mathworks.engine.MatlabEngine;
import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.DataInputOutput.GraphDataLoader;
import edu.ut.austin.FeatureExtractor.FeatureExtractor;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.Introspection.DetectionSnapshot;
import edu.ut.austin.Introspection.IntrospectionModule;
import edu.ut.austin.Introspection.NBD_State;
import edu.ut.austin.ML_Models.SparkRandomForestWrapper;
import edu.ut.austin.ML_Models.TrainML_Models;
import edu.ut.austin.NBD.Cluster;
import edu.ut.austin.NBD.ClusterLSH;
import edu.ut.austin.NBD.DomainBasedNBD;
import edu.ut.austin.NBD.NBD_Interface;
import edu.ut.austin.NBD.PrefixGraph.PrefixGraph;
import edu.ut.austin.NBD.PrefixGraph.TreeNode;
import edu.ut.austin.NodeSelectionPolicy.AllNodesNodeSelection;
import edu.ut.austin.NodeSelectionPolicy.ConservativeNodeSelection;
import edu.ut.austin.NodeSelectionPolicy.NodeSelectionPolicy;
import org.apache.commons.lang3.tuple.*;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RealTimeDetection {
    private int min_num_machines;
    private MatlabEngine matlab_engine;
    private NodeSelectionPolicy<DefaultDirectedGraphWrapper> node_selector;
    private Map<String, FileInfo> file_info;
    private GraphDataLoader loader;

    private SparkRandomForestWrapper GRF;
    private ShapeGD_Generic shape_detector;

    Set<DefaultDirectedGraphWrapper> suspicious_graphs_bank;
    Map<TreeNode<DefaultDirectedGraphWrapper>, DefaultDirectedGraphWrapper> map_t = new HashMap<TreeNode<DefaultDirectedGraphWrapper>, DefaultDirectedGraphWrapper>();

    public RealTimeDetection(ShapeGD_Generic shape_detector, GraphDataLoader loader) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        this.min_num_machines = parameters.min_num_files_per_nbd;
        this.matlab_engine = null;
        this.shape_detector = shape_detector;
        this.loader = loader;
        this.file_info = loader.getFileInfoMap();
        this.suspicious_graphs_bank = new HashSet<>();

        switch (parameters.node_selection_policy_type) {
            case CONSERVATIVE:
                this.node_selector = new ConservativeNodeSelection<>();
                break;
            case ALL_NODES:
                this.node_selector = new AllNodesNodeSelection<>();
        }

        this.GRF = TrainML_Models.loadGraphRF_Model();
    }

    public void destroyExecutorPool() {
        closeMatlabSession();
    }

    public void openMatlabSession() {
        try {
            // matlab.engine.shareEngine
            String[] engines = MatlabEngine.findMatlab();
            if (engines.length == 0)
                throw new RuntimeException("No Active MATLAB session\n" +
                        "Run matlab.engine.shareEngine in MATLAB session");
            matlab_engine = MatlabEngine.connectMatlab(engines[0]);
        } catch (EngineException e) {
            e.printStackTrace();
            System.exit(-1);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public void closeMatlabSession() {
        try {
            if (matlab_engine != null)
                matlab_engine.close();
        } catch (EngineException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public void process(long current_virtual_time, int thread_id, NBD_Interface nbd_storage, FeatureExtractor feature_extractor) {
        if (nbd_storage instanceof PrefixGraph) {
            PrefixGraph prefix_graph = (PrefixGraph) nbd_storage;
            processAuxillary(current_virtual_time, thread_id, prefix_graph, feature_extractor);
        } else
            processAuxillary(current_virtual_time, thread_id, nbd_storage, feature_extractor);
    }

    public void processAuxillary(long current_virtual_time, int thread_id, NBD_Interface cluster_obj, FeatureExtractor feature_extractor) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        ConcurrentHashMap<Integer, Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>>> nbd_parameters; //<cluster_id, <machine_id, <benign files, malicious files>>
        Map<Integer, Cluster> suspicious_clusters = null;

        if (parameters.detection_mechanism == ConfigurationParameters.DetectionEnum.NONE)
            return;
        IntrospectionModule stat = IntrospectionModule.getInstance();

        if (cluster_obj instanceof ClusterLSH)
            suspicious_clusters = node_selector.getNodesForDetection((ClusterLSH) cluster_obj, min_num_machines);
        else if (cluster_obj instanceof DomainBasedNBD)
            suspicious_clusters = node_selector.getNodesForDetection((DomainBasedNBD) cluster_obj, min_num_machines);

        stat.getDetectionSnapshot(current_virtual_time).suspicious_nbd_count = suspicious_clusters.size();
        stat.getDetectionSnapshot(current_virtual_time).total_distinct_suspicious_graphs_count = -1;

        nbd_parameters = new ConcurrentHashMap<>(suspicious_clusters.size());
        // converting hashes to FileInfo references
        suspicious_clusters.values().forEach(cluster -> nbd_parameters.put(
                cluster.getClusterID(), cluster.getBenignMaliciousNodeMap().entrySet().stream().collect(
                        Collectors.toMap(Map.Entry::getKey, entry -> new ImmutablePair<>(
                                entry.getValue().getLeft().stream().map(hash -> file_info.get(hash)).collect(Collectors.toSet()),
                                entry.getValue().getRight().stream().map(hash -> file_info.get(hash)).collect(Collectors.toSet())
                        ))
                )
        ));

        System.out.println("Running Host-Level Detector (Shape GD)");
        Map<Integer, Triple<Boolean, Double, double[][]>> shape_results = runHostLevelDetector(current_virtual_time, null, nbd_parameters, suspicious_clusters);
        computeGraphDetectorInputOutputStatistics(current_virtual_time, thread_id, shape_results, nbd_parameters, suspicious_clusters);
    }

    @Deprecated
    public void processAuxillary(long current_virtual_time, int thread_id, PrefixGraph prefix_graph, FeatureExtractor feature_extractor) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        ArrayList<Map<Long, Pair<Integer, Integer>>> nbd_parameters; //<machine_id, <benign files, malicious files>>
        ArrayList<Pair<TreeNode<DefaultDirectedGraphWrapper>, DefaultDirectedGraphWrapper>> suspicous_prefix_graph_nodes;
        ArrayList<Triple<double[], Set<Long>, DefaultDirectedGraphWrapper>> suspicious_graphs;
        IntrospectionModule stat = IntrospectionModule.getInstance();

        if (parameters.detection_mechanism == ConfigurationParameters.DetectionEnum.NONE)
            return;

        suspicous_prefix_graph_nodes = node_selector.getNodesForDetection(prefix_graph, min_num_machines);
        System.out.println("# of PrefixGraph nodes selected: " + suspicous_prefix_graph_nodes.size());
        suspicous_prefix_graph_nodes.forEach(item -> suspicious_graphs_bank.add(item.getValue()));
        stat.getDetectionSnapshot(current_virtual_time).suspicious_nbd_count = suspicous_prefix_graph_nodes.size();
        stat.getDetectionSnapshot(current_virtual_time).total_distinct_suspicious_graphs_count = suspicious_graphs_bank.size();

        nbd_parameters = new ArrayList<>(suspicous_prefix_graph_nodes.size());
        suspicious_graphs = new ArrayList<>(suspicous_prefix_graph_nodes.size()); // used only for debugging purposes

        IntStream.range(0, suspicous_prefix_graph_nodes.size())
                .forEach(i -> {nbd_parameters.add(null); suspicious_graphs.add(null);});

        class ParallelFeatureExtractor implements Callable{
            private final int vpid, thread_count;

            ParallelFeatureExtractor(int vpid) {
                this.vpid = vpid;
                this.thread_count = GlobalParameters.getInstance().executor_size;;
            }

            @Override
            public Void call() {
                int size = suspicous_prefix_graph_nodes.size();
                Pair<TreeNode<DefaultDirectedGraphWrapper>, DefaultDirectedGraphWrapper> pair;

                for (int index = vpid; index < size; index += thread_count) {
                    pair = suspicous_prefix_graph_nodes.get(index);
                    double graph_fv[] = pair.getRight().getFeatureVector(feature_extractor);

                    Map<Long, Pair<Integer, Integer>> nbd_structure = prefix_graph.getBenignMaliciousNodeCount(pair.getLeft());
                    Set<Long> machine_ids = pair.getLeft().machine_ids;
                    nbd_parameters.set(index, nbd_structure);
                    suspicious_graphs.set(index, new ImmutableTriple<>(graph_fv, machine_ids, pair.getRight()));
                }
                return null;
            }
        }

        int vcpu_count = GlobalParameters.getInstance().executor_size;
        List<Callable<Void>> callables = IntStream.range(0, vcpu_count)
                .mapToObj(i -> (Callable<Void>) new ParallelFeatureExtractor(i)).collect(Collectors.toList());

        try {
            System.out.println("Extracting Features");
            GlobalParameters.getInstance().executors.get(thread_id).invokeAll(callables);
            System.out.println("Feature Vectors extracted");
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        System.out.println("Running Graph-Level Detector");
    }

    private Map<Integer, Triple<Boolean, Double, double[][]>> runHostLevelDetector(long current_virtual_time, ArrayList<Double> graph_labels,
                                Map<Integer, Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>>> nbds, Map<Integer, Cluster> suspicious_clusters) {
        ConfigurationParameters parameters;
        GlobalParameters global_parameters;
        IntrospectionModule stat = IntrospectionModule.getInstance();

        parameters = ConfigurationParameters.getInstance();
        global_parameters = GlobalParameters.getInstance();
        Map<Integer, Triple<Boolean, Double, double[][]>> ret = new ConcurrentHashMap<>(nbds.size());

        nbds.keySet().parallelStream().forEach(i -> {
            int detection_ctr = 0;
            double score = 0;
            Triple<Boolean, Double, double[][]> shape_gd_ret = null;
            Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>> nbd_structure = nbds.get(i);
            Cluster cluster = suspicious_clusters.get(i);

            for (int trial = 0; trial < parameters.average_across_num_trials; trial ++) {
                ShapeGD shape_gd_temp = shape_detector.getClusterSensitiveShapeGD(i);
                if (shape_gd_temp == null) {
                    global_parameters.logger.info("No ShapeGD for the cluster " + i);
                    continue;
                }
                shape_gd_ret = shape_gd_temp.processNBD(current_virtual_time, i, trial, nbd_structure, cluster);

                if (shape_gd_ret == null)
                    break;
                if (shape_gd_ret.getLeft()) {
                    detection_ctr++;
                }
                score += shape_gd_ret.getMiddle();
            }
            if (shape_gd_ret != null)
                ret.put(i, new ImmutableTriple<>(detection_ctr > parameters.average_across_num_trials / 2,
                        score / parameters.average_across_num_trials, shape_gd_ret.getRight()));
        });

        int tps_count = (int) ret.values().stream().filter(Objects::nonNull).map(Triple::getLeft).filter(flag -> flag != null && flag).count();
        stat.getDetectionSnapshot(current_virtual_time).shape_detector_nbd_alerts = tps_count;
        return ret;
    }

    private ArrayList<Double> runGraphLevelDetector(long current_virtual_time, ArrayList<double[]> graphs){
        IntrospectionModule stat = IntrospectionModule.getInstance();
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        if (graphs.isEmpty())
            return new ArrayList<>();

        ArrayList<Double> ret = graphs.stream().map(fv -> Vectors.dense(fv))
                .map(vec -> GRF.predict(vec)).collect(Collectors.toCollection(ArrayList::new));
        int tps_count = (int) ret.stream().filter(d -> d >= parameters.graphRF_threshold).count();
        stat.getDetectionSnapshot(current_virtual_time).graph_detector_tp = tps_count;
        return ret;
    }

    @Deprecated
    private ArrayList<Double> runGraphLevelDetectorOld(long current_virtual_time, ArrayList<Triple<double[], Set<Long>, DefaultDirectedGraphWrapper>> graphs){
        ArrayList<Double> ret = new ArrayList<>();
        IntrospectionModule stat = IntrospectionModule.getInstance();
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        if (graphs.isEmpty())
            return ret;
        List<Vector> input_data = new LinkedList<>();
        graphs.forEach(fv -> input_data.add(Vectors.dense(fv.getLeft())));
        List<Double> ret_rdd = GRF.predict(input_data);
        ret.addAll(ret_rdd);

        int tps_count = (int) ret.stream().filter(d -> d >= parameters.graphRF_threshold).count();
        stat.getDetectionSnapshot(current_virtual_time).graph_detector_tp = tps_count;
        return ret;
    }

    private void computeGraphDetectorInputOutputStatistics(long current_virtual_time, int thread_id, Map<Integer, Triple<Boolean, Double, double[][]>> shape_results,
                    Map<Integer, Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>>> nbd_parameters, Map<Integer, Cluster> suspicious_clusters) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        if (!parameters.debug_info)
            return;

        shape_results.keySet().parallelStream().forEach(i -> {
            NBD_State nbd_state = IntrospectionModule.getInstance().getDetectionSnapshot(current_virtual_time).addNBD(i, 0);
            boolean is_nbd_malicious = shape_results.get(i) != null ? shape_results.get(i).getLeft() : false;

            nbd_state.initNBD_State(suspicious_clusters.get(i).getAllClusterNodes());
            nbd_state.cluster_hash_id = suspicious_clusters.get(i).getClusterName().hashCode() + "_-_-_" + current_virtual_time;
            nbd_state.cluster_init_url = suspicious_clusters.get(i).getClusterName();
            nbd_state.time_stamp = current_virtual_time;
            nbd_state.graph_detector_label = null;
            nbd_state.shape_detector_label = shape_results.get(i) != null ? shape_results.get(i).getLeft() : null;
            nbd_state.score = shape_results.get(i) != null ? shape_results.get(i).getMiddle() : null;
            nbd_state.fv = shape_results.get(i) != null ? shape_results.get(i).getRight() : null;
            nbd_state.relabelNBD_with_XGBoost(loader);

            String fv_str = nbd_state.getFV_String(loader);
            global_parameters.synchronizedDataDump(global_parameters.nbd_fv_writer, fv_str);
            nbd_state.dumpNBDToFile(loader);

            if (!nbd_state.isMalicious() && parameters.store_only_suspicious_nbds)
                IntrospectionModule.getInstance().removeNBD(current_virtual_time, i);
        });

        DetectionSnapshot snapshot = IntrospectionModule.getInstance().getDetectionSnapshot(current_virtual_time);
        snapshot.cleanUpNBDs(shape_results.keySet());
        global_parameters.logger.info("Saved epoch stat. " + snapshot.getMaliciousNBD_Count() + "/" + snapshot.getNBD_Count() + " (malicious/total)");
    }

    @Deprecated
    private void computeGraphDetectorInputOutputStatistics(long current_virtual_time, ArrayList<Double> graph_labels, Boolean[] shape_resutls,
                                                           ArrayList<Triple<double[], Set<Long>, DefaultDirectedGraphWrapper>> suspicious_graphs) {
        NBD_State nbd_state;
        Triple<double[], Set<Long>, DefaultDirectedGraphWrapper> triple;
        DefaultDirectedGraphWrapper graph;

        if (!ConfigurationParameters.getInstance().debug_info
                || graph_labels.isEmpty())
            return;
        System.out.println("Graph Level Analysis: <graph_label, shape_label, <mal_ratio, mal_files, ben_files, machines>>");

        for (int i = 0; i < graph_labels.size(); i ++) {
            triple = suspicious_graphs.get(i);
            graph = triple.getRight();

            nbd_state = IntrospectionModule.getInstance().getNBD(current_virtual_time, i);
            nbd_state.graph_detector_label = graph_labels.get(i) > 0.5;
            nbd_state.machine_ids = triple.getMiddle();
            nbd_state.shape_detector_label = shape_resutls[i];

            if (nbd_state.graph_detector_label && nbd_state.mal_ratio > 0.2)
                GlobalParameters.getInstance().detection_logger.info(nbd_state.toString());
        }
    }

    @Deprecated
    private Map<Long, Pair<Integer, Integer>> tileDGWithIGs(Set<Long> machine_ids, DefaultDirectedGraphWrapper graph) {
        // <machine_id, <benign_count, malicious_count>>
        Map<Long, Pair<Integer, Integer>> ret = new HashMap<>();
        Map<Long, Set<DefaultDirectedGraphWrapper>> IGs = null;

        for (Map.Entry<Long, Set<DefaultDirectedGraphWrapper>> entry : IGs.entrySet()) {
            //<benign IGs, malicious IGs>
            MutablePair<Integer, Integer> pair = new MutablePair<>(0, 0);
            for (DefaultDirectedGraphWrapper subgraph : entry.getValue()) {
                if (!graph.containsSubGraph(subgraph))
                    continue;
                if (subgraph.isGraphBenign())
                    pair.setLeft(pair.getLeft() + 1);
                else
                    pair.setRight(pair.getRight() + 1);
            }
            if (pair.getLeft() != 0 || pair.getRight() != 0)
                ret.put(entry.getKey(), pair);
        }
        return ret;
    }

    // <label, score>
    private ArrayList<Pair<Double, Double>> processGraphsMatlab(ArrayList<double[]> graphs){
        int fv_dims;
        double[] input_data;
        double ret_t[][] = null;
        ArrayList<Pair<Double, Double>> ret = new ArrayList<>();

        if (graphs == null || graphs.isEmpty())
            return ret;
        fv_dims = graphs.get(0).length;
        input_data = new double[graphs.size()*fv_dims];

        for (int i = 0; i < graphs.size(); i ++)
            System.arraycopy(graphs.get(i), 0, input_data, i*fv_dims, fv_dims);
        try {
            ret_t = matlab_engine.feval("predictGraphLabels", input_data, fv_dims);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        for (int i = 0; i < ret_t.length; i ++) {
            Pair<Double, Double> pair = new MutablePair<>(ret_t[i][0], ret_t[i][1]);
            ret.add(pair);
        }
        return ret;
    }
}
