package edu.ut.austin.ML_Models;

import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import ml.dmlc.xgboost4j.java.Booster;
import ml.dmlc.xgboost4j.java.DMatrix;
import ml.dmlc.xgboost4j.java.XGBoostError;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.tree.model.RandomForestModel;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 7/26/2017.
 */
public class LocalDetector implements Serializable {
    private RandomForestModel spark_RF = null;
    private ConcurrentHashMap<String, Pair<Double, Double>> xgboost_lookup_predictor = null;
    private Booster xgboost_model = null;

    public void setPredictionEngine(RandomForestModel spark_RF) {
        this.spark_RF = spark_RF;
    }

    public void setPredictionEngine(ConcurrentHashMap<String, Pair<Double, Double>> xgboost_predictor) {
        this.xgboost_lookup_predictor = xgboost_predictor;
    }

    public void setPredictionEngine(Booster xgboost_model) {
        this.xgboost_model = xgboost_model;
    }

    public boolean isMalware(String sha_256, Vector feature_vector) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        if (spark_RF != null)
            return Stream.of(spark_RF.trees()).mapToDouble(tree ->
                    tree.predict(feature_vector)).average().getAsDouble() > 0.5;
        else if(xgboost_lookup_predictor != null)
            return xgboost_lookup_predictor.get(sha_256).getRight() > parameters.LD_threshold;
        else {
            try {
                float vec[] = new float[feature_vector.size()];
                IntStream.range(0, feature_vector.size()).forEach(i -> vec[i] = (float) feature_vector.apply(i));
                DMatrix dmatrix = new DMatrix(vec, 1, vec.length);
                return xgboost_model.predict(dmatrix)[0][1] >= parameters.LD_threshold;
            } catch (XGBoostError xgBoostError) {
                xgBoostError.printStackTrace();
                System.exit(-1);
            }
        }
        return false;
    }

    // returns probability of the label "1", i.e. probability of malicious class
    public double predict(String sha_256, Vector feature_vector) {
        if (spark_RF != null)
            return Stream.of(spark_RF.trees()).mapToDouble(tree ->
                tree.predict(feature_vector)).average().getAsDouble();
        else if(xgboost_lookup_predictor != null)
            return xgboost_lookup_predictor.get(sha_256).getRight();
        else {
            try {
                float vec[] = new float[feature_vector.size()];
                IntStream.range(0, feature_vector.size()).forEach(i -> vec[i] = (float) feature_vector.apply(i));
                DMatrix dmatrix = new DMatrix(vec, 1, vec.length);
                return (double) xgboost_model.predict(dmatrix)[0][1];
            } catch (XGBoostError xgBoostError) {
                xgBoostError.printStackTrace();
                System.exit(-1);
            }
        }
        return -1;
    }

    public List<Double> predict(String sha_256, List<Vector> feature_vectors) {
        List<Double> ret = new LinkedList<>();
        if (spark_RF != null) {
            feature_vectors.forEach(fv -> {
                double probability = Stream.of(spark_RF.trees()).mapToDouble(tree ->
                        tree.predict(fv)).average().getAsDouble();
                ret.add(probability);
            });
        } else if(xgboost_lookup_predictor != null)
            ret.add(xgboost_lookup_predictor.get(sha_256).getRight());
        else {
            try {
                for (Vector feature_vector : feature_vectors) {
                    float vec[] = new float[feature_vector.size()];
                    IntStream.range(0, feature_vector.size()).forEach(i -> vec[i] = (float) feature_vector.apply(i));
                    DMatrix dmatrix = new DMatrix(vec, 1, vec.length);
                    float prediction = xgboost_model.predict(dmatrix)[0][1];
                    ret.add((double) prediction);
                }
            } catch (XGBoostError xgBoostError) {
                xgBoostError.printStackTrace();
                System.exit(-1);
            }
        }
        return ret;
    }
}
