package edu.ut.austin.ML_Models;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.tree.RandomForest;
import org.apache.spark.mllib.tree.model.DecisionTreeModel;
import org.apache.spark.mllib.tree.model.RandomForestModel;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 4/27/2017.
 */
public class SparkRandomForestWrapper {
    private RandomForestModel model;

    public SparkRandomForestWrapper(RandomForestModel model) {
        this.model = model;
    }

    // returns probability of the label "1", i.e. probability of malicious class
    public double predict(Vector feature_vector) {
        return Stream.of(model.trees()).mapToDouble(tree ->
                tree.predict(feature_vector)).average().getAsDouble();
    }

    public List<Double> predict(List<Vector> feature_vectors) {
        List<Double> ret = new LinkedList<>();
        feature_vectors.forEach(fv -> {
            double probability = Stream.of(model.trees()).mapToDouble(tree ->
                    tree.predict(fv)).average().getAsDouble();
            ret.add(probability);
        });
        return ret;
    }
}
