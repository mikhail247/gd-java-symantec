package edu.ut.austin.ML_Models;

import edu.ut.austin.DataInputOutput.GraphDataLoader;
import edu.ut.austin.DataInputOutput.ShapeGD_DataLoader;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.RealTimeSimulation.ClusterSensitiveShapeGD;
import edu.ut.austin.RealTimeSimulation.ShapeGD_Generic;
import edu.ut.austin.RealTimeSimulation.ShapeGD_Spark;
import edu.ut.austin.Utils.Utils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.ml.Pipeline;
import org.apache.spark.ml.PipelineModel;
import org.apache.spark.ml.PipelineStage;
import org.apache.spark.ml.feature.Bucketizer;
import org.apache.spark.ml.feature.PCA;
import org.apache.spark.ml.feature.StandardScaler;
import org.apache.spark.ml.linalg.DenseVector;
import org.apache.spark.mllib.clustering.BisectingKMeansModel;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Mikhail on 3/30/2017.
 */
@SuppressWarnings("unchecked")
public class TrainShapeGD {
    public static ClusterSensitiveShapeGD trainMultipleConditionalShapeGDs(GraphDataLoader loader) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        BisectingKMeansModel clustering_model = TrainML_Models.loadBisectingKMeansModel();
        if (!parameters.train_GraphRF
                || parameters.shape_gd_type != ConfigurationParameters.ShapeGDType.PerNBD_GD)
            return null;

        if (loader == null) {
            loader = new GraphDataLoader();
            loader.loadData();
        }

        LocalDetector ld_model = TrainML_Models.loadLD_Model(parameters.LF_implementation_type);
        String dir_input_name = "results" + File.separator + "features";
        int pca_dim_count = parameters.GD_pca_dim_count;
        int bucket_count = parameters.GD_bin_count;
        int repetitions_count = parameters.GD_repetition_count;
        Dataset<Row> benign_data = null, malicious_data = null;
        global_parameters.logger.info("Training Shape GD");

        switch(parameters.shape_gd_training_method) {
            case EXTERNAL_DATASET:
                benign_data = global_parameters.spark.read().parquet(dir_input_name + "/features_benignware_training_" + parameters.n_gram_size +  ".parquet");
                malicious_data = global_parameters.spark.read().parquet(dir_input_name + "/features_malware_training_" + parameters.n_gram_size +  ".parquet");
                break;
            case VT_DATASET:
                String parquet_file_name = "results/features/features_vt_binaries_training_" + parameters.n_gram_size + ".parquet";
                Dataset<Row> data = global_parameters.spark.read().parquet(parquet_file_name)
                        .drop("scaled_features").drop("pca_features");
                benign_data = data.filter("label < 0.5");
                malicious_data = data.filter("label > 0.5");
                break;
            default:
                System.exit(-1);
        }

        StructType schema = benign_data.schema().add(new StructField("LD", DataTypes.DoubleType, false, Metadata.empty()));
        ExpressionEncoder row_encoder = RowEncoder.apply(schema);

        // add LD's label and project on GD's basis
        benign_data = benign_data.map(new MapFunction<Row, Row>() {
            @Override
            public Row call(Row row) throws Exception {
                String sha_256 = row.<String>getAs("sha_256");
                DenseVector vec = row.<org.apache.spark.ml.linalg.Vector>getAs("features").toDense();
                org.apache.spark.mllib.linalg.Vector features = org.apache.spark.mllib.linalg.Vectors.dense(vec.toArray());
                Double prediction = ld_model.predict(sha_256, features);

                Object array[] = new Object[row.size() + 1];
                IntStream.range(0, row.size()).forEach(i -> {
                    array[i] = row.get(i);
                });
                array[row.size()] = prediction;
                return RowFactory.create(array);
            }
        }, row_encoder);

        // add LD's label and project on GD's basis
        malicious_data = malicious_data.map(new MapFunction<Row, Row>() {
            @Override
            public Row call(Row row) throws Exception {
                String sha_256 = row.<String>getAs("sha_256");
                DenseVector vec = row.<org.apache.spark.ml.linalg.Vector>getAs("features").toDense();
                org.apache.spark.mllib.linalg.Vector features = org.apache.spark.mllib.linalg.Vectors.dense(vec.toArray());
                Double prediction = ld_model.predict(sha_256, features);

                Object array[] = new Object[row.size() + 1];
                IntStream.range(0, row.size()).forEach(i -> {
                    array[i] = row.get(i);
                });
                array[row.size()] = prediction;
                return RowFactory.create(array);
            }
        }, row_encoder);

        PipelineModel projection_model;
        try {
            projection_model = PipelineModel.load("results/Projection_Model");
            global_parameters.logger.info("Projection Model model loaded");
        } catch (Exception e) {
            double decision_threshold = 0.5;
            if (parameters.LF_implementation_type != ConfigurationParameters.LD_ImplementationType.SPARK_RF)
                decision_threshold = parameters.LD_threshold;
            global_parameters.logger.info("Learning Projection Model");
            Dataset<Row> data_fps = global_parameters.spark.createDataFrame(
                    benign_data.filter("LD > " + decision_threshold).toJavaRDD().takeSample(false, parameters.GD_PCA_sample_count), benign_data.schema());
            Dataset<Row> data_tps = global_parameters.spark.createDataFrame(
                    malicious_data.filter("LD > " + decision_threshold).toJavaRDD().takeSample(false, parameters.GD_PCA_sample_count), benign_data.schema());
            Dataset<Row> data;
            if (parameters.GD_PCA_FP_ONLY)
                data = data_fps;
            else
                data = data_fps.union(data_tps);
            StandardScaler scaler = new StandardScaler().setInputCol("features").setOutputCol("scaled_features").setWithStd(false).setWithMean(true);
            PCA pca = new PCA().setInputCol(scaler.getOutputCol()).setOutputCol("pca_features").setK(pca_dim_count);
            Pipeline pipeline = new Pipeline().setStages(new PipelineStage[] {scaler, pca});
            projection_model = pipeline.fit(data);
            try {
                projection_model.save("results/Projection_Model");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            global_parameters.logger.info("Projection Model model trained");
        }
        benign_data = projection_model.transform(benign_data).drop("scaled_features");

        // split pca projected data into individual columns
        schema = benign_data.schema();
        for (int i = 0; i < pca_dim_count; i ++)
            schema = schema.add(new StructField("pca_feature_"+i, DataTypes.DoubleType, false, Metadata.empty()));
        row_encoder = RowEncoder.apply(schema);

        Dataset<Row> disassembled_data = benign_data.map(new MapFunction<Row, Row>() {
            @Override
            public Row call(Row row) throws Exception {
                double pca_features[] = row.<org.apache.spark.ml.linalg.Vector>getAs("pca_features").toArray();
                Object array[] = new Object[row.size() + pca_dim_count];
                IntStream.range(0, row.size()).forEach(i -> {
                    array[i] = row.get(i);
                });
                for (int i = row.size(); i < array.length; i ++)
                    array[i] = pca_features[i - row.size()];
                return RowFactory.create(array);
            }
        }, row_encoder);

        JavaRDD<String> raw_graph_data = global_parameters.java_spark_context.textFile(parameters.GRF_Matlab_data);
        // 10-Fold Cross Validation
        JavaRDD<Row> java_row_rdd = raw_graph_data.map(new Function<String, Row>() {
            @Override
            public Row call(String str) throws Exception {
                String array[] = str.split(",");
                double values[] = new double[array.length - 1];
                for (int i = 0; i < array.length - 1; i++) {
                    values[i] = Double.parseDouble(array[i + 1]);
                }
                Double label = Double.parseDouble(array[0]);
                org.apache.spark.ml.linalg.Vector features = org.apache.spark.ml.linalg.Vectors.dense(values);
                Row row_new = RowFactory.create(label, features);
                return row_new;
            }
        });

        ArrayList<DefaultDirectedGraphWrapper> graphs = new ArrayList<>();
        try (
                BufferedReader br = new BufferedReader(new FileReader(parameters.GRF_graph_ids))
        ) {
            String line;
            DefaultDirectedGraphWrapper graph;

            while ((line = br.readLine()) != null) {
                String ids[] = line.split(",");
                long machine_id = Long.parseLong(ids[0]);
                String root_id = ids[1];
                graph = loader.getUnmodifiableData().get(machine_id).get(root_id);
                graphs.add(graph);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // <cluster id, FV, graph>
        List<Triple<Integer, Vector, DefaultDirectedGraphWrapper>> graph_ids = new LinkedList<>();
        try (
                BufferedReader br = new BufferedReader(new FileReader(parameters.GRF_Matlab_data))
        ) {
            int counter = 0;
            String line;
            DefaultDirectedGraphWrapper graph;

            while ((line = br.readLine()) != null) {
                String array[] = line.split(",");
                double values[] = new double[array.length - 1];
                for (int i = 0; i < array.length - 1; i++) {
                    values[i] = Double.parseDouble(array[i + 1]);
                }
                Double label = Double.parseDouble(array[0]);
                Vector features = Vectors.dense(values);
                graph = graphs.get(counter ++);
                int cluster_index = clustering_model.predict(features);
                if (label < 0.5) // benign graph
                    graph_ids.add(new ImmutableTriple<>(cluster_index, features, graph));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        double lower_bound[] = new double[pca_dim_count];
        double upper_bound[] = new double[pca_dim_count];
        double edges[][] = new double[pca_dim_count][bucket_count+1];
        double decision_threshold = 0.5;
        if (parameters.LF_implementation_type != ConfigurationParameters.LD_ImplementationType.SPARK_RF)
            decision_threshold = parameters.LD_threshold;

        for (int i = 0; i < pca_dim_count; i ++) {
            double ret[] = disassembled_data.filter("LD > " + decision_threshold).stat().approxQuantile("pca_feature_"+i, new double[]{0.01, 0.99}, 0.01);
            lower_bound[i] = ret[0];
            upper_bound[i] = ret[1];
            double bucketization_step = (upper_bound[i] - lower_bound[i])/(bucket_count - 2);
            edges[i][0] = Double.NEGATIVE_INFINITY;
            edges[i][bucket_count] = Double.POSITIVE_INFINITY;
            for (int j = 1; j < bucket_count; j ++)
                edges[i][j] = lower_bound[i] + (j-1)*bucketization_step;
        }

        // transformation phase is separate from quantile estimation phase
        for (int i = 0; i < pca_dim_count; i ++) {
            Bucketizer bucketizer = new Bucketizer().setInputCol("pca_feature_"+i)
                    .setOutputCol("pca_feature_bucket_"+i).setSplits(edges[i]);
            disassembled_data = bucketizer.transform(disassembled_data).drop("pca_feature_"+i);
        }
        disassembled_data = disassembled_data.drop("features").drop("pca_features");
        ArrayList<Row> data_list = new ArrayList<>(disassembled_data.collectAsList());

        // estimating parameters
        double ref_hist[][] = getFP_NormalizedHistogram(data_list, pca_dim_count, bucket_count, parameters.GD_ref_hist_sample_count, true);
        double dim_weights[] = computeWeights(ref_hist);
        Random random = new Random(12345);

        String parquet_file_name = "results/features/features_vt_binaries_augmented_" + parameters.n_gram_size +  ".parquet";
        Dataset<Row> data_t = global_parameters.spark.read().parquet(parquet_file_name);
        List<Row> data_local = data_t.collectAsList();

        ClusterSensitiveShapeGD cluster_sensitive_shape_gd = new ClusterSensitiveShapeGD();
        IntStream.range(0, clustering_model.k()).parallel().forEach(cluster_id -> {
            ArrayList<Pair<Vector, DefaultDirectedGraphWrapper>> graphs_data = graph_ids.stream()
                    .filter(triple -> triple.getLeft() == cluster_id).map(triple ->
                            new ImmutablePair<>(triple.getMiddle(), triple.getRight())).collect(Collectors.toCollection(ArrayList::new));

            if (graphs_data.isEmpty()) {
                global_parameters.logger.info("Cluster " + cluster_id + " is lacking training data");
                return;
            }
            TreeMap<Integer, double[]> thresholds = new TreeMap<>();

            for (int fvs_per_nbd_count = 500; fvs_per_nbd_count <= 1*1000; fvs_per_nbd_count += 500) {
                int file_count = 0;
                Map<String, Integer> selected_sha_256_files = new HashMap<>();
                while(file_count < fvs_per_nbd_count) {
                    int index = random.nextInt(graphs_data.size());
                    Map<String, Long> temp = graphs_data.get(index).getValue().vertexSet().stream().filter(node -> node.file_info != null)
                            .map(node -> node.file_info.redirect.sha2).collect(Collectors.groupingBy(java.util.function.Function.identity(), Collectors.counting()));

                    for (Map.Entry<String, Long> entry : temp.entrySet()) {
                        selected_sha_256_files.putIfAbsent(entry.getKey(), 0);
                        int value = selected_sha_256_files.get(entry.getKey());
                        selected_sha_256_files.put(entry.getKey(), (int) (value + entry.getValue()));
                    }
                    file_count = selected_sha_256_files.values().stream().mapToInt(Integer::valueOf).sum();
                }

                ArrayList<Row> rows = new ArrayList<>(fvs_per_nbd_count);
                List<Row> final_data_local = data_local;
                selected_sha_256_files.forEach((sha_256, count) -> {
                    for (Row row : final_data_local) {
                        if (!row.<String>getAs("sha256").equals(sha_256))
                            continue;
                        IntStream.range(0, count).forEach(i -> rows.add(row));
                    }
                });

                double thresholds_vector[] = new double[repetitions_count];
                for (int i = 0; i < repetitions_count; i++) {
                    double hist[][] = getFP_NormalizedHistogram(rows, pca_dim_count, bucket_count, fvs_per_nbd_count, false);
                    thresholds_vector[i] = computeWassersteinDistance(ref_hist, hist, dim_weights);
                }
                thresholds.put(fvs_per_nbd_count, thresholds_vector);
            }
            ShapeGD_Spark shape_detector_spark = new ShapeGD_Spark(edges, ref_hist, dim_weights, thresholds);
            cluster_sensitive_shape_gd.addShapeGD(cluster_id, shape_detector_spark);
            global_parameters.logger.info("Cluster " + cluster_id + " processed");
        });
        cluster_sensitive_shape_gd.save();
        return cluster_sensitive_shape_gd;
    }

    public static ShapeGD_Spark trainGlobalShapeGD() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        LocalDetector ld_model = TrainML_Models.loadLD_Model(parameters.LF_implementation_type);
        String dir_input_name = "data" + File.separator + "features";
        int pca_dim_count = parameters.GD_pca_dim_count;
        int bucket_count = parameters.GD_bin_count;
        int repetitions_count = parameters.GD_repetition_count;
        Dataset<Row> benign_data = null, malicious_data = null, raw_data = null;
        global_parameters.logger.info("Training Shape GD");

        switch(parameters.shape_gd_training_method) {
            case EXTERNAL_DATASET:
                benign_data = global_parameters.spark.read().parquet(dir_input_name + "/features_benignware_training_" + parameters.n_gram_size +  ".parquet");
                malicious_data = global_parameters.spark.read().parquet(dir_input_name + "/features_malware_training_" + parameters.n_gram_size +  ".parquet");
                raw_data = benign_data.union(malicious_data);
                break;
            case VT_DATASET:
                String parquet_file_name = "data/features/features_vt_binaries_training_" + parameters.n_gram_size + ".parquet";
                raw_data = global_parameters.spark.read().parquet(parquet_file_name)
                        .drop("scaled_features").drop("pca_features");
                break;

            case VT_REPORTS:
                Set<String> malware_hashes = Utils.loadMalwareHashes();
                List<StructField> fields = new ArrayList<>();
                fields.add(DataTypes.createStructField("label", DataTypes.IntegerType, false));
                fields.add(DataTypes.createStructField("sha_256", DataTypes.StringType, false));
                fields.add(DataTypes.createStructField("features", new org.apache.spark.ml.linalg.VectorUDT(), false));
                StructType schema = DataTypes.createStructType(fields);

                int hash_trick_size = 1024;
                parquet_file_name = "data/features/vt_static_report_features_fixed_raw_text/" + hash_trick_size;
                JavaRDD<String> raw_data_string = global_parameters.java_spark_context.textFile(parquet_file_name);
                JavaRDD<Row> java_row_rdd = raw_data_string.filter(str -> !str.startsWith("Time")).map(new Function<String, Row>() {
                    @Override
                    public Row call(String str) {
                        String sha256 = str.substring(3, str.indexOf("', "));
                        String libsvm_format = str.substring(str.indexOf("{")+1, str.indexOf("}")).replace(",", "");
                        libsvm_format = libsvm_format.replaceAll(": ", ":");
                        int label = malware_hashes.contains(sha256) ? 1 : 0;
                        String lexems[] = libsvm_format.split(" ");
                        int indexes[] = new int[lexems.length];
                        double values[] = new double[lexems.length];
                        IntStream.range(0, lexems.length).forEach(i -> {
                            String tokens[] = lexems[i].split(":");
                            indexes[i] = Integer.parseInt(tokens[0]);
                            values[i] = Double.parseDouble(tokens[1]);
                        });
                        org.apache.spark.ml.linalg.Vector features = org.apache.spark.ml.linalg.Vectors.sparse(hash_trick_size, indexes, values);
                        return RowFactory.create(label, sha256, features);
                    }
                });
                raw_data = global_parameters.spark.createDataFrame(java_row_rdd, schema);
                System.out.format("Loaded %,d feature vectors\n", raw_data.count());

                raw_data = raw_data.limit(300*1000);
                break;
            default:
                System.exit(-1);
        }

        StructType schema = raw_data.schema().add(new StructField("LD", DataTypes.DoubleType, false, Metadata.empty()));
        ExpressionEncoder row_encoder = RowEncoder.apply(schema);

        // add LD's label and project on GD's basis
        Dataset<Row> raw_data_augmented_LD = raw_data.map(new MapFunction<Row, Row>() {
            @Override
            public Row call(Row row) throws Exception {
                String sha_256 = row.<String>getAs("sha_256");
                DenseVector vec = row.<org.apache.spark.ml.linalg.Vector>getAs("features").toDense();
                org.apache.spark.mllib.linalg.Vector features = org.apache.spark.mllib.linalg.Vectors.dense(vec.toArray());
                Double prediction = ld_model.predict(sha_256, features);

                Object array[] = new Object[row.size() + 1];
                IntStream.range(0, row.size()).forEach(i -> {
                    array[i] = row.get(i);
                });
                array[row.size()] = prediction;
                return RowFactory.create(array);
            }
        }, row_encoder);

        benign_data = raw_data_augmented_LD.filter("label < 0.5");
        malicious_data = raw_data_augmented_LD.filter("label > 0.5");

        PipelineModel projection_model;
        try {
            projection_model = PipelineModel.load("results/Projection_Model");
            global_parameters.logger.info("Projection Model model loaded");
        } catch (Exception e) {
            global_parameters.logger.info("Learning Projection Model");
            double decision_threshold = 0.5;
            if (parameters.LF_implementation_type != ConfigurationParameters.LD_ImplementationType.SPARK_RF)
                decision_threshold = parameters.LD_threshold;

            Dataset<Row> data_fps = global_parameters.spark.createDataFrame(
                    benign_data.filter("LD > " + decision_threshold).toJavaRDD().takeSample(false, parameters.GD_PCA_sample_count), benign_data.schema());
            Dataset<Row> data_tps = global_parameters.spark.createDataFrame(
                    malicious_data.filter("LD > " + decision_threshold).toJavaRDD().takeSample(false, parameters.GD_PCA_sample_count), malicious_data.schema());
            Dataset<Row> data;
            if (parameters.GD_PCA_FP_ONLY)
                data = data_fps;
            else
                data = data_fps.union(data_tps);
            StandardScaler scaler = new StandardScaler().setInputCol("features").setOutputCol("scaled_features").setWithStd(false).setWithMean(true);
            PCA pca = new PCA().setInputCol(scaler.getOutputCol()).setOutputCol("pca_features").setK(pca_dim_count);
            Pipeline pipeline = new Pipeline().setStages(new PipelineStage[] {scaler, pca});
            projection_model = pipeline.fit(data);
            try {
                projection_model.save("results/Projection_Model");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            global_parameters.logger.info("Projection Model model trained");
        }
        benign_data = projection_model.transform(benign_data).drop("scaled_features");

        // split pca projected data into individual columns
        schema = benign_data.schema();
        for (int i = 0; i < pca_dim_count; i ++)
            schema = schema.add(new StructField("pca_feature_"+i, DataTypes.DoubleType, false, Metadata.empty()));
        row_encoder = RowEncoder.apply(schema);

        Dataset<Row> disassembled_data = benign_data.map(new MapFunction<Row, Row>() {
            @Override
            public Row call(Row row) throws Exception {
                double pca_features[] = row.<org.apache.spark.ml.linalg.Vector>getAs("pca_features").toArray();
                Object array[] = new Object[row.size() + pca_dim_count];
                IntStream.range(0, row.size()).forEach(i -> {
                    array[i] = row.get(i);
                });
                for (int i = row.size(); i < array.length; i ++)
                    array[i] = pca_features[i - row.size()];
                return RowFactory.create(array);
            }
        }, row_encoder);

        double lower_bound[] = new double[pca_dim_count];
        double upper_bound[] = new double[pca_dim_count];
        double edges[][] = new double[pca_dim_count][bucket_count+1];
        for (int i = 0; i < pca_dim_count; i ++) {
            System.out.println("Dim " + i);
            double decision_threshold = 0.5;
            if (parameters.LF_implementation_type != ConfigurationParameters.LD_ImplementationType.SPARK_RF)
                decision_threshold = parameters.LD_threshold;

            double ret[] = disassembled_data.filter("LD > " + decision_threshold).stat().approxQuantile("pca_feature_"+i, new double[]{0.01, 0.99}, 0.01);
            lower_bound[i] = ret[0];
            upper_bound[i] = ret[1];
            double bucketization_step = (upper_bound[i] - lower_bound[i])/(bucket_count - 2);
            edges[i][0] = Double.NEGATIVE_INFINITY;
            edges[i][bucket_count] = Double.POSITIVE_INFINITY;
            for (int j = 1; j < bucket_count; j ++)
                edges[i][j] = lower_bound[i] + (j-1)*bucketization_step;
        }

        // transformation phase is separate from quantile estimation phase
        for (int i = 0; i < pca_dim_count; i ++) {
            Bucketizer bucketizer = new Bucketizer().setInputCol("pca_feature_"+i)
                    .setOutputCol("pca_feature_bucket_"+i).setSplits(edges[i]);
            disassembled_data = bucketizer.transform(disassembled_data).drop("pca_feature_"+i);
        }
        disassembled_data = disassembled_data.drop("features").drop("pca_features");

        ArrayList<Row> data_list = new ArrayList<>(disassembled_data.collectAsList());
        String pca_feature_cols[] = new String[pca_dim_count];
        IntStream.range(0, pca_dim_count).forEach(i -> {pca_feature_cols[i] = "pca_feature_bucket_"+i;});

        // estimating parameters
        double ref_hist[][] = getFP_NormalizedHistogram(data_list, pca_dim_count, bucket_count, parameters.GD_ref_hist_sample_count, true);
        double dim_weights[] = computeWeights(ref_hist);
        TreeMap<Integer, double[]> thresholds = new TreeMap<>();

        System.out.print("Estimating threshold Gamma ");
        for (int fvs_per_nbd_count = parameters.GD_fvs_per_nbd_count; fvs_per_nbd_count <= 10*1000; fvs_per_nbd_count += 500) {
            System.out.println("fvs_per_nbd_count: " + fvs_per_nbd_count);
            double thresholds_vector[] = new double[repetitions_count];
            for (int i = 0; i < repetitions_count; i++) {
                double hist[][] = getFP_NormalizedHistogram(data_list, pca_dim_count,
                        bucket_count, fvs_per_nbd_count, false);
                thresholds_vector[i] = computeWassersteinDistance(ref_hist, hist, dim_weights);
            }
            thresholds.put(fvs_per_nbd_count, thresholds_vector);
        }
        System.out.print("");
        ShapeGD_Spark shape_detector_spark = new ShapeGD_Spark(edges, ref_hist, dim_weights, thresholds);
        shape_detector_spark.save();
        return shape_detector_spark;
    }

    public static void dumpMatlabData(boolean add_label) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        String dir_input_name = "results" + File.separator + "features";

        Arrays.asList("benignware", "malware").forEach(file_name -> {
            String input_file_path = dir_input_name + File.separator + "features_" + file_name + "_training.parquet";
            Dataset<Row> dataset = global_parameters.spark.read().parquet(input_file_path);

            // saving plain comma separated values
            Dataset<String> dataset_encoded = dataset.map(new MapFunction<Row, String>() {

                @Override
                public String call(Row row) throws Exception {
                    String str = (add_label ? row.<Integer>getAs("label") + "," : "")
                                    + Arrays.stream(row.<org.apache.spark.ml.linalg.Vector>getAs("features").toArray()).mapToObj(Double::toString)
                                    .collect(Collectors.joining(","));
                    return str;
                }
            }, Encoders.STRING());

            List<String> data = dataset_encoded.collectAsList();
            String output_file_path = dir_input_name + File.separator + "features_" + file_name + "_training.csv";
            try (
                    OutputStreamWriter stream_writer = new FileWriter(output_file_path);
                    BufferedWriter writer = new BufferedWriter(stream_writer);
            ) {
                for (String str : data)
                    writer.write(str + "\n");
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        });
    }

    @SuppressWarnings("unchecked")
    private static Dataset<Row> preprocessDataForShapeGD(ShapeGD_Generic shape_gd_spark, String input_parquet_file, String output_parquet_file, int hash_trick_size) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        LocalDetector ld_model = TrainML_Models.loadLD_Model(parameters.LF_implementation_type);
        String dir_input_name = "data" + File.separator + "features";
        String parquet_file_abs_path = dir_input_name + File.separator + input_parquet_file + File.separator  + hash_trick_size;
        int pca_dim_count = parameters.GD_pca_dim_count;

        if (!Files.exists(Paths.get(parquet_file_abs_path)))
            return null;

        global_parameters.logger.info("Encoding parquet file: " + input_parquet_file);
        Set<String> malware_hashes = Utils.loadMalwareHashes();

        List<StructField> fields = new ArrayList<>();
        fields.add(DataTypes.createStructField("label", DataTypes.IntegerType, false));
        fields.add(DataTypes.createStructField("sha256", DataTypes.StringType, false));
        fields.add(DataTypes.createStructField("features", new org.apache.spark.ml.linalg.VectorUDT(), false));
        StructType schema = DataTypes.createStructType(fields);

        JavaRDD<String> raw_data_string = global_parameters.java_spark_context.textFile(parquet_file_abs_path);
        JavaRDD<Row> java_row_rdd = raw_data_string.filter(str -> !str.startsWith("Time")).map(new Function<String, Row>() {
            @Override
            public Row call(String str) {
                String sha256 = str.substring(3, str.indexOf("', "));
                String libsvm_format = str.substring(str.indexOf("{")+1, str.indexOf("}")).replace(",", "");
                libsvm_format = libsvm_format.replaceAll(": ", ":");
                int label = malware_hashes.contains(sha256) ? 1 : 0;
                String lexems[] = libsvm_format.split(" ");
                int indexes[] = new int[lexems.length];
                double values[] = new double[lexems.length];
                IntStream.range(0, lexems.length).forEach(i -> {
                    String tokens[] = lexems[i].split(":");
                    indexes[i] = Integer.parseInt(tokens[0]);
                    values[i] = Double.parseDouble(tokens[1]);
                });
                org.apache.spark.ml.linalg.Vector features = org.apache.spark.ml.linalg.Vectors.sparse(hash_trick_size, indexes, values);
                return RowFactory.create(label, sha256, features);
            }
        });
        Dataset<Row> data = global_parameters.spark.createDataFrame(java_row_rdd, schema);
        System.out.format("Loaded %,d feature vectors\n", data.count());

        PipelineModel projection_model = null;
        try {
            projection_model = PipelineModel.load("results/Projection_Model");
            global_parameters.logger.info("Projection Model model loaded");
        } catch (Exception e) {
            global_parameters.logger.info("Projection Model model not found");
            System.exit(-1);
        }
        data = projection_model.transform(data).drop("LD").drop("scaled_features");

        schema = data.schema().add(new StructField("LD", DataTypes.DoubleType, false, Metadata.empty()));
        ExpressionEncoder row_encoder = RowEncoder.apply(schema);
        // add LD's label and project on GD's basis
        data = data.map(new MapFunction<Row, Row>() {
            @Override
            public Row call(Row row) throws Exception {
                String sha_256 = row.<String>getAs("sha256");
                DenseVector vec = row.<org.apache.spark.ml.linalg.Vector>getAs("features").toDense();
                org.apache.spark.mllib.linalg.Vector features = org.apache.spark.mllib.linalg.Vectors.dense(vec.toArray());
                Double prediction = ld_model.predict(sha_256, features);

                Object array[] = new Object[row.size() + 1];
                IntStream.range(0, row.size()).forEach(i -> {
                    array[i] = row.get(i);
                });
                array[row.size()] = prediction;
                return RowFactory.create(array);
            }
        }, row_encoder).drop("features");

        // split pca projected data into individual columns
        schema = data.schema();
        for (int i = 0; i < pca_dim_count; i ++)
            schema = schema.add(new StructField("pca_feature_"+i, DataTypes.DoubleType, false, Metadata.empty()));
        row_encoder = RowEncoder.apply(schema);

        Dataset<Row> disassembled_data = data.map(new MapFunction<Row, Row>() {
            @Override
            public Row call(Row row) throws Exception {
                double pca_features[] = row.<org.apache.spark.ml.linalg.Vector>getAs("pca_features").toArray();
                Object array[] = new Object[row.size() + pca_dim_count];
                IntStream.range(0, row.size()).forEach(i -> {
                    array[i] = row.get(i);
                });
                for (int i = row.size(); i < array.length; i ++)
                    array[i] = pca_features[i - row.size()];
                return RowFactory.create(array);
            }
        }, row_encoder);

        // transformation phase is separate from quantile estimation phase
        for (int i = 0; i < pca_dim_count; i ++) {
            Bucketizer bucketizer = new Bucketizer().setInputCol("pca_feature_"+i)
                    .setOutputCol("pca_feature_bucket_"+i).setSplits(shape_gd_spark.getClusterSensitiveShapeGD(0).getEdges()[i]);
            disassembled_data = bucketizer.transform(disassembled_data).drop("pca_feature_"+i);
        }
        disassembled_data = disassembled_data.drop("pca_features");
        disassembled_data.write().mode("overwrite").parquet(dir_input_name + File.separator + output_parquet_file);
        return disassembled_data;
    }

    public static void processFVsWithLDs(ShapeGD_Generic shape_gd_spark, int hash_trick_size) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        switch(parameters.shape_gd_training_method) {
            case VT_DATASET:
                processFVsWithLDs_asm(shape_gd_spark);
                break;

            case VT_REPORTS:
                processFVsWithLDs_vt_reports(shape_gd_spark, hash_trick_size);
                break;

            default:
                System.exit(-1);
        }
    }

    public static Dataset<Row> simulateLD(Dataset<Row> data) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        Random random = new Random(12345);
        double LD_FP_rate = parameters.LD_FP_rate;
        double LD_TP_rate = parameters.LD_TP_rate;
        ExpressionEncoder row_encoder = RowEncoder.apply(data.schema());

        return data.map(new MapFunction<Row, Row>() {
            @Override
            public Row call(Row row) throws Exception {
                int label = row.<Integer>getAs("label");
                int ld_index = row.fieldIndex("LD");
                Object array[] = new Object[row.size()];
                IntStream.range(0, row.size()).forEach(i -> {
                    array[i] = row.get(i);
                });

                if (label == 0) { // benign
                    array[ld_index] = random.nextDouble() < (1 - LD_FP_rate) ? 0.0 : 1.0;
                } else if (label == 1) { // malicious
                    array[ld_index] = random.nextDouble() < LD_TP_rate ? 1.0 : 0.0;
                }
                Row row_new = RowFactory.create(array);
                return row_new;
            }
        }, row_encoder);
    }

    private static void processFVsWithLDs_vt_reports(ShapeGD_Generic shape_gd_spark, int hash_trick_size) {
        String dir_output_name = "results" + File.separator + "features";
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        Dataset<Row> ds_vt = TrainShapeGD.preprocessDataForShapeGD(shape_gd_spark,
                "vt_static_report_features_fixed_raw_text",
                "vt_report_features_" + hash_trick_size + ".parquet", hash_trick_size);

    }

    private static void processFVsWithLDs_asm(ShapeGD_Generic shape_gd_spark) {
        String dir_output_name = "results" + File.separator + "features";
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        Dataset<Row> ds_benign_training = TrainShapeGD.preprocessDataForShapeGD(shape_gd_spark,
                "features_benignware_training_" + parameters.n_gram_size +  ".parquet",
                "features_benignware_training_augmented_" + parameters.n_gram_size +  ".parquet", -1);

        Dataset<Row> ds_malware_training = TrainShapeGD.preprocessDataForShapeGD(shape_gd_spark,
                "features_malware_training_" + parameters.n_gram_size +  ".parquet",
                "features_malware_training_augmented_" + parameters.n_gram_size +  ".parquet", -1);

        Dataset<Row> ds_vt = TrainShapeGD.preprocessDataForShapeGD(shape_gd_spark,
                "features_vt_binaries_" + parameters.n_gram_size +  ".parquet",
                "features_vt_binaries_augmented_" + parameters.n_gram_size +  ".parquet", -1);

        Dataset<Row> ds_vt_training = TrainShapeGD.preprocessDataForShapeGD(shape_gd_spark,
                "features_vt_binaries_training_" + parameters.n_gram_size +  ".parquet",
                "features_vt_binaries_training_augmented_" + parameters.n_gram_size +  ".parquet", -1);
    }

    private static Dataset<Row> loadData(boolean benign, boolean training) {
        Dataset<Row> data = null;
        String parquet_file_name;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        switch(parameters.shape_gd_training_method) {
            case VT_DATASET:
                if (training)
                    parquet_file_name = "results/features/features_vt_binaries_training_" + parameters.n_gram_size + ".parquet";
                else
                    parquet_file_name = "results/features/features_vt_binaries_augmented_" + parameters.n_gram_size + ".parquet";
                data = global_parameters.spark.read().parquet(parquet_file_name)
                        .drop("scaled_features").drop("pca_features");
                break;

            case VT_REPORTS:
                parquet_file_name = "results/features/vt_report_features.parquet";
                data = global_parameters.spark.read().parquet(parquet_file_name)
                        .drop("scaled_features").drop("pca_features");
                break;
            default:
                System.exit(-1);
        }

        if (benign)
            return data.filter("label < 0.5");//.limit(parameters.shape_gd_training_method_file_count);
        else
            return data.filter("label > 0.5");//.limit(parameters.shape_gd_training_method_file_count);
    }

    public static void visualizeShapeGD(ShapeGD_Generic shape_gd, String suffix, int hash_trick_size) {
        int exp_count = 10*1000;
        Dataset<Row> benign_data, malicious_data;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        String dir_name = "data" + File.separator + "features";
        double decision_threshold = 0.5;
        if (parameters.LF_implementation_type != ConfigurationParameters.LD_ImplementationType.SPARK_RF)
            decision_threshold = parameters.LD_threshold;

        if (suffix.equals("training")) {
            String parquet_file_name = "results/features/features_vt_binaries_training_augmented_" + parameters.n_gram_size + ".parquet";
            Dataset<Row> data = global_parameters.spark.read().parquet(parquet_file_name)
                    .drop("scaled_features").drop("pca_features");

            benign_data = data.filter("label < 0.5").filter("LD > " + decision_threshold).drop("scaled_features").drop("pca_features");
            malicious_data = data.filter("label > 0.5").filter("LD > " + decision_threshold).drop("scaled_features").drop("pca_features");
        } else if (suffix.equals("testing")) {
            String parquet_file_name = dir_name + "/vt_report_features_" + hash_trick_size + ".parquet";
            Dataset<Row> data = global_parameters.spark.read().parquet(parquet_file_name)
                    .drop("scaled_features").drop("pca_features");

            benign_data = data.filter("label < 0.5").filter("LD > " + decision_threshold).drop("scaled_features").drop("pca_features");
            malicious_data = data.filter("label > 0.5").filter("LD > " + decision_threshold).drop("scaled_features").drop("pca_features");
        } else {
            benign_data = global_parameters.spark.read().parquet(dir_name + "/features_benignware_" + suffix + "_augmented_" + parameters.n_gram_size + ".parquet");
            malicious_data = global_parameters.spark.read().parquet(dir_name + "/features_malware_" + suffix + "_augmented_" + parameters.n_gram_size + ".parquet");
        }
        ArrayList<Row> benign_data_local = new ArrayList<>(benign_data.collectAsList());
        ArrayList<Row> malicious_data_local = new ArrayList<>(malicious_data.collectAsList());

        int pca_dim_count = parameters.GD_pca_dim_count;
        int bucket_count = parameters.GD_bin_count;
        double shape_scores[][] = new double[exp_count][2];

        IntStream.range(1, 21).map(i -> i*200).forEach(fv_count -> {
            double hist[][];
            for (int i = 0; i < exp_count; i++) {
                hist = getFP_NormalizedHistogram(benign_data_local, pca_dim_count, bucket_count, fv_count, false);
                shape_scores[i][0] = computeWassersteinDistance(hist, shape_gd);
                hist = getFP_NormalizedHistogram(malicious_data_local, pca_dim_count, bucket_count, fv_count, false);
                shape_scores[i][1] = computeWassersteinDistance(hist, shape_gd);
            }
            System.out.println(fv_count);

            try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(dir_name + File.separator + "shape_gd_hist_" + suffix + "_" + fv_count + ".csv"), "utf-8")))
            {
                for (double[] shape_score : shape_scores) {
                    writer.write(shape_score[0] + "," + shape_score[1] + "\n");
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(-1);
            }
        });

    }

    static double[][] getFP_NormalizedHistogram(ArrayList<Row> data, int pca_dim_count, int bucket_count, int fv_count, boolean filter_before) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        Random rand = new Random(12345);
        double hist[][] = new double[pca_dim_count][bucket_count];
        double decision_threshold_t = 0.5;
        if (parameters.LF_implementation_type != ConfigurationParameters.LD_ImplementationType.SPARK_RF)
            decision_threshold_t = parameters.LD_threshold;
        double decision_threshold = decision_threshold_t;

        if (filter_before)
            data = data.parallelStream().filter(row ->
                    row.<Double>getAs("LD") > decision_threshold).collect(Collectors.toCollection(ArrayList::new));

        final ArrayList<Row> ds = data;
        rand.ints(fv_count, 0, ds.size()).parallel().mapToObj(i -> ds.get(i))
                .forEach(row -> {
                    if (row.<Double>getAs("LD") > decision_threshold) {
                        for (int i = 0; i < pca_dim_count; i++) {
                            int bucket_id = (int) row.<Double>getAs("pca_feature_bucket_" + i).longValue();
                            hist[i][bucket_id]++;
                        }
                    }
                });

        // normalize ref_hist
        for (int i = 0; i < pca_dim_count ; i++) {
            double sum = 0;
            for (int j = 0; j < bucket_count; j++)
                sum += hist[i][j];
            for (int j = 0; j < bucket_count; j++)
                hist[i][j] /= sum;
        }
        return hist;
    }

    static double[][] getFP_NormalizedHistogram(Dataset<Row> data, int pca_dim_count, int bucket_count, int fv_count, boolean filter_before) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        double decision_threshold_t = 0.5;
        if (parameters.LF_implementation_type != ConfigurationParameters.LD_ImplementationType.SPARK_RF)
            decision_threshold_t = parameters.LD_threshold;
        double decision_threshold = decision_threshold_t;

        Dataset<Row> ds_sample = data;
        double hist[][] = new double[pca_dim_count][bucket_count];

        if (filter_before)
            ds_sample = data.filter("LD > " + decision_threshold);
        ds_sample.toJavaRDD().takeSample(false, fv_count).forEach(new Consumer<Row>() {
            @Override
            public void accept(Row row) {
                if (row.<Double>getAs("LD") > decision_threshold) {
                    for (int i = 0; i < pca_dim_count; i++) {
                        int bucket_id = (int) row.<Double>getAs("pca_feature_bucket_" + i).longValue();
                        hist[i][bucket_id]++;
                    }
                }
            }
        });

        // normalize ref_hist
        for (int i = 0; i < pca_dim_count ; i++) {
            double sum = 0;
            for (int j = 0; j < bucket_count; j++)
                sum += hist[i][j];
            for (int j = 0; j < bucket_count; j++)
                hist[i][j] /= sum;
        }
        return hist;
    }

    static double[] computeWeights(double ref_hist[][]) {
        INDArray nd4j_array = Nd4j.create(ref_hist);
        INDArray std = nd4j_array.std(1); // std along rows
        double norm = std.norm2Number().doubleValue();
        return std.div(norm).dup().data().asDouble();
    }

    static double computeWassersteinDistance(double hist[][], ShapeGD_Generic shape_gd) {
        double dim_weights[] = shape_gd.getDimWeights();
        double ref_hist[][] = shape_gd.getRefHist();
        return computeWassersteinDistance(hist, ref_hist, dim_weights);
    }

    static double computeWassersteinDistance(double hist_1[][], double hist_2[][], double dim_weights[]) {
        int dims = hist_1.length;
        int bin_count = hist_1[0].length;
        double distance[] = new double[dims];

        for (int dim = 0; dim < dims; dim ++) {
            double pairwise_distance[] = new double[bin_count+1];
            for (int i = 0; i < bin_count; i ++)
                pairwise_distance[i+1] = pairwise_distance[i] + hist_1[dim][i] - hist_2[dim][i];

            for (int i = 0; i < bin_count; i ++)
                distance[dim] += Math.abs(pairwise_distance[i]);
        }

        double dist = 0;
        for (int i = 0; i < dims; i ++)
            dist += distance[i]*dim_weights[i];
        if (Double.isNaN(dist))
            throw new RuntimeException("NaN distance");
        return dist;
    }
}
