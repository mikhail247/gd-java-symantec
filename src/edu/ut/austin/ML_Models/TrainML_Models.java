package edu.ut.austin.ML_Models;

import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.DataInputOutput.GraphDataLoader;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.RealTimeSimulation.ShapeGD_Matlab;
import ml.dmlc.xgboost4j.java.Booster;
import ml.dmlc.xgboost4j.java.XGBoost;
import ml.dmlc.xgboost4j.java.XGBoostError;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.ml.Pipeline;
import org.apache.spark.ml.PipelineStage;
import org.apache.spark.ml.classification.RandomForestClassifier;
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator;
import org.apache.spark.ml.feature.HashingTF;
import org.apache.spark.ml.feature.NGram;
import org.apache.spark.ml.feature.Normalizer;
import org.apache.spark.ml.linalg.SparseVector;
import org.apache.spark.ml.param.ParamMap;
import org.apache.spark.ml.tuning.CrossValidator;
import org.apache.spark.ml.tuning.CrossValidatorModel;
import org.apache.spark.ml.tuning.ParamGridBuilder;
import org.apache.spark.mllib.clustering.BisectingKMeans;
import org.apache.spark.mllib.clustering.BisectingKMeansModel;
import org.apache.spark.mllib.clustering.KMeans;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.tree.RandomForest;
import org.apache.spark.mllib.tree.model.RandomForestModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import scala.Tuple2;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 3/15/2017.
 */
public class TrainML_Models {
    /*
    * Spark jobs should be initiated from the static context, otherwise Spark driver will try to
    * serialize the current object (current context) and send it to workers.
    * */
    public static RandomForestModel trainGraphRF_Model() {
        // Empty categoricalFeaturesInfo indicates all features are continuous.
        Integer num_classes = 2;
        Integer num_trees = 40;
        String feature_subset_strategy = "auto";
        String impurity = "gini";
        Integer max_depth = 5;
        Integer max_bins = 32;
        Integer seed = 12345;

        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        JavaRDD<String> raw_data = global_parameters.java_spark_context.textFile(parameters.GRF_Matlab_data);

        // 10-Fold Cross Validation
        JavaRDD<Row> java_row_rdd = raw_data.map(new Function<String, Row>() {
            @Override
            public Row call(String str) throws Exception {
                String array[] = str.split(",");
                double values[] = new double[array.length - 1];
                for (int i = 0; i < array.length - 1; i++) {
                    values[i] = Double.parseDouble(array[i + 1]);
                }
                Double label = Double.parseDouble(array[0]);
                org.apache.spark.ml.linalg.Vector features = org.apache.spark.ml.linalg.Vectors.dense(values);
                Row row_new = RowFactory.create(label, features);
                return row_new;
            }
        });

        // Generate the schema based on the string of schema
        List<StructField> fields = new ArrayList<>();
        fields.add(DataTypes.createStructField("label", DataTypes.DoubleType, false));
        fields.add(DataTypes.createStructField("features", new org.apache.spark.ml.linalg.VectorUDT(), false));
        StructType schema = DataTypes.createStructType(fields);

        // Apply the schema to the RDD
        final String metric = "accuracy";
        Dataset<Row> df = GlobalParameters.getInstance().spark.createDataFrame(java_row_rdd, schema);
        RandomForestClassifier rf = new RandomForestClassifier().setNumTrees(num_trees);
        Pipeline pipeline = new Pipeline().setStages(new PipelineStage[]{rf});
        ParamMap[] paramGrid = new ParamGridBuilder().build();
        MulticlassClassificationEvaluator evaluator = new MulticlassClassificationEvaluator()
                .setLabelCol("label")
                // "f1" (default), "weightedPrecision", "weightedRecall", "accuracy"
                .setMetricName(metric);
        CrossValidator cv = new CrossValidator().setEstimator(pipeline).setEvaluator(evaluator)
                .setEstimatorParamMaps(paramGrid).setNumFolds(10);
        CrossValidatorModel cvModel = cv.fit(df);
        System.out.println("RF: 10-fold CV " + metric + ": " + 100 * cvModel.avgMetrics()[0] + "%");

        // Training Random Forest Classifier
        JavaRDD<LabeledPoint> data = raw_data.map(
                new Function<String, LabeledPoint>() {
                    public LabeledPoint call(String s) {
                        String array[] = s.split(",");
                        double values[] = new double[array.length - 1];
                        for (int i = 0; i < array.length - 1; i++) {
                            values[i] = Double.parseDouble(array[i + 1]);
                        }
                        Double label = Double.parseDouble(array[0]);
                        Vector features = Vectors.dense(values);
                        return new LabeledPoint(label, features);
                    }
                }
        );

        HashMap<Integer, Integer> categorical_features_info = new HashMap<>();
        final RandomForestModel model = RandomForest.trainClassifier(data, num_classes,
                categorical_features_info, num_trees, feature_subset_strategy, impurity, max_depth, max_bins, seed);
        System.out.println(model);

        // Evaluate model on training partition
        JavaPairRDD<Double, Double> prediction_and_label = data.mapToPair(new PairFunction<LabeledPoint, Double, Double>() {
            @Override
            public Tuple2<Double, Double> call(LabeledPoint p) {
                return new Tuple2<>(model.predict(p.features()), p.label());
            }
        });

        Double train_error = 1.0 * prediction_and_label.filter(new Function<Tuple2<Double, Double>, Boolean>() {
            @Override
            public Boolean call(Tuple2<Double, Double> pl) {
                return !pl._1().equals(pl._2());
            }
        }).count() / data.count();
        System.out.println("RF: Training Error: " + 100 * train_error + "%");

        double classification_threshold = parameters.graphRF_threshold;
        long tps = prediction_and_label.filter(p -> (p._2 > 0.5) && (p._1 > classification_threshold)).count();
        long fps = prediction_and_label.filter(p -> (p._2 < 0.5) && (p._1 > classification_threshold)).count();
        long fns = prediction_and_label.filter(p -> (p._2 > 0.5) && (p._1 < classification_threshold)).count();
        long tps_truth = prediction_and_label.filter(p -> (p._2 > 0.5)).count();
        long fps_truth = prediction_and_label.filter(p -> (p._2 < 0.5)).count();
        System.out.println("RF: tps: " + tps + " fps: " + fps + " tps_truth: " + tps_truth + " fps_truth: " + fps_truth);
        System.out.println("RF: Precision: " + (double) tps / (tps + fps));
        System.out.println("RF: Recall: " + (double) tps / (tps + fns));
        System.out.println("RF: TP rate: " + (double) tps / tps_truth);
        System.out.println("RF: FP rate: " + (double) fps / fps_truth);

        try {
            Path path = new Path(parameters.Graph_RF_model_file_name);
            FileSystem file_system = org.apache.hadoop.fs.FileSystem.get(path.toUri(), global_parameters.java_spark_context.hadoopConfiguration());
            boolean removed = file_system.delete(path, true);
            GlobalParameters.getInstance().detection_logger.info("Old Random Forest model removed: " + removed);
        } catch (IOException e) {
            e.printStackTrace();
        }
        model.save(global_parameters.java_spark_context.sc(), parameters.Graph_RF_model_file_name);
        return model;
    }

    public static SparkRandomForestWrapper loadGraphRF_Model() {
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        RandomForestModel model = RandomForestModel.load(global_parameters.java_spark_context.sc(),
                ConfigurationParameters.getInstance().Graph_RF_model_file_name);
        return new SparkRandomForestWrapper(model);
    }

    public static KMeansModel trainKmeansModel() {
        int numClusters = 500;
        int numIterations = 200;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        RandomForestModel GRF = RandomForestModel.load(global_parameters.java_spark_context.sc(),
                ConfigurationParameters.getInstance().Graph_RF_model_file_name);

        // parse CSV data and skip the first two columns, which are <# of nodes in a nbd, mal ratio>
        JavaRDD<String> raw_data = global_parameters.java_spark_context.textFile(parameters.NBD_Matlab_data);
        JavaRDD<Vector> data = raw_data.map(
                new Function<String, Vector>() {
                    public Vector call(String s) {
                        String array[] = s.split(",");
                        double values[] = new double[array.length - 2];
                        for (int i = 0; i < array.length - 2; i++) {
                            values[i] = Double.parseDouble(array[i + 2]);
                        }
                        return Vectors.dense(values);
                    }
                }
        );
        JavaRDD<Vector> suspicious_data = data.filter(feature_vector -> GRF.predict(feature_vector) > parameters.graphRF_threshold);
        long tps_count = suspicious_data.count();
        System.out.println("# of FVs for clustering: " + tps_count);

        KMeansModel clusters = KMeans.train(suspicious_data.rdd(), numClusters, numIterations);
        double cost = clusters.computeCost(suspicious_data.rdd());
        System.out.println("Cost: " + cost);

        // Evaluate clustering by computing Within Set Sum of Squared Errors
        double WSSSE = clusters.computeCost(suspicious_data.rdd());
        System.out.println("Within Set Sum of Squared Errors = " + WSSSE);

        try {
            Path path = new Path(parameters.Kmeans_model_file_name);
            FileSystem file_system = org.apache.hadoop.fs.FileSystem.get(path.toUri(), global_parameters.java_spark_context.hadoopConfiguration());
            boolean removed = file_system.delete(path, true);
            GlobalParameters.getInstance().detection_logger.info("Old Random Forest model removed: " + removed);
        } catch (IOException e) {
            e.printStackTrace();
        }
        clusters.save(global_parameters.java_spark_context.sc(), parameters.Kmeans_model_file_name);
        return clusters;
    }

    public static BisectingKMeansModel trainBisectingKMeansModel() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        if (!parameters.train_GraphRF
                || parameters.clustering_type != ConfigurationParameters.ClusteringTypeEnum.BISECTING_KMEANS_OFFLINE)
            return null;

        global_parameters.logger.info("Training Bisecting clustering model");
        RandomForestModel GRF = RandomForestModel.load(global_parameters.java_spark_context.sc(),
                ConfigurationParameters.getInstance().Graph_RF_model_file_name);

        // parse CSV data and skip the first two columns, which are <# of nodes in a nbd, mal ratio>
        JavaRDD<String> raw_data = global_parameters.java_spark_context.textFile(parameters.NBD_Matlab_data);
        JavaRDD<Vector> data = raw_data.map(
                new Function<String, Vector>() {
                    public Vector call(String s) {
                        String array[] = s.split(",");
                        double values[] = new double[array.length - 2];
                        for (int i = 0; i < array.length - 2; i++) {
                            values[i] = Double.parseDouble(array[i + 2]);
                        }
                        return Vectors.dense(values);
                    }
                }
        );

        List<Vector> suspicious_data_local = data.collect().stream().filter(feature_vector ->
                GRF.predict(feature_vector) > parameters.graphRF_threshold).collect(Collectors.toList());
        JavaRDD<Vector> suspicious_data = global_parameters.java_spark_context.parallelize(suspicious_data_local);
        long tps_count = suspicious_data.count();
        System.out.println("# of FVs for clustering: " + tps_count);

        BisectingKMeans bisecting_kmeans = new BisectingKMeans().setK(parameters.max_cluster_count)
                .setMinDivisibleClusterSize(parameters.min_divisible_cluster_size).setSeed(System.currentTimeMillis());
        BisectingKMeansModel bisect_kmeans_model = bisecting_kmeans.run(suspicious_data);
        double cost = bisect_kmeans_model.computeCost(suspicious_data.rdd());
        System.out.println("Cost: " + cost);

        // Evaluate clustering by computing Within Set Sum of Squared Errors
        double WSSSE = bisect_kmeans_model.computeCost(suspicious_data.rdd());
        System.out.println("Within Set Sum of Squared Errors = " + WSSSE);

        try {
            Path path = new Path(parameters.bisecting_Kmeans_model_file_name);
            FileSystem file_system = org.apache.hadoop.fs.FileSystem.get(path.toUri(), global_parameters.java_spark_context.hadoopConfiguration());
            boolean removed = file_system.delete(path, true);
            GlobalParameters.getInstance().detection_logger.info("Old Bisecting clustering model removed: " + removed);
        } catch (IOException e) {
            e.printStackTrace();
        }
        bisect_kmeans_model.save(global_parameters.java_spark_context.sc(), parameters.bisecting_Kmeans_model_file_name);
        return bisect_kmeans_model;
    }

    public static BisectingKMeansModel loadBisectingKMeansModel() {
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        BisectingKMeansModel model = BisectingKMeansModel.load(global_parameters.java_spark_context.sc(),
                ConfigurationParameters.getInstance().bisecting_Kmeans_model_file_name);
        return model;
    }

    public static KMeansModel loadKmeansModel() {
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        KMeansModel model = KMeansModel.load(global_parameters.java_spark_context.sc(),
                ConfigurationParameters.getInstance().Kmeans_model_file_name);
        return model;
    }

    public static void saveEncodedAsmData(String input_folder, String file_output, boolean add_label, boolean dump_csv_file, boolean recompute_ngrams) {
        final int file_processing_chunk = 7*1000;
        int file_counter = 0;
        Dataset<Row> dataset = null;
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        global_parameters.logger.info("saveEncodedAsmData: processing " + input_folder);
        String dir_output_name = "results" + File.separator + "features";
        new File(dir_output_name).mkdirs();
        Set<String> files = Stream.of(new File(input_folder).listFiles()).map(File::getAbsolutePath).collect(Collectors.toSet());
        String parquet_path = dir_output_name + File.separator + file_output + ".parquet";
        if (recompute_ngrams) {
            try {
                FileUtils.deleteDirectory(new File(parquet_path));
            } catch (IOException e) {
                global_parameters.logger.warning("Cannot remove parquet file: " + parquet_path);
                e.printStackTrace();
            }
        }
        if (Files.exists(Paths.get(parquet_path)))
            dataset = global_parameters.spark.read().parquet(parquet_path);

        Set<String> processed_files = dataset != null ? dataset.select("sha256").collectAsList()
                .stream().map(row -> input_folder + File.separator + row.<String>getAs("sha256") + ".asm").collect(Collectors.toSet()) : new HashSet<>();
        dataset = null;
        files.removeAll(processed_files);
        List<String> files_list = new LinkedList(files);
        global_parameters.logger.info("Extracting n-grams: " + processed_files.size() + " files have been processed");

        for (int i = 0; i < (files.size() + file_processing_chunk - 1) / file_processing_chunk; i ++) {
            List<String> files_sublist = files_list.subList(i * file_processing_chunk, Math.min(files.size(), (i+1) * file_processing_chunk));
            Dataset<Row> ds = encodeAsmDataFileLevel(files_sublist);
            global_parameters.logger.info("Saving n-grams to " + file_output + " file");
            ds.write().mode("append").parquet(dir_output_name + File.separator + file_output + ".parquet");
            file_counter += file_processing_chunk;
            global_parameters.logger.info("Extracted n-grams from " + file_counter + " files");
            System.gc();
        }

        // saving plain comma separated values
        if(dump_csv_file) {
            Dataset<String> dataset_encoded = dataset.map(new MapFunction<Row, String>() {

                @Override
                public String call(Row row) throws Exception {
                    String str =
                            (add_label ? row.<Integer>getAs("label") + "," : "")
                                    + Arrays.stream(row.<SparseVector>getAs("features").toArray()).mapToObj(Double::toString)
                                    .collect(Collectors.joining(","));
                    return str;
                }
            }, Encoders.STRING());

            List<String> data = dataset_encoded.collectAsList();
            try (
                    OutputStreamWriter stream_writer = new FileWriter(dir_output_name + File.separator + file_output + ".csv");
                    BufferedWriter writer = new BufferedWriter(stream_writer);
            ) {
                for (String str : data)
                    writer.write(str + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* Sliding a window of size asm_lines_per_fv lines by asm_lines_shift */
    private static Dataset<Row> encodeAsmDataFileLevel(List<String> files) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        int ngram_size = parameters.n_gram_size;
        int hash_size = (int) Math.pow(2, parameters.hash_bit_length);

        String dir_input_name = "results" + File.separator + "disasm";
        List<Row> training_data = Collections.synchronizedList(new LinkedList<>());

        files.parallelStream().forEach(file -> {
            int label = file.contains("benignware") ? 0 : 1; // 0 - benignware, 1 -malware
            try {
                String content = FileUtils.readFileToString(new File(file));
                String array[] = content.split("\\n");
                String file_name = array[0];
                String sha256 = array[1];
                String md5 = array[2];
                List<String> asm_inst = Arrays.asList(array);

                int start_line_id = 3;
                if (parameters.asm_lines_per_fv < 0) {
                    List<String> asm_inst_fv = asm_inst.subList(start_line_id, asm_inst.size());
                    Row row = RowFactory.create(label, file_name, sha256, md5, asm_inst_fv);
                    training_data.add(row);
                } else {
                    for (; start_line_id < array.length; start_line_id += parameters.asm_lines_shift) {
                        int end_line_id = Math.min(start_line_id + parameters.asm_lines_per_fv, array.length);
                        List<String> asm_inst_fv = asm_inst.subList(start_line_id, end_line_id);
                        Row row = RowFactory.create(label, file_name, sha256, md5, asm_inst_fv);
                        training_data.add(row);
                    }
                }
            } catch (Exception e) {
                global_parameters.logger.info(e.toString());
            }
        });

        StructType schema = new StructType(new StructField[]{
                new StructField("label", DataTypes.IntegerType, false, Metadata.empty()),
                new StructField("file_name", DataTypes.StringType, false, Metadata.empty()),
                new StructField("sha256", DataTypes.StringType, false, Metadata.empty()),
                new StructField("md5", DataTypes.StringType, false, Metadata.empty()),
                new StructField("asm", DataTypes.createArrayType(DataTypes.StringType), false, Metadata.empty())
        });

        Dataset<Row> data_frame = GlobalParameters.getInstance().spark.createDataFrame(training_data, schema);
        NGram n_gram_transformer = new NGram().setN(ngram_size).setInputCol("asm").setOutputCol("ngrams");

        System.out.println("Extracting n-grams");
        Dataset<Row> ngram_data_frame = n_gram_transformer.transform(data_frame).drop("asm");

        System.out.println("Hashing n-grams");
        HashingTF hashingTF = new HashingTF().setInputCol("ngrams").setOutputCol("features").setNumFeatures(hash_size);
        Dataset<Row> featurized_data = hashingTF.transform(ngram_data_frame).drop("ngrams");

        // normalize feature vectors
        System.out.println("Normalizing feature vectors");
        Normalizer normalizer = new Normalizer().setInputCol("features").setOutputCol("normalized_features").setP(1.0);
        featurized_data = normalizer.transform(featurized_data).drop("features").withColumnRenamed("normalized_features", "features");
        return featurized_data;
    }

    private static Dataset<Row> encodeAsmDataDirectoryLevel(List<String> dirs) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        int ngram_size = parameters.n_gram_size;
        int hash_size = (int) Math.pow(2, parameters.hash_bit_length);

        String dir_input_name = "results" + File.separator + "disasm";
        List<Row> training_data = new LinkedList<>();

        global_parameters.logger.info("Loading data: " + dirs);
        dirs.stream().forEach(input_dir -> {
            int counter = 0;
            double prev_percentage = -1;
            String dir_input_name_t = dir_input_name + File.separator + input_dir;
            File folder = new File(dir_input_name_t);
            int label = input_dir.startsWith("benignware") ? 0 : 1; //0 - benignware, 1 -malware
            File file_list[] = folder.listFiles();

            for (File file : file_list) {
                double percentage = (double) counter++ / folder.listFiles().length;
                if (percentage - prev_percentage > 0.05) {
                    prev_percentage = percentage;
                    System.out.format(" ==> : %.02f", percentage);
                }

                try {
                    String content = FileUtils.readFileToString(file);
                    String array[] = content.split("\\n");
                    String file_name = array[0];
                    String sha256 = array[1];
                    String md5 = array[2];
                    List<String> asm_inst = Arrays.asList(array);

                    int start_line_id = 3;
                    for (; start_line_id < array.length; start_line_id += parameters.asm_lines_per_fv) {
                        int end_line_id = Math.min(start_line_id + parameters.asm_lines_per_fv, array.length);
                        List<String> asm_inst_fv = asm_inst.subList(start_line_id, end_line_id);
                        Row row = RowFactory.create(label, file_name, sha256, md5, asm_inst_fv);
                        training_data.add(row);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        System.out.format(" ==> : %.02f\n", 1.0);

        StructType schema = new StructType(new StructField[]{
                new StructField("label", DataTypes.IntegerType, false, Metadata.empty()),
                new StructField("file_name", DataTypes.StringType, false, Metadata.empty()),
                new StructField("sha256", DataTypes.StringType, false, Metadata.empty()),
                new StructField("md5", DataTypes.StringType, false, Metadata.empty()),
                new StructField("asm", DataTypes.createArrayType(DataTypes.StringType), false, Metadata.empty())
        });

        Dataset<Row> data_frame = GlobalParameters.getInstance().spark.createDataFrame(training_data, schema);
        NGram n_gram_transformer = new NGram().setN(ngram_size).setInputCol("asm").setOutputCol("ngrams");

        System.out.println("Extracting n-grams");
        Dataset<Row> ngram_data_frame = n_gram_transformer.transform(data_frame).drop("asm");

        System.out.println("Hashing n-grams");
        HashingTF hashingTF = new HashingTF().setInputCol("ngrams").setOutputCol("features").setNumFeatures(hash_size);
        Dataset<Row> featurized_data = hashingTF.transform(ngram_data_frame).drop("ngrams");

        // normalize feature vectors
        System.out.println("Normalizing feature vectors");
        Normalizer normalizer = new Normalizer().setInputCol("features").setOutputCol("normalized_features").setP(1.0);
        featurized_data = normalizer.transform(featurized_data).drop("features").withColumnRenamed("normalized_features", "features");
        return featurized_data;
    }

    public static Dataset<Row> loadVTReportFeatures(String dir_name) {
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        GraphDataLoader loader = new GraphDataLoader();
        Map<String, FileInfo>  map_t = loader.getFileInfoMapWithoutMapping();
        Map<String, FileInfo> map = map_t.values().parallelStream()
                .collect(Collectors.toConcurrentMap(file_info -> file_info.sha2, java.util.function.Function.identity()));


        File dirs[] = new File(dir_name).listFiles(File::isDirectory);
        JavaRDD<String> rdd_string = Arrays.stream(dirs).map(dir ->
                global_parameters.java_spark_context.textFile(dir.getAbsolutePath())).reduce((rdd_1, rdd_2) -> rdd_1.union(rdd_2)).get();

        JavaRDD<Row> rdd = rdd_string.filter(str -> !str.startsWith("Time"))
                .map(new Function<String, Row>() {
            @Override
            public Row call(String str) throws Exception {
                int index = str.indexOf(',');
                String sha256 = str.substring(3, index-1);

                String vec = str.substring(index+15, str.length()-2);
                String array[] = vec.split(",");
                int vec_size = Integer.parseInt(array[0].trim());
                List<Integer> positions = new LinkedList<>();
                List<Double> values = new LinkedList<>();

                for (int i = 1; i < array.length; i ++) {
                    String temp[] = array[i].split(":");
                    positions.add(Integer.parseInt(temp[0].replaceAll("[\\{\\}]", "").trim()));
                    values.add(Double.parseDouble(temp[1].replaceAll("[\\{\\}]", "").trim()));
                }
                org.apache.spark.ml.linalg.Vector features = org.apache.spark.ml.linalg.Vectors.sparse(vec_size, positions.stream().mapToInt(i -> i).toArray(), values.stream().mapToDouble(d -> d).toArray());

                int is_benign = map.get(sha256).is_benign ? 0 : 1;
                return RowFactory.create(is_benign, sha256, features);
            }
        });

        global_parameters.logger.info("Total number of loaded FVs: " + rdd.count());
        // Generate the schema based on the string of schema
        List<StructField> fields = new ArrayList<>();
        fields.add(DataTypes.createStructField("label", DataTypes.IntegerType, false));
        fields.add(DataTypes.createStructField("sha256", DataTypes.StringType, false));
        fields.add(DataTypes.createStructField("features", new org.apache.spark.ml.linalg.VectorUDT(), false));
        StructType schema = DataTypes.createStructType(fields);

        Dataset<Row> df = GlobalParameters.getInstance().spark.createDataFrame(rdd, schema);
        df.write().mode("overwrite").parquet("results/features/vt_report_features.parquet");
        return df;
    }

    /* First, save data in parquet format with saveEncodedAsmData(),
    *  then call trainLD_RF_Model()
    * */
    public static RandomForestModel trainLD_RF_Model() {
        int rf_num_trees = 30;
        int rf_num_classes = 2;
        String feature_subset_strategy = "auto";
        String impurity = "gini";
        String parquet_file_name;
        Integer max_depth = 5;
        Integer max_bins = 32;
        Integer seed = 12345;
        Dataset<Row> df_1 = null, df_2 = null, featurized_data_t;

        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        if (parameters.LF_implementation_type != ConfigurationParameters.LD_ImplementationType.SPARK_RF) {
            global_parameters.logger.info("Using XGBoost model");
            return null;
        }

        global_parameters.logger.info("Training n-gram Random Forest");
        String dir_input_name = "results" + File.separator + "features";

        switch(parameters.shape_gd_training_method) {
            case EXTERNAL_DATASET:
                df_1 = global_parameters.spark.read().parquet(dir_input_name + "/features_benignware_training_" + parameters.n_gram_size +  ".parquet");
                df_2 = global_parameters.spark.read().parquet(dir_input_name + "/features_malware_training_" + parameters.n_gram_size +  ".parquet");
                break;
            case VT_DATASET:
                parquet_file_name = "results/features/features_vt_binaries_training_" + parameters.n_gram_size + ".parquet";
                featurized_data_t = global_parameters.spark.read().parquet(parquet_file_name)
                        .drop("scaled_features").drop("pca_features");
                df_1 = featurized_data_t.filter("label < 0.5");
                df_2 = featurized_data_t.filter("label > 0.5");
                break;

            case VT_REPORTS:
                parquet_file_name = "results/features/vt_report_features.parquet";
                featurized_data_t = global_parameters.spark.read().parquet(parquet_file_name)
                        .drop("scaled_features").drop("pca_features");
                df_1 = featurized_data_t.filter("label < 0.5");
                df_2 = featurized_data_t.filter("label > 0.5");
                // TODO remove
                df_1 = global_parameters.spark.createDataFrame(df_1.toJavaRDD().takeSample(true, 20000), df_1.schema());
                df_2 = global_parameters.spark.createDataFrame(df_2.toJavaRDD().takeSample(true, 20000), df_2.schema());
                break;
            default:
                System.exit(-1);
        }

        Dataset<Row> featurized_data = df_1.union(df_2);
        System.out.format("# of Benign unique files: %d, # of FVs: %d\n", df_1.select("sha256").count(), df_1.count());
        System.out.format("# of Malicious unique files: %d, # of FVs: %d\n", df_2.select("sha256").count(), df_2.count());

        // Perform 10-fold CV
        if (parameters.enable_LD_CV) {
            global_parameters.logger.info("10-fold CV Random Forest");
            RandomForestClassifier rf = new RandomForestClassifier().setNumTrees(rf_num_trees);
            Pipeline pipeline = new Pipeline().setStages(new PipelineStage[]{rf});
            ParamMap paramGrid[] = new ParamGridBuilder().build();

            Arrays.asList("accuracy", "weightedRecall", "weightedPrecision", "f1").stream().forEach(metric -> {
                MulticlassClassificationEvaluator evaluator = new MulticlassClassificationEvaluator()
                        .setLabelCol("label").setMetricName(metric);

                CrossValidator cv = new CrossValidator().setEstimator(pipeline).setEvaluator(evaluator)
                        .setEstimatorParamMaps(paramGrid).setNumFolds(10);
                CrossValidatorModel cvModel = cv.fit(featurized_data);
                System.out.println("RF: 10-fold CV " + metric + ": " + 100 * cvModel.avgMetrics()[0] + "%");
            });
        }

        JavaRDD<Row> featurized_data_rdd = featurized_data.toJavaRDD();
        JavaRDD<LabeledPoint> data = featurized_data_rdd.map(
                new Function<Row, LabeledPoint>() {
                    @Override
                    public LabeledPoint call(Row row) throws Exception {
                        double label = ((Integer) row.getAs("label")).doubleValue();
                        org.apache.spark.ml.linalg.DenseVector vec = row.<org.apache.spark.ml.linalg.SparseVector>getAs("features").toDense();
                        Vector features = Vectors.dense(vec.toArray());
                        LabeledPoint labeled_point = new LabeledPoint(label, features);
                        return labeled_point;
                    }
                }
        );

        // Training Random Forest Classifier
        global_parameters.logger.info("Training a Random Forest classifier");
        HashMap<Integer, Integer> categorical_features_info = new HashMap<>();
        final RandomForestModel model = RandomForest.trainClassifier(data, rf_num_classes,
                categorical_features_info, rf_num_trees, feature_subset_strategy, impurity, max_depth, max_bins, seed);
        System.out.println(model);

        // Evaluate model on training partition
        JavaPairRDD<Double, Double> prediction_and_label = data.mapToPair(new PairFunction<LabeledPoint, Double, Double>() {
            @Override
            public Tuple2<Double, Double> call(LabeledPoint p) {
                return new Tuple2<>(model.predict(p.features()), p.label());
            }
        });

        Double train_error = 1.0 * prediction_and_label.filter(new Function<Tuple2<Double, Double>, Boolean>() {
            @Override
            public Boolean call(Tuple2<Double, Double> pl) {
                return !pl._1().equals(pl._2());
            }
        }).count() / data.count();
        System.out.println("RF: Training Error: " + 100 * train_error + "%");

        double classification_threshold = 0.5;
        long tps = prediction_and_label.filter(p -> (p._2 > 0.5) && (p._1() > classification_threshold)).count();
        long fps = prediction_and_label.filter(p -> (p._2 < 0.5) && (p._1() > classification_threshold)).count();
        long fns = prediction_and_label.filter(p -> (p._2 > 0.5) && (p._1() < classification_threshold)).count();
        long tps_truth = prediction_and_label.filter(p -> (p._2 > 0.5)).count();
        long fps_truth = prediction_and_label.filter(p -> (p._2 < 0.5)).count();
        System.out.println("RF: tps: " + tps + " fps: " + fps + " tps_truth: " + tps_truth + " fps_truth: " + fps_truth);
        System.out.println("RF: Precision: " + (double) tps / (tps + fps));
        System.out.println("RF: Recall: " + (double) tps / (tps + fns));
        System.out.println("RF: TP rate: " + (double) tps / tps_truth);
        System.out.println("RF: FP rate: " + (double) fps / fps_truth);

        try {
            Path path = new Path(parameters.LD_RF_model_file_name);
            FileSystem file_system = org.apache.hadoop.fs.FileSystem.get(path.toUri(), global_parameters.java_spark_context.hadoopConfiguration());
            boolean removed = file_system.delete(path, true);
            GlobalParameters.getInstance().detection_logger.info("Old Random Forest model removed: " + removed);
        } catch (IOException e) {
            e.printStackTrace();
        }
        model.save(global_parameters.java_spark_context.sc(), parameters.LD_RF_model_file_name);
        return model;
    }

    /*
    * Save alert-FVs in results/features/alert_FVs.parquet
    * */
    @Deprecated
    public static void processFVsWithLDs(ShapeGD_Matlab shape_detector) {
        String dir_output_name = "results" + File.separator + "features";
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        RandomForestModel rf_model = loadLD_RF_Model();

        global_parameters.logger.info("Labeling Testing FVs using Random Forest");
        String dir_input_name = "results" + File.separator + "features";
        Dataset<Row> df_1 = global_parameters.spark.read().parquet(dir_input_name + File.separator + "features_benignware_testing.parquet");
        Dataset<Row> df_2 = global_parameters.spark.read().parquet(dir_input_name + File.separator + "features_malware_testing.parquet");
        Dataset<Row> data = df_1.union(df_2);

        StructType schema = data.schema().add(new StructField("LD", DataTypes.DoubleType, false, Metadata.empty()));
        ExpressionEncoder row_encoder = RowEncoder.apply(schema);

        final INDArray pca_mean = shape_detector.getPCA_Mean();
        final INDArray pca_basis = shape_detector.getPCA_Basis();
        // add LD's label and project on GD's basis
        Dataset<Row> enriched_data = data.map(new MapFunction<Row, Row>() {
            @Override
            public Row call(Row row) throws Exception {
                org.apache.spark.ml.linalg.DenseVector vec = row.<org.apache.spark.ml.linalg.SparseVector>getAs("features").toDense();
                Vector features = Vectors.dense(vec.toArray());
                Double prediction = rf_model.predict(features);

                INDArray nd4j_vec = Nd4j.create(vec.toArray());
                INDArray res = nd4j_vec.subi(pca_mean).mmul(pca_basis);
                int ind = row.fieldIndex("features");

                Object array[] = new Object[row.size() + 1];
                IntStream.range(0, row.size()).forEach(i -> {
                    array[i] = row.get(i);
                });
                array[row.size()] = prediction;
                array[ind] = Vectors.dense(res.data().asDouble()).asML();
                Row row_new = RowFactory.create(array);
                return row_new;
            }
        }, row_encoder);

        global_parameters.logger.info("Alert-FVs.parquet generated");
        enriched_data.write().mode("overwrite").parquet(dir_output_name + File.separator + "alert_FVs.parquet");
    }

    private static RandomForestModel loadLD_RF_Model() {
        RandomForestModel model = null;
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        try {
            model = RandomForestModel.load(global_parameters.java_spark_context.sc(),
                    ConfigurationParameters.getInstance().LD_RF_model_file_name);
        } catch (Exception e) {
            global_parameters.logger.info("LD_RF model not found");
        }
        return model;
    }

    public static LocalDetector loadLD_Model(ConfigurationParameters.LD_ImplementationType ld_type) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        LocalDetector local_detector = new LocalDetector();

        switch(ld_type) {
            case SPARK_RF:
                RandomForestModel model = null;
                try {
                    model = RandomForestModel.load(global_parameters.java_spark_context.sc(),
                            ConfigurationParameters.getInstance().LD_RF_model_file_name);
                } catch (Exception e) {
                    global_parameters.logger.info("LD_RF model not found");
                }
                if (model == null)
                    return null;
                local_detector.setPredictionEngine(model);
                break;

            case XGBOOST_LOOKUP_TABLE:
                String line;
                //<sha 256, <label, prediction>>
                ConcurrentHashMap<String, Pair<Double, Double>> predictions = new ConcurrentHashMap();
                try (
                        BufferedReader br = new BufferedReader(new FileReader(parameters.vt_report_classification_score));
                ){
                    while ((line = br.readLine()) != null) {
                        String array[] = line.split(",");
                        predictions.put(array[0], new ImmutablePair<>(Double.parseDouble(array[1]), Double.parseDouble(array[2])));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
                local_detector.setPredictionEngine(predictions);
                break;

            case XGBOOST_APPROXIMATION:
                Booster booster = null;

                try {
                    booster = XGBoost.loadModel("results/ld_xgboost_64.model");
                } catch (XGBoostError xgBoostError) {
                    xgBoostError.printStackTrace();
                    System.exit(-1);
                }
                local_detector.setPredictionEngine(booster);
        }
        return local_detector;
    }

    public static Booster loadXGBoostNBD_Classifier() {
        Booster booster = null;

        try {
            booster = XGBoost.loadModel("results/nbd_xgboost_model.model");
        } catch (XGBoostError xgBoostError) {
            xgBoostError.printStackTrace();
            System.exit(-1);
        }
        return booster;
    }

    public static Map<String, Double> loadXGBoostNBD_Predictions() {
        String line;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        String file_name = "data/nbd_classification_fp_tp_" + parameters.data_file_count + ".csv";
        Map<String, Double> xgboost_predictor = new ConcurrentHashMap<>();

        try (
                BufferedReader br = new BufferedReader(new FileReader(file_name));
        ){
            while ((line = br.readLine()) != null) {
                String array[] = line.split(",");
                double prediction = Double.parseDouble(array[1]);
                xgboost_predictor.put(array[0], prediction);
            }
        } catch (Exception e) {
            e.printStackTrace();
            global_parameters.logger.log(Level.SEVERE, file_name + " not found");
            return null;
        }
        file_name = "data/nbd_classification_fp_tp_thresholds_" + parameters.data_file_count + ".csv";

        if (parameters.automatically_set_nbd_xgboost_threshold) {
            try (
                    BufferedReader br = new BufferedReader(new FileReader(file_name));
            ) {
                while ((line = br.readLine()) != null) {
                    String array[] = line.split(",");
                    double fp_rate = Double.parseDouble(array[1]);
                    // set threshold such that NBD-level FP rate is 5%
                    if (fp_rate > parameters.nbd_threshold)
                        continue;
                    double tp_rate = Double.parseDouble(array[2]);
                    parameters.nbd_xgboost_threshold = Double.parseDouble(array[0]);
                    global_parameters.logger.info("XGBoost NBD threshold: "
                            + parameters.nbd_xgboost_threshold + " NBD FP rate: " + fp_rate + " TP rate: " + tp_rate);
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
                global_parameters.logger.log(Level.SEVERE, file_name + " not found");
                return null;
            }
        }

        return xgboost_predictor;
    }
}
