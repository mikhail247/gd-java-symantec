package edu.ut.austin.MachineSelectionStrategy;

import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.Introspection.NBD_State;
import edu.ut.austin.ML_Models.LocalDetector;
import edu.ut.austin.ML_Models.TrainML_Models;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 4/13/2017.
 */
public class LD_MachineSelection implements MachineSelectionStrategy {
    private static ConfigurationParameters parameters = ConfigurationParameters.getInstance();
    private static LocalDetector local_detector = TrainML_Models.loadLD_Model(parameters.LF_implementation_type);

    private boolean select_malicious_nbds_only;

    public LD_MachineSelection(boolean select_malicious_nbds_only) {
        this.select_malicious_nbds_only = select_malicious_nbds_only;
    }

    @Override
    public Set<Long> getSuspiciousMachines(NBD_State nbd) {
        if (select_malicious_nbds_only && !nbd.isMalicious())
            return new HashSet<>();

        Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>> nbd_structure = nbd.getNBD_Structure(0);
        return nbd_structure.entrySet().stream().filter(entry -> Stream.concat(entry.getValue().getLeft().stream(),
                entry.getValue().getRight().stream()).anyMatch(file_info -> local_detector.isMalware(file_info.redirect.sha2, null)))
                .map(Map.Entry::getKey).collect(Collectors.toSet());
    }

    @Override
    public Set<String> getSuspiciousFiles(NBD_State nbd) {
        if (select_malicious_nbds_only && !nbd.isMalicious())
            return new HashSet<>();

        // <machine_id, <benign files, malicious files>>
        Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>> nbd_structure = nbd.getNBD_Structure(0);
        return nbd_structure.values().stream().flatMap(pair -> Stream.concat(pair.getLeft().stream(), pair.getRight().stream()))
                .filter(file_info -> local_detector.isMalware(file_info.redirect.sha2, null))
                .map(file_info -> file_info.sha2_id).collect(Collectors.toSet());
    }

    @Override
    public Pair<Set<Long>, Set<String>> getSuspiciousEntities(NBD_State nbd) {
        if (select_malicious_nbds_only && !nbd.isMalicious())
            return new ImmutablePair<>(new HashSet<>(), new HashSet<>());

        Set<Long> suspicious_machines = getSuspiciousMachines(nbd);
        Set<String> suspicious_files = getSuspiciousFiles(nbd);
        return new ImmutablePair<>(suspicious_machines, suspicious_files);
    }

    @Override
    public String getArgs() {
        return "null";
    }

    @Override
    public String toString() {
        String args = select_malicious_nbds_only ? "only_mal_nbds" : "all_nbds";
        return this.getClass().getName() + "_" + args;
    }
}
