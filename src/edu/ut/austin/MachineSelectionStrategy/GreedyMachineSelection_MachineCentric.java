package edu.ut.austin.MachineSelectionStrategy;

import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.Introspection.NBD_State;
import edu.ut.austin.RealTimeSimulation.ShapeGD;
import ml.dmlc.xgboost4j.java.Booster;
import ml.dmlc.xgboost4j.java.DMatrix;
import ml.dmlc.xgboost4j.java.XGBoost;
import ml.dmlc.xgboost4j.java.XGBoostError;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 4/13/2017.
 */
public class GreedyMachineSelection_MachineCentric implements MachineSelectionStrategy {
    private ShapeGD shape_detector;
    private Map<String, List<int[]>> data;
    private Booster xgboost_model = null;

    public GreedyMachineSelection_MachineCentric(ShapeGD shape_detector) {
        this.shape_detector = shape_detector;
        this.data = shape_detector.getData();

        try {
            xgboost_model = XGBoost.loadModel("results/nbd_xgboost_64.model");
        } catch (XGBoostError xgBoostError) {
            xgBoostError.printStackTrace();
            System.exit(-1);
        }
    }

    private TreeMap<Float, List<Long>> predict(List<Pair<double[][], Long>> input) {
        int vec_counter = 0;
        int rows = input.get(0).getLeft().length;
        int columns = input.get(0).getLeft()[0].length;

        float matrix[] = new float[rows*columns*input.size()];
        for (Pair<double[][], Long> pair : input) {
            float fv[] = getLinearFV(pair.getLeft());
            System.arraycopy(fv, 0, matrix, vec_counter*rows*columns, fv.length);
            vec_counter ++;
        }

        try {
            DMatrix dmatrix = new DMatrix(matrix, input.size(), rows*columns);
            float predictions[][] = xgboost_model.predict(dmatrix);
            Map<Float, List<Long>> ret_ = IntStream.range(0, input.size()).mapToObj(i ->
                    new ImmutablePair<>(predictions[i][0], input.get(i).getRight()))
                    .collect(Collectors.groupingBy(ImmutablePair::getLeft, Collectors.mapping(ImmutablePair::getRight, Collectors.toList())));
            TreeMap<Float, List<Long>> ret = new TreeMap<>();
            ret.putAll(ret_);
            return ret;
        } catch (XGBoostError xgBoostError) {
            xgBoostError.printStackTrace();
            System.exit(-1);
        }
        return null;
    }

    private double predict(double hist[][]) {
        try {
            float fv[] = getLinearFV(hist);
            DMatrix dmatrix = new DMatrix(fv, 1, fv.length);
            return xgboost_model.predict(dmatrix)[0][0];
        } catch (XGBoostError xgBoostError) {
            xgBoostError.printStackTrace();
            System.exit(-1);
        }
        return -1;
    }

    private float[] getLinearFV(double fv[][]) {
        if (fv == null)
            return null;

        int rows = fv.length;
        int columns = fv[0].length;
        int counter = 0;
        float fv_linear[] = new float[rows * columns];

        for (int i = 0; i < rows; i ++) {
            for (int j = 0; j < columns; j ++) {
                fv_linear[counter ++] = (float) fv[i][j];
            }
        }
        return fv_linear;
    }

    @Override
    public Set<Long> getSuspiciousMachines(NBD_State nbd) {
        if (!nbd.isMalicious())
            return new HashSet<>();

        int threshold;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        Comparator<Pair<Float, Long>> comparator = (entry_1, entry_2) -> {
            float ratio_1 = entry_1.getLeft();
            float ratio_2 = entry_2.getLeft();
            if (ratio_1 == ratio_2) return 0;
            return ratio_1 > ratio_2 ? -1 : +1;
        };

        // <machine id, hash list>
        Map<Long, List<String>> file_hashes = nbd.getNBD_Structure(0).entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey,
                        entry -> Stream.concat(entry.getValue().getLeft().stream(), entry.getValue().getRight().stream())
                                .map(file_info -> file_info.redirect.sha2).collect(Collectors.toList())
                ));
        // <chosen machine, current ShapeScore>
        List<Pair<List<Long>, Double>> suspicious_machines = new LinkedList<>();
        if (nbd.score == null) // the nbd has not been analyzed
            return new HashSet<>();
        double score = nbd.score;

        if (parameters.greedy_search_nbd_portion != null) {
            threshold = (int) (parameters.greedy_search_nbd_portion * file_hashes.size());
            threshold = Math.max(threshold, file_hashes.size() - 200);
        } else {
            threshold = file_hashes.size() * nbd.getUniqueMaliciousFiles_GroundTruth(0).size() / nbd.getUniqueBenignFiles_GroundTruth(0).size();
            if (!nbd.getUniqueMaliciousFiles_GroundTruth(0).isEmpty())
                threshold = Math.max(threshold, 1); // select at least 1 file, i.e. 1 machine
        }

        if (file_hashes.size() > 10*1000) return new HashSet<>();
        while(file_hashes.size() > threshold) {
            List<Pair<double[][], Long>> tmp = file_hashes.keySet().parallelStream().map(machine_id -> {
                Map<String, Long> reduced_nbd_map = file_hashes.entrySet().stream()
                        .filter(entry -> !entry.getKey().equals(machine_id)).flatMap(entry ->
                                entry.getValue().stream()).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

                List<int[]> fvs = shape_detector.getBucketVectorsFromSpark(data, reduced_nbd_map);
                double hist[][] = shape_detector.buildNormalizedHistogramFromBucketIndices(fvs);
                return new ImmutablePair<>(hist, machine_id);
            }).collect(Collectors.toList());

            TreeMap<Float, List<Long>> machine_scores = predict(tmp);
            List<Long> machine_ids = machine_scores.firstEntry().getValue();
            file_hashes.keySet().removeAll(machine_ids);
            suspicious_machines.add(new ImmutablePair<>(machine_ids, score));
            score = machine_scores.firstKey();
        }
        return machineSelectionPolicy(suspicious_machines);
    }

    private Set<Long> machineSelectionPolicy(List<Pair<List<Long>, Double>> suspicious_machines) {
        Set<Long> ret = new HashSet<>();
        Double prev_score = Double.POSITIVE_INFINITY;
        List<Pair<List<Long>, Double>> monotone_part = new LinkedList<>();
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        for (Pair<List<Long>, Double> pair : suspicious_machines) {
            if (prev_score < pair.getRight())
                break;
            monotone_part.add(pair);
            prev_score = pair.getRight();
        }

        if (monotone_part.size() < 3)
            return ret;

        Pair<List<Long>, Double> pair_left = monotone_part.get(0);
        Pair<List<Long>, Double> pair_center =  monotone_part.get(1);
        ret.addAll(pair_left.getLeft()); ret.addAll(pair_center.getLeft());

        for (int i = 2; i < monotone_part.size(); i ++) {
            Pair<List<Long>, Double> pair_right = monotone_part.get(i);
            double first_derivative = (pair_right.getRight() - pair_left.getRight())/2;
            double second_derivative = pair_left.getRight() -2*pair_center.getRight() + pair_right.getRight();
            if (Math.abs(first_derivative) < parameters.greedy_machine_selection_min_derivative_threshold)
                break;
            ret.addAll(pair_right.getLeft());
            pair_left = pair_center;
            pair_center = pair_right;
        }
        return ret;
    }

    @Override
    // return sha2_ids
    public Set<String> getSuspiciousFiles(NBD_State nbd) {
        if (!nbd.isMalicious())
            return new HashSet<>();

        // <machine_id, <benign files, malicious files>>
        Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>> nbd_structure = nbd.getNBD_Structure(0);
        Map<Long, Set<FileInfo>> flattened_nbd_structure = nbd_structure.entrySet().stream().collect(Collectors.toMap(
                Map.Entry::getKey, entry -> {
                    Set<FileInfo> set = new HashSet<>();
                    set.addAll(entry.getValue().getLeft());
                    set.addAll(entry.getValue().getRight());
                    return set;
                }
        ));
        Set<Long> suspicious_machines = getSuspiciousMachines(nbd);
        flattened_nbd_structure.keySet().retainAll(suspicious_machines);
        return fileSelectionPolicy(flattened_nbd_structure);
    }

    @Override
    public Pair<Set<Long>, Set<String>> getSuspiciousEntities(NBD_State nbd) {
        if (!nbd.isMalicious())
            return new ImmutablePair<>(new HashSet<>(), new HashSet<>());

        Set<Long> suspicious_machines = getSuspiciousMachines(nbd);
        // <machine_id, <benign files, malicious files>>
        Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>> nbd_structure = nbd.getNBD_Structure(0);
        Map<Long, Set<FileInfo>> flattened_nbd_structure = nbd_structure.entrySet().stream().collect(Collectors.toMap(
                Map.Entry::getKey, entry -> {
                    Set<FileInfo> set = new HashSet<>();
                    set.addAll(entry.getValue().getLeft());
                    set.addAll(entry.getValue().getRight());
                    return set;
                }
        ));
        flattened_nbd_structure.keySet().retainAll(suspicious_machines);
        Set<String> suspicious_files = fileSelectionPolicy(flattened_nbd_structure);
        return new ImmutablePair<>(suspicious_machines, suspicious_files);
    }

    // return sha2_ids
    private Set<String> fileSelectionPolicy(Map<Long, Set<FileInfo>> map) {
        return map.values().stream().flatMap(Set::stream)
                .map(file_info -> file_info.sha2_id).collect(Collectors.toSet());
    }

    @Override
    public String getArgs() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        return parameters.greedy_machine_selection_min_derivative_threshold + "_" +
                parameters.greedy_search_nbd_portion;
    }

    @Override
    public String toString() {
        return this.getClass().getName();
    }
}
