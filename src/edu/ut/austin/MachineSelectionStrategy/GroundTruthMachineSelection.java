package edu.ut.austin.MachineSelectionStrategy;

import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.Introspection.NBD_State;
import edu.ut.austin.RealTimeSimulation.ShapeGD;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.ivy.core.module.descriptor.Configuration;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 4/13/2017.
 */
public class GroundTruthMachineSelection implements MachineSelectionStrategy {

    @Override
    public Set<Long> getSuspiciousMachines(NBD_State nbd) {
        if (!nbd.isMalicious())
            return new HashSet<>();

        Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>> nbd_structure = nbd.getNBD_Structure(0);
        return new HashSet<>(nbd_structure.keySet());
    }

    @Override
    public Set<String> getSuspiciousFiles(NBD_State nbd) {
        if (!nbd.isMalicious())
            return new HashSet<>();

        // <machine_id, <benign files, malicious files>>
        Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>> nbd_structure = nbd.getNBD_Structure(0);
        return nbd_structure.values().stream().flatMap(pair -> Stream.concat(pair.getLeft().stream(), pair.getRight().stream()))
                .map(file_info -> file_info.sha2_id).collect(Collectors.toSet());
    }

    @Override
    public Pair<Set<Long>, Set<String>> getSuspiciousEntities(NBD_State nbd) {
        if (!nbd.isMalicious())
            return new ImmutablePair<>(new HashSet<>(), new HashSet<>());

        Set<Long> suspicious_machines = getSuspiciousMachines(nbd);
        Set<String> suspicious_files = getSuspiciousFiles(nbd);
        return new ImmutablePair<>(suspicious_machines, suspicious_files);
    }

    @Override
    public String getArgs() {
        return "null";
    }

    @Override
    public String toString() {
        return this.getClass().getName();
    }
}
