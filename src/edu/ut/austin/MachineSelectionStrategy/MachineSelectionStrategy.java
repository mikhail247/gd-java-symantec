package edu.ut.austin.MachineSelectionStrategy;

import edu.ut.austin.GraphClasses.Node;
import edu.ut.austin.Introspection.NBD_State;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Set;

/**
 * Created by Mikhail on 4/13/2017.
 */
public interface MachineSelectionStrategy {
    public Set<Long> getSuspiciousMachines(NBD_State nbd);
    public Set<String> getSuspiciousFiles(NBD_State nbd);
    public Pair<Set<Long>, Set<String>> getSuspiciousEntities(NBD_State nbd);
    public String getArgs();
}
