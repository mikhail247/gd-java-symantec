package edu.ut.austin.MachineSelectionStrategy;

import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.Introspection.NBD_State;
import edu.ut.austin.RealTimeSimulation.ShapeGD;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 4/13/2017.
 */
public class GreedyMachineSelection_FileCentric implements MachineSelectionStrategy {
    private ShapeGD shape_detector;
    private Map<String, List<int[]>> data;
    private Map<String, String> sha256_to_sha2_id;

    public GreedyMachineSelection_FileCentric(ShapeGD shape_detector, Map<String, FileInfo> file_info) {
        this.shape_detector = shape_detector;
        this.data = shape_detector.getData();
        Map<String, List<FileInfo>> temp = file_info.values()
                .parallelStream().collect(Collectors.groupingBy(file -> file.redirect.sha2, Collectors.toList()));
        this.sha256_to_sha2_id = temp.entrySet().parallelStream()
                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue().get(0).redirect.sha2_id));
    }

    @Override
    // returns sha2_ids
    public Set<String> getSuspiciousFiles(NBD_State nbd) {
        if (!nbd.isMalicious())
            return new HashSet<>();

        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        Map<FileInfo, Long> file_hashes = nbd.getNBD_Structure(0).values().stream()
                .flatMap(pair -> Stream.concat(pair.getLeft().stream(), pair.getRight().stream()))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        // <chosen machine, current ShapeScore>
        List<Pair<FileInfo, Double>> suspicious_files = new LinkedList<>();
        if (nbd.score == null) // the nbd has not been analyzed
            return new HashSet<>();
        double score = nbd.score;

        int threshold = (int) (parameters.greedy_search_nbd_portion*file_hashes.size());
        while(file_hashes.size() > threshold) {
            ConcurrentSkipListMap<Double, FileInfo> file_scores = new ConcurrentSkipListMap<>();
            file_hashes.keySet().parallelStream().forEach(file_id -> {
                Map<FileInfo, Long> reduced_hash_map = new HashMap<>(file_hashes);
                reduced_hash_map.remove(file_id);

                Map<String, Long> reduced_hash_map_sha256 = reduced_hash_map.entrySet().stream()
                        .collect(Collectors.groupingBy(entry -> entry.getKey().redirect.sha2,
                                Collectors.mapping(entry -> entry.getValue(), Collectors.summingLong(Long::valueOf))
                        ));

                List<int[]> fvs = shape_detector.getBucketVectorsFromSpark(data, reduced_hash_map_sha256);
                double hist[][] = shape_detector.buildNormalizedHistogramFromBucketIndices(fvs);
                double score_left = shape_detector.computeWassersteinDistance(hist);
                file_scores.put(score_left, file_id);
            });
            FileInfo file_id = file_scores.firstEntry().getValue();
            file_hashes.remove(file_id);
            suspicious_files.add(new ImmutablePair<>(file_id, score));
            score = file_scores.lastKey();
        }
        Set<FileInfo> ret = fileSelectionPolicy(suspicious_files);
        return ret.stream().map(file_info -> file_info.sha2_id).collect(Collectors.toSet());
    }

    // returns <sha256>
    private Set<FileInfo> fileSelectionPolicy(List<Pair<FileInfo, Double>> suspicious_files) {
        Set<FileInfo> ret = new HashSet<>();
        Double prev_score = Double.POSITIVE_INFINITY;
        List<Pair<FileInfo, Double>> monotone_part = new LinkedList<>();
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        // assumption is that score may start rising up due to small number of FVs
        // thus we need to use only descending part of the curve
        for (Pair<FileInfo, Double> pair : suspicious_files) {
            if (prev_score < 0.85*pair.getRight())
                break;
            monotone_part.add(pair);
            prev_score = pair.getRight();
        }

        if (monotone_part.size() < 3)
            return ret;

        Pair<FileInfo, Double> pair_left = monotone_part.get(0);
        Pair<FileInfo, Double> pair_center =  monotone_part.get(1);
        ret.add(pair_left.getLeft()); ret.add(pair_center.getLeft());

        for (int i = 2; i < monotone_part.size(); i ++) {
            Pair<FileInfo, Double> pair_right = monotone_part.get(i);
            double first_derivative = (pair_right.getRight() - pair_left.getRight())/2;
            double second_derivative = pair_left.getRight() -2*pair_center.getRight() + pair_right.getRight();
            if (Math.abs(first_derivative) < parameters.greedy_machine_selection_min_derivative_threshold)
                break;
            ret.add(pair_right.getLeft());
            pair_left = pair_center;
            pair_center = pair_right;
        }
        return ret;
    }

    @Override
    public Set<Long> getSuspiciousMachines(NBD_State nbd) {
        if (!nbd.isMalicious())
            return new HashSet<>();

        // <machine_id, <benign files, malicious files>>
        Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>> nbd_structure = nbd.getNBD_Structure(0);
        Map<String, Set<Long>> sha2_id_to_machine_ids = new HashMap<>();

        for (Map.Entry<Long, Pair<Set<FileInfo>, Set<FileInfo>>> entry : nbd_structure.entrySet()) {
            Set<String> sha2_id_set = Stream.concat(entry.getValue().getLeft().stream(), entry.getValue().getRight().stream())
                    .map(file_info -> file_info.sha2_id).collect(Collectors.toSet());

            for (String sha2_id : sha2_id_set) {
                Set<Long> machine_id_set = sha2_id_to_machine_ids.computeIfAbsent(sha2_id, k -> new HashSet<>());
                machine_id_set.add(entry.getKey());
            }
        }
        Set<String> suspicious_files = getSuspiciousFiles(nbd);

        return suspicious_files.stream().flatMap(file_id ->
                sha2_id_to_machine_ids.get(file_id).stream()).collect(Collectors.toSet());
    }

    @Override
    public Pair<Set<Long>, Set<String>> getSuspiciousEntities(NBD_State nbd) {
        if (!nbd.isMalicious())
            return new ImmutablePair<>(new HashSet<>(), new HashSet<>());

        Set<String> suspicious_files = getSuspiciousFiles(nbd);
        // <machine_id, <benign files, malicious files>>
        Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>> nbd_structure = nbd.getNBD_Structure(0);
        Map<String, Set<Long>> sha2_id_to_machine_ids = new HashMap<>();

        for (Map.Entry<Long, Pair<Set<FileInfo>, Set<FileInfo>>> entry : nbd_structure.entrySet()) {
            Set<String> sha2_id_set = Stream.concat(entry.getValue().getLeft().stream(), entry.getValue().getRight().stream())
                    .map(file_info -> file_info.sha2_id).collect(Collectors.toSet());

            for (String sha2_id : sha2_id_set) {
                Set<Long> machine_id_set = sha2_id_to_machine_ids.computeIfAbsent(sha2_id, k -> new HashSet<>());
                machine_id_set.add(entry.getKey());
            }
        }

        Set<Long> suspicious_machines = suspicious_files.stream().flatMap(file_id ->
                sha2_id_to_machine_ids.get(file_id).stream()).collect(Collectors.toSet());
        return new ImmutablePair<>(suspicious_machines, suspicious_files);
    }

    @Override
    public String getArgs() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        return parameters.greedy_machine_selection_min_derivative_threshold + "_" +
                parameters.greedy_search_nbd_portion;
    }

    @Override
    public String toString() {
        return this.getClass().getName();
    }
}
