package edu.ut.austin.FeatureExtractor;

import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.GraphClasses.Edge;
import edu.ut.austin.GraphClasses.Node;
import org.apache.commons.lang3.StringUtils;
import org.jgrapht.alg.shortestpath.FloydWarshallShortestPaths;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class FeatureExtractor {
    private volatile Map<String, Double> file_affinity_cache;    // <domain_or_url, # of unique downloaded files>
    private volatile Map<String, Double> dropper_affinity_cache; // <domain_or_url, # of unique initiators>
    private volatile Map<String, Double> file_prevalence_map; // <node_id, # of hosts>
    private final Collection<DefaultDirectedGraphWrapper> data;
    private static double weights[] = null;

    /* If prefix_graph is not provided, then global features can't be computed
    * */
    public FeatureExtractor(Collection<DefaultDirectedGraphWrapper> data) {
        this.data = data;
        this.file_affinity_cache = null;
        this.dropper_affinity_cache = null;
        this.file_prevalence_map = null;
    }

    /* Number of unique downloads from a given URL (UDFL): For
     * every URL (domain_or_url), we count the number of unique executables
     * downloaded from the domain_or_url, aggregated across all machines.
     *
     * Compute the number of downloads per domain_or_url  per host
     * <machine_id, graph>
    * */
    private synchronized void buildFileAffinityCache() {
        Map<String, List<Double>> domains_map;

        if (file_affinity_cache != null) return;
        domains_map = new HashMap<>();
        System.out.println("Building File Affinity Cache");

        for (DefaultDirectedGraphWrapper graph : data) {
           Map<String, Integer> map = graph.edgeSet().stream()
                            .collect(Collectors.groupingBy(edge -> edge.domain,
                            Collectors.collectingAndThen(Collectors.mapping(edge -> edge.target.sha2_id, Collectors.toSet()), Set::size)));

            for (Map.Entry<String, Integer> entry_t : map.entrySet()) {
                domains_map.putIfAbsent(entry_t.getKey(), new LinkedList<>());
                domains_map.get(entry_t.getKey()).add((double) entry_t.getValue() / graph.edgeSet().size());
            }
        }
        file_affinity_cache = domains_map.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
                entry -> entry.getValue().stream().mapToDouble(Double::valueOf).average().getAsDouble()));
        System.out.println("File Affinity Cache Built");
    }

    /* Number of unique downloaders accessing given URL (UDPL):
    *  For every URL (domain_or_url), we count the number of unique downloaders
    *  that used the domain_or_url to download new executables,
    *  aggregated across all machines.
    * */
    private synchronized void buildDropperAffinityCache() {
        Map<String, List<Double>> domains_map;

        if (dropper_affinity_cache != null) return;
        domains_map = new HashMap<>();
        System.out.println("Building Dropper Affinity Cache");

        for (DefaultDirectedGraphWrapper graph : data) {
            Map<String, Integer> map = graph.edgeSet().stream()
                    .collect(Collectors.groupingBy(edge -> edge.domain,
                            Collectors.collectingAndThen(Collectors.mapping(edge -> edge.source.sha2_id, Collectors.toSet()), Set::size)));

            for (Map.Entry<String, Integer> entry_t : map.entrySet()) {
                domains_map.putIfAbsent(entry_t.getKey(), new LinkedList<>());
                domains_map.get(entry_t.getKey()).add((double) entry_t.getValue() / graph.edgeSet().size());
            }
        }
        dropper_affinity_cache = domains_map.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
                entry -> entry.getValue().stream().mapToDouble(Double::valueOf).average().getAsDouble()));
        System.out.println("Dropper Affinity Cache Built");
    }

    // normalized over the number of nodes across all graphs
    private synchronized void fillOutFilePrevalenceMap() {
        if (file_prevalence_map != null) return;
        System.out.println("Building File Prevalence Map");
        file_prevalence_map = new HashMap<>();

        for (DefaultDirectedGraphWrapper graph : data) {
            graph.vertexSet().stream().map(node -> node.sha2_id).forEach(sha2_id -> {
                file_prevalence_map.putIfAbsent(sha2_id, 0.0);
                double count = file_prevalence_map.get(sha2_id);
                file_prevalence_map.put(sha2_id, count + 1);
            });
        }

        int node_count = data.stream().mapToInt(graph -> graph.vertexSet().size()).sum();
        for (Map.Entry<String, Double> entry : file_prevalence_map.entrySet())
            file_prevalence_map.put(entry.getKey(), entry.getValue() / node_count);
    }

    public double[] getFeatureWeights() {
        return weights;
    }

    public double[] extractFeatures(DefaultDirectedGraphWrapper graph) {
        return extractRobustToEvasiveMalwareFeatures(graph);
    }

    private double[] extractRobustToEvasiveMalwareFeatures(DefaultDirectedGraphWrapper graph) {
        double features[] = {
                domainNameSimilarity_F6(graph),
                alexaDomains1M_F7(graph),
                averageScore_F8(graph),
                stdScore_F9(graph),
                averageFilePrevalence_F14(graph),
                averageDistinctFileAffinity_F15(graph),
                averageDistinctDropperAffinity_F16(graph)
        };
        if (weights == null)
            weights = IntStream.range(0, features.length).mapToDouble(i -> 1.0).toArray();
        return features;
    }

    private double[] extractMostSignificantFeatures(DefaultDirectedGraphWrapper graph) {
        double features[] = {
                averageFilePrevalence_F14(graph),
                averageDistinctFileAffinity_F15(graph),
                averageDistinctDropperAffinity_F16(graph),
                lifeSpan_F10(graph),
                alexaDomains1M_F7(graph),
                growthRate_F11(graph),
                intraChildrenTimeSpread_F13(graph),
                graphDiameter_F1(graph),
                childrenSpread_F12(graph),
        };
        if (weights == null)
            weights = new double[]{1.0, 1.0, 0.75, 0.625, 0.625, 0.56, 0.5, 0.375, 0.375};
        return features;
    }

    private double[] extractGlobalFeatures(DefaultDirectedGraphWrapper graph) {
        double features[] = {
                graphDiameter_F1(graph),
                graphClusteringCoefficient_F2(graph),
                graphDensity_F3(graph),
                totalNumberOfDownloads_F4(graph),
                numberOfUniqueDomains_F5(graph),
                domainNameSimilarity_F6(graph),
                alexaDomains1M_F7(graph),
                averageScore_F8(graph),
                stdScore_F9(graph),
                lifeSpan_F10(graph),
                growthRate_F11(graph),
                childrenSpread_F12(graph),
                intraChildrenTimeSpread_F13(graph),
                averageFilePrevalence_F14(graph),
                averageDistinctFileAffinity_F15(graph),
                averageDistinctDropperAffinity_F16(graph)
        };
        if (weights == null)
            weights = IntStream.range(0, features.length).mapToDouble(i -> 1.0).toArray();
        return features;
    }

    private double[] extractNonGlobalFeatures(DefaultDirectedGraphWrapper graph) {
        double features[] = {
                graphDiameter_F1(graph),
                graphClusteringCoefficient_F2(graph),
                graphDensity_F3(graph),
                totalNumberOfDownloads_F4(graph),
                numberOfUniqueDomains_F5(graph),
                domainNameSimilarity_F6(graph),
                alexaDomains1M_F7(graph),
                averageScore_F8(graph),
                stdScore_F9(graph),
                growthRate_F11(graph),
                childrenSpread_F12(graph),
                intraChildrenTimeSpread_F13(graph),
        };
        if (weights == null)
            weights = IntStream.range(0, features.length).mapToDouble(i -> 1.0).toArray();
        return features;
    }

    private int graphDiameter_F1 (DefaultDirectedGraphWrapper graph) {
        DefaultDirectedGraphWrapper graph_t = (DefaultDirectedGraphWrapper) graph.clone();
        List<Edge> self_loops = graph_t.edgeSet().stream().filter(edge ->
                    edge.source == edge.target).collect(Collectors.toList());
        graph_t.removeAllEdges(self_loops);

        List<Node> tree_roots = graph_t.vertexSet().stream().filter(node ->
                    graph_t.inDegreeOf(node) == 0).collect(Collectors.toList());
        // processing cyclic graphs
        if (tree_roots.isEmpty())
            return graph_t.vertexSet().size() - 1;
        OptionalInt opt = tree_roots.stream().mapToInt(root -> graphTreeHeight(root, graph_t, new HashSet<>())).max();
        return opt.getAsInt();
    }

    private int graphTreeHeight (Node node, DefaultDirectedGraphWrapper graph, Set<Node> visited_nodes) {
        // a tiny portion of graphs may contain cycles, thus we add this stub
        if (!visited_nodes.add(node))
            return 0;
        if (graph.outDegreeOf(node) == 0)
            return 0;
        return 1 + graph.outgoingEdgesOf(node).stream().map(edge -> edge.target)
                .mapToInt(succ -> graphTreeHeight(succ, graph, visited_nodes)).max().getAsInt();
    }

    /* Computes diameter using Floyd-Warshall algorithm -> very slow
    * Diameter of disconnected graph (set of nodes) is 1 because we may think that
    * some downloads are initiated by a browser, which is missing in the data set
    * */
    @Deprecated
    private double graphDiameterFW_F1 (DefaultDirectedGraphWrapper graph) {
        FloydWarshallShortestPaths<Node,Edge> floyd_warshall = new FloydWarshallShortestPaths(graph);
        Double diameter = floyd_warshall.getDiameter();
        if (diameter == Double.POSITIVE_INFINITY) {
            /* we're here because the graph has some disconected nodes, thus
            * JGrapT returns infinite diameter, and we're going to retrieve
            * the distance matrix and analyze it
            */
            double distance_matrix[][] = null;
            try {
                Field distance_matrix_field = FloydWarshallShortestPaths.class.getDeclaredField("d");
                distance_matrix_field.setAccessible(true);
                distance_matrix = (double [][]) distance_matrix_field.get(floyd_warshall);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
                System.exit(-1);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                System.exit(-1);
            }
            diameter = Arrays.stream(distance_matrix).flatMapToDouble(d -> Arrays.stream(d))
                    .filter(d -> d != Double.POSITIVE_INFINITY).max().orElse(0);
            return diameter;
        }
        return diameter;
    }

    // Average of local clustering coefficients
    private double graphClusteringCoefficient_F2(DefaultDirectedGraphWrapper graph) {
        double coefficient = 0;

        for (Node node : graph.vertexSet()) {
            int degree = graph.inDegreeOf(node) + graph.outDegreeOf(node);
            int max_edges_count = degree * (degree -1);
            int edge_count = 0;

            Set<Edge> edges = graph.edgesOf(node);
            Set<Node> neighbors = edges.stream()
                    .flatMap(edge -> Arrays.asList(edge.source, edge.target).stream()).collect(Collectors.toSet());
            neighbors.remove(node);
            for (Node node_1 : neighbors) {
                for (Node node_2 : neighbors)
                    if (!node_1.equals(node_2) && graph.containsEdge(node_1, node_2))
                        edge_count ++;
            }
            if (max_edges_count > 0)
                coefficient += edge_count / max_edges_count;
        }
        return coefficient / graph.vertexSet().size();
    }

    private double graphDensity_F3 (DefaultDirectedGraphWrapper graph) {
        double density = 0;
        int edge_count = graph.edgeSet().size();
        int node_count = graph.vertexSet().size();

        if (node_count > 1)
            density = edge_count / (node_count * (node_count-1));
        return density;
    }

    // why not normalized ??? the same as the number of nodes ???
    // The # of downloads = the # of nodes in the graph
    private int totalNumberOfDownloads_F4 (DefaultDirectedGraphWrapper graph) {
        return graph.vertexSet().size();
    }

    private long numberOfUniqueDomains_F5 (DefaultDirectedGraphWrapper graph) {
        return graph.edgeSet().stream().map(edge -> edge.domain).distinct().count();
    }

    // averaging all the pairwise distances
    // distinct or all domains ???
    private double domainNameSimilarity_F6 (DefaultDirectedGraphWrapper graph) {
        double distance, similarity = 0;
        ArrayList<String> domains = graph.edgeSet().stream()
                .map(edge -> edge.domain).distinct().collect(Collectors.toCollection(ArrayList::new));

        for (int i = 0; i < domains.size(); i ++) {
            String domain_1 = domains.get(i);
            for (int j = i+1; j < domains.size(); j ++) {
                String domain_2 = domains.get(j);
                distance = StringUtils.getLevenshteinDistance(domain_1, domain_2);
                similarity += 1 - distance / Math.min(domain_1.length(), domain_2.length());
            }
        }
        similarity = domains.isEmpty() ? 0 : similarity / domains.size();
        return similarity;
    }

    // Considering only distinct domains within an IG
    private double alexaDomains1M_F7 (DefaultDirectedGraphWrapper graph) {
        int edge_count = graph.edgeSet().size();
        int alexa_edge_count = (int) graph.edgeSet().stream()
                .filter(edge -> edge.alexa_domain).map(edge -> edge.domain).distinct().count();
        return edge_count != 0 ? (double) alexa_edge_count/edge_count : 0;
    }

    private double averageScore_F8 (DefaultDirectedGraphWrapper graph) {
        return graph.vertexSet().stream().mapToInt(node -> node.score).average().getAsDouble();
    }

    private double stdScore_F9 (DefaultDirectedGraphWrapper graph) {
        Supplier<IntStream> stream_supplier = () -> graph.vertexSet().stream().mapToInt(node -> node.score);
        double average = stream_supplier.get().average().getAsDouble();
        double square_average = stream_supplier.get().map(i -> i*i).average().getAsDouble();
        return Math.pow(square_average - average*average, 0.5);
    }

    // Handles disconnected graphs as well
    // Seems to inapplicable to online detection system
    private long lifeSpan_F10 (DefaultDirectedGraphWrapper graph) {
        Supplier<LongStream> stream_supplier = () -> graph.vertexSet().stream().mapToLong(node -> node.time_stamp);
        long oldest_time_stamp = stream_supplier.get().min().getAsLong();
        long newest_time_stamp = stream_supplier.get().max().getAsLong();
        return oldest_time_stamp - newest_time_stamp;
    }

    private double growthRate_F11 (DefaultDirectedGraphWrapper graph) {
        return (double) lifeSpan_F10(graph)/graph.vertexSet().size();
    }

    // average of parent -- child time stamp differences
    private double childrenSpread_F12 (DefaultDirectedGraphWrapper graph) {
        double average_time = graph.edgeSet().stream().mapToLong(edge -> edge.target.time_stamp
                    - edge.source.time_stamp).average().orElse(-1);
        return average_time;
    }

    /* For each node, compute intra-children time spread and average it across the children,
     * then sum up averages and average across the nodes in a graph.
    * */
    private double intraChildrenTimeSpread_F13 (DefaultDirectedGraphWrapper graph) {
        double time_spread = 0;

        for (Node node : graph.vertexSet()) {
            ArrayList<Node> targets = graph.outgoingEdgesOf(node).stream()
                    .map(edge -> edge.target).collect(Collectors.toCollection(ArrayList::new));

            int target_count = targets.size();
            double node_level_time_spread = 0;
            for (int i = 0; i < targets.size(); i ++) {
                Node node_1 = targets.get(i);
                for (int j = i+1; j < targets.size(); j ++) {
                    Node node_2 = targets.get(j);
                    node_level_time_spread += Math.abs(node_1.time_stamp - node_2.time_stamp);
                }
            }
            if (target_count > 1)
                node_level_time_spread /= (target_count * (target_count - 1));
            time_spread += node_level_time_spread;
        }
        return time_spread / graph.vertexSet().size();
    }

    /* Normalized file prevalence score, i.e. the percentage of machines that have downloaded a file,
    * because the size of the tree is growing, and the number of hosts grows as well over time.
    *
    * If we return -1, Apache Spark will not be able to train RandomForest because of the presence
    * irrelevant features -- it's likely due to a bug in Spark
    * */
    private double averageFilePrevalence_F14 (DefaultDirectedGraphWrapper graph) {
        fillOutFilePrevalenceMap();
        return graph.vertexSet().stream().mapToDouble(node ->
                file_prevalence_map.get(node.sha2_id)).sum();
    }

    private double averageDistinctFileAffinity_F15 (DefaultDirectedGraphWrapper graph) {
        // lazy data initialization
        buildFileAffinityCache();
        return graph.edgeSet().stream().map(edge -> edge.domain).distinct()
                .mapToDouble(domain -> file_affinity_cache.get(domain)).sum();
    }

    private double averageDistinctDropperAffinity_F16 (DefaultDirectedGraphWrapper graph) {
        // lazy data initialization
        buildDropperAffinityCache();
        return graph.edgeSet().stream().map(edge -> edge.domain)
                .distinct().mapToDouble(domain -> dropper_affinity_cache.get(domain)).sum();
    }
}