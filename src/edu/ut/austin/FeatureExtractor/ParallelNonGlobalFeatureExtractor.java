package edu.ut.austin.FeatureExtractor;

import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by Mikhail on 2/8/2017.
 */
public class ParallelNonGlobalFeatureExtractor implements Callable<Void>{
    private List<DefaultDirectedGraphWrapper> graphs;
    private FeatureExtractor feature_extractor;

    public ParallelNonGlobalFeatureExtractor(List<DefaultDirectedGraphWrapper> graphs, FeatureExtractor feature_extractor) {
        this.graphs = graphs;
        this.feature_extractor = feature_extractor;
    }

    @Override
    public Void call() throws Exception {
        for (DefaultDirectedGraphWrapper graph : graphs)
            graph.getFeatureVector(feature_extractor);
        return null;
    }
}
