package edu.ut.austin.Misc;

import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.DataInputOutput.GraphDataLoader;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.ML_Models.TrainML_Models;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.ml.feature.NGram;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CombineParquetFiles {
    public static void main(String args[]) {
        augmentDataset();
        System.gc();
        partitionDataset();
    }
    private static Dataset<Row> augmentDataset() {
        String input_folder = "results/features/combine";
        String output_folder = "results/features/combine";
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        Dataset<Row> dataset = Arrays.stream(new File(input_folder).listFiles()).sorted()
                .filter(file -> file.isDirectory())
                .map(file -> {
                    System.out.println(file.getName());
                    return global_parameters.spark.read().parquet(file.getAbsolutePath());
                })
                .reduce((ds_1, ds_2) -> ds_1.union(ds_2)).get();

        System.out.println("Total number of rows " + dataset.count());
        System.out.println("Malicious threshold " + 100*parameters.malicious_threshold + "%");

        GraphDataLoader loader = new GraphDataLoader();
        Map<String, FileInfo> file_info = loader.getFileInfoMapWithoutMapping();

        int file_info_size = (int) file_info.values().parallelStream().filter(file -> file.is_benign != null).count();
        System.out.println(file_info_size);
        Map<String, Integer> vt_data_map = new ConcurrentHashMap<>(file_info_size);
        file_info.values().parallelStream().filter(Objects::nonNull).filter(file -> file.sha2 != null)
            .forEach(file -> {
                int label = file.is_benign == null || file.is_benign ? 0 : 1;
                vt_data_map.put(file.sha2, label);
            });

        StructType schema = dataset.schema();
        List<Row> local_dataset = dataset.collectAsList().parallelStream()
            .flatMap(row -> {
                List<Row> ret = new LinkedList();
                String sha256 = row.<String>getAs("sha256");
                if (vt_data_map.get(sha256) == null)
                    return ret.stream();

                Row row_new = RowFactory.create(vt_data_map.get(sha256), row.<String>getAs("file_name"),
                        sha256, row.<String>getAs("md5"), row.<String>getAs("features"));
                ret.add(row_new);
                return ret.stream();
            }).collect(Collectors.toList());

        Dataset<Row> augmented_dataset = global_parameters.spark.createDataFrame(local_dataset, schema);
        augmented_dataset.write().mode("overwrite").parquet("results/features/features_vt_binaries_temp_" + parameters.n_gram_size +  ".parquet");
        System.out.println("# of malicious samples ");
        System.out.println(augmented_dataset.filter("label < 0.5").count());
        System.out.println("# of benign samples ");
        System.out.println(augmented_dataset.filter("label > 0.5").count());
        augmented_dataset.select("label").distinct().show();
        System.out.println("augmented_dataset: count: " + augmented_dataset.count());
        augmented_dataset.show(100);
        System.out.println("Successfully wrote the file features_vt_binaries_temp" + parameters.n_gram_size +  ".parquet");
        return augmented_dataset;
    }

    private static void partitionDataset() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        Dataset<Row> augmented_dataset = global_parameters.spark.read().parquet("results/features/features_vt_binaries_temp_" + parameters.n_gram_size +  ".parquet");

        Dataset<Row> augmented_dataset_training = global_parameters.spark.createDataFrame(
                augmented_dataset.filter("label < 0.5").toJavaRDD().takeSample(false, parameters.shape_gd_training_method_file_count), augmented_dataset.schema());
        augmented_dataset_training = augmented_dataset_training.union(global_parameters.spark.createDataFrame(
                augmented_dataset.filter("label > 0.5").toJavaRDD().takeSample(false, parameters.shape_gd_training_method_file_count), augmented_dataset.schema()));
        Set<String> keys_training = augmented_dataset_training.select("sha256")
                .collectAsList().stream().map(row -> row.<String>getAs("sha256")).collect(Collectors.toSet());

        Dataset<Row> augmented_dataset_testing = augmented_dataset.filter(new FilterFunction<Row>() {
            @Override
            public boolean call(Row row) throws Exception {
                String sha256 = row.<String>getAs("sha256");
                return !keys_training.contains(sha256);
            }
        });

        augmented_dataset_training.write().mode("overwrite").parquet( "results/features/" + "features_vt_binaries_training_" + parameters.n_gram_size +  ".parquet");
        augmented_dataset_testing.write().mode("overwrite").parquet("results/features/features_vt_binaries_" + parameters.n_gram_size +  ".parquet");
        try {
            Path path = new Path("results/features/features_vt_binaries_temp_" + parameters.n_gram_size +  ".parquet");
            FileSystem file_system = org.apache.hadoop.fs.FileSystem.get(path.toUri(), global_parameters.java_spark_context.hadoopConfiguration());
            boolean removed = file_system.delete(path, true);
            GlobalParameters.getInstance().detection_logger.info("features_vt_binaries_temp_" + parameters.n_gram_size +  ".parquet removed: " + removed);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("augmented_dataset_testing: " + augmented_dataset_testing.count());
        System.out.println("augmented_dataset_training: " + augmented_dataset_training.count());
        System.out.println("Dataset has been successfully augmented");
    }
}
