package edu.ut.austin.Misc;

import edu.ut.austin.DataInputOutput.GraphDataLoader;
import edu.ut.austin.FeatureExtractor.FeatureExtractor;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Mikhail on 5/12/2017.
 */
public class TrainClustering {

    public void dumpClusteringTrainingData(GraphDataLoader loader, int graph_count) {
        int benign_graph_count = 0, malicious_graph_count = 0;
        Map<Long, DefaultDirectedGraphWrapper> graphs = new HashMap<>();

        while (graphs.size() < graph_count) {
            DefaultDirectedGraphWrapper graph = loader.pollGraphAtRandom(false, true);
            graphs.put(graph.machine_id, graph);
        }

        FeatureExtractor feature_extractor = new FeatureExtractor(graphs.values());
        Map<Integer, Queue<double[]>> fvs = partitionGraphs(graphs.values(), feature_extractor);
        write(fvs);
    }

    private Map<Integer, Queue<double[]>> partitionGraphs(Collection<DefaultDirectedGraphWrapper> graphs, FeatureExtractor feature_extractor) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        global_parameters.logger.info("Augmenting NBD Storage. # of graphs: " + graphs.size());
        Map<Integer, Queue<double[]>> fvs = new ConcurrentHashMap<>();
        ArrayList<String> family_name_dictionary = graphs.parallelStream()
                .flatMap(graph -> graph.vertexSet().stream()).map(node -> node.file_info.family_name)
                .map(family_name -> family_name == null ? "null" : family_name).distinct().collect(Collectors.toCollection(ArrayList::new));
        Map<String, Integer> map_family_name_to_cluster_id = IntStream.range(0, family_name_dictionary.size())
                .boxed().collect(Collectors.toMap(i -> family_name_dictionary.get(i), Function.identity()));

        graphs.stream().forEach(graph -> {
            Map<String, Long> map = graph.vertexSet().stream().map(node -> node.file_info.family_name)
                    .filter(Objects::nonNull).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            if (!map.isEmpty()) {
                long max_value = Collections.max(map.values());
                String family_name = null;

                for (Map.Entry<String, Long> entry : map.entrySet()) {
                    if (entry.getValue() == max_value) {
                        family_name = entry.getKey();
                        break;
                    }
                }
                int cluster_index = map_family_name_to_cluster_id.get(family_name);
                fvs.putIfAbsent(cluster_index, new ConcurrentLinkedQueue<>());
                fvs.get(cluster_index).add(graph.getFeatureVector(feature_extractor));
            }
        });
        return fvs;
    }

    private void write(Map<Integer, Queue<double[]>> fvs) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        try (
                Writer writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(parameters.clustering_training_data), "ascii"));
        ) {
            for (Map.Entry<Integer, Queue<double[]>> entry : fvs.entrySet()) {
                for (double fv[] : entry.getValue()) {
                    writer.write(entry.getKey().toString());
                    for (double d : fv)
                        writer.write("," + d);
                    writer.write("\n");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
