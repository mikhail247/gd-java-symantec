package edu.ut.austin.Misc;

import edu.ut.austin.DataInputOutput.GraphDataLoader;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.Introspection.IntrospectionModule;
import edu.ut.austin.ML_Models.TrainML_Models;
import edu.ut.austin.ML_Models.TrainShapeGD;
import edu.ut.austin.NBD.ClusterLSH;
import edu.ut.austin.RealTimeSimulation.ClusterSensitiveShapeGD;
import edu.ut.austin.RealTimeSimulation.ShapeGD_Generic;
import edu.ut.austin.RealTimeSimulation.ShapeGD_Spark;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.ivy.core.module.descriptor.Configuration;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

;

/**
 Ex. objdump -d matlab.exe | java -jar Shape_GD_With_Downloader_Graphs.jar
 Output format:
 file_name
 sha256
 md5
 asm instructions
 */
public class ObjDumpParser {
    private static String exec_extensions[] = {".exe", ".com"};


    public static void main(String args[]) {
        ConfigurationParameters.init(args, null);
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        if (TrainML_Models.loadLD_Model(parameters.LF_implementation_type) == null)
            TrainML_Models.trainLD_RF_Model();

        ShapeGD_Generic shape_gd = new ShapeGD_Generic(true);
        if (shape_gd.is_invalid)
            shape_gd.trainShapeGD(null);

        TrainShapeGD.processFVsWithLDs(shape_gd, 1024);
        TrainShapeGD.visualizeShapeGD(shape_gd, "testing", 1024);
    }

    @SuppressWarnings("all")
    private static void disassembleFiles(boolean is_incremental) {
        String dir_output_name_training;
        String dir_output_name = "results/disasm";
        new File(dir_output_name).mkdirs();

        try {
            System.out.println("[Training] Processing benignware");
            ArrayList<File> files_benignware = Arrays.stream(new File("data/binary_samples/benignware")
                    .listFiles()).filter(Objects::nonNull).parallel().filter(folder -> folder.isDirectory()).flatMap(folder ->
                    Arrays.stream(folder.getAbsoluteFile().listFiles())).collect(Collectors.toCollection(ArrayList::new));

            dir_output_name_training = "results/disasm/benignware_training";
            try {
                if (!is_incremental)
                    FileUtils.deleteDirectory(new File(dir_output_name_training));
            } catch (IOException e) {
                e.printStackTrace();
            }
            new File(dir_output_name_training).mkdirs();
            processFile(files_benignware, dir_output_name_training, is_incremental);
        } catch (NullPointerException e) {
            GlobalParameters.getInstance().logger.info("data/binary_samples/benignware not found");
        }

        try {
            System.out.println("[Training] Processing malware");
            ArrayList<File> files_malware = Arrays.stream(new File("data/binary_samples/malware")
                    .listFiles()).filter(Objects::nonNull).parallel().filter(folder -> folder.isDirectory()).flatMap(folder ->
                    Arrays.stream(folder.getAbsoluteFile().listFiles())).collect(Collectors.toCollection(ArrayList::new));
            dir_output_name_training = "results/disasm/malware_training";
            try {
                if (!is_incremental)
                    FileUtils.deleteDirectory(new File(dir_output_name_training));
            } catch (IOException e) {
                e.printStackTrace();
            }
            new File(dir_output_name_training).mkdirs();
            processFile(files_malware, dir_output_name_training, is_incremental);
        } catch (NullPointerException e) {
            GlobalParameters.getInstance().logger.info("data/binary_samples/malware not found");
        }

        try {
            System.out.println("[Testing] Processing malware");
            ArrayList<File> files_malware_testing =
                    Arrays.asList(new File("data/binary_samples/vt_binaries/partition_001"),
                            new File("data/binary_samples/vt_binaries/partition_002"), new File("data/binary_samples/vt_binaries/partition_003"))
                            .stream().flatMap(folder -> Arrays.stream(folder.listFiles()))
                            .filter(Objects::nonNull).collect(Collectors.toCollection(ArrayList::new));

            dir_output_name_training = "results/disasm/vt_binaries";
            try {
                if (!is_incremental)
                    FileUtils.deleteDirectory(new File(dir_output_name_training));
            } catch (IOException e) {
                e.printStackTrace();
            }
            new File(dir_output_name_training).mkdirs();
            processFile(files_malware_testing, dir_output_name_training, is_incremental);
        } catch (NullPointerException e) {
            GlobalParameters.getInstance().logger.info("data/binary_samples/vt_binaries not found");
        }
    }

    private static void extractFeatures() {
        System.out.println("Extracting features");
        Arrays.stream(new File("results/disasm").listFiles()).sorted()
                .filter(file -> file.isDirectory()).forEach(folder -> {
            boolean dump_csv_file = folder.getName().contains("training");
            String output_feature_file = "features_" + folder.getName() + "_" + ConfigurationParameters.getInstance().n_gram_size;
            TrainML_Models.saveEncodedAsmData(folder.getAbsolutePath(),
                    output_feature_file, false, false, false);
        });
    }

    private static void removePCA_Folder() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        global_parameters.logger.info("Computing PCA");

        try {
            // removing PCA model
            Path path = new Path("results/PCA_Model");
            FileSystem file_system = org.apache.hadoop.fs.FileSystem.get(path.toUri(), global_parameters.java_spark_context.hadoopConfiguration());
            boolean removed = file_system.delete(path, true);
            GlobalParameters.getInstance().detection_logger.info("PCA model removed: " + removed);
            // removing ShapeGD
            new File(parameters.shape_gd_spark_file_name).delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void processFile(ArrayList<File> files, String dir_output_name, boolean is_incremental) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        final AtomicInteger file_count = new AtomicInteger(0);

        Set<String> processed_files = is_incremental ? Arrays.stream(new File(dir_output_name).listFiles())
                .parallel().map(ObjDumpParser::getFileNameOutOfAsmFile).collect(Collectors.toSet())
                : null;
        GlobalParameters.getInstance().logger.info("Disassembled " + processed_files.size() + " files");

        files.parallelStream().limit(parameters.disasm_files_count)
                .filter(file -> !processed_files.contains(file.getName()))
                .forEach(file -> processFiles(file, dir_output_name, file_count, processed_files));
    }

    private static String getFileNameOutOfAsmFile(File file) {
        try (
                BufferedReader br = new BufferedReader(new FileReader(file.getAbsolutePath()));
        ) {
            return br.readLine();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void processFiles(File file, String dir_output_name, AtomicInteger file_count, Set<String> processed_files) {
        String file_output_name;

        try { // just in case if something goes wrong
            // compute sha256
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(Files.readAllBytes(Paths.get(file.getAbsolutePath())));
            byte[] md5_digest = md.digest();
            String md5_str = DatatypeConverter.printHexBinary(md5_digest).toLowerCase();

            // compute md5
            md = MessageDigest.getInstance("SHA-256");
            md.update(Files.readAllBytes(Paths.get(file.getAbsolutePath())));
            byte[] sha256_digest = md.digest();
            String sha256_str = DatatypeConverter.printHexBinary(sha256_digest).toLowerCase();

            boolean is_executable = Arrays.stream(exec_extensions)
                    .map(ext -> file.getName().endsWith(ext)).reduce(false, (a, b) -> a | b);

            file_output_name = dir_output_name;
            file_output_name += File.separator + sha256_str + ".asm";
            System.out.println(file_count.getAndIncrement() + "  File " + file.getAbsolutePath());

            Process process = new ProcessBuilder("objdump", "-d", file.getAbsolutePath()).start();
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            parseObjDumpOutput(br, file_output_name, file.getName(), sha256_str, md5_str);
            process.waitFor();

            // add sha256 and md5 of the file
            File file_asm = new File(new File(file_output_name), "r");
            if (file_asm.exists()) {
                int header_length = file.getName().length() + sha256_str.length() + md5_str.length() + 200;
                if (file_asm.length() < header_length) {
                    Files.delete(Paths.get(file_output_name));
                    System.out.println("Deleted " + file_output_name);
                    file_count.decrementAndGet();
                }
            }
        } catch (Exception e)  {
            e.printStackTrace();
        }
    }

    private static boolean parseObjDumpOutput(BufferedReader br, String file_output_name, String file_name, String sha256, String md5) {
        boolean ret = true;
        String line;

        try (
                Writer writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(file_output_name), "utf-8"));
        ) {
            writer.write(file_name + '\n');
            writer.write(sha256 + '\n');
            writer.write(md5 + '\n');

            while ((line = br.readLine()) != null) {
                try {
                    if (line.contains("File format not recognized"))
                        ret = false;
                    if (line.toLowerCase().contains("(bad)"))
                        continue;
                    int pos = line.indexOf(':');
                    if (pos == -1)
                        continue;
                    String address = line.substring(0, pos);

                    try {
                        Long.parseLong(address.trim(), 16);
                    } catch (NumberFormatException e) {
                        continue;
                    }

                    pos = line.length() - 1;
                    if (line.charAt(pos) != ' ') {
                        // skip operands
                        while (line.charAt(--pos) != ' ') ;
                    }
                    while (pos > 0 && line.charAt(--pos) == ' ') ;

                    int ins_name_end = pos;
                    // skip white space after an instruction
                    while (pos > 0 && line.charAt(--pos) != '\t') ;

                    if (!line.substring(0, pos + 1).contains(":"))
                        continue;

                    String ins_name = line.substring(pos + 1, ins_name_end + 1);
                    if (ins_name.contains(" ") || line.isEmpty())
                        continue;
                    writer.write(ins_name + '\n');
                } catch (Exception e) { // catching exceptions related to parsing
                    continue;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            ret = false;
        }
        return ret;
    }
}
