package edu.ut.austin.Misc;

import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.ThresholdingOutputStream;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mikhail on 2/28/2017.
 */
public class VirusShareParser {
    public static void extractTorrentLinks() {
        if (!ConfigurationParameters.getInstance().load_torrent_files)
            return;

        List<String> links = loadTorrentLinks("data/virus_share/VirusShare.com.html");
        saveFile(links, "data/virus_share/torrents");
    }

    private static List<String> loadTorrentLinks(String file_name) {
        String line;
        List<String> ret = new LinkedList<>();
        Pattern pattern = Pattern.compile("https.+torrent.+\\\"");

        try (BufferedReader br = new BufferedReader(new FileReader(file_name))) {
            while ((line = br.readLine()) != null) {
                if (!line.contains("tracker.virusshare.com"))
                    continue;

                Matcher matcher = pattern.matcher(line);
                boolean match_found = matcher.find();
                if (!match_found)
                    continue;
                String match = matcher.group().replace("\"", "");
                ret.add(match);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    private static void saveFile(List<String> links, String folder) {
        for (String link : links) {
            String array[] = link.split("\\?");
            array = array[0].split("/");
            String file_name = array[array.length - 1];
            file_name = folder + "/" + file_name;

            try {
                ProcessBuilder builder = new ProcessBuilder(
                        "cmd.exe", "/c", "start chrome", link);
                builder.redirectErrorStream(true);
                Process p = builder.start();
                BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String line;
                while (true) {
                    line = r.readLine();
                    if (line == null) { break; }
                    System.out.println(line);
                }

                Thread.sleep(100);
//                Runtime.getRuntime().exec("cmd.exe start chrome" + link);
                //URL url = new URL(link);
                //FileUtils.copyURLToFile(url, new File(file_name));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
