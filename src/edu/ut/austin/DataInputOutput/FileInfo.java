package edu.ut.austin.DataInputOutput;

import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 4/10/2017.
 */
public class FileInfo implements Serializable {
    private int hash_code = -1;
    private ConfigurationParameters.VT_FileType is_executable;
    public Boolean is_benign;
    public String sha2, md5, sha2_id;
    public String prefix, family_name, suffix;
    public Long vt_time_stamp, ig_detection_time, earliest_wine_time;
    public int positives, av_count; // info from VIRUS_TOTAL
    public String file_type_str;
    // redirects to either itself or another FileInfo structure
    // that corresponds to a successfully processed file
    public FileInfo redirect;

    public FileInfo(Boolean is_benign) {
        this.is_executable = ConfigurationParameters.VT_FileType.UNKNOWN;
        this.is_benign = is_benign;
        this.sha2 = null;
        this.md5 = null;
        this.prefix = null;
        this.family_name = null;
        this.suffix = null;
        this.file_type_str = null;

        this.vt_time_stamp = null;
        this.ig_detection_time = null;
        this.earliest_wine_time = null;
        this.redirect = this;
    }

    public FileInfo(Boolean is_benign, String sha2, String md5) {
        this(is_benign);
        this.sha2 = sha2;
        this.md5 = md5;
    }

    public FileInfo(int positives, int av_count, String sha2, String md5, String sha2_id, String vt_time_stamp) {
        this(true);
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        this.is_benign = (av_count == 0) || (((double) positives / av_count) <= parameters.malicious_threshold);
        this.sha2 = sha2;
        this.md5 = md5;
        this.sha2_id = sha2_id;
        this.positives = positives;
        this.av_count = av_count;
        this.redirect = this; // maps either to itself to the FileInfo structure of an existing file

        if (vt_time_stamp != null && vt_time_stamp.equals("None")) {
            if (!is_benign)
                // conservatively approximate vt detection time
                this.vt_time_stamp = GraphDataLoader.start_date.getTime() / 1000;
            else
                this.vt_time_stamp = null;
        } else if (vt_time_stamp != null) {
            try {
                Date time_stamp_date = GraphDataLoader.format.parse(vt_time_stamp);
                this.vt_time_stamp = time_stamp_date.getTime() / 1000;
            } catch (ParseException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public FileInfo(int positives, int av_count, String sha2, String md5, String sha2_id,
                    String vt_time_stamp, String prefix, String family_name, String suffix, String file_type) {
        this(positives, av_count, sha2, md5, sha2_id, vt_time_stamp);
        if (!prefix.isEmpty())
            this.prefix = prefix;
        if (!family_name.isEmpty())
            this.family_name = family_name;
        if (!suffix.isEmpty())
            this.suffix = suffix;
        if (!file_type.isEmpty())
            this.file_type_str = file_type.toUpperCase();
        if (file_type.contains("UNKNOWN"))
            this.is_executable = ConfigurationParameters.VT_FileType.UNKNOWN;
        else if (Stream.of("EXE", "DLL", "ELF", "Installer").anyMatch(file_type::contains))
            this.is_executable = ConfigurationParameters.VT_FileType.EXECUTABLE;
        else
            this.is_executable = ConfigurationParameters.VT_FileType.NON_EXECUTABLE;
    }

    public boolean hasVT_Info() {
        return this == this.redirect;
    }

    public ConfigurationParameters.VT_FileType getExecutableProperty() {
        return is_executable;
    }

    @Override
    public int hashCode() {
        if (hash_code != -1)
            return hash_code;

        hash_code = 17;
        hash_code = 31 * hash_code + (is_benign != null ? is_benign.hashCode() : 0);
        hash_code = 31 * hash_code + (sha2 != null ? sha2.hashCode() : 0);
        hash_code = 31 * hash_code + (md5 != null ? md5.hashCode() : 0);
        hash_code = 31 * hash_code + (sha2_id != null ? sha2_id.hashCode() : 0);
        hash_code = 31 * hash_code + (vt_time_stamp != null ? vt_time_stamp.hashCode() : 0);
        hash_code = 31 * hash_code + (ig_detection_time != null ? ig_detection_time.hashCode() : 0);
        hash_code = 31 * hash_code + (earliest_wine_time != null ? earliest_wine_time.hashCode() : 0);
        hash_code = 31 * hash_code + positives;
        hash_code = 31 * hash_code + av_count;
        hash_code = 31 * hash_code + (file_type_str != null ? file_type_str.hashCode() : 0);
        return hash_code;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof FileInfo)) {
            return false;
        }

        FileInfo file_info = (FileInfo) obj;
        return Objects.equals(this.is_benign, file_info.is_benign) && Objects.equals(this.sha2, file_info.sha2)
                && Objects.equals(this.md5, file_info.md5) && Objects.equals(this.sha2_id, file_info.sha2_id)
                && Objects.equals(this.vt_time_stamp, file_info.vt_time_stamp) && Objects.equals(this.ig_detection_time, file_info.ig_detection_time)
                && Objects.equals(this.earliest_wine_time, file_info.earliest_wine_time)
                && Objects.equals(this.file_type_str, file_info.file_type_str)
                && this.positives == file_info.positives && this.av_count == file_info.av_count;
    }

    public String toString() {
        StringBuffer str = new StringBuffer();
        str.append("benign: ");
        str.append(is_benign == null ? "null" : (is_benign ? "b" : "m"));
        str.append(" type: " + file_type_str);
        return str.toString();
    }
}
