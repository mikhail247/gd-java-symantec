package edu.ut.austin.DataInputOutput;

import com.google.common.base.Functions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.MalformedJsonException;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.GraphClasses.Edge;
import edu.ut.austin.GraphClasses.Node;
import edu.ut.austin.GraphViz.GraphViz;
import edu.ut.austin.Introspection.IntrospectionModule;
import edu.ut.austin.Debug.Debug;
import edu.ut.austin.Utils.Utils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.*;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.jgrapht.DirectedGraph;
import org.jgrapht.Graphs;
import org.jgrapht.traverse.BreadthFirstIterator;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 1/27/2017.
 */
public class GraphDataLoader implements Serializable {

    private enum EARLY_DETECTION_TIME_TYPE {IG_DETECTION, BEST_POSSIBLE_DETECTION};
    private Gson gson;
    // machines used for training of graph level RF
    private Set<Long> excluded_machine_ids;
    // <machine_id, graph>
    private Map<Long, DefaultDirectedGraphWrapper> data;
    // <machine_id, <root, IG graph>>
    private Map<Long, Map<String, DefaultDirectedGraphWrapper>> ig_data;
    // <time_stamp, node>
    private ConcurrentSkipListMap<Long, Node> time_stamps;
    // <sha_id, is_benign> loaded from filerep.csv
    private Map<String, Boolean> is_file_benign_symantec;
    // <file_id, file_info>
    private ConcurrentHashMap<String, FileInfo> file_info;
    // for computing early detection
    // <file_id> contains only malicious downloaders
    private Set<String> file_info_ccs_eval, file_info_full_eval;
    // top 1M domains
    private Set<String> alexa_domains;
    // <sha2_id, score>
    private Map<String, Integer> downloader_score;
    // <machine, <root_id, CCS'15 is_benign_label>>  true = benign
    private Map<String, Map<String, Boolean>> ig_labels_ccs;
    //<machine, time_stamp>
    private Map<Long, Long> machine_detection_time;
    // <domain_or_url, frequency>
    private Map<String, Long> domain_frequency;
    // <domain_or_url, <suspicious score, cluster index>>
    private Map<String, Pair<Double, Integer>> domain_name_suspiciousness;
    // statistics over domain_or_url names
    // <domain_or_url, <malware percentage, # benign downloads, 3 of malicious downloads>>
    private Map<String, MutableTriple<Double, Integer, Integer>> benignware_domain_distribution, malware_domain_distribution;
    // statistics over URLs
    // <domain_or_url, <malware percentage, # benign downloads, # of malicious downloads>>
    private Map<String, MutableTriple<Double, Integer, Integer>> benignware_url_distribution, malware_url_distribution;

    // <machine id, geo_id, isp, country>
    private Map<Long, Triple<Integer, String, String>> geo_location;
    // load file_ids that are used for exoeriments
    private Set<String> files_set;
    // <domain, bucketed features>
    private Map<String, int[]> domain_bucketed_scores;

    private ArrayList<DefaultDirectedGraphWrapper> benign_graphs, malicious_graphs;

    private static final Calendar calender =  new GregorianCalendar(2008, 1, 1);
    public static final Date start_date = calender.getTime();
    public static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.ENGLISH);

    class NodeParser {
        public String name, fn, ts;
        public int type;
    }

    class EdgeParser {
        public String url;
        public int source, target, uncert;
    }

    class GraphParser {
        public ArrayList<NodeParser> nodes = new ArrayList<>();
        public ArrayList<EdgeParser> links = new ArrayList<>();
    }

    public GraphDataLoader() {
        files_set = null;
        excluded_machine_ids = new HashSet<>();
        data = new ConcurrentHashMap<>();
        ig_data = new ConcurrentHashMap<>();
        time_stamps = new ConcurrentSkipListMap<>();
        file_info = new ConcurrentHashMap<>();
        is_file_benign_symantec = new HashMap<>();
        file_info_ccs_eval = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
        file_info_full_eval = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
        alexa_domains = new HashSet<>();
        downloader_score = new HashMap<>();
        ig_labels_ccs = new HashMap<>();
        benign_graphs = new ArrayList<>();
        malicious_graphs = new ArrayList<>();
        machine_detection_time = new HashMap<>();
        domain_bucketed_scores = new HashMap<>();
        benignware_domain_distribution = new ConcurrentHashMap<>();
        domain_name_suspiciousness = new ConcurrentHashMap<>();
        benignware_url_distribution = new ConcurrentHashMap<>();

        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
    }

    public void loadData() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        loadAlexaDomainNames(parameters.alexa_domains_file_name);
        loadDomainNameScores(parameters.domain_names_suspiciousness_file_name);
        loadDomainNameBucketedScores();
        loadDownloaderScores(parameters.downloader_score_file_name);
        loadMachinesUsedForGraphRF_Training();
        loadMachineGeoLocation(parameters.geo_location_file);

        File file = new File(parameters.file_set_file_name);

        if (parameters.ground_truth == ConfigurationParameters.GroundTruthEnum.APPROXIMATE) {
            loadFileInfo(parameters.file_info_file_name);
            load_CCS_EvalSet(parameters.vt_detection_time);
            Stream.of(parameters.file_info_MalSha2_file_name).forEachOrdered(this::loadFileInfo_MalSha2);
            loadInternalSymantecData(parameters.load_file_rep);
        } else if (parameters.ground_truth == ConfigurationParameters.GroundTruthEnum.VIRUS_TOTAL) {
            loadVTGroundTruth(parameters.vt_info, true);
            Debug.computeVT_TypeDistribution(file_info);
            load_CCS_EvalSet(parameters.vt_detection_time);
        }
        Stream.of(parameters.ig_labels_file_info).forEachOrdered(this::loadLabeledIGs);
        System.out.println("# of benign files (VT): " + (double) file_info.values().stream().filter(e -> e.is_benign).count() / file_info.size());

        parameters.csv_file_name.parallelStream().forEach(file_name -> {
            System.out.println("Loading: " + file_name + "...");
            loadIGs(file_name);
            System.out.println("Loaded: " + file_name);
        });
        propagateAndCheckNodeTimestamps();
        initializeEdgeInformationInNodes();
        // remove info about the files that are not in the current data set partition
        Debug.computeWine_TypeDistribution(data);
        Debug.printNodeStatistics(file_info, data, ig_data);
        whiteListBrowsers();

        // Removing non-executable nodes (i.e. browser nodes)
        if (parameters.file_type_optimized_fv_assignment) {
            data.values().parallelStream().forEach(graph -> {
                Set<Node> nodes_to_remove = graph.vertexSet().stream()
                        .filter(graph_node -> !graph_node.isExecutable()).collect(Collectors.toSet());
                graph.removeAllVertices(nodes_to_remove);
            });

            Set<Long> set = data.entrySet().parallelStream().filter(entry -> entry.getValue().isGraphEmpty())
                    .map(Map.Entry::getKey).collect(Collectors.toSet());
            data.keySet().removeAll(set);
            data.values().parallelStream().forEach(DefaultDirectedGraphWrapper::setTimeSpan);

            ig_data.values().parallelStream().flatMap(map -> map.values().stream()).forEach(graph -> {
                Set<Node> nodes_to_remove = graph.vertexSet().stream()
                        .filter(graph_node -> !graph_node.isExecutable()).collect(Collectors.toSet());
                graph.removeAllVertices(nodes_to_remove);
            });

            ig_data.values().parallelStream().forEach(map -> {
                Set<String> keys_to_remove = map.entrySet().stream().filter(entry -> entry.getValue().isGraphEmpty())
                        .map(Map.Entry::getKey).collect(Collectors.toSet());
                map.keySet().removeAll(keys_to_remove);
            });

            set = ig_data.entrySet().parallelStream().filter(entry -> entry.getValue().isEmpty())
                    .map(Map.Entry::getKey).collect(Collectors.toSet());
            ig_data.keySet().removeAll(set);
            ig_data.values().parallelStream().flatMap(map ->
                    map.values().stream()).forEach(DefaultDirectedGraphWrapper::setTimeSpan);
        }
        Debug.printNodeStatistics(file_info, data, ig_data);
        Debug.evaluateLD_Parameters(file_info, data);

        initializeEdgeInformation();
        initializeEdgeURLInformation();
        Utils.dumpSuspiciousURLs(malware_url_distribution, benignware_url_distribution, "urls_for_training_" + parameters.data_file_count + ".csv");
        Utils.dumpURL_File_Associations(data);

        if (parameters.use_ML_url_predictor) {
            malware_url_distribution.keySet().retainAll(domain_name_suspiciousness.keySet());
            domain_name_suspiciousness.entrySet().stream().filter(entry -> !malware_url_distribution.containsKey(entry.getKey()))
                    .forEach(entry -> malware_url_distribution.put(entry.getKey(), new MutableTriple<>(0.0, 1, 0)));
        } else {
            domain_name_suspiciousness.keySet().retainAll(malware_domain_distribution.keySet());
            domain_name_suspiciousness.clear();

            Map<Integer, Integer> counter_domain_map = new HashMap<Integer, Integer>() {
                {
                    put(8, -1);
                    put(16, -1);
                }
            };

            int top_N_domains = -1;
            if (counter_domain_map.containsKey(parameters.data_file_count))
                top_N_domains = counter_domain_map.get(parameters.data_file_count);

            if (top_N_domains < 0) {
                if (parameters.use_urls_as_nbd_seeds)
                    top_N_domains = malware_url_distribution.size();
                else
                    top_N_domains = malware_domain_distribution.size();
            }

            if (ConfigurationParameters.brute_force_domain_urls)
                top_N_domains = parameters.top_N_domains;
            System.out.println("Debug section");
            int counter = 0;
            domain_name_suspiciousness = new ConcurrentHashMap<>();

            if (parameters.use_urls_as_nbd_seeds) {
                for (String domain : malware_url_distribution.keySet().stream().limit(top_N_domains).collect(Collectors.toSet())) {
                    Triple<Double, Integer, Integer> triple = malware_url_distribution.get(domain);
                    domain_name_suspiciousness.put(domain, new ImmutablePair<>(triple.getLeft(), counter++));
                }
            } else {
                for (String domain_or_url : malware_domain_distribution.keySet().stream().limit(top_N_domains).collect(Collectors.toSet())) {
                    Triple<Double, Integer, Integer> triple = malware_domain_distribution.get(domain_or_url);
                    domain_name_suspiciousness.put(domain_or_url, new ImmutablePair<>(triple.getLeft(), counter++));
                }
            }

            Map<Integer, Integer> counter_url_map = new HashMap<Integer, Integer>() {
                {
                    put(8, 6000);
                    put(16, 6000);
                    put(64, 50000); // 12K gives 100% file precision; 30K is good, 40K is good
                }
            };
            int top_N_urls = counter_url_map.containsKey(parameters.data_file_count) ?
                    counter_url_map.get(parameters.data_file_count) : malware_url_distribution.size();
            if (ConfigurationParameters.brute_force_domain_urls)
                top_N_urls = parameters.top_N_urls;

            malware_url_distribution = malware_url_distribution.entrySet().stream().limit(top_N_urls).collect(Collectors.toMap(Map.Entry::getKey,
                    entry -> new MutableTriple<>(entry.getValue().getLeft(), entry.getValue().getMiddle(), entry.getValue().getRight()),
                    (e1, e2) -> e1, LinkedHashMap::new));
        }
        Utils.dumpDomainNames(data);

        System.out.println("domain_name_suspiciousness: " + domain_name_suspiciousness.size());
        System.out.println("malware_url_distribution: " + malware_url_distribution.size());

        assert(malware_domain_distribution instanceof LinkedHashMap);
        assert(malware_url_distribution instanceof LinkedHashMap);

        global_parameters.suspicious_domain_count = domain_name_suspiciousness.size();
        domain_frequency = data.values().parallelStream().flatMap(graph -> graph.edgeSet().stream())
                .map(edge -> edge.domain).filter(Objects::nonNull)
                .collect(Collectors.groupingByConcurrent(Function.identity(), Collectors.counting()));

        fillOutTimeStamps();
        splitGraphsIntoBenignAndMalicious();
        fillOutGraphDetectionTime();
        Set<String> unique_malicious_files = new HashSet<>();
        ig_data.values().stream().flatMap(map -> map.values().stream())
                .filter(graph -> !graph.isGraphBenign()).flatMap(graph -> graph.vertexSet().stream())
                .forEach(node -> unique_malicious_files.add(node.sha2_id));
        System.err.println("# of unique malware files labeled by DG Detector from CCS'15: " + unique_malicious_files.size());
        System.out.println("Dataset's timescale: " + getDatasetTimeScale());

        Utils.compromised_url_lifetime_distribution(data);
        global_parameters.addLineToLogFile("# of machines," + data.size());
        global_parameters.addLineToLogFile("# of files," + time_stamps.size());
        global_parameters.addLineToLogFile("# of unique files," + file_info.size());
        printMachineLevelDetectionResultsForDownloaderDetector();
        Utils.dumpRandomlySelectedNBDs(data);
    }

    private void printMachineLevelDetectionResultsForDownloaderDetector() {
        // compute machine-level stat using downloader graph detector
        Set<Long> machines = data.values().parallelStream().map(graph -> graph.machine_id).collect(Collectors.toSet());
        Set<Long> infected_machines = data.values().parallelStream()
                .filter(DefaultDirectedGraphWrapper::isGraphMalicious).map(graph -> graph.machine_id).collect(Collectors.toSet());
        Set<Long> benign_machines = new HashSet<>(machines);
        benign_machines.removeAll(infected_machines);

        Set<Long> ccs_benign_machines = ig_labels_ccs.entrySet().parallelStream().filter(entry -> {
            int size = entry.getValue().size();
            int benign_count = (int) entry.getValue().values().stream().filter(flag -> flag).count();
            return benign_count >= (double) size/2;
        }).map(entry -> Long.parseLong(entry.getKey())).collect(Collectors.toSet());

        Set<Long> ccs_malicious_machines = ig_labels_ccs.keySet()
                .parallelStream().map(Long::parseLong).collect(Collectors.toSet());
        ccs_malicious_machines.removeAll(ccs_benign_machines);

        Set<Long> fp_machines = new HashSet<>(ccs_malicious_machines);
        fp_machines.removeAll(infected_machines);

        Set<Long> tp_machines = new HashSet<>(ccs_malicious_machines);
        tp_machines.retainAll(infected_machines);

        double avg_ttd = tp_machines.parallelStream().map(machine_id -> data.get(machine_id)).mapToDouble(graph -> {
            long infection_timestamp = graph.vertexSet().stream()
                    .filter(Node::isMalicious).mapToLong(node -> node.time_stamp).min().getAsLong();
            long max_timestamp = graph.vertexSet().stream()
                    .mapToLong(node -> node.time_stamp).max().getAsLong();
            return Utils.convertUnixTimeStampToDays(max_timestamp - infection_timestamp);
        }).average().getAsDouble();

        double precision = (double) 100*tp_machines.size() / (tp_machines.size() + fp_machines.size());
        double recall = (double) 100*tp_machines.size() / infected_machines.size();
        double f_1 = 2*precision*recall / (precision + recall);
        System.out.format("Machine-level (CCS results) TP: %,d FP: %,d\n", tp_machines.size(), fp_machines.size());
        System.out.format("Machine-level (CCS results) Precision: %f TP/Recall: %f  F-1: %f avg-Time-To-Detection %f\n", precision, recall, f_1, avg_ttd);
    }

    private void loadMachineGeoLocation(String file_name) {
        Long machine_id;
        String line = null, line_t;
        geo_location = new ConcurrentHashMap<>();

        try (
                BufferedReader br = new BufferedReader(new FileReader(file_name))
        ){
            br.readLine(); // skip header
            while ((line_t = br.readLine()) != null) {
                Integer geo_id = null;
                String isp = null, country = null;

                if (line_t.endsWith("\"")) {
                    line = line_t.substring(0, line_t.length()-1);
                    continue;
                } else if (line_t.startsWith("\""))
                    line += line_t.substring(1);
                else
                    line = line_t;

                String array[] = line.split(",");
                if (array.length < 2)
                    continue;

                if (array.length > 2)
                    geo_id = Integer.parseInt(array[1]);
                if (array.length > 3)
                    isp = array[2];
                if (array.length > 3)
                    country = array[3];

                machine_id = Long.parseLong(array[0]);
                geo_location.put(machine_id, new ImmutableTriple<>(geo_id, isp, country));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public Map<Long, Triple<Integer, String, String>> getGeoLocationMap() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        if (geo_location == null)
            loadMachineGeoLocation(parameters.geo_location_file);
        return geo_location;
    }

    private void whiteListBrowsers() {
        Set<String> browsers = new HashSet<>(Arrays.asList("explore", "mozilla", "firefox", "opera", "chrome"));
        List<String> browser_nodes = data.values().parallelStream().flatMap(graph -> graph.vertexSet().stream()).filter(Node::isBenign)
                .filter(node -> node.file_name != null).filter(node -> browsers.stream()
                .anyMatch(browser -> node.file_name.toLowerCase().contains(browser))).map(node -> node.sha2_id).collect(Collectors.toList());

        data.values().parallelStream().flatMap(graph -> graph.vertexSet().stream()).filter(Node::isBenign)
                .filter(node -> node.file_name != null).filter(node -> browsers.stream()
                .anyMatch(browser -> node.file_name.toLowerCase().contains(browser))).forEach(Node::setNonExecutable);

        GlobalParameters.getInstance().logger.info(String.format("Removed %,d browser nodes", browser_nodes.size()));
        GlobalParameters.getInstance().logger.info(String.format("Removed unique %,d browser nodes", browser_nodes.stream().distinct().count()));
    }
    private void initializeEdgeInformationInNodes() {
        data.values().parallelStream().forEach(graph -> {
            graph.vertexSet().forEach(node -> {
                node.incoming_url_edges = graph.incomingEdgesOf(node).stream().map(edge -> edge.url).collect(Collectors.toSet());
                node.outgoing_url_edges = graph.outgoingEdgesOf(node).stream().map(edge -> edge.url).collect(Collectors.toSet());
            });
        });

        ig_data.values().parallelStream().flatMap(map -> map.values().stream()).forEach(graph -> {
            graph.vertexSet().forEach(node -> {
                node.incoming_url_edges = graph.incomingEdgesOf(node).stream().map(edge -> edge.url).collect(Collectors.toSet());
                node.outgoing_url_edges = graph.outgoingEdgesOf(node).stream().map(edge -> edge.url).collect(Collectors.toSet());
            });
        });
        GlobalParameters.getInstance().logger.info("initializeEdgeInformationInNodes(): initialized");
    }

    private void propagateAndCheckNodeTimestamps() {
        Logger logger = GlobalParameters.getInstance().logger;
        AtomicInteger counter = new AtomicInteger();

        long graphs_with_null_timestamps_count = data.values().parallelStream().filter(graph -> {
                    return graph.vertexSet().stream().anyMatch(node -> node.getDate() == null);
                }).count();
        logger.info(graphs_with_null_timestamps_count + " out of " + data.size() + " graphs have null timestamps");

        // forward pass
        data.values().parallelStream().forEach(graph -> {
            List<Node> root_nodes = graph.vertexSet().stream()
                    .filter(node -> graph.inDegreeOf(node) == 0).collect(Collectors.toList());

            BreadthFirstIterator<Node, Edge> iterator;

            for (Node root_node : root_nodes) {
                iterator = new BreadthFirstIterator<>(graph, root_node);
                while (iterator.hasNext()) {
                    Node node = iterator.next();
                    Date date = graph.outgoingEdgesOf(node).stream().map(edge -> edge.target.getDate())
                            .filter(Objects::nonNull).reduce((date_1, date_2) -> date_1.before(date_2) ? date_1 : date_2).orElse(null);
                    if (node.getDate() == null) {
                        node.setDate(date);
                        continue;
                    }

                    if (date != null && node.getDate().after(date)) {
                        node.setDate(date);
                        counter.getAndIncrement();
                    }
                }
            }
            graph.setTimeSpan();
        });
        ig_data.values().parallelStream().flatMap(map -> map.values().stream()).forEach(graph -> graph.setTimeSpan());
        logger.info("propagateAndCheckNodeTimestamps (forward pass): " + counter.get() + " inconsistent timestamps fixed");


        // backward pass
        counter.set(0);
        data.values().parallelStream().forEach(graph -> {
            List<Node> nodes_with_null_timestamps = graph.vertexSet().stream()
                    .filter(node -> node.getDate() == null).collect(Collectors.toList());

            for (Node node : nodes_with_null_timestamps) {
                Date date = graph.incomingEdgesOf(node).stream().map(edge -> edge.source.getDate())
                        .filter(Objects::nonNull).reduce((date_1, date_2) -> date_1.before(date_2) ? date_2 : date_1).orElse(null);
                    node.setDate(date);
                    counter.getAndIncrement();
            }
        });
        logger.info("propagateAndCheckNodeTimestamps (backward pass): " + counter.get() + " timestamps fixed");

        graphs_with_null_timestamps_count = data.values().parallelStream().filter(graph -> {
            return graph.vertexSet().stream().anyMatch(node -> node.getDate() == null);
        }).count();
        logger.info("After fixing timestamps: " + graphs_with_null_timestamps_count +
                " out of " + data.size() + " graphs have null timestamps");

        if (graphs_with_null_timestamps_count != 0) {
            logger.severe("Timestamp propagation algorithm left null timestamps");
            System.exit(-1);
        }
    }

    private void initializeEdgeInformation() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        System.out.println("initializeEdgeInformation");

        Comparator<Map.Entry<String, MutableTriple<Double, Integer, Integer>>> comparator = (entry_1, entry_2) -> {
            double ratio_1 = entry_1.getValue().getLeft();
            double ratio_2 = entry_2.getValue().getLeft();
            if (ratio_1 == ratio_2) return 0;
            return ratio_1 > ratio_2 ? -1 : +1;
        };

        // <domain_or_url, <benign nodes, malicious nodes>>
        Map<String, MutablePair<Integer, Integer>> map = new ConcurrentHashMap<>();
        data.values().parallelStream().flatMap(graph -> graph.edgeSet().stream())
                .filter(edge -> edge.target.isMalicious() || edge.source.isMalicious()).forEach(edge -> {
                String domain = edge.domain;
                map.putIfAbsent(domain, new MutablePair<>(0, 0));
                MutablePair<Integer, Integer> pair = map.get(domain);

                int benign_node_count = (int) Stream.of(edge.source, edge.target).filter(Node::isBenign).count();
                int malicious_node_count = (int) Stream.of(edge.source, edge.target).filter(Node::isMalicious).count();
                pair.setLeft(pair.getLeft() + benign_node_count);
                pair.setRight(pair.getRight() + malicious_node_count);
        });

        malware_domain_distribution = map.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
                        entry -> new MutableTriple<>((double) entry.getValue().getRight() / (entry.getValue().getLeft() + parameters.epsilon),
                                entry.getValue().getLeft(), entry.getValue().getRight())))
                .entrySet().stream().sorted(comparator)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        data.values().parallelStream().flatMap(graph -> graph.edgeSet().stream())
                .filter(edge -> !malware_domain_distribution.containsKey(edge.domain)).forEach(edge -> {
                String domain = edge.domain;
                benignware_domain_distribution.putIfAbsent(domain, new MutableTriple<>(0.0, 0, 0));
                MutableTriple<Double, Integer, Integer> triple = benignware_domain_distribution.get(domain);
                int benign_node_count = (int) Stream.of(edge.source, edge.target).filter(Node::isBenign).count();
                triple.setMiddle(triple.getMiddle() + benign_node_count);
        });
    }

    private void initializeEdgeURLInformation() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        System.out.println("initializeEdgeURLInformation");

        Comparator<Map.Entry<String, MutableTriple<Double, Integer, Integer>>> comparator = (entry_1, entry_2) -> {
            double ratio_1 = entry_1.getValue().getLeft();
            double ratio_2 = entry_2.getValue().getLeft();
            if (ratio_1 == ratio_2) return 0;
            return ratio_1 > ratio_2 ? -1 : +1;
        };

        // <domain_or_url, <benign nodes, malicious nodes>>
        Map<String, MutablePair<AtomicInteger, AtomicInteger>> map = new ConcurrentHashMap<>();
        data.values().parallelStream().forEach(graph -> {
            graph.vertexSet().forEach(node -> {
                node.incoming_url_edges.forEach(url -> {
                    map.putIfAbsent(url, new MutablePair<>(new AtomicInteger(0), new AtomicInteger(0)));
                    MutablePair<AtomicInteger, AtomicInteger> pair = map.get(url);
                    if (node.isBenign())
                        pair.getLeft().getAndIncrement();
                    else
                        pair.getRight().getAndIncrement();
                });
            });
        });
        System.out.println("map size: " + map.size());

        malware_url_distribution = map.entrySet().parallelStream()
                .filter(entry -> entry.getValue().getRight().get() > 0).collect(Collectors.toConcurrentMap(Map.Entry::getKey,
                entry -> new MutableTriple<>((double) entry.getValue().getRight().get() / (entry.getValue().getLeft().get() + entry.getValue().getRight().get() + parameters.epsilon),
                        entry.getValue().getLeft().get(), entry.getValue().getRight().get())))
                .entrySet().stream().sorted(comparator)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        benignware_url_distribution = map.entrySet().parallelStream()
                .filter(entry -> entry.getValue().getRight().get() == 0).collect(Collectors.toConcurrentMap(Map.Entry::getKey,
                        entry -> new MutableTriple<>((double) entry.getValue().getRight().get() / (entry.getValue().getLeft().get() + entry.getValue().getRight().get() + parameters.epsilon),
                                entry.getValue().getLeft().get(), entry.getValue().getRight().get())))
                .entrySet().stream().sorted(comparator)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        System.out.println("malware_url_distribution size: " + malware_url_distribution.size());
        System.out.println("benignware_url_distribution size: " + benignware_url_distribution.size());
    }

    private void collectEdgeStatistics() {
        String line;
        Set<String> suspicious_domains = new HashSet<>();

        try (
                BufferedReader br = new BufferedReader(new FileReader("data/suspicious_domains.csv"));
        ){
            while ((line = br.readLine()) != null) {
                suspicious_domains.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }

        Map<String, Long> map = data.values().parallelStream().flatMap(graph -> graph.edgeSet().stream())
                //.filter(edge -> !edge.alexa_domain)
                .map(edge -> edge.domain).filter(Objects::nonNull)
                .collect(Collectors.groupingByConcurrent(Function.identity(), Collectors.counting()));

        Set<String> benign_nodes_t = data.values().parallelStream().flatMap(graph -> graph.vertexSet().stream())
                .filter(Node::isBenign).map(node -> node.sha2_id).collect(Collectors.toSet());

        Set<String> malicious_nodes_t = data.values().parallelStream().flatMap(graph -> graph.vertexSet().stream())
                .filter(Node::isMalicious).map(node -> node.sha2_id).collect(Collectors.toSet());

        System.out.format("benign_nodes_t: %d, malicious_nodes_t: %d\n", benign_nodes_t.size(), malicious_nodes_t.size());

        Predicate<String> predicate = suspicious_domains::contains;

        for (int percentile = 5; percentile <= 100; percentile += 5) {
            double d[] = map.values().stream().mapToDouble(Double::valueOf).toArray();
            double threshold = (new Percentile()).evaluate(d, percentile);
            Set<String> domains = map.entrySet().stream().filter(entry ->
                    entry.getValue() <= threshold).map(Map.Entry::getKey).collect(Collectors.toSet());

            Set<String> target_benign_nodes = data.values().parallelStream().flatMap(graph -> graph.edgeSet().stream())
                    .filter(edge -> predicate.test(edge.domain)).filter(Objects::nonNull).filter(edge -> edge.target.isBenign())
                    .map(edge -> edge.target.sha2_id).collect(Collectors.toSet());

            Set<String> benign_nodes = data.values().parallelStream().flatMap(graph -> graph.edgeSet().stream())
                    .filter(edge -> predicate.test(edge.domain)).filter(Objects::nonNull).flatMap(edge -> Stream.of(edge.source, edge.target))
                    .filter(Node::isBenign).map(node -> node.sha2_id).collect(Collectors.toSet());

            Set<String> target_malicious_nodes = data.values().parallelStream().flatMap(graph -> graph.edgeSet().stream())
                    .filter(edge -> predicate.test(edge.domain)).filter(Objects::nonNull).filter(edge -> edge.target.isMalicious())
                    .map(edge -> edge.target.sha2_id).collect(Collectors.toSet());

            Set<String> malicious_nodes = data.values().parallelStream().flatMap(graph -> graph.edgeSet().stream())
                    .filter(edge -> predicate.test(edge.domain)).filter(Objects::nonNull).flatMap(edge -> Stream.of(edge.source, edge.target))
                    .filter(Node::isMalicious).map(node -> node.sha2_id).collect(Collectors.toSet());

            System.out.format("threshold: %d/%f target_benign_nodes: %d, benign_nodes: %d, target_malicious_nodes: %d, malicious_nodes: %d\n",
                    percentile, threshold, target_benign_nodes.size(), benign_nodes.size(), target_malicious_nodes.size(), malicious_nodes.size());
        }
    }

    public void loadMachinesUsedForGraphRF_Training() {
        String line;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        if (!parameters.train_GraphRF)
            return;

        if (parameters.graphRF_threshold < parameters.epsilon)
            return;

        Path path = Paths.get(parameters.selected_machines_for_training_GraphRF);
        if (Files.notExists(path))
            return;

        try (
                BufferedReader br = new BufferedReader(new FileReader(parameters.selected_machines_for_training_GraphRF));
        ) {
            while ((line = br.readLine()) != null) {
                excluded_machine_ids.add(Long.parseLong(line));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public String getDatasetTimeScale() {
        Date start_date = new Date(getFirstTimeStamp()*1000);
        Date end_date = new Date(getLastTimeStamp()*1000);
        return start_date + " - " + end_date;
    }

    private void fillOutGraphDetectionTime() {
        for (Map.Entry<Long, Map<String, DefaultDirectedGraphWrapper>> entry : ig_data.entrySet()) {
            OptionalLong infection_time_stamp_optional = entry.getValue().entrySet().stream().map(entry_t -> entry_t.getValue())
                    .filter(graph -> !graph.isGraphBenign()).mapToLong(graph -> graph.getMaxTimeStamp()).min();
            Long time_stamp = machine_detection_time.get(entry.getKey());
            infection_time_stamp_optional.ifPresent(infection_time_stamp ->
                    machine_detection_time.put(entry.getKey(), time_stamp == null ? infection_time_stamp : Math.min(time_stamp, infection_time_stamp)));
        }

        ig_data.values().stream().flatMap(map -> map.entrySet().stream()).forEach(entry ->
        {
            FileInfo file_info = this.file_info.get(entry.getKey());
            if (file_info != null) {
                if (file_info.ig_detection_time != null) {
                    long ig_detection_time = file_info.ig_detection_time;
                    long time_stamp = Math.min(entry.getValue().getMaxTimeStamp(), ig_detection_time);
                    file_info.ig_detection_time = time_stamp;
                } else
                    file_info.ig_detection_time = entry.getValue().getMaxTimeStamp();
            }
        });

        ig_data.values().stream().flatMap(map -> map.values().stream()).flatMap(graph -> graph.vertexSet().stream()).forEach(node ->
        {
            FileInfo file_info = this.file_info.get(node.sha2_id);
            if (file_info != null) {
                if (file_info.earliest_wine_time != null) {
                    long earliest_wine_time = file_info.earliest_wine_time;
                    long time_stamp = Math.min(node.time_stamp, earliest_wine_time);
                    file_info.earliest_wine_time = time_stamp;
                } else
                    file_info.earliest_wine_time = node.time_stamp;
            }
        });

        //TODO: remove later
        long g = ig_data.values().stream().flatMap(map -> map.entrySet().stream()).map(Map.Entry::getKey).count();
        long gg = ig_data.values().stream().flatMap(map -> map.entrySet().stream()).map(Map.Entry::getKey).distinct().count();
        System.err.println("# of all nodes = " + g);
        System.err.println("# of unique downloader hashes = " + gg);

        ig_data.values().stream().flatMap(map -> map.values().stream())
                .flatMap(graph -> graph.vertexSet().stream()).filter(node -> node.sha2_id.equals("1184575487")).forEach(n -> {
                    System.out.println('x');
        });

        showIG_GraphStat();
        dumpIG_GraphStat();
        dumpFullGraphStat();
        dumpDomainStat();

        System.out.println("malware_url_distribution.containsKey(null): " + malware_url_distribution.containsKey(null));
        System.out.println("malware_url_distribution.containsKey(\"\"): " + malware_url_distribution.containsKey(""));
        System.out.println("domain_name_suspiciousness: " + domain_name_suspiciousness.size());
        System.out.println("malware_url_distribution: " + malware_url_distribution.size());
        System.out.println("malware_url_distribution size: " + malware_url_distribution.size());
        Debug.extractSuspiciousNodesBasedOnURLs(null, malware_url_distribution, data, true);

        long counter_1 = data.values().parallelStream().flatMap(graph -> graph.vertexSet().stream())
                .filter(node -> node.isBenign()).filter(node -> node.file_info == null).count();
        long counter_2 = data.values().parallelStream().flatMap(graph -> graph.vertexSet().stream())
                .filter(node -> node.isMalicious()).filter(node -> node.file_info == null).count();
        System.out.format("# of benign files with NULL FileInfo %d, malicious files %d\n", counter_1, counter_2);
        Debug.analyzeDomainEdgeCoverage(domain_name_suspiciousness, malware_url_distribution, data);
    }

    public Map<String, Pair<Double, Integer>> getDomainNameSuspiciousness() {
        return Collections.unmodifiableMap(domain_name_suspiciousness);
    }

    public Map<String,MutableTriple<Double,Integer,Integer>> getURLSuspiciousness() {
        return malware_url_distribution;
    }

    private void dumpDomainStat() {
        try (
                OutputStreamWriter stream_writer = new FileWriter("results/domain_stat_distribution.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (Map.Entry<String, MutableTriple<Double, Integer, Integer>> entry : malware_domain_distribution.entrySet())
                writer.write(entry.getKey() + "," + (-1)*entry.getValue().getLeft() + "," + (-1)*entry.getValue().getRight() + "\n");
            for (Map.Entry<String, MutableTriple<Double, Integer, Integer>> entry : benignware_domain_distribution.entrySet())
                writer.write(entry.getKey() + "," + entry.getValue().getLeft() + "," + entry.getValue().getRight() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        try (
                OutputStreamWriter stream_writer = new FileWriter("results/url_stat_distribution.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (Map.Entry<String, MutableTriple<Double, Integer, Integer>> entry : malware_url_distribution.entrySet())
                writer.write(entry.getKey() + "," + (-1)*entry.getValue().getLeft() + "," + (-1)*entry.getValue().getMiddle() + "," + (-1)*entry.getValue().getRight() + "\n");
            for (Map.Entry<String, MutableTriple<Double, Integer, Integer>> entry : benignware_url_distribution.entrySet())
                writer.write(entry.getKey() + "," + entry.getValue().getLeft() + "," + (-1)*entry.getValue().getMiddle() + "," + entry.getValue().getRight() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private void showIG_GraphStat() {
        long s1 = file_info_ccs_eval.parallelStream().map(sha2_id -> file_info.get(sha2_id)).filter(file_info ->
                !file_info.is_benign && file_info.vt_time_stamp != null && file_info.earliest_wine_time != null)
                .map(file_info -> file_info.sha2_id).distinct().count();

        long s2 = file_info_ccs_eval.parallelStream().map(sha2_id -> file_info.get(sha2_id)).filter(file_info ->
                !file_info.is_benign && file_info.vt_time_stamp != null && file_info.earliest_wine_time != null)
                .filter(file_info -> file_info.earliest_wine_time < file_info.vt_time_stamp)
                .map(file_info -> file_info.sha2_id).distinct().count();
        System.out.format("file_info_ccs_eval: size/nonNull/early_detection: %d/%d/%d\n", file_info_ccs_eval.size(), s1, s2);

        long s3 = file_info_full_eval.parallelStream().map(sha2_id -> file_info.get(sha2_id)).filter(file_info ->
                !file_info.is_benign && file_info.vt_time_stamp != null && file_info.earliest_wine_time != null)
                .map(file_info -> file_info.sha2_id).distinct().count();

        long s4 = file_info_full_eval.parallelStream().map(sha2_id -> file_info.get(sha2_id)).filter(file_info ->
                !file_info.is_benign && file_info.vt_time_stamp != null && file_info.earliest_wine_time != null)
                .filter(file_info -> file_info.earliest_wine_time < file_info.vt_time_stamp)
                .map(file_info -> file_info.sha2_id).distinct().count();
        System.out.format("file_info: size/nonNull/early_detection: %d/%d/%d\n", file_info_full_eval.size(), s3, s4);

        Set<String> set = new HashSet<>();
        set.addAll(ig_data.values().stream().flatMap(map -> map.entrySet().stream()).map(e -> e.getKey()).collect(Collectors.toList()));
        set.retainAll(file_info_ccs_eval);
        int parameter_1 = set.size();
        System.err.println("File Info Evaluation files that overlap with Downloaders within IGs = " + parameter_1);

        set = new HashSet<>();
        set.addAll(ig_data.values().stream().flatMap(map -> map.values().stream()).flatMap(graph -> graph.vertexSet().stream()).map(node -> node.sha2_id).collect(Collectors.toList()));
        set.retainAll(file_info_ccs_eval);
        int parameter_2 = set.size();
        System.err.println("File Info Evaluation files that are present in IGs = " + parameter_2);

        set = new HashSet<>();
        set.addAll(ig_labels_ccs.values().stream().flatMap(map -> map.keySet().stream()).collect(Collectors.toList()));
        set.retainAll(file_info_ccs_eval);
        int parameter_3 = set.size();
        System.err.println("ig_labels_ccs & file_info_ccs_eval = " + parameter_3);

        int selected_nodes = ig_data.values().parallelStream().flatMap(map -> map.values().stream()).filter(graph -> graph.isGraphMalicious())
                .flatMap(graph -> graph.vertexSet().stream()).map(node -> node.sha2_id).collect(Collectors.toSet()).size();
        int malicious_nodes = ig_data.values().parallelStream().flatMap(map -> map.values().stream()).filter(graph -> graph.isGraphMalicious())
                .flatMap(graph -> graph.vertexSet().stream()).filter(node -> node.isMalicious()).map(node -> node.sha2_id).collect(Collectors.toSet()).size();
        System.out.format("Best IG selection strategy %d/%d\n", malicious_nodes, selected_nodes);

        Set<Long> s = ig_data.values().parallelStream().flatMap(map -> map.values().stream()).map(graph -> graph.machine_id).collect(Collectors.toSet());
        s.removeAll(ig_data.keySet());
        System.out.format("ig_data is %s\n", s.isEmpty() ? "consistent" : "inconsistent");
        for (Long machine_id : s)
            System.out.println(machine_id);

        // print stat
        {
            long malicious_downloader_count = ig_data.values().parallelStream().flatMap(map -> map.values().stream())
                    .filter(graph -> graph.root_node.isMalicious()).count();
            long malicious_file_count = data.values().parallelStream()
                    .flatMap(graph -> graph.vertexSet().stream()).filter(Node::isMalicious).count();
            long benign_file_count = data.values().parallelStream()
                    .flatMap(graph -> graph.vertexSet().stream()).filter(Node::isBenign).count();
            System.out.format("Malware stat %,d/%,d/%,d (malicious downloaders total/malicious files total/benign_file_count total)\n",
                    malicious_downloader_count, malicious_file_count, benign_file_count);
        }

        // print stat
        {
            long malicious_downloader_count = ig_data.values().parallelStream().flatMap(map -> map.values().stream())
                    .filter(graph -> graph.root_node.isMalicious()).map(graph -> graph.root_node.sha2_id).distinct().count();
            long malicious_file_count = data.values().parallelStream()
                    .flatMap(graph -> graph.vertexSet().stream()).filter(Node::isMalicious).map(node -> node.sha2_id).distinct().count();
            long benign_file_count = data.values().parallelStream()
                    .flatMap(graph -> graph.vertexSet().stream()).filter(Node::isBenign).map(node -> node.sha2_id).distinct().count();
            System.out.format("Malware stat %,d/%,d/%,d (malicious downloaders distinct/malicious files distinct/benign_file_count distinct)\n",
                    malicious_downloader_count, malicious_file_count, benign_file_count);
        }
    }

    private void dumpIG_GraphStat() {
        Map<Integer, Long> node_stat = ig_data.values().parallelStream().flatMap(map -> map.values().stream())
                .collect(Collectors.groupingByConcurrent(graph -> graph.vertexSet().size(), Collectors.counting()));

        Map<Integer, Long> edge_stat = ig_data.values().parallelStream().flatMap(map -> map.values().stream())
                .collect(Collectors.groupingByConcurrent(graph -> graph.edgeSet().size(), Collectors.counting()));

        Map<Double, Long> graph_density = ig_data.values().parallelStream().flatMap(map -> map.values().stream())
                .collect(Collectors.groupingByConcurrent(graph -> Math.pow(graph.vertexSet().size(), 2)/graph.edgeSet().size(), Collectors.counting()));
        dumpGraphStatToFile(node_stat, edge_stat, graph_density, "ig");
    }

    private void dumpFullGraphStat() {
        Map<Integer, Long> node_stat = data.values().parallelStream()
                .collect(Collectors.groupingByConcurrent(graph -> graph.vertexSet().size(), Collectors.counting()));

        Map<Integer, Long> edge_stat = data.values().parallelStream()
                .collect(Collectors.groupingByConcurrent(graph -> graph.edgeSet().size(), Collectors.counting()));

        Map<Double, Long> graph_density = data.values().parallelStream()
                .collect(Collectors.groupingByConcurrent(graph -> Math.pow(graph.vertexSet().size(), 2)/graph.edgeSet().size(), Collectors.counting()));
        dumpGraphStatToFile(node_stat, edge_stat, graph_density, "full");
    }

    private void dumpGraphStatToFile(Map<Integer, Long> node_stat, Map<Integer, Long> edge_stat,
                                     Map<Double, Long> graph_density, String prefix) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        try (
                OutputStreamWriter stream_writer = new FileWriter(parameters.out_folder + File.separator + prefix + "_node_stat_distribution.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (Map.Entry<Integer, Long> entry : node_stat.entrySet())
                writer.write(entry.getKey() + "," + entry.getValue() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        try (
                OutputStreamWriter stream_writer = new FileWriter(parameters.out_folder + File.separator + prefix + "_edge_stat_distribution.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (Map.Entry<Integer, Long> entry : edge_stat.entrySet())
                writer.write(entry.getKey() + "," + entry.getValue() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        try (
                OutputStreamWriter stream_writer = new FileWriter(parameters.out_folder + File.separator + prefix + "_graph_density_distribution.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (Map.Entry<Double, Long> entry : graph_density.entrySet())
                writer.write(entry.getKey() + "," + entry.getValue() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public Set<String> getLoadedFileShaIDs(IntrospectionModule.ProcessNodeStatType processing_type) {
        switch (processing_type) {
            case CCS_EVALUATION:
                return getAllDownloaders().parallelStream().map(sha2_id -> file_info.get(sha2_id))
                        .map(file_info -> file_info.sha2_id).collect(Collectors.toSet());
            case FULL_EVALUATION:
                return data.values().parallelStream().flatMap(graph -> graph.vertexSet().stream())
                        .map(node -> file_info.get(node.sha2_id)).map(file_info -> file_info.sha2_id).collect(Collectors.toSet());
        }
        return null;
    }

    public Set<String> getLoadedFileShaIDs(IntrospectionModule.ProcessNodeStatType processing_type, boolean request_benign) {
        Set<String> set = getLoadedFileShaIDs(processing_type);
        Set<String> ret = set.parallelStream().map(sha_id -> file_info.get(sha_id)).filter(file_info ->
                request_benign ? file_info.is_benign : !file_info.is_benign).map(file_info -> file_info.sha2_id).collect(Collectors.toSet());
        return ret;
    }

    // <file_id, <machine_id, file time_stamp>>
    public Map<String, List<Pair<Long, Long>>> getFileToMachineMap(IntrospectionModule.ProcessNodeStatType processing_type) {
        Map<String, List<Pair<Long, Long>>> ret = null;
        Supplier<Stream<Node>> supplier = null;

        switch (processing_type) {
            case CCS_EVALUATION:
                supplier = () -> ig_data.values().parallelStream().flatMap(map -> map.values().stream()).map(graph -> graph.root_node);
                break;

            case FULL_EVALUATION:
                supplier = () -> data.values().parallelStream().flatMap(graph -> graph.vertexSet().stream());
                break;
        }
        return supplier.get().collect(Collectors.groupingByConcurrent(node -> node.sha2_id,
                Collectors.mapping(node -> new ImmutablePair<>(node.machine_id, node.time_stamp), Collectors.toList())));
    }

    public Set<Long> getMachineIDsDownloadedFiles(Set<String> sha2_id_set) {
        Set<Long> ret = Collections.newSetFromMap(new ConcurrentHashMap<Long, Boolean>());
        ig_data.values().parallelStream().flatMap(map_value -> map_value.values().stream())
                .flatMap(graph -> graph.vertexSet().stream()).filter(node -> sha2_id_set.contains(node.sha2_id))
                .forEach(node -> ret.add(node.machine_id));
        return ret;
    }

    public Set<String> getPossibleEarlyDetectionShaIDs(IntrospectionModule.ProcessNodeStatType processing_type) {
        Set<FileInfo> set;

        switch (processing_type) {
            case CCS_EVALUATION:
                set = file_info_ccs_eval.parallelStream()
                        .map(sha2_id -> file_info.get(sha2_id)).collect(Collectors.toSet());
                break;
            case FULL_EVALUATION:
                set = file_info_full_eval.parallelStream()
                        .map(sha2_id -> file_info.get(sha2_id)).collect(Collectors.toSet());
                break;
            default:
                set = null;
        }
        return set.parallelStream().filter(file_info ->
                !file_info.is_benign && file_info.vt_time_stamp != null && file_info.earliest_wine_time != null)
                .filter(file_info -> file_info.earliest_wine_time < file_info.vt_time_stamp)
                .map(file_info -> file_info.sha2_id).collect(Collectors.toSet());
    }

    public Long getGraphDetectionTime(long machine_id) {
        return machine_detection_time.get(machine_id);
    }

    public Map<String, Pair<Integer, Integer>> getCCS_Results() {
        Map<String, Pair<Integer, Integer>> resutls = new HashMap<>();
        Supplier<Stream<Node>> supplier = () -> ig_data.values().parallelStream()
                .flatMap(map -> map.values().stream()).map(DefaultDirectedGraphWrapper::getRootNode);

        int benign_downloader_count = (int) supplier.get().filter(Node::isBenign).count();
        int malicious_downloader_count = (int) supplier.get().filter(Node::isMalicious).count();

        int fp_downloader_count = (int) supplier.get().filter(Node::isBenign)
                .map(node -> {
                    Map<String, Boolean> map = ig_labels_ccs.get(node.machine_id);
                    if (map == null)
                        return null;
                    return map.get(node.sha2_id);
                }).filter(Objects::nonNull).filter(label -> !label).count();

        int tp_downloader_count = (int) supplier.get().filter(Node::isMalicious)
                .map(node -> {
                    Map<String, Boolean> map = ig_labels_ccs.get(node.machine_id);
                    if (map == null)
                        return null;
                    return map.get(node.sha2_id);
                }).filter(Objects::nonNull).filter(label -> !label).count();

        resutls.put("benignware", new ImmutablePair<>(fp_downloader_count, benign_downloader_count));
        resutls.put("malware", new ImmutablePair<>(tp_downloader_count, malicious_downloader_count));
        return resutls;
    }

    private void fillOutTimeStamps() {
        getUnmodifiableData().values().parallelStream().flatMap(map -> map.values().stream()).flatMap(graph -> graph.vertexSet()
                .stream()).forEach(node -> time_stamps.put(node.time_stamp, node));
    }

    private DefaultDirectedGraphWrapper parseJson(String json, long machine_id, String root_node) throws MalformedJsonException {
        Node node;
        Edge edge;
        GraphParser graph_parser = null;
        FileInfo file;

        try {
            json = json.replace("\\", "/");
            graph_parser = gson.fromJson(json, GraphParser.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return null;
        }
        ArrayList<Node> nodes = new ArrayList<>(graph_parser.nodes.size());
        DefaultDirectedGraphWrapper graph = new DefaultDirectedGraphWrapper(machine_id);

        for (NodeParser node_parser : graph_parser.nodes) {
            try {
                file = file_info.get(node_parser.name);
                Integer score = downloader_score.get(node_parser.name);

                if (score == null)
                    score = 0;
                node = new Node(node_parser.name, node_parser.fn, node_parser.ts, machine_id, file, score);
                if (!graph.addVertex(node))
                    System.err.println("Duplicate vertex");
                nodes.add(node);
            } catch (ParseException e) {
                if (ConfigurationParameters.getInstance().debug_info)
                    e.printStackTrace();
            }
        }
        graph.setRootNode(root_node);

        for (EdgeParser edge_parser : graph_parser.links) {
            try {
                Node node_source = nodes.get(edge_parser.source);
                Node node_target = nodes.get(edge_parser.target);

                if (node_source == null || node_target == null)
                    continue;
                if (!graph.containsVertex(node_source) || !graph.containsVertex(node_target))
                    continue;
                edge = new Edge(edge_parser.url, node_source, node_target);
                edge.alexa_domain = isAlexaDomain(edge.domain);
                Pair<Double, Integer> pair = domain_name_suspiciousness.get(edge.domain);
                if (pair != null)
                    edge.score = pair.getLeft();
                graph.addEdge(node_source, node_target, edge);
            } catch (Exception e) {
                if (ConfigurationParameters.getInstance().debug_info)
                    e.printStackTrace();
            }
        }
        return graph;
    }

    private boolean isAlexaDomain(String domain) {
        return domain == null ? false : alexa_domains.contains(domain);
    }

    public void loadIGs(String file_name) {
        int index[] = new int[2];
        Long machine_id;
        String line;

        try (
                BufferedReader br = new BufferedReader(new FileReader(file_name));
        ) {
            while ((line = br.readLine()) != null) {
                index[0] = line.indexOf(",");                               // search for the first comma
                index[1] = line.indexOf(",", index[0] + 1);     // search for the first comma
                String machine_id_str = line.substring(1, index[0] - 1);
                String ig_root_node_id_str = line.substring(index[0] + 2, index[1] - 1);

                if (machine_id_str.equals("achin"))
                    continue;
                try {
                    machine_id = Long.valueOf(machine_id_str);
                } catch (NumberFormatException e) {
                    if (ConfigurationParameters.getInstance().debug_info)
                        System.err.println("NumericFormatException: " + machine_id_str + ", " + ig_root_node_id_str);
                    continue;
                }
                String json = line.substring(index[1] + 2, line.length() - 1);

                try {
                    DefaultDirectedGraphWrapper graph_t = parseJson(json, machine_id, ig_root_node_id_str);
                    if (graph_t == null || excluded_machine_ids.contains(graph_t.machine_id))
                        continue;

                    // store IGs
                    ig_data.putIfAbsent(machine_id, new ConcurrentHashMap<>());
                    Map ig_map = ig_data.get(machine_id);
                    Map<String, Boolean> map = ig_labels_ccs.get(machine_id_str);
                    Boolean graph_label = map == null ? null : map.get(ig_root_node_id_str);
                    graph_t.setBenignLabel(graph_label);
                    ig_map.put(ig_root_node_id_str, graph_t);

                    data.putIfAbsent(machine_id, new DefaultDirectedGraphWrapper(machine_id));
                    DefaultDirectedGraphWrapper graph = data.get(machine_id);
                    synchronized (machine_id) {
                        Graphs.addGraph(graph, graph_t);
                    }
                } catch (JsonSyntaxException e) {
                    if (ConfigurationParameters.getInstance().debug_info)
                        System.err.println(e);
                    continue;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void propagateIGLabelsToNodes(DirectedGraph<Node, Edge> graph, String machine_id, String root_id) {
        if (ConfigurationParameters.getInstance().ground_truth == ConfigurationParameters.GroundTruthEnum.APPROXIMATE)
            return;

        long malicious_nodes = graph.vertexSet().stream().filter(Node::isBenign).count();
        int graph_size = graph.vertexSet().size();

        FileInfo file_info = this.file_info.get(root_id);
        Boolean is_root_benign = file_info == null ? null : file_info.is_benign;

        Map<String, Boolean> map = ig_labels_ccs.get(machine_id);
        Boolean is_graph_labeled_benign = map == null ? null : map.get(root_id);

        double malicious_confidence = (double) malicious_nodes/graph_size;
        if (is_root_benign != null)
            malicious_confidence += is_root_benign ? -0.3 : +0.3;

        if (is_graph_labeled_benign != null)
            malicious_confidence += is_graph_labeled_benign ? -0.15 : +0.15;

        final boolean label_as_benign = malicious_confidence <= 0.5;
        graph.vertexSet().stream().filter(node -> node.file_info.is_benign == null)
                .forEach(node -> node.file_info.is_benign = label_as_benign);
    }

    public Map<Long, Map<String, DefaultDirectedGraphWrapper>> getUnmodifiableData() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        if (parameters.use_ig_data) {
            throw new RuntimeException("Incorrect configuration: use of IG data");
        } else {
            Map<Long, Map<String, DefaultDirectedGraphWrapper>> map = new ConcurrentHashMap<>();
            data.entrySet().parallelStream().forEach(entry -> {
                Map<String, DefaultDirectedGraphWrapper> map_t = new HashMap<>();
                map_t.put("none", entry.getValue());
                map.put(entry.getKey(), map_t);
            });
            return map;
        }
    }

    public Map<Long, DefaultDirectedGraphWrapper> getData() {
        return Collections.unmodifiableMap(data);
    }

    public Map<Long, Map<String, DefaultDirectedGraphWrapper>> getIGData() {
        return Collections.unmodifiableMap(ig_data);
    }

    public long getFirstTimeStamp() {
        return time_stamps.firstKey();
    }

    public long getLastTimeStamp() {
        return time_stamps.lastKey();
    }

    private void loadAlexaDomainNames(String file_name) {
        String line, match;
        Pattern pattern;
        pattern = Pattern.compile("\\.?[^\\.]+(\\..{2,3})?\\.\\D{2,4}$");

        try (
                BufferedReader br = new BufferedReader(new FileReader(file_name));
        ){
            while ((line = br.readLine()) != null) {
                String array[] = line.split(",");
                Matcher matcher = pattern.matcher(array[1]);
                boolean match_found = matcher.find();
                if (!match_found)
                    // incorrect url
                    match = array[1];
                else {
                    match = matcher.group();
                    if (match.startsWith("."))
                        match = match.substring(1);
                }
                alexa_domains.add(match);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private void loadDomainNameScores(String file_name) {
        int counter = 0;
        String line;

        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        List<String> fp_domains = new LinkedList<>();

        try (
                // <url, label, score, benign_count, malicious_count>
                BufferedReader br = new BufferedReader(new FileReader(file_name));
        ){
            while ((line = br.readLine()) != null) {
                String array[] = line.split(",");
                String domain = array[0];
                int ground_truth = Integer.parseInt(array[1]);
                double score = Double.parseDouble(array[2]);
                if (score > parameters.domain_score_threshold)
                    domain_name_suspiciousness.put(domain, new ImmutablePair<>(score, counter ++));
                if (ground_truth > 0.5 && score > parameters.domain_score_threshold)
                    fp_domains.add(domain);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        Utils.dumpCollectionToCSV(fp_domains, parameters.out_folder + "/domain_name_fps.csv");
        GlobalParameters.getInstance().logger.info("loadDomainNameScores() loaded "
                + domain_name_suspiciousness.size() + " suspicious URLs");
    }

    private void loadDomainNameBucketedScores() {
        String line;
        BufferedReader buffered_stream;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        Runtime rt = Runtime.getRuntime();

        try {
            Process proc = rt.exec("python domain_bucketization.py " + parameters.out_folder);
            buffered_stream = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            while ( (line = buffered_stream.readLine()) != null)
                System.out.println(line);

            buffered_stream = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
            System.out.println("<ERROR>");
            while ( (line = buffered_stream.readLine()) != null)
                System.out.println(line);
            System.out.println("</ERROR>");

            int exit_value = proc.waitFor();
            if (exit_value != 0) {
                System.err.println("domain_bucketization.py failed");
                System.exit(-1);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        try (
                // <domain, 10 x buckets>
                BufferedReader br = new BufferedReader(new FileReader(parameters.out_folder + "/url_bucketed_fvs.csv"));
        ){
            while ((line = br.readLine()) != null) {
                String array[] = line.split(",");
                String domain = array[0];
                int fv[] = IntStream.range(1, array.length).map(i -> Integer.parseInt(array[i])).toArray();
                domain_bucketed_scores.put(domain, fv);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        domain_bucketed_scores = Collections.unmodifiableMap(domain_bucketed_scores);
    }

    public Map<String, int[]> getDomainBucketedScores() {
        return domain_bucketed_scores;
    }

    private void loadVTGroundTruth(String file_name, boolean do_mapping) {
        String line;

        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        Map<String, String> sha256_to_id = new HashMap<>();
        Map<String, String> md5_to_id = new HashMap<>();
        Map<String, String> id_to_sha256 = new HashMap<>();
        Map<String, String> id_to_md5 = new HashMap<>();
        assert(file_info.isEmpty());

        try (
                BufferedReader br = new BufferedReader(new FileReader("data/filesha2.csv"));
        ) {
            while ((line = br.readLine()) != null) {
                String array[] = line.split(",");
                String file_id = array[0];
                String sha256 = array[1];
                sha256_to_id.put(sha256, file_id);
                id_to_sha256.put(file_id, sha256);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }

        try (
                BufferedReader br = new BufferedReader(new FileReader("data/portalfilemd5.csv"));
        ) {
            while ((line = br.readLine()) != null) {
                String array[] = line.split(",");
                String file_id = array[0];
                String md5 = array[1];
                md5_to_id.put(md5, file_id);
                id_to_md5.put(file_id, md5);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }

        // load VT's statistic
        Set<String> family_names = new HashSet<>();
        family_names.add("Trojan");

        try (
                BufferedReader br = new BufferedReader(new FileReader(file_name));
        ) {
            while ((line = br.readLine()) != null) {
                String prefix = "", family_name = "", suffix = "", file_type = "";
                String array[] = line.split(",");
                String sha256 = array[0];
                String md5 = array[1];
                int positives = Integer.parseInt(array[2]);
                int av_count = Integer.parseInt(array[3]);
                String vt_time_stamp = array[4];
                if (array.length > 5)
                    prefix = array[5];
                if (array.length > 6) {
                    family_name = array[6];
                    family_names.add(family_name);
                }
                if (array.length > 7)
                    suffix = array[7];
                if (array.length > 8)
                    file_type = array[8];
                String file_id = sha256_to_id.get(sha256);
                if (file_id == null)
                    file_id = md5_to_id.get(md5);
                FileInfo file = new FileInfo(positives, av_count, sha256, md5, file_id, vt_time_stamp, prefix, family_name, suffix, file_type);
                if (!file.is_benign) {
                    file.family_name = (new ArrayList<>(family_names)).get((int) (Math.random()*family_names.size()));
                }
                file_info.put(file_id, file);
                file_info_full_eval.add(file_id);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }

        // don't map all WINE files to successfully disassembled files
        if (!do_mapping) {
            GlobalParameters.getInstance().logger.info("Loading VT reports without performing the mapping");
            return;
        }

        long loaded_malware_samples_count = file_info.values().parallelStream().filter(file_info -> !file_info.is_benign).count();
        long loaded_malware_samples_symantec_count = file_info.values().parallelStream().filter(file_info -> !file_info.is_benign && file_info.family_name != null).count();
        global_parameters.logger.info("Loaded: " + loaded_malware_samples_count + " malware samples, Symantec detected: " + loaded_malware_samples_symantec_count);

        /* Map files that are not present in VT's database or that can't be disassembled correctly to
        other files with the same number of positives
        * */
        Dataset<Row> data = global_parameters.spark.read().parquet(parameters.data_parquet_file);
        // contains sha_256 of loaded FVs
        Set<String> vt_files = data.select("sha256").collectAsList().parallelStream()
                .map(row -> row.<String>getAs("sha256")).collect(Collectors.toSet());

        Map<String, String> sha256_to_id_t = file_info.values().parallelStream()
                .collect(Collectors.toMap(file_info -> file_info.sha2, file_info -> file_info.sha2_id));
        vt_files.retainAll(sha256_to_id_t.keySet()); // fixing file mismatch due to VT's errors, affects only a few files

        // <malware family, <samples>>
        Map<String, ArrayList<FileInfo>> vt_files_partitioning_malware_family = vt_files.parallelStream()
                .filter(sha256 -> file_info.get(sha256_to_id_t.get(sha256)).family_name != null).collect(Collectors.groupingBy(
                sha256 -> file_info.get(sha256_to_id_t.get(sha256)).family_name,
                Collectors.mapping(sha256 -> file_info.get(sha256_to_id_t.get(sha256)), Collectors.toCollection(ArrayList::new))));

        // <# of detections, <samples>>
        TreeMap<Integer, ArrayList<FileInfo>> vt_files_partitioning_detections = vt_files.parallelStream().collect(Collectors.groupingBy(
                sha256 -> file_info.get(sha256_to_id_t.get(sha256)).positives, TreeMap::new,
                Collectors.mapping(sha256 -> file_info.get(sha256_to_id_t.get(sha256)), Collectors.toCollection(ArrayList::new))));

        global_parameters.logger.info("File Mapping [family-based partitioning] Size: " + vt_files_partitioning_malware_family.size());
        global_parameters.logger.info("File Mapping [detections] Size: " + vt_files_partitioning_detections.size());
        global_parameters.logger.info("Original number of file_info structures: " + file_info.size());

        vt_files_partitioning_detections.forEach((key, value) -> global_parameters.logger.info("Positives: " + key + " Size: " + value.size()));
        Random rand = new Random(12345);
        String mappings_file_name = "results/file_info_mapping_" + parameters.data_file_count + ".csv";
        Map<String, String>  mapping_t = loadFileInfoMappingFile(mappings_file_name);
        boolean mapping_loaded = mapping_t != null;
        if (!mapping_loaded)
            mapping_t = new ConcurrentHashMap<>();
        final Map<String, String>  mapping = mapping_t;

        Stream.concat(sha256_to_id.entrySet().stream(), md5_to_id.entrySet().stream()).parallel().forEach(entry -> {
            int positive_rate = 0;
            String file_id = entry.getValue();
            if (!vt_files.contains(entry.getKey())) {
                FileInfo file_info = this.file_info.get(file_id);

                if (mapping_loaded) {
                    if (file_info == null) {
                        file_info = new FileInfo(0, -1, id_to_sha256.get(file_id), id_to_md5.get(file_id), file_id, null);
                        this.file_info.put(file_id, file_info);
                    }
                    file_info.redirect = this.file_info.get(mapping.get(file_info.sha2_id));
                } else {
                    // redirect to the same family
                    if (file_info != null && file_info.family_name != null
                            && !file_info.family_name.isEmpty() && vt_files_partitioning_malware_family.containsKey(file_info.family_name)) {
                        int index = rand.nextInt(vt_files_partitioning_malware_family.get(file_info.family_name).size());
                        file_info.redirect = vt_files_partitioning_malware_family.get(file_info.family_name).get(index);
                    } else {
                        if (file_info != null) { // redirecting based on the detection rate
                            positive_rate = Math.min(file_info.positives, vt_files_partitioning_detections.size() - 1);
                        } else {
                            file_info = new FileInfo(0, -1, id_to_sha256.get(file_id), id_to_md5.get(file_id), file_id, null);
                            this.file_info.put(file_id, file_info);
                        }
                        int index = rand.nextInt(vt_files_partitioning_detections.floorEntry(positive_rate).getValue().size());
                        file_info.redirect = vt_files_partitioning_detections.floorEntry(positive_rate).getValue().get(index);
                    }
                    mapping.put(file_info.sha2_id, file_info.redirect.sha2_id);
                }
            }
        });
        if (!mapping_loaded)
            saveFileInfoMappingFile(mapping, mappings_file_name);
    }

    // <map sha2_id, to sha2_id>
    private Map<String, String> loadFileInfoMappingFile(String file_name) {
        String line;
        Map<String, String> map = new ConcurrentHashMap<>();

        try (
                BufferedReader br = new BufferedReader(new FileReader(file_name));
        ) {
            while ((line = br.readLine()) != null) {
                String array[] = line.split(",");
                map.put(array[0], array[1]);
            }
        } catch (Exception e) {
            System.err.println(file_name + " not found");
            return null;
        }
        return map;
    }

    private void saveFileInfoMappingFile(Map<String, String> map, String file_name) {
        try (
                OutputStreamWriter stream_writer = new FileWriter(file_name);
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (Map.Entry<String, String> entry : map.entrySet())
                writer.write(entry.getKey() + "," + entry.getValue() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }


    public Set<String> getAllFileIDs() {
        Set<String> concurrent_set = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
        data.values().parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).forEach(node -> concurrent_set.add(node.sha2_id));
        return concurrent_set;
    }

    private void loadFileInfo(String file_name) {
        Boolean is_benign;
        String sha2, md5;
        String line;

        try (
            BufferedReader br = new BufferedReader(new FileReader(file_name));
        ) {
            while ((line = br.readLine()) != null) {
                if (line.startsWith("file_sha2_id"))
                    continue;
                String array[] = line.split(",");
                is_benign = array[1].isEmpty() ? null : (array[1].equals("b") ? true : false);
                sha2 = array[2].isEmpty() ? null : array[2];
                md5 = array.length < 4 || array[3].isEmpty() ? null : array[3];

                FileInfo file = new FileInfo(is_benign, sha2, md5);
                file_info.put(array[0], file);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private void loadInternalSymantecData(String file_name) {
        String line;

        try (BufferedReader br = new BufferedReader(new FileReader(file_name))) {
            while ((line = br.readLine()) != null) {
                if (line.startsWith("file_sha2_id"))
                    continue;
                String array[] = line.split(",");
                String sha2_id = array[0];
                double avg_score = Double.parseDouble(array[2]);
                Boolean is_benign = null;
                if (avg_score > 50)         // [-50; +50} gap
                    is_benign = true;
                else if (avg_score < -50)
                    is_benign = false;
                is_file_benign_symantec.put(sha2_id, is_benign);
                FileInfo file_info = this.file_info.get(sha2_id);
                if (file_info == null) {
                    file_info = new FileInfo(is_benign);
                    this.file_info.put(sha2_id, file_info);
                } else if (file_info.is_benign == null) {
                    file_info.is_benign = is_benign;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private void load_CCS_EvalSet(String file_name) {
        String line;

        try (BufferedReader br = new BufferedReader(new FileReader(file_name))) {
            while ((line = br.readLine()) != null) {
                if (line.startsWith("file_sha2_id"))
                    continue;
                String array[] = line.split(",");
                if (array.length < 2)
                    continue;
                String file_sha2_id = array[0];
                String time_stamp = array[1];
                FileInfo file_info = this.file_info.get(file_sha2_id);
                // missing around 37 hashes
                if (file_info == null)
                    continue;
                file_info_ccs_eval.add(file_sha2_id);
                file_info.is_benign = false;

                try {
                    Date time_stamp_date = format.parse(time_stamp);
                    file_info.vt_time_stamp = time_stamp_date.getTime() / 1000;
                } catch (ParseException e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    // file format: file_sha2_id, file_sha2
    // (at least one detection (VT) + appear in malekal.com)
    private void loadFileInfo_MalSha2(String file_name) {
        String line;

        try (
            BufferedReader br = new BufferedReader(new FileReader(file_name));
        ){
            while ((line = br.readLine()) != null) {
                if (line.startsWith("file_sha2_id"))
                    continue;
                String array[] = line.split(",");
                FileInfo file = new FileInfo(false, array[1], null);
                file_info.put(array[0], file);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private void loadDownloaderScores(String file_name) {
        String line;

        try (
            BufferedReader br = new BufferedReader(new FileReader(file_name));
        ) {
            while ((line = br.readLine()) != null) {
                if (line.startsWith("file_sha2_id"))
                    continue;
                String array[] = line.split(",");
                downloader_score.put(array[0], Integer.parseInt(array[1]));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    // File format: machine, label, root,<prediction>
    private void loadLabeledIGs(String file_name) {
        String line;

        try (
                BufferedReader br = new BufferedReader(new FileReader(file_name));
        ){
            while ((line = br.readLine()) != null) {
                if (line.startsWith("machine"))
                    continue;
                String array[] = line.split(",");
                String machine_id = array[0];
                String root_id = array[2];
                boolean is_benign_label = array[1].equals("b") ? true : false;
                ig_labels_ccs.putIfAbsent(machine_id, new HashMap<>());
                Map<String, Boolean> map = ig_labels_ccs.get(machine_id);
                map.put(root_id, is_benign_label);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private void splitGraphsIntoBenignAndMalicious() {
        benign_graphs = new ArrayList<>(100*1000);
        malicious_graphs = new ArrayList<>(100*1000);

        // must be a sequential stream
        ig_data.values().stream().limit(100*1000).flatMap(map ->
                map.values().stream()).forEach(graph -> {
            if (graph.isGraphBenign())
                benign_graphs.add(graph);
            else
                malicious_graphs.add(graph);
        });
    }

    public DefaultDirectedGraphWrapper pollGraphAtRandom(boolean benign_graph, boolean use_ig_data) {
        DefaultDirectedGraphWrapper graph;
        ArrayList<DefaultDirectedGraphWrapper> ref;
        Random random;

        ref = benign_graph ? benign_graphs : malicious_graphs;
        if (ref.isEmpty())
            return null;

        random = new Random(12345);
        int size = ref.size();
        int rand = random.nextInt(size);
        graph = ref.get(rand);
        ref.set(rand, ref.get(ref.size() - 1));
        ref.remove(ref.size() - 1);
        if (use_ig_data)
            ig_data.get(graph.machine_id).remove(graph.root_node);
        else
            data.remove(graph.machine_id);
        return graph;
    }

    public Map<Long, Set<DefaultDirectedGraphWrapper>> getMachineIGs(Set<Long> machine_ids) {
        Map<Long, Set<DefaultDirectedGraphWrapper>> map = new HashMap<>();

        for (long machine_id : machine_ids) {
            Set<DefaultDirectedGraphWrapper> set = ig_data.get(machine_id).entrySet()
                    .stream().map(entry -> entry.getValue()).collect(Collectors.toSet());
            map.put(machine_id, set);
        }
        return map;
    }

    public Long getNextSimulationTimeStamp(long current_time) {
        long simulation_period = ConfigurationParameters.getInstance().shift_rolling_time_window;
        int min_node_increment = ConfigurationParameters.getInstance().min_node_count_increment;
        long time_stamp = current_time + simulation_period;
        Long key = time_stamps.higherKey(current_time);
        if (key == null)
            return null;

        SortedMap<Long, Node> time_stamps_tail = time_stamps.tailMap(key);
        int node_count = 0; // an estimate because we don't handle collisions properly

        for (Map.Entry<Long, Node> entry : time_stamps_tail.entrySet()) {
            node_count++;
            if (entry.getKey() > time_stamp && node_count > min_node_increment) {
                time_stamp = entry.getKey();
                break;
            }
        }
        return time_stamp;
    }

    public Long getEarlyDetectionTime_CCS_Evaluation(String sha2_id, long current_time_stamp) {
        if (!file_info_ccs_eval.contains(sha2_id))
            return null;

        FileInfo file_info = this.file_info.get(sha2_id);
        if (file_info == null)
            return null;
        return file_info.vt_time_stamp - current_time_stamp;
    }

    public Long getEarlyDetectionTime_FullEvaluation(String file_name, long current_time_stamp) {
        FileInfo file_info = this.file_info.get(file_name);
        if (file_info == null || file_info.is_benign)
            return null;
        return file_info.vt_time_stamp - current_time_stamp;
    }

    // <median (days), average (days), samples_used, samples_ignored>
    private Triple<Double, Double, Pair<Integer, Integer>> getEarliestPossibleDetection(EARLY_DETECTION_TIME_TYPE type) {
        int ignore_counter = 0;
        List<Double> unix_early_detection_times = new LinkedList<>();

        for (String sha2_id : file_info_ccs_eval) {
            FileInfo file_info = this.file_info.get(sha2_id);
            if (file_info.ig_detection_time != null) {
                Long time = type == EARLY_DETECTION_TIME_TYPE.IG_DETECTION ? file_info.ig_detection_time : file_info.earliest_wine_time;
                if (time == null)
                    continue;
                long diff = file_info.vt_time_stamp - time;
                if (diff <= 0) {
                    ignore_counter ++;
                    continue;
                }
                unix_early_detection_times.add((double) diff);
            }
        }

        int sample_size = unix_early_detection_times.size();
        List<Double> date_early_detection_times = unix_early_detection_times.stream()
                .map(item -> (double) Utils.convertUnixTimeStampToDays(item)).collect(Collectors.toList());
        String suffix = type == EARLY_DETECTION_TIME_TYPE.IG_DETECTION ? "IG" : "WINE";
        Utils.dumpCollectionToCSV(date_early_detection_times, "results/potential_early_detection_times_" + suffix + ".csv");
        double d[] = ArrayUtils.toPrimitive(date_early_detection_times.toArray(new Double[0]));
        double median_early_detection_time = (new Percentile()).evaluate(d, 50);
        double avg_early_detection_time = date_early_detection_times.stream()
                .mapToDouble(Double::valueOf).average().getAsDouble();
        return new ImmutableTriple<>(median_early_detection_time, avg_early_detection_time, new ImmutablePair<>(sample_size, ignore_counter));
    }

    public Map<String, FileInfo> getFileInfoMap() {
        if (file_info.isEmpty())
            loadVTGroundTruth(ConfigurationParameters.getInstance().vt_info, true);
        return file_info;
    }

    public Map<String, FileInfo> getFileInfoMapWithoutMapping() {
        if (file_info.isEmpty())
            loadVTGroundTruth(ConfigurationParameters.getInstance().vt_info, false);
        return file_info;
    }

    public Set<String> getFileInfoCCSEvaluationMap() {
        return file_info_ccs_eval;
    }

    public Set<String> getFileInfoFullEvaluationMap() {
        return file_info_full_eval;
    }

    private Set<String> getAllDownloaders() {
        return ig_data.values().parallelStream().flatMap(map -> map.values()
                .stream()).map(graph -> graph.root_node.sha2_id).collect(Collectors.toSet());
    }

    public void printStatistics() {
        Triple<Double, Double, Pair<Integer, Integer>> ig_detection = getEarliestPossibleDetection(EARLY_DETECTION_TIME_TYPE.IG_DETECTION);
        Triple<Double, Double, Pair<Integer, Integer>> best_possible_detection = getEarliestPossibleDetection(EARLY_DETECTION_TIME_TYPE.BEST_POSSIBLE_DETECTION);
        Set<String> malicious_downloaders = new HashSet(file_info_ccs_eval);
        Set<String> detected_malicious_downloaders = ig_data.values().stream().flatMap(map -> map.entrySet().stream())
                .filter(entry -> !entry.getValue().isGraphBenign()).map(entry -> entry.getKey()).collect(Collectors.toSet());
        malicious_downloaders.retainAll(detected_malicious_downloaders);

        System.out.println("Loaded " + data.size() + " Downloader graphs");
        System.out.println(time_stamps.size() + " different time stamps");
        System.err.println("Detected malicious downloaders:" + malicious_downloaders.size()
                + " out of " + file_info_ccs_eval.size());
        System.out.println("IG Early Detection Time: median: " + ig_detection.getLeft()
                + " days, average: " + ig_detection.getMiddle() + " days, (computed over "
                + ig_detection.getRight().getLeft() + " IGs) (ignored: " + ig_detection.getRight().getRight() + ")");
        System.out.println("Best possible Early Detection Time: median: " + best_possible_detection.getLeft()
                + " days, average: " + best_possible_detection.getMiddle() + " days, (computed over "
                + best_possible_detection.getRight().getLeft() + " IGs) (ignored: " + best_possible_detection.getRight().getRight() + ")");
    }

    public void printNodeNames() {
        {
            TreeSet<String> set = new TreeSet<>();

            for (DirectedGraph<Node, Edge> graph : data.values()) {
                Set<Node> nodes = graph.vertexSet();

                for (Node node : nodes)
                    if (node.file_name != null)
                        set.add(node.file_name);
            }

            if (ConfigurationParameters.getInstance().print_file_names) {
                for (String name : set)
                    System.out.println(name);
            }
            System.out.println("# of unique files " + set.size());
        }

        {
            Set<Node> set_t = time_stamps.values().stream().collect(Collectors.toSet());
            System.out.println("# of unique nodes " + set_t.size());
        }

        {
            Set<String> set_t = time_stamps.values().stream()
                    .map(node -> node.sha2_id).collect(Collectors.toSet());
            System.out.println("# of unique node_ids " + set_t.size());
        }

        {
            // <node, machine_id>
            Map<Node, Set<Long>> map = new HashMap<>();
            for (Node node : time_stamps.values()) {
                Set<Long> s = map.get(node);
                if (s == null)
                    s = new HashSet<>();
                s.add(node.machine_id);
                map.put(node, s);
            }

            for (Map.Entry<Node, Set<Long>> entry : map.entrySet()) {
                if (entry.getValue().size() > 1000)
                    System.out.println(entry.getValue().size() + "  :  " + entry.getKey());
            }
        }
    }
}
