package edu.ut.austin.DataInputOutput;

import edu.ut.austin.FeatureExtractor.FeatureExtractor;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.GraphClasses.Node;
import edu.ut.austin.ML_Models.TrainML_Models;
import org.apache.commons.lang3.tuple.*;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import org.nustaq.serialization.FSTConfiguration;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Mikhail on 2/6/2017.
 */
public class RF_TrainingProcedure {
    // <machine_id, graph>
    private List<DefaultDirectedGraphWrapper> complete_graphs;
    List<Triple<Integer, Integer, DefaultDirectedGraphWrapper>> sliced_graphs;
    private FeatureExtractor feature_extractor;
    private GraphDataLoader loader;

    public RF_TrainingProcedure(GraphDataLoader loader) {
        complete_graphs = new LinkedList<>();
        sliced_graphs = new LinkedList<>();
        this.loader = loader;
    }

    public void trainGraphLevelRF_Detector() {
        if (!ConfigurationParameters.getInstance().train_GraphRF)
            return;

        selectGraphs();
        feature_extractor = new FeatureExtractor(complete_graphs);
        saveGraphScalingParameters();
        writeDataToFile();
        TrainML_Models.trainGraphRF_Model();
        GlobalParameters.getInstance().logger.info("Training Data Dumped");
    }

    private void selectGraphs() {
        int benign_graph_count, malicious_graph_count;
        ConfigurationParameters parameters;
        GlobalParameters global_parameters;
        Set<Long> machine_ids = new HashSet<>();

        benign_graph_count = 0;
        malicious_graph_count = 0;
        parameters = ConfigurationParameters.getInstance();
        global_parameters = GlobalParameters.getInstance();

        while (benign_graph_count < parameters.RF_training_data_min_size
                || malicious_graph_count < parameters.RF_training_data_min_size) {

            boolean benign_label = benign_graph_count < malicious_graph_count;
            DefaultDirectedGraphWrapper graph = loader.pollGraphAtRandom(benign_label, true);
            if (graph == null) {
                global_parameters.logger.info("Not enough " + (benign_label ? "benign" : "malicious") + " graphs");
                System.exit(-1);
            }
            machine_ids.add(graph.machine_id);
            complete_graphs.add(graph);

            Pair<Integer, Integer> pair = partitionGraphs(graph);
            benign_graph_count += pair.getLeft();
            malicious_graph_count += pair.getRight();
        }

        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(parameters.selected_machines_for_training_GraphRF), "utf-8"))) {
            for (Long hash : machine_ids)
                writer.write(hash + "\n");
        } catch (IOException e) {
            global_parameters.logger.log(Level.SEVERE, "Can't write to " + parameters.selected_machines_for_training_GraphRF);
            System.exit(-1);
        }
        System.out.format("RF_training. %d benign graphs and %d malicious graphs\n", benign_graph_count, malicious_graph_count);
    }

    // <fp fvs, tp fvs>
    private Pair<Integer, Integer> getGraphParameters(DefaultDirectedGraphWrapper graph) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        int benign_node_count = graph.getBenignNodeCount();
        int malicious_node_count = graph.getMaliciousNodeCount();

        int fp_count = (int) (benign_node_count*parameters.benign_trace_length*parameters.LD_FP_rate);
        int tp_count = (int) (malicious_node_count*parameters.malicious_trace_length*parameters.LD_TP_rate);
        return new ImmutablePair<>(fp_count, tp_count);
    }

    // <benign_graph_count, malicious_graph_count>
    private Pair<Integer, Integer> partitionGraphs(DefaultDirectedGraphWrapper graph) {
        ConfigurationParameters params;
        DefaultDirectedGraphWrapper graph_slice;
        MutablePair<Integer, Integer> pair;
        Pair<Integer, Integer> graph_properties;

        pair = new MutablePair<>(0, 0);
        params = ConfigurationParameters.getInstance();
        long time_stamps[] = graph.vertexSet().stream().mapToLong(node -> node.time_stamp).sorted().toArray();
        int max_partition_count = Math.min(params.RF_training_data_parts_count, time_stamps.length);
        float items_per_step = (float) time_stamps.length / max_partition_count;

        for (int i = 1; i < max_partition_count; i ++) {
            int time_stamp_index = (int) (i*items_per_step);
            long time_stamp = time_stamps[time_stamp_index];

            if (!graph.isGraphSliceable(time_stamp))
                continue;
            graph_slice = (DefaultDirectedGraphWrapper) graph.clone();
            graph_slice.sliceGraph(time_stamp);

            if (graph_slice.isGraphBenign())
                pair.setLeft(pair.getLeft() + 1);
            else
                pair.setRight(pair.getRight() + 1);
            graph_properties = getGraphParameters(graph_slice);
            sliced_graphs.add(new ImmutableTriple<>(graph_properties.getLeft(), graph_properties.getRight(), graph_slice));
        }

        graph_properties = getGraphParameters(graph);
        sliced_graphs.add(new ImmutableTriple<>(graph_properties.getLeft(), graph_properties.getRight(), graph));
        if (graph.isGraphBenign())
            pair.setLeft(pair.getLeft() + 1);
        else
            pair.setRight(pair.getRight() + 1);
        return pair;
    }

    @Deprecated
    private DefaultDirectedGraphWrapper getGraphSlice(DefaultDirectedGraphWrapper graph, long time_stamp) {
        DefaultDirectedGraphWrapper sliced_graph = (DefaultDirectedGraphWrapper) graph.clone();
        // remove "future" vertices and touching them edges
        List<Node> nodes_to_remove = sliced_graph.vertexSet().stream()
                .filter(node -> node.time_stamp > time_stamp).collect(Collectors.toList());

        // removing entire graph
        if (graph.vertexSet().size() == nodes_to_remove.size())
            return null;

        for (Node node : nodes_to_remove)
            sliced_graph.removeVertex(node);
        return sliced_graph;
    }

    private void saveGraphScalingParameters() {
        final int size = 16;
        ArrayList<List<Double>> fvs = new ArrayList<>(size);
        ArrayList<Pair<Double, Double>> feature_scaling = new ArrayList<>(size);
        IntStream.range(0, size).forEach(i -> fvs.add(new LinkedList()));

        for (Triple<Integer, Integer, DefaultDirectedGraphWrapper> entry : sliced_graphs) {
            double feature_vector[] = entry.getRight().getFeatureVector(feature_extractor);
            for (int i = 0; i < feature_vector.length; i ++)
                fvs.get(i).add(feature_vector[i]);
        }

        for (int i = 0; i < size && !fvs.get(i).isEmpty(); i ++) {
            double array[] = fvs.get(i).stream().mapToDouble(d -> d).toArray();
            double min = (new Percentile()).evaluate(array, 1);
            double max = (new Percentile()).evaluate(array, 99);
            feature_scaling.add(new ImmutablePair<>(min, max));
        }

        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        global_parameters.logger.info("Serializing graph_scaling_parameters object");
        FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();
        byte serialized_object[] = conf.asByteArray(feature_scaling);

        try (
                OutputStream file = new FileOutputStream(parameters.graph_scaling_parameters);
                OutputStream output = new BufferedOutputStream(file);
        ) {
            output.write(serialized_object);
        } catch (IOException ex) {
            global_parameters.logger.log(Level.SEVERE, "Cannot serialize graph_scaling_parameters", ex);
            System.exit(-1);
        }
    }

    private void writeDataToFile() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        try (
            Writer writer_matlab_classification = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(parameters.GRF_Matlab_data), "ascii"));
            Writer writer_libsvm_classification = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(parameters.GRF_LibSVM_data), "ascii"));
            Writer writer_matlab_cluster = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(parameters.NBD_Matlab_data), "ascii"));
            Writer writer_graph_id = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(parameters.GRF_graph_ids), "ascii"));
        ) {
            for (Triple<Integer, Integer, DefaultDirectedGraphWrapper> entry : sliced_graphs) {
                char label = entry.getRight().isGraphBenign() ? '0' : '1';
                // init feature vectors
                double feature_vector[] = entry.getRight().getFeatureVector(feature_extractor);
                writer_graph_id.write(entry.getRight().machine_id + "," + entry.getRight().root_node + "\n");
                String features = Arrays.stream(feature_vector).mapToObj(Double::toString)
                        .collect(Collectors.joining(","));
                writer_matlab_classification.write(label + "," + features + "\n");

                writer_libsvm_classification.write(label);
                for (int i = 0; i < feature_vector.length; i ++) {
                    if (feature_vector[i] != 0.0)
                        writer_libsvm_classification.write(" " + (i + 1) + ":" + feature_vector[i]);
                }
                writer_libsvm_classification.write("\n");

                // fp_count, tp_count, FV
                writer_matlab_cluster.write(entry.getLeft() + "," + entry.getMiddle() + ",");
                writer_matlab_cluster.write(features + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
