package edu.ut.austin.DataInputOutput;

import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.RealTimeSimulation.ShapeGD;
import edu.ut.austin.RealTimeSimulation.ShapeGD_Matlab;
import edu.ut.austin.RealTimeSimulation.ShapeGD_Spark;
import org.apache.commons.math3.util.Pair;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Level;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 2/8/2017.
 */
public class ShapeGD_DataLoader {
    public static ShapeGD load() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        switch (parameters.LD_type) {
            case SYSCALLS:
                return loadShapeGD_Matlab();
            case STATIC_FILE_SCANNER:
            case SIMULATED_STATIC_FILE_SCANNER:
                return loadShapeGD_Spark(false);
            default:
                return null;
        }
    }

    public static ShapeGD_Matlab loadShapeGD_Matlab() {
        ShapeGD_Matlab.Data benignware = null;
        ShapeGD_Matlab.Data malware = null;
        Dataset<Row> data = null;

        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        int suffix = parameters.min_fv_count_for_detection;
        String dir_output_name = "results" + File.separator + "features";

        double ref_hist[][] = load2DMatrixCSV("data/GD/GD_" + suffix + "/GD_ref_hist.csv");
        double edges[][] = load2DMatrixCSV("data/GD/GD_" + suffix + "/GD_edges.csv");
        double dim_weight[] = load1DVectorCSV("data/GD/GD_" + suffix + "/GD_dim_weight.csv");
        double thresholds_vector[] = load1DVectorCSV("data/GD/GD_" + suffix + "/GD_thresholds.csv");
        double pca_mean[] = load1DVectorCSV("data/GD/GD_" + suffix + "/GD_pca_mean.csv");
        double pca_basis[][] = load2DMatrixCSV("data/GD/GD_" + suffix + "/GD_pca_basis.csv");

        switch (parameters.LD_type) {
            case SYSCALLS:
                benignware = loadData("data/GD/benignware_indices.csv", "data/GD/benignware_data.csv");
                malware = loadData("data/GD/malware_indices.csv", "data/GD/malware_data.csv");
                break;
            case STATIC_FILE_SCANNER:
            case SIMULATED_STATIC_FILE_SCANNER:
                String parquet_file_name = dir_output_name + File.separator + "alert_FVs.parquet";
                try {
                    data = global_parameters.spark.read().parquet(parquet_file_name);
                } catch (Exception e) {
                    global_parameters.logger.severe(parquet_file_name + " is not found");
                    System.exit(-1);
                }
                break;
        }
        TreeMap<Integer, double[]> thresholds = new TreeMap<>();
        thresholds.put(500, thresholds_vector); //TODO needs to be fixed
        return new ShapeGD_Matlab(ref_hist, edges, dim_weight, thresholds, pca_mean, pca_basis, benignware, malware);
    }

    public static ShapeGD_Spark loadShapeGD_Spark(boolean load_shape_gd_only) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        String parquet_file_name = parameters.data_parquet_file;
        ShapeGD_Spark shape_gd_spark = ShapeGD_Spark.load();
        Dataset<Row> data = null;

        if (load_shape_gd_only)
            return shape_gd_spark;

        if (shape_gd_spark == null)
            return null;

        try {
            data = global_parameters.spark.read().parquet(parquet_file_name).drop("scaled_features").drop("pca_features");
        } catch (Exception e) {
            global_parameters.logger.severe(parquet_file_name + " is not found");
            System.exit(-1);
        }
        data = data.filter("LD > " + parameters.LD_threshold);

        try {
            shape_gd_spark.initData(data);
        } catch (NullPointerException e) {
            global_parameters.logger.log( Level.SEVERE, "shape_gd_spark was not found", e);
            System.exit(-1);
        } catch (Exception e) {
            global_parameters.logger.log( Level.SEVERE, "Probably need to increase 'ulimit -n'", e);
            System.exit(-1);
        }
        return shape_gd_spark;
    }

    private static double[][] load2DMatrixCSV(String file_name) {
        String line;
        int row_id = 0;
        double data[][] = null;

        // assumes that the first and the last values are -Inf and + Inf
        try (BufferedReader br = new BufferedReader(new FileReader(file_name))) {
            int row_count = (int) Files.lines(Paths.get(file_name)).count();
            data = new double[row_count][];

            while ((line = br.readLine()) != null)
                data[row_id ++] = Stream.of(line.split(",")).mapToDouble(Double::parseDouble).toArray();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return data;
    }

    private static double[] load1DVectorCSV(String file_name) {
        String line;
        double data[] = null;

        try (BufferedReader br = new BufferedReader(new FileReader(file_name))) {
            while ((line = br.readLine()) != null) {
                data = Stream.of(line.split(",")).mapToDouble(Double::parseDouble).toArray();
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return data;
    }

    private static ShapeGD_Matlab.Data loadData(String index_file_name, String data_file_name) {
        String line;
        int row_id, row_count;
        boolean fv_indices[] = null;
        double data[][] = null;
        Pair<boolean[], double[][]> pair;

        try (BufferedReader br = new BufferedReader(new FileReader(index_file_name))) {
            row_id = 0;
            row_count = (int) Files.lines(Paths.get(index_file_name)).count();
            fv_indices = new boolean[row_count];

            while ((line = br.readLine()) != null)
                fv_indices[row_id ++] = line.equals("1");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        try (BufferedReader br = new BufferedReader(new FileReader(data_file_name))) {
            row_id = 0;
            row_count = (int) Files.lines(Paths.get(data_file_name)).count();
            data = new double[row_count][];

            while ((line = br.readLine()) != null)
                data[row_id ++] = Stream.of(line.split(",")).mapToDouble(Double::parseDouble).toArray();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return new ShapeGD_Matlab.Data(fv_indices, data);
    }
}
