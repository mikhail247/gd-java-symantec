package edu.ut.austin.GraphViz;

import javax.swing.*;

import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GraphClasses.Edge;
import edu.ut.austin.GraphClasses.Node;
import org.jgrapht.DirectedGraph;
import org.jgrapht.ext.DOTExporter;
import org.jgrapht.ext.JGraphXAdapter;
import org.jgrapht.graph.DefaultWeightedEdge;
import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.swing.mxGraphComponent;

import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Set;

public class GraphViz {
    private static final Dimension DEFAULT_SIZE = new Dimension(2500, 2000);
    private volatile static DirectedGraph<Node, Edge> graph;
    private volatile static JFrame frame = null;

    private static void createAndShowGui() {
        frame = new JFrame("Graph Visualization");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        JGraphXAdapter<Node, Edge> graphAdapter = new JGraphXAdapter<>(graph);
        mxIGraphLayout layout = new mxCircleLayout(graphAdapter);
        layout.execute(graphAdapter.getDefaultParent());
        frame.add(new mxGraphComponent(graphAdapter));
        frame.pack();
        frame.setLocationByPlatform(true);
        frame.setSize(DEFAULT_SIZE);
        setUIFont(new javax.swing.plaf.FontUIResource("Tahoma",Font.PLAIN,24));;
        frame.setVisible(true);
    }

    private static void setUIFont(javax.swing.plaf.FontUIResource f)
    {
        java.util.Enumeration keys = UIManager.getDefaults().keys();
        while(keys.hasMoreElements())
        {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if(value instanceof javax.swing.plaf.FontUIResource) UIManager.put(key, f);
        }
    }

    public static void visualize(Collection<DirectedGraph<Node, Edge>> graphs, int graphs_num) {
        for (DirectedGraph<Node, Edge> graph : graphs) {
            if (graphs_num-- <= 0)
                break;
            visualize(graph);
            prettyPrintGraph(graph);
        }
    }

    public static void visualize(DirectedGraph<Node, Edge> graph) {
        if (!ConfigurationParameters.getInstance().run_visualization)
            return;

        GraphViz.graph = graph;
        try {
            if (frame != null) {
                frame.dispose();
            }
            SwingUtilities.invokeAndWait(new Runnable() {
                public void run() {
                    createAndShowGui();
                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static class MyEdge extends DefaultWeightedEdge {
        @Override
        public String toString() {
            return String.valueOf(getWeight());
        }
    }

    public static void exportToDOT(DirectedGraph<Node, Edge> graph, String file_name) {
        DOTExporter exporter = new DOTExporter();

        try {
            exporter.exportGraph(graph, new FileWriter(file_name));
        }catch (IOException e){}
    }

    public static void prettyPrintGraph(Collection<DirectedGraph<Node, Edge>> collection, int graphs_num) {
        for (DirectedGraph<Node, Edge> graph : collection) {
            if (graphs_num-- <= 0)
                break;
            prettyPrintGraph(graph);
        }
    }

    public static void prettyPrintGraph(DirectedGraph<Node, Edge> graph) {
        Set<Node> nodes = graph.vertexSet();
        System.out.println(graph.hashCode() + "  :  " + nodes);
    }
}
