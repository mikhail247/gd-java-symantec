package edu.ut.austin.GraphClasses;

import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mikhail on 1/26/2017.
 */
public class Node implements Serializable {
    private int hash = -1;
    private final static Date start_date;
    private final static Pattern pattern;
    private final static ThreadLocal<DateFormat> format;
    private Date date;
    private ConfigurationParameters.VT_FileType is_executable;

    public String sha2_id;  // node unique id
    public String file_name;
    public long time_stamp; // sec
    public long machine_id;
    public FileInfo file_info; //reference to an object containing file info
    public int score;
    public Double RFC_score;
    public Set<String> incoming_url_edges, outgoing_url_edges; // incoming and outgoing edges

    static {
        Calendar calender =  new GregorianCalendar(2008, 1, 1);
        start_date = calender.getTime();
        pattern = Pattern.compile("\\[\\d+\\]");
        // ex. 2010-08-08 12:52:41
        format = new ThreadLocal<DateFormat>() {
            @Override
            protected SimpleDateFormat initialValue() {
                return  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.ENGLISH);
            }
        };
    }

    /*
     * Skip nodes that have 'N/A' in time_stamp field
     */
    public Node(String node_id, String file_name, String date, long machine_id, FileInfo file_info, int score) throws ParseException {
        this.sha2_id = node_id;
        this.machine_id = machine_id;
        this.file_info = file_info;
        this.score = score;
        this.date = null;
        this.time_stamp = 0;
        this.RFC_score = null;
        this.is_executable = ConfigurationParameters.VT_FileType.EXECUTABLE;

        // ex. javasetup7u6[1].exe --> javasetup7u6.exe
        Matcher matcher = pattern.matcher(file_name);
        file_name = matcher.replaceAll("");
        this.file_name = file_name.isEmpty() || file_name.equals("N/A") ? null : file_name;

        try {
            if (date != null && !date.isEmpty() && !date.equals("N/A"))
                this.date = format.get().parse(date);
        } catch (ParseException e) {
            if (ConfigurationParameters.getInstance().debug_info)
                e.printStackTrace();
            throw e;
        }
        if (this.date != null) {
            assert (this.date.after(start_date));
            if (!this.date.after(start_date)) {
                System.out.println("Wrong date: " + this.date);
                this.date = new Date(start_date.getTime());
            }
            time_stamp = this.date.getTime() / 1000;
        }
        updateExecutableInfo();
    }

    /*  sets executable property based on either VT report or based on the extension
        Extension-based method is only used for benign files
        due to being benign, those files have no intention to hide their real extension
        and this method is not invoked for malicious files, because their type
        is approximated using VT as a ground truth
    * */
    private void updateExecutableInfo() {
        if (!ConfigurationParameters.getInstance().remove_non_exec_nodes)
            return;

        if (file_info.hasVT_Info() && file_info.getExecutableProperty() != ConfigurationParameters.VT_FileType.UNKNOWN)
            is_executable = file_info.getExecutableProperty();
        else if (file_name != null && (file_name.endsWith(".exe") || file_name.endsWith(".dll") &&
                file_name.endsWith(".msi") && file_name.endsWith(".bin") && file_name.endsWith(".so")
                && file_name.endsWith(".com")))
            this.is_executable = ConfigurationParameters.VT_FileType.EXECUTABLE;
        else
            this.is_executable = ConfigurationParameters.VT_FileType.NON_EXECUTABLE;
    }

    public ConfigurationParameters.VT_FileType getExecutableProperty() {
        return is_executable;
    }

    public boolean isExecutable() {
        return is_executable == ConfigurationParameters.VT_FileType.EXECUTABLE;
    }

    public void setNonExecutable() {
        is_executable = ConfigurationParameters.VT_FileType.NON_EXECUTABLE;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
        time_stamp = this.date.getTime() / 1000;
    }

    public boolean isBenign() {
        return file_info == null || file_info.is_benign;
    }

    public boolean hasVT_Info() {
        return file_info == file_info.redirect;
    }
    public boolean isMalicious() {
        return !isBenign();
    }

    public int hashCode() {
        if (hash != -1)
            return hash;
        hash = 17;
        hash = 31*hash + sha2_id.hashCode();
        hash = 31*hash + Long.hashCode(time_stamp);
        return hash;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Node))
            return false;
        Node node_obj = (Node) obj;
        return this.time_stamp == time_stamp && this.sha2_id.equals(node_obj.sha2_id);
    }

    @Override
    public String toString() {
        return String.valueOf(sha2_id) + ":" + file_name
                + ":" + isBenign() + ":date " + date;
    }
}
