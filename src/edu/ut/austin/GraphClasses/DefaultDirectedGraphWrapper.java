package edu.ut.austin.GraphClasses;

import edu.ut.austin.FeatureExtractor.FeatureExtractor;
import edu.ut.austin.Introspection.IntrospectionModule;
import edu.ut.austin.ML_Models.SparkRandomForestWrapper;
import edu.ut.austin.ML_Models.TrainML_Models;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.jgrapht.EdgeFactory;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.traverse.BreadthFirstIterator;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Mikhail on 2/4/2017.
 */
public class DefaultDirectedGraphWrapper extends DefaultDirectedGraph<Node, Edge> implements Serializable {
    private Boolean is_benign_propagated_label = null;
    private double feature_vector[] = null;
    private double feature_vector_non_global[] = null;
    public Node root_node;
    public final long machine_id;

    private long min_time_stamp, max_time_stamp;
    private static SparkRandomForestWrapper graph_level_detector;

    static {
        graph_level_detector = TrainML_Models.loadGraphRF_Model();
    }

    public DefaultDirectedGraphWrapper(long machine_id) {
        super(Edge.class);
        this.min_time_stamp = -1;
        this.max_time_stamp = -1;
        this.root_node = null;
        this.machine_id = machine_id;
    }

    public DefaultDirectedGraphWrapper(EdgeFactory<Node, Edge> ef) {
        super(ef);
        this.min_time_stamp = -1;
        this.max_time_stamp = -1;
        this.root_node = null;
        this.machine_id = -1;
    }

    public void setRootNode(String root_node_id) {
        for (Node node : vertexSet()) {
            if (root_node_id.equals(node.sha2_id)) {
                root_node = node;
                break;
            }
        }
    }

    @Override
    public boolean addVertex(Node node) {
        return super.addVertex(node);
    }

    public void setTimeSpan() {
        LongSummaryStatistics stat = vertexSet().stream()
                .mapToLong(node -> node.time_stamp).summaryStatistics();
        min_time_stamp = stat.getMin();
        max_time_stamp = stat.getMax();
    }

    @Override
    public Object clone() {
        DefaultDirectedGraphWrapper obj = (DefaultDirectedGraphWrapper) super.clone();
        return obj;
    }

    public long getMachineID() {
        return machine_id;
    }
    public double[] getFeatureVector(FeatureExtractor feature_extractor) {
        if (feature_vector != null) {
            IntrospectionModule.getInstance().existing_fvs_count.getAndIncrement();
            return feature_vector;
        }
        IntrospectionModule.getInstance().computed_fvs_count.getAndIncrement();
        feature_vector = feature_extractor.extractFeatures(this);
        return feature_vector;
    }

    public void setBenignLabel(Boolean label) {
        is_benign_propagated_label = label;
    }

    public Node getRootNode() {
        return root_node;
    }

    /*
     * A DG graph is considered malicious if it contains at least one malicious
     * IG, which is equivalent to having at least one malicious node
    * */
    @Deprecated
    public boolean isGraphBenignOld() {
        if (is_benign_propagated_label != null)
                return is_benign_propagated_label;
        propagateNodeLabelsToDownloaderGraphs();
        return is_benign_propagated_label;
    }

    public boolean isGraphBenign() {
        return vertexSet().stream().allMatch(Node::isBenign);
    }

    public boolean isGraphMalicious() {
        return vertexSet().stream().anyMatch(Node::isMalicious);
    }

    private void propagateNodeLabelsToDownloaderGraphs() {
        Set<Node> node_set = vertexSet();
        is_benign_propagated_label = !node_set.stream().anyMatch(node -> !node.isBenign());
    }

    public int getMaliciousNodeCount() {
        return (int) vertexSet().stream().filter(node -> !node.isBenign()).count();
    }

    public int getBenignNodeCount() {
        return vertexSet().size() - getMaliciousNodeCount();
    }

    public Set<String> getMaliciousNodeIDs() {
        return vertexSet().stream().filter(node -> !node.isBenign())
                .map(node -> node.sha2_id).collect(Collectors.toSet());
    }

    public Set<String> getBenignNodeIDs() {
        return vertexSet().stream().filter(Node::isBenign)
                .map(node -> node.sha2_id).collect(Collectors.toSet());
    }

    public boolean isGraphEmpty() {
        return vertexSet().isEmpty();
    }

    public boolean containsSubGraph(DefaultDirectedGraphWrapper graph) {
        Set<Node> nodes = vertexSet();
        Set<Node> graph_nodes = graph.vertexSet();
        if (!nodes.containsAll(graph_nodes))
            return false;

        Set<Edge> edges = edgeSet();
        Set<Edge> graph_edges = graph.edgeSet();
        if (!edges.containsAll(graph_edges))
            return false;

        return true;
    }

    public int containsSubGraph(Collection<DefaultDirectedGraphWrapper> graphs) {
        int counter = 0;

        for (DefaultDirectedGraphWrapper graph : graphs)
            if (containsSubGraph(graph))
                counter ++;
        return counter;
    }

    public void subtractGraph(DefaultDirectedGraphWrapper graph) {
        if (graph.isGraphEmpty())
            return;

        Set<Edge> graph_edges = graph.edgeSet();
        removeAllEdges(graph_edges);

        for (Node node : graph.vertexSet()) {
            try {
                if (edgesOf(node).isEmpty())
                    removeVertex(node);
            } catch (IllegalArgumentException e) {
                System.out.println(e);
                System.exit(-1);
            }
        }
        is_benign_propagated_label = null;
        feature_vector = null;
    }

    public boolean isGraphSliceable(long current_time) {
        return min_time_stamp <= current_time;
    }

    public boolean isGraphSliceable(long start_time, long end_time) {
        if (max_time_stamp < start_time || min_time_stamp > end_time)
            return false;
        return true;
    }

    public boolean sliceGraph(long current_time) {
        // remove "future" vertices and touching them edges
        Set<Node> nodes_to_remove = vertexSet().stream()
                .filter(node -> node.time_stamp > current_time).collect(Collectors.toSet());
        boolean modified = removeAllVertices(nodes_to_remove);
        if (modified) {
            setTimeSpan();
            is_benign_propagated_label = null;
            feature_vector = null;
            feature_vector_non_global = null;
        }
        return modified;
    }

    public boolean sliceGraph(long start_time, long end_time) {
        // remove "future" vertices and touching them edges
        Set<Node> nodes_to_remove = vertexSet().stream()
                .filter(node -> node.time_stamp < start_time || node.time_stamp > end_time).collect(Collectors.toSet());
        boolean modified = removeAllVertices(nodes_to_remove);
        if (modified) {
            setTimeSpan();
            is_benign_propagated_label = null;
            feature_vector = null;
            feature_vector_non_global = null;
        }
        return modified;
    }

    public long getAvgTimeStamp() {
        return (long) vertexSet().stream().mapToLong(node -> node.time_stamp).average().getAsDouble();
    }

    public Long getEarliestMaliciousTimeStamp() {
        OptionalLong ret = vertexSet().stream().filter(node -> !node.isBenign()).mapToLong(node -> node.time_stamp).min();
        return ret.isPresent() ? ret.getAsLong() : null;
    }

    public long getMinTimeStamp() {
        return min_time_stamp;
    }

    public long getMaxTimeStamp() {
        return max_time_stamp;
    }
}
