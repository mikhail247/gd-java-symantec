package edu.ut.austin.GraphClasses;
import edu.ut.austin.Utils.Utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mikhail on 1/26/2017.
 */
public class Edge implements Serializable {
    private int hash = -1;
    public String url, domain;
    public Node source, target;
    public boolean alexa_domain;
    public Double score;

    private static Set<String> suspicious_domains;

    static {
            String line;
            suspicious_domains = new HashSet<>();

            try (
                    BufferedReader br = new BufferedReader(new FileReader("data/suspicious_domains.csv"));
            ){
                while ((line = br.readLine()) != null) {
                    suspicious_domains.add(line);
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(-1);
            }
    }

    public Edge(String url, Node source, Node target) {
        final Pattern pattern = Pattern.compile("\\.?[^\\.]+(\\..{2,3})?\\.\\D{2,4}$");
        // ex. url = "http://get.daum.net";
        this.source = source;
        this.target = target;
        this.score = null;

        if (url.isEmpty() || url.equals("N/A")) {
            this.url = null;
            this.domain = null;
        } else {
            if (url.startsWith("http://"))
                url = url.substring(7);
            if (url.startsWith("www."))
                url = url.substring(4);
            int port_index = url.lastIndexOf(":");
            if (port_index != -1)
                url = url.substring(0, port_index);

            this.url = url;
            Matcher matcher = pattern.matcher(url);
            boolean b = matcher.find();
            if (b == false)
                this.domain = url;
            else {
                this.domain = matcher.group();
                if (this.domain.startsWith("."))
                    this.domain = this.domain.substring(1);
            }
        }
    }

    public int hashCode() {
        if (hash != -1)
            return hash;
        hash = 17;
        hash = 31*hash + url.hashCode();
        hash = 31*hash + source.hashCode();
        hash = 31*hash + target.hashCode();
        return hash;
    }

    public boolean equals(Object obj) {
        if (! (obj instanceof Edge))
            return false;
        Edge edge_obj = (Edge) obj;
        return this.url.equals(edge_obj.url) &&
                this.source.equals(edge_obj.source) &&
                this.target.equals(edge_obj.target);
    }

    @Override
    public String toString() {
        double days = Utils.convertUnixTimeStampToDays((double) Math.abs(source.time_stamp - target.time_stamp));
        String time_interval = String.format("%.1f", days);
        return url + ":alexa " + alexa_domain + ":suspicious "
                + suspicious_domains.contains(domain) + ":time_diff " + time_interval + ":score " + score;
    }
}
