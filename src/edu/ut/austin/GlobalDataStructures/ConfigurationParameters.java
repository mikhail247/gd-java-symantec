package edu.ut.austin.GlobalDataStructures;

import edu.ut.austin.Introspection.NBD_State;
import edu.ut.austin.NBD.PrefixGraph.GraphMatchingOptions;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.io.FileUtils;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.ExplicitBooleanOptionHandler;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;

public class ConfigurationParameters {
    public enum ModeEnum {RUN_ENGINE, RUN_ANALISYS}
    public enum DetectionEnum {NONE, SHAPE_GD, GRAPH_DETECTOR, BOTH}
    public enum NodeSelectionPolicyEnum {CONSERVATIVE, ALL_NODES}
    public enum ClusteringTypeEnum {K_MEANS_OFFLINE, BISECTING_KMEANS_ONLINE, BISECTING_KMEANS_OFFLINE, IDEAL_SYMANTEC_FAMILY_CLUSTERING}
    public enum LD_TypeEnum {SYSCALLS, STATIC_FILE_SCANNER, SIMULATED_STATIC_FILE_SCANNER}
    public enum GroundTruthEnum {VIRUS_TOTAL, APPROXIMATE}
    public enum ShapeGDTraining {EXTERNAL_DATASET, VT_DATASET, VT_REPORTS}
    public enum ShapeGDType {GlobalGD, PerNBD_GD}
    public enum ShapeGDImplementation {THRESHOLD, POST_PROCESSING_XGBOOST, REAL_TIME_XGBOOST, IDEAL}
    public enum VT_FileType {EXECUTABLE, NON_EXECUTABLE, UNKNOWN};
    public enum ClusterMergingStrategy {GROUND_TRUTH, URL_SCORE, FV_PERCANTAGE};
    public enum LD_ImplementationType {SPARK_RF, XGBOOST_LOOKUP_TABLE, XGBOOST_APPROXIMATION}

    public ShapeGDImplementation shape_gd_implementation = ShapeGDImplementation.REAL_TIME_XGBOOST;
    public ShapeGDType shape_gd_type = ShapeGDType.GlobalGD;
    public ModeEnum mode = ModeEnum.RUN_ENGINE;
    @Option(name="-enable_av_strategy",handler=ExplicitBooleanOptionHandler.class, usage="Enable simulation of AV team")
    public boolean enable_av_strategy = false;
    public final boolean use_ig_data = false;
    public final boolean simulate_ig_detection = true;
    public int engine_thread_count;
    public final boolean use_static_hash_mapping = true;
    public Double enable_ideal_shape_gd = 0.05; // minimum percentage of malicious files in a nbd
    public final boolean use_adaptive_shape_gd = false;
    public final DetectionEnum detection_mechanism = DetectionEnum.SHAPE_GD;
    public final NodeSelectionPolicyEnum node_selection_policy_type = NodeSelectionPolicyEnum.CONSERVATIVE;
    public final GroundTruthEnum ground_truth = GroundTruthEnum.VIRUS_TOTAL;
    @Option(name="-malicious_threshold",usage="Malicious threshold for VT reprots")
    public double malicious_threshold = 0.3; // converting VT detection rate into benignware/malware labeles
    @Option(name="-malicious_files_per_machine",usage="Min. # of malicious files per machine to consider it infected")
    public double malicious_files_per_machine = 0; // must be at least 0
    @Option(name="-recompute_stat",handler=ExplicitBooleanOptionHandler.class, usage="Disabling previously computed stat")
    public boolean recompute_stat = false;

    public final boolean debug_info = true;
    public final boolean print_file_names = false;
    public final boolean run_visualization = true;

    public final GraphMatchingOptions matching_type = GraphMatchingOptions.GRAPH_BASED;
    public Class matching_type_class = null;
    @Option(name="-shift_rolling_time_window",usage="Shift window by N days")
    public long shift_rolling_time_window = 10; // sec
    @Option(name="-time_window_size",usage="Correlation time window N days")
    public long time_window_size = 150;
    public final int min_node_count_increment = 10;

    // Graph RF training options
    public final boolean train_GraphRF = false;
    public final int RF_training_data_min_size = 10*1000; // downloader graphs or hosts
    public final String selected_machines_for_training_GraphRF = "results/selected_machines_for_training_GraphRF.csv";
    public final int RF_training_data_parts_count = 1; // split a DG into N partial graphs and dump them separately

    // Graph RF operating options
    @Option(name="-graphRF_threshold",usage="Detection threshold for Graph-level Random Forest")
    public double graphRF_threshold = 0.5; // true - enable classifier, false - disable it

    // Clustering options
    public final ClusteringTypeEnum clustering_type = ClusteringTypeEnum.BISECTING_KMEANS_ONLINE; //BISECTING_KMEANS_ONLINE; //ClusteringTypeEnum.BISECTING_KMEANS_ONLINE;
    @Option(name="-clustering_retraining_interval",usage="Time interval between subsequent updates of the clustering model")
    public long clustering_retraining_interval = 150;
    @Option(name="-min_divisible_cluster_size",usage="Minimum size of a cluster")
    public int min_divisible_cluster_size = 100; // affects only BISECTING_KMEANS_ONLINE clustering
    @Option(name="-max_cluster_count",usage="Maximum number of clusters")
    public int max_cluster_count = 50;

    public final int average_across_num_trials = 1; // keep 1 for the "subset-search algo", otherwise, we would have to average across all these trials as well
    public Integer benign_trace_length = 15;
    public Integer malicious_trace_length = 4;

    // LD options   TODO: update later
    public final LD_TypeEnum LD_type = LD_TypeEnum.SIMULATED_STATIC_FILE_SCANNER;
    public final int n_gram_size = 3;
    public final int hash_bit_length = 12;
    public final int asm_lines_per_fv = -1;
    public final int asm_lines_shift = 10*1000;     // sliding by 10K, extracting 50K
    public final Integer disasm_files_count = 10*1000*1000; // process all training files //10K
    public final boolean enable_LD_CV = false;

    // Shape GD options
    @Option(name="-threshold_percentile",usage="ShapeGD's Gamma threshold")
    public double threshold_percentile = 99;
    public final double LD_FP_rate;
    public final double LD_TP_rate;
    // must be 5,000 for syscalls or >= 400 for static file scanning
    public final int min_fv_count_for_detection;
    // GD options
    public final int GD_pca_dim_count = 10;
    public final int GD_bin_count = 50;
    public final int GD_ref_hist_sample_count = 50*1000; // 5K-20K
    public final int GD_PCA_sample_count = 20*1000; //20K of FPs in matlab
    @Option(name="-GD_fvs_per_nbd_count",usage="ShapeGD's minimum neighborhood size (# of FVs)")
    public int GD_fvs_per_nbd_count = 1000; //1200 - perfect
    public final int GD_repetition_count = 1000;
    public final int GD_visualization_fv_count = 200;
    public static final boolean GD_PCA_FP_ONLY = true;
    public final String GD_benign_training_file_name = "features_benignware_training.parquet";
    public final String GD_malicious_training_file_name = "features_malware_training.parquet";
    public final ShapeGDTraining shape_gd_training_method = ShapeGDTraining.VT_REPORTS;
    public final int shape_gd_training_method_file_count = 25*1000; // for ShapeGDTraining.VT_DATASET

    // greedy algorithm for selecting suspicious machines
    @Option(name="-greedy_machine_selection_min_derivative_threshold",usage="Greedy Subset Search: minimum abs. (1st/2nd) derivative value")
    public double greedy_machine_selection_min_derivative_threshold = 0.001;
    @Option(name="-greedy_search_nbd_portion",usage="Greedy Subset Search: check at most (1-N) portion of items")
    public Double greedy_search_nbd_portion = null;

    public final int min_num_files_per_nbd;
    @Option(name="-data_file_count",usage="Size of the testing dataset")
    public int data_file_count = 4;
    public List<String> csv_file_name;
    public final String alexa_domains_file_name = "data/top-1m.csv";
    public final String domain_names_suspiciousness_file_name = "data/url_classification_score.csv";
    public final String file_info_file_name = "data/downloaders_label.csv";
    public final String load_file_rep = "data/filerep.csv";
    public final String file_info_MalSha2_file_name[] = {"data/malsha2_1.csv", "data/malsha2_1_more.csv"};
    public final String downloader_score_file_name = "data/downloader_score.csv";
    public final String vt_detection_time = "data/mal_dwn_list.csv";
    public final String geo_location_file = "data/machine_geo.csv";
    public final String vt_report_classification_score = "data/vt_report_classification_score_1024.csv";
    /* updated_igs_labeled_all.csv: List of IGs that are malicious/benign (labeled by the labeling scheme i.e. root detection rate >= 30 …)
    *  updated_ig_unlabeled_labeled.csv: List of IGs that are labeled through the classifier
    * */
    public final String data_parquet_file = "data/features/vt_report_features_1024.parquet";
    public final String ig_labels_file_info[] = {"data/updated_ig_unlabeled_labeled.csv", "data/updated_igs_labeled_all.csv"};
    public final String vt_info = "data/vt_file_info_results.csv";
    public final String file_set_file_name = "results/file_set.csv";
    @Option(name="-output_folder_suffix",usage="Output folder suffix")
    public String output_folder_suffix = String.valueOf(Math.abs(this.getClass().hashCode()));
    public String out_folder = "results/results_" + output_folder_suffix;
    public final String GRF_LibSVM_data = "results/RF_training_data_LibSVM.csv";
    public final String GRF_Matlab_data = "results/RF_training_data_Matlab.csv";
    public final String GRF_graph_ids = "results/RF_training_data_graph_ids.csv";
    public final String NBD_Matlab_data = "results/NBD_training_data_Matlab.csv";
    public final String clustering_training_data = "results/clustering_training_data.csv";
    public final String Graph_RF_model_file_name = "results/GRF_Model";
    public final String LD_RF_model_file_name = "results/LD_RF_Model";
    public final String Kmeans_model_file_name = "results/Kmeans_Model";
    public final String bisecting_Kmeans_model_file_name = "results/BisectingKmeans_Model";
    public final String shape_gd_spark_file_name = "results/shape_gd_spark.serialized";
    public final String graph_scaling_parameters = "results/graph_scaling_parameters.serialized";
    public String prefix_graph_file_name = out_folder + "/prefix_graph_stat.csv";
    public String introspection_folder_name = out_folder + "/introspection_module.serialized";
    public String introspection_ground_truth_stat_file_name = out_folder + "/introspection_visualization";
    public String introspection_greedy_selection_stat_file_name = out_folder + "/introspection_module_greedy_selection_statistics.csv";
    public String graph_cache_folder = out_folder + "/graph_cache";
    public String nbd_stat_folder_name = out_folder + "/nbd_stat";
    public final boolean load_torrent_files = false;
    @Option(name="-mode",usage="Processing mode or Analysis mode")
    private String mode_t = "run_engine";

    @Option(name="-subset_selection_algorithms", handler=StringArrayOptionHandler.class, usage="Subset Selection Algorithms")
    private List<String> subset_selection_algorithms_t = new LinkedList<>();
    public Set<String> subset_selection_algorithms;

    public final Boolean rebuild_graph_cache = null;
    public final boolean store_only_suspicious_nbds = true; // must be false for correct training of nbd-level XGBoost
    public final double epsilon = 1E-6;
    private boolean initialized = false;

    public int top_N_domains, top_N_urls;
    public final boolean use_urls_as_nbd_seeds = true;
    public static boolean brute_force_domain_urls = false;
    public final boolean file_type_optimized_fv_assignment = true;
    public final boolean use_ML_url_predictor = true;

    // threshold 0.06 fp 0.437280555794 tp 0.93439768636 alerts 148,608 fp_file 0.235508062943 tp_file 0.971546029373
    // threshold 0.095 fp 0.390735014228 tp 0.898542170808 alerts 134,945 fp_file 0.190280489104 tp_file 0.954091836529
    public final double domain_score_threshold = 0.095;//0.095;//0.03;//0.12; //0.15; //0.2 ?
    public final LD_ImplementationType LF_implementation_type = LD_ImplementationType.XGBOOST_LOOKUP_TABLE;

    // Threshold: 0.078000   Overall FP:TP = 0.050101/0.904654,   VT FP:TP = 0.077653/0.922293
    public double LD_threshold = 0.078;
    public final boolean dump_nbd_fvs = true; // must be true
    public final boolean dump_cluster_stat = false;
    public final boolean dump_nbd_instances = true;
    public final double nbd_threshold = 0.05; // treat NBDs having less than 5% of malware as benign

    // threshold 0.14 fp 0.0803095931265 tp 0.891216655823 file-fp 2097626
    // threshold 0.47 fp 0.0506994248385 tp 0.842940793754 file-fp 1366628
    // threshold 0.5 fp 0.0489952424909 tp 0.84007807417 file-fp 1329451
    public double real_time_nbd_xgboost_threshold = 0.50; //0.14 should be manually set to match 5% FP rate
    public final boolean remove_non_exec_nodes = false;
    public final boolean dump_shape_ld_roc = true;
    public final boolean dump_shape_nbd_roc = true;
    public final boolean use_shape_based_domain_features = true;
    public double nbd_xgboost_threshold = 0.14;
    public final ClusterMergingStrategy use_ground_thruth_for_merging_clusters = ClusterMergingStrategy.FV_PERCANTAGE;
    public final boolean store_detection_snapshots = false;
    public boolean automatically_set_nbd_xgboost_threshold = false;
    public boolean mal_check = true;

    private static final ConfigurationParameters instance = new ConfigurationParameters();

    private ConfigurationParameters() {
        switch(LD_type) {
            case SYSCALLS:
                benign_trace_length = 15;
                malicious_trace_length = 4;
                LD_FP_rate = 0.06;
                LD_TP_rate = 0.93;
                // must be 5,000 for syscalls or >= 400 for static file scanning
                min_fv_count_for_detection = 5000;
                break;

            case STATIC_FILE_SCANNER:
                benign_trace_length = 1;
                malicious_trace_length = 1;
                LD_FP_rate = 0.25;
                LD_TP_rate = 0.65;
                min_fv_count_for_detection = 0;
                break;

            case SIMULATED_STATIC_FILE_SCANNER:
                benign_trace_length = 1;
                malicious_trace_length = 1;
                LD_FP_rate = 0.05;
                LD_TP_rate = 0.95;
                // must be 5,000 for syscalls or >= 400 for static file scanning
                min_fv_count_for_detection = 0;
                break;

            default:
                LD_FP_rate = -1;
                LD_TP_rate = -1;
                min_fv_count_for_detection = -1;
                new RuntimeException("Unknown LD Type");
        }
        min_num_files_per_nbd = (int) (min_fv_count_for_detection*LD_FP_rate/(malicious_trace_length*LD_TP_rate));

        try {
            switch (matching_type) {
                case GRAPH_BASED:
                    matching_type_class = Class.forName("edu.ut.austin.NBD.PrefixGraph.PrefixGraph_GraphBased");
                    break;
                case VERTEX_BASED:
                    matching_type_class = Class.forName("edu.ut.austin.NBD.PrefixGraph.PrefixGraph_VertexBased");
                    break;
                case FILE_NAME_BASED:
                    matching_type_class = Class.forName("edu.ut.austin.NBD.PrefixGraph.PrefixGraph_FileNameBased");
                    break;
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static void init(String[] args, String results_subfolder_name) {
        CmdLineParser parser = new CmdLineParser(instance);
        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            e.printStackTrace();
            parser.printUsage(System.err);
            System.exit(-1);
        }
        if (instance.mode_t.equals("run_engine"))
            instance.mode = ModeEnum.RUN_ENGINE;
        else if (instance.mode_t.equals("run_analysis")) {
            instance.mode = ModeEnum.RUN_ANALISYS;
            instance.engine_thread_count = 1;
        }

        instance.initialized = true;
        instance.shift_rolling_time_window *= 24*3600;
        instance.time_window_size *= 24*3600;
        instance.clustering_retraining_interval *= 24*3600;
        instance.out_folder = "results/results_" + instance.output_folder_suffix;

        if (results_subfolder_name != null)
            instance.out_folder += "/" + results_subfolder_name;

        instance.prefix_graph_file_name = instance.out_folder + "/prefix_graph_stat.csv";
        instance.introspection_folder_name = instance.out_folder + "/introspection_module.serialized";
        instance.introspection_ground_truth_stat_file_name = instance.out_folder + "/introspection_visualization";
        instance.introspection_greedy_selection_stat_file_name = instance.out_folder + "/introspection_module_greedy_selection_statistics.csv";
        instance.graph_cache_folder = instance.out_folder + "/graph_cache";
        instance.nbd_stat_folder_name = instance.out_folder + "/nbd_stat";

        instance.csv_file_name = IntStream.range(0, instance.data_file_count).mapToObj(i -> String.format("data/ig/json_%03d.csv", i)).collect(Collectors.toList());
        instance.subset_selection_algorithms = new HashSet<>(Arrays.asList("GroundTruthMachineSelection"));
        instance.subset_selection_algorithms.addAll(instance.subset_selection_algorithms_t);
        instance.engine_thread_count = instance.enable_av_strategy ? 1 : 2;

        if(instance.malicious_files_per_machine < 0)
            throw new RuntimeException("Illegal value of malicious_files_per_machine");
        if (instance.enable_av_strategy)
            instance.subset_selection_algorithms.add("GreedyMachineSelection_MachineCentric");

        if (instance.mode == ModeEnum.RUN_ENGINE) {
            initFolder(instance.out_folder);
            initFolder(instance.out_folder + "/nbd_dump");
            initFolder(instance.introspection_folder_name);
        }

        // init NBD_State
        if (instance.use_shape_based_domain_features) {
            NBD_State.url_dim_count = 100;
            NBD_State.url_bin_count = 5;
        } else {
            NBD_State.url_dim_count = 1;
            NBD_State.url_bin_count = 50;
        }
    }

    private static void initFolder(String folder_name) {
        try {
            FileUtils.deleteDirectory(new File(folder_name));
            Files.createDirectories(Paths.get(folder_name));
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static ConfigurationParameters getInstance() {
        if (!instance.initialized) {
            System.out.println("ConfigurationParameters hasn't been initialized");
            System.exit(-1);
        }
        return instance;
    }

    public String toString() {
        return "malicious_threshold: " + malicious_threshold  + "\n"
            + "shift_rolling_time_window: " + shift_rolling_time_window/(24*3600)  + "\n"
                + "time_window_size: " + time_window_size/(24*3600)  + "\n"
                + "GraphRF_threshold: " + graphRF_threshold  + "\n"
                + "clustering_type: " + clustering_type  + "\n"
                + "clustering_retraining_interval: " + clustering_retraining_interval/(24*3600)  + "\n"
                + "min_divisible_cluster_size: " + min_divisible_cluster_size  + "\n"
                + "max_cluster_count: " + max_cluster_count  + "\n"
                + "threshold_percentile: " + threshold_percentile + "\n"
                + "greedy_machine_selection_min_derivative_threshold: " + greedy_machine_selection_min_derivative_threshold  + "\n"
                + "greedy_search_nbd_portion: " + greedy_search_nbd_portion  + "\n"
                + "csv_file_name_count: " + csv_file_name.size()  + "\n"
                + "enable_av_strategy: " + enable_av_strategy;
    }
}