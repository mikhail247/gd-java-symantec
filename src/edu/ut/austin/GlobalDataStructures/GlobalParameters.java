package edu.ut.austin.GlobalDataStructures;

import edu.ut.austin.AV_Strategy.AV_Strategy;
import edu.ut.austin.DataInputOutput.ShapeGD_DataLoader;
import edu.ut.austin.Introspection.NBD_State;
import edu.ut.austin.MachineSelectionStrategy.GreedyMachineSelection_MachineCentric;
import edu.ut.austin.MachineSelectionStrategy.GroundTruthMachineSelection;
import edu.ut.austin.MachineSelectionStrategy.LD_MachineSelection;
import edu.ut.austin.MachineSelectionStrategy.MachineSelectionStrategy;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GlobalParameters {
    private static final GlobalParameters instance = new GlobalParameters();
    private ConfigurationParameters parameters = ConfigurationParameters.getInstance();
    public BufferedWriter log_writer, nbd_fv_writer;
    public BufferedWriter cluster_writer_before_merging, cluster_writer_after_merging;
    public int executor_size;
    public ArrayList<ExecutorService> executors;
    public final Logger detection_logger, prefix_tree_logger, logger;
    public static SparkConf spark_conf;
    public static JavaSparkContext java_spark_context;
    public static SparkSession spark;
    public final long start_execution;
    public int suspicious_domain_count;

    public List<MachineSelectionStrategy> subset_selector;
    public AV_Strategy av_strategy;

    // for debugging
    public Set<String> malware_1, malware_2, malware_3, malware_4;

    static {
        org.apache.log4j.Logger.getLogger("org").setLevel(org.apache.log4j.Level.OFF);
        org.apache.log4j.Logger.getLogger("akka").setLevel(org.apache.log4j.Level.OFF);

        String path;
        String computer_name = getComputerName();
        String user_name = System.getProperty("user.name");
        if (computer_name.startsWith("asterix") || computer_name.startsWith("obelix"))
            path = "/home/" + user_name + "/tmp";
        else if (computer_name.startsWith("sparkc"))
            path = "/local/home/" + user_name + "/tmp";
        else
            path = "/mnt/ssd_data/home/" + user_name + "/tmp";

        spark_conf = new SparkConf().setAppName("Graph Processing Engine").setMaster("local[2]")
                .set("spark.executors.memory", "1g").set("spark.driver.maxResultSize", "20g")
                .set("spark.driver.memory", "20g").set("spark.local.dir", path)
                .set("spark.driver.allowMultipleContexts", "true");
        java_spark_context = new JavaSparkContext(spark_conf);
        spark = SparkSession.builder().appName("Graph Processing Engine").getOrCreate();
    }

    private GlobalParameters() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        start_execution = System.currentTimeMillis();
        executor_size = Runtime.getRuntime().availableProcessors() / parameters.engine_thread_count;
        executors = IntStream.range(0, parameters.engine_thread_count).mapToObj(i ->
                Executors.newFixedThreadPool(executor_size)).collect(Collectors.toCollection(ArrayList::new));
        detection_logger = Logger.getLogger("Detection");
        prefix_tree_logger = Logger.getLogger("Prefix Tree");
        logger = Logger.getLogger("MISC");
        subset_selector = new LinkedList<>();
        av_strategy = null;

        detection_logger.setLevel(Level.OFF);
        prefix_tree_logger.setLevel(Level.OFF);
        logger.setLevel(Level.ALL);

        OutputStreamWriter log_stream = null, nbd_fv_stream = null;
        OutputStreamWriter cluster_stream_before_merging = null, cluster_stream_after_merging = null;
        try {
            log_stream = new FileWriter(parameters.out_folder + "/global_log.csv", true);
            if (parameters.mode != ConfigurationParameters.ModeEnum.RUN_ANALISYS)
                nbd_fv_stream = new FileWriter(parameters.out_folder + "/nbd_fvs_" + parameters.data_file_count + ".csv", false);
            cluster_stream_before_merging = new FileWriter(parameters.out_folder + "/cluster_stat_before_merging_" + parameters.data_file_count + ".csv", false);
            cluster_stream_after_merging = new FileWriter(parameters.out_folder + "/cluster_stat_after_merging_" + parameters.data_file_count + ".csv", false);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        log_writer = new BufferedWriter(log_stream);

        if (parameters.mode != ConfigurationParameters.ModeEnum.RUN_ANALISYS) {
            nbd_fv_writer = new BufferedWriter(nbd_fv_stream);
            cluster_writer_before_merging = new BufferedWriter(cluster_stream_before_merging);
            cluster_writer_after_merging = new BufferedWriter(cluster_stream_after_merging);
            String header = NBD_State.getNBD_Header();
            try {
                nbd_fv_writer.write(header + "\n");
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
        addLineToLogFile(parameters.toString());
        addLineToLogFile(ToStringBuilder.reflectionToString(parameters));
    }

    private static String getComputerName() {
        String hostname = "Unknown";
        try {
            InetAddress addr;
            addr = InetAddress.getLocalHost();
            hostname = addr.getHostName();
        }
        catch (UnknownHostException ex) {
            System.out.println("Hostname can not be resolved");
        }
        return hostname;
    }

    public void loadSubsetSelectors() {
        subset_selector.add(new GroundTruthMachineSelection());

        if (ConfigurationParameters.getInstance().subset_selection_algorithms.contains("LD_MachineSelection"))
            subset_selector.add(new LD_MachineSelection(true));

        if (ConfigurationParameters.getInstance().subset_selection_algorithms.contains("GreedyMachineSelection_MachineCentric"))
            subset_selector.add(new GreedyMachineSelection_MachineCentric(ShapeGD_DataLoader.load()));
    }

    public static GlobalParameters getInstance() {
        return instance;
    }

    public void synchronizedDataDump(BufferedWriter writer, String str) {
        if (str == null)
            return;

        synchronized (writer) {
            try {
                writer.write(str + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void addLineToLogFile(String str) {
        try {
            log_writer.write(str);
            if (str.charAt(str.length() - 1) != '\n')
                log_writer.write("\n");
            log_writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void closeNBD_Writer() {
        try {
            if (nbd_fv_writer != null)
                nbd_fv_writer.close();
            nbd_fv_writer = null;

            if (cluster_writer_before_merging != null)
                cluster_writer_before_merging.close();
            cluster_writer_before_merging = null;

            if (cluster_writer_after_merging != null)
                cluster_writer_after_merging.close();
            cluster_writer_after_merging = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void freeResources() {
        shutdownExecutors();

        try {
            if (log_writer != null)
                log_writer.close();

            if (nbd_fv_writer != null)
                nbd_fv_writer.close();

            if (cluster_writer_before_merging != null)
                cluster_writer_before_merging.close();

            if (cluster_writer_after_merging != null)
                cluster_writer_after_merging.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void shutdownExecutors() {
        executors.forEach(ExecutorService::shutdown);

        while (executors.stream().anyMatch(executor -> !executor.isTerminated())) {
            try {
                Thread.sleep(5*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void shutdownSparkContext() {
        // we can stop Spark to reduce memory consumption
        GlobalParameters.java_spark_context.stop();
        GlobalParameters.spark.stop();
    }
}
