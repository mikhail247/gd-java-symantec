package edu.ut.austin.AV_Strategy;

import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.Introspection.DetectionSnapshot;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.stream.Collectors;

/**
 * Created by Mikhail on 4/27/2017.
 */
public class KasperskyResponseTeam extends AV_Strategy {
    // 100 alerts per person per 12 hours
    private final int files_per_person_per_day = 100;
    private final int human_beings = 10;

    private Map<String, FileInfo> file_info;

    // load whitelisted files
    public KasperskyResponseTeam(Map<String, FileInfo> file_info) {
        this.file_info = file_info;
        this.blacklisted_files = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
        this.whitelisted_files = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
    }

    @Override
    public void processAlerts(DetectionSnapshot snapshot, Collection<DefaultDirectedGraphWrapper> data) {
        if (!ConfigurationParameters.getInstance().enable_av_strategy)
            return;

        Set<Long> machine_ids = snapshot.getMachineIDs_SubsetSelector("edu.ut.austin.MachineSelectionStrategy.GreedyMachineSelection_MachineCentric");
        Set<String> file_ids = snapshot.getFileIDs_SubsetSelector("edu.ut.austin.MachineSelectionStrategy.GreedyMachineSelection_MachineCentric");
        int budget = human_beings * files_per_person_per_day;
        Map<Integer, Queue<Pair<Long, Set<String>>>> machines = new ConcurrentSkipListMap<>(Collections.reverseOrder());

        data.parallelStream().forEach(graph -> {
            if (machine_ids.contains(graph.machine_id)) {
                Set<String> set = graph.vertexSet().stream().map(node -> node.sha2_id).collect(Collectors.toSet());
                set.retainAll(file_ids);
                machines.putIfAbsent(set.size(), new ConcurrentLinkedQueue<>());
                machines.get(set.size()).add(new ImmutablePair<>(graph.machine_id, set));
            }
        });

        Set<String> checked_files = machines.values().stream().flatMap(Queue::stream).flatMap(pair ->
                pair.getRight().stream()).limit(budget).collect(Collectors.toSet());

        checked_files.parallelStream().forEach(file_id -> {
            if (file_info.get(file_id).is_benign)
                blacklisted_files.add(file_id);
            else
                whitelisted_files.add(file_id);
        });
    }

    public String toString() {
        return this.getClass().getName();
    }

}
