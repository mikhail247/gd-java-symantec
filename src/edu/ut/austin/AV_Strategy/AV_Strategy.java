package edu.ut.austin.AV_Strategy;

import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.GraphClasses.Edge;
import edu.ut.austin.GraphClasses.Node;
import edu.ut.austin.Introspection.DetectionSnapshot;
import org.jgrapht.traverse.BreadthFirstIterator;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Mikhail on 4/27/2017.
 */
public abstract class AV_Strategy {
    protected Set<String> blacklisted_files = null;
    protected Set<String> whitelisted_files = null;
    public abstract void processAlerts(DetectionSnapshot snapshot, Collection<DefaultDirectedGraphWrapper> data);

    // data transformer
    @SuppressWarnings({"ConstantConditions", "SuspiciousMethodCalls"})
    public void removeKnownMalware(Collection<DefaultDirectedGraphWrapper> data) {
        if (!ConfigurationParameters.getInstance().enable_av_strategy)
            return;
        data.parallelStream().filter(graph -> !graph.isGraphEmpty()).forEach(graph -> {
            Set<Node> removed = new HashSet<>();
            graph.vertexSet().stream().filter(node ->
                    blacklisted_files.contains(node.sha2_id)).forEach(removed::add);

            for (Node node : removed) {
                if (!graph.containsVertex(node))
                    continue;

                Set<Node> nodes = new HashSet<>();
                BreadthFirstIterator<Node, Edge> iterator = new BreadthFirstIterator<>(graph, node);
                while (iterator.hasNext())
                    nodes.add(iterator.next());
                graph.removeAllVertices(nodes);
            }
        });

        data.removeIf(DefaultDirectedGraphWrapper::isGraphEmpty);
    }

    public void printStatistics() {
        if (!ConfigurationParameters.getInstance().enable_av_strategy)
            return;

        GlobalParameters.getInstance().logger.info(String.format("[%s] black list: %d, white list: %d",
                this.toString(), blacklisted_files.size(), whitelisted_files.size()));
    }
}
