package edu.ut.austin.Debug;

import edu.ut.austin.DataInputOutput.GraphDataLoader;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.Utils.Utils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 7/17/2017.
 */
public class ExperimentWithShapeGD {
    public static void dumpDataForXGBoostLD(String args[]) {
        String output_file;
        JavaRDD<Pair<String, String>> data;

        ConfigurationParameters.init(args, null);
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        Set<String> malware_hashes = Utils.loadMalwareHashes();

        String parquet_file_name = "results/features/vt_static_report_features_fixed_raw_text/1024";
        JavaRDD<String> raw_data = global_parameters.java_spark_context.textFile(parquet_file_name);
        data = raw_data.filter(str -> !str.startsWith("Time")).map(new Function<String, Pair<String, String>>() {
            @Override
            public Pair<String, String> call(String str) {
                String sha256 = str.substring(3, str.indexOf("', "));
                String libsvm_format = str.substring(str.indexOf("{")+1, str.indexOf("}")).replace(",", "");
                libsvm_format = libsvm_format.replaceAll(": ", ":");
                String label = malware_hashes.contains(sha256) ? "1" : "0";
                return new ImmutablePair<>(sha256, label + " " + libsvm_format);
            }
        });
        global_parameters.logger.info("Loaded " + data.count() + " FVs");
        String raw_str = raw_data.take(1).get(0);
        int index_start = raw_str.indexOf("SparseVector(") + "SparseVector(".length();
        int index_end = raw_str.indexOf(",", index_start);
        int hashing_trick_size = Integer.parseInt(raw_str.substring(index_start, index_end));

        output_file = "results/vt_report_features_" + hashing_trick_size + "_sha_256.libsvm";
        try (
                OutputStreamWriter stream_writer = new FileWriter(output_file);
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (Pair<String, String> pair : data.collect())
                writer.write(pair.getLeft() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        output_file = "results/vt_report_features_" + hashing_trick_size + ".libsvm";
        try (
                OutputStreamWriter stream_writer = new FileWriter(output_file);
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (Pair<String, String> pair : data.collect())
                writer.write(pair.getRight() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        data = raw_data.filter(str -> !str.startsWith("Time")).map(new Function<String, Pair<String, String>>() {
            @Override
            public Pair<String, String> call(String str) throws Exception {
                int index = str.indexOf(',');
                String sha256 = str.substring(3, index-1);

                String vec = str.substring(index+15, str.length()-2);
                String array[] = vec.split(",");
                int vec_size = Integer.parseInt(array[0].trim());
                List<Integer> positions = new LinkedList<>();
                List<Double> values = new LinkedList<>();

                for (int i = 1; i < array.length; i ++) {
                    String temp[] = array[i].split(":");
                    positions.add(Integer.parseInt(temp[0].replaceAll("[\\{\\}]", "").trim()));
                    values.add(Double.parseDouble(temp[1].replaceAll("[\\{\\}]", "").trim()));
                }
                org.apache.spark.ml.linalg.Vector features = org.apache.spark.ml.linalg.Vectors.sparse(vec_size,
                        positions.stream().mapToInt(i -> i).toArray(), values.stream().mapToDouble(d -> d).toArray());

                int label = malware_hashes.contains(sha256) ? 0 : 1;
                String fv_str = label + "," + Arrays.stream(features.toDense().toArray())
                        .mapToObj(Double::toString).collect(Collectors.joining(","));
                return new ImmutablePair<>(sha256, fv_str);
            }
        });

        String header = IntStream.range(0, hashing_trick_size).mapToObj(i -> "fv_" + i).collect(Collectors.joining(","));
        output_file = "results/vt_report_features_" + hashing_trick_size + ".csv";
        try (
                OutputStreamWriter stream_writer = new FileWriter(output_file);
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            writer.write("label," + header + "\n");
            for (Pair<String, String> pair : data.collect())
                writer.write(pair.getRight() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static void dumpSuccessfullyProcessedHashes(String args[]) {
        ConfigurationParameters.init(args, null);
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        List<JavaRDD<String>> data_list = Stream.of("/local/home/mikhail/VT_report_processing_dimva_gunalan/hashed_feature_rdds", "/local/home/mikhail/VT_report_processing_dimva_gunalan/additional_hashed_feature_rdds")
                .flatMap(folder -> Arrays.stream(new File(folder).listFiles())).filter(file -> file.isDirectory())
                .map(file -> {
                    String parquet_file_name = file.getAbsolutePath();
                    System.out.println("Reading file: " + file.getName());
                    JavaRDD<String> raw_data = global_parameters.java_spark_context.textFile(parquet_file_name);
                    JavaRDD<String> rdd = raw_data.filter(str -> !str.startsWith("Time")).map(new Function<String, String>() {
                        @Override
                        public String call(String str) {
                            String sha256 = str.substring(3, str.indexOf("', "));
                            return sha256;
                        }
                    });
                    return rdd;
                }).collect(Collectors.toList());
        System.out.println("Loaded " + data_list.size() + " parquet files");


        List<String> hashes_temp = data_list.parallelStream()
                .flatMap(rdd -> rdd.collect().stream()).collect(Collectors.toList());
        Set<String> hashes = new HashSet<>(hashes_temp);
        System.out.println("Processed " + String.format("%,d", hashes_temp.size()) + " hashes");
        System.out.println("Processed (unique) " + String.format("%,d", hashes.stream().distinct().count()) + " hashes");

        try (
                OutputStreamWriter stream_writer = new FileWriter("results/processed_sha256_by_gunalan.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (String line : hashes)
                writer.write(line + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static void main(String args[]) {
        dumpSuccessfullyProcessedHashes(args);
        dumpDataForXGBoostLD(args);
    }
}
