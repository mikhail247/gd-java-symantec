package edu.ut.austin.Debug;

import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.GraphClasses.Edge;
import edu.ut.austin.GraphClasses.Node;
import edu.ut.austin.ML_Models.LocalDetector;
import edu.ut.austin.ML_Models.TrainML_Models;
import edu.ut.austin.Utils.Utils;
import org.apache.commons.lang3.tuple.*;
import org.jgrapht.traverse.BreadthFirstIterator;
import org.spark_project.jetty.util.ConcurrentArrayQueue;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 6/3/2017.
 */
public class Debug {

    public static void analyzeFileTypes(Map<Long, DefaultDirectedGraphWrapper> data) {
        Comparator<Map.Entry<String, Long>> comparator = (entry_1, entry_2) -> {
            double count_1 = entry_1.getValue();
            double count_2 = entry_2.getValue();
            if (count_1 == count_2) return 0;
            return count_1 > count_2 ? -1 : +1;
        };

        Supplier<Stream<String>> supplier_all_files = () -> data.values().parallelStream().flatMap(graph -> graph.vertexSet().stream())
                .map(node -> {
                    if (node.file_name == null)
                        return null;
                    Integer dot_index = node.file_name.lastIndexOf('.');
                    if (dot_index == null)
                        return null;
                    return node.file_name.substring(dot_index+1);
                });

        Supplier<Stream<String>> supplier_infected_files = () -> data.values().parallelStream()
                .flatMap(graph -> graph.vertexSet().stream()).filter(Node::isMalicious).map(node -> {
                    if (node.file_name == null)
                        return null;
                    Integer dot_index = node.file_name.lastIndexOf('.');
                    if (dot_index == null)
                        return null;
                    return node.file_name.substring(dot_index+1);
                });

        BiFunction<Supplier<Stream<String>>, String, Void> write_to_file = (Supplier<Stream<String>> supplier, String file_name) -> {
            ConfigurationParameters parameters = ConfigurationParameters.getInstance();

            long total_file_count = supplier.get().count();
            long files_without_extension = supplier.get().filter(Objects::isNull).count();
            Map<String, Long> map = supplier.get().filter(Objects::nonNull)
                    .collect(Collectors.groupingByConcurrent(Function.identity(), Collectors.counting()))
                    .entrySet().parallelStream().sorted(comparator).collect(
                            Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
            System.out.println("Files without extension: " + 100.0 * files_without_extension / total_file_count + "%");

            try (
                    OutputStreamWriter stream_writer = new FileWriter(parameters.out_folder + "/" + file_name);
                    BufferedWriter writer = new BufferedWriter(stream_writer);
            ) {
                writer.write("Files without extension: " + 100.0 * files_without_extension / total_file_count + "%\n");

                for (Map.Entry<String, Long> entry : map.entrySet()) {
                    String str = String.format("%s, %.5f%%\n", entry.getKey(), 100.0 * entry.getValue() / total_file_count);
                    writer.write(str);
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }
            return null;
        };

        write_to_file.apply(supplier_all_files, "file_extension_distribution.csv");
        write_to_file.apply(supplier_infected_files, "file_malware_extension_distribution.csv");
        System.exit(-1);
    }

    public static List<Triple<Integer, Double, Double>> analyzeURLEdgeCoverage(Map<String, MutableTriple<Double, Integer, Integer>> malware_url_distribution,
                                                                               Map<Long, DefaultDirectedGraphWrapper> data) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        int domain_step = 100;

        Comparator<Map.Entry<String, MutableTriple<Double, Integer, Integer>>> comparator = (entry_1, entry_2) -> {
            double ratio_1 = entry_1.getValue().getLeft();
            double ratio_2 = entry_2.getValue().getLeft();
            if (ratio_1 == ratio_2) return 0;
            return ratio_1 > ratio_2 ? -1 : +1;
        };
        List<Triple<Integer, Double, Double>> results = new LinkedList<>();

        for (int limit = domain_step; limit <= malware_url_distribution.size(); limit += domain_step) {
            LinkedHashMap<String, MutableTriple<Double, Integer, Integer>> map = malware_url_distribution.entrySet()
                    .stream().sorted(comparator).limit(limit).collect(Collectors.toMap(Map.Entry::getKey,
                            entry -> new MutableTriple<>(entry.getValue().getLeft(), entry.getValue().getMiddle(), entry.getValue().getRight()),
                            (e1, e2) -> e1, LinkedHashMap::new));

            double ratio = map.values().stream().mapToDouble(Triple::getLeft).min().getAsDouble();
            System.out.println("URL_step: " + limit + " ratio: " + ratio);
            List<Double> list = extractSuspiciousNodesBasedOnURLs(null, map, data, true);
            results.add(new ImmutableTriple<>(limit, list.get(0), list.get(1)));
        }

        try (
                OutputStreamWriter stream_writer = new FileWriter(parameters.out_folder + "/url_node_coverage.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (Triple<Integer, Double, Double> triple : results)
                writer.write(triple.getLeft() + "," + triple.getMiddle() + "," + triple.getRight() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return results;
    }

    public static void analyzeDomainEdgeCoverage(Map<String, Pair<Double, Integer>> malware_domain_distribution,
                                           Map<String, MutableTriple<Double, Integer, Integer>> malware_url_distribution, Map<Long, DefaultDirectedGraphWrapper> data) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        int domain_step = 1000;

        long malicious_file_count_total = data.values().parallelStream()
                .flatMap(graph -> graph.vertexSet().stream()).filter(Node::isMalicious).count();

        long benign_file_count_total = data.values().parallelStream()
                .flatMap(graph -> graph.vertexSet().stream()).filter(Node::isBenign).count();

        long malicious_file_count_distinct = data.values().parallelStream()
                .flatMap(graph -> graph.vertexSet().stream()).filter(Node::isMalicious).map(node -> node.sha2_id).distinct().count();

        long benign_file_count_distinct = data.values().parallelStream()
                .flatMap(graph -> graph.vertexSet().stream()).filter(Node::isBenign).map(node -> node.sha2_id).distinct().count();

        Comparator<Map.Entry<String, Pair<Double, Integer>>> comparator = (entry_1, entry_2) -> {
            double ratio_1 = entry_1.getValue().getLeft();
            double ratio_2 = entry_2.getValue().getLeft();
            if (ratio_1 == ratio_2) return 0;
            return ratio_1 > ratio_2 ? -1 : +1;
        };

        Comparator<Map.Entry<String, MutableTriple<Double, Integer, Integer>>> comparator_triple = (entry_1, entry_2) -> {
            double ratio_1 = entry_1.getValue().getLeft();
            double ratio_2 = entry_2.getValue().getLeft();
            if (ratio_1 == ratio_2) return 0;
            return ratio_1 > ratio_2 ? -1 : +1;
        };
        List<List<Double>> results = new LinkedList<>();

        for (int limit = domain_step; limit <= malware_domain_distribution.size(); limit += domain_step)
        {
            LinkedHashMap<String, Pair<Double, Integer>> map = malware_domain_distribution.entrySet()
                    .stream().sorted(comparator).limit(limit).collect(Collectors.toMap(Map.Entry::getKey,
                            entry -> new ImmutablePair<>(entry.getValue().getLeft(), entry.getValue().getRight()),
                            (e1, e2) -> e1, LinkedHashMap::new));

            double ratio = map.values().stream().mapToDouble(Pair::getLeft).min().getAsDouble();
            System.out.println("Domain_step: " + limit + " ratio: " + ratio);

            for (int limit_url_map = 500; limit_url_map <= malware_url_distribution.size(); limit_url_map += 2500) {
                Map<String, MutableTriple<Double, Integer, Integer>> malware_url_distribution_view =
                        malware_url_distribution.entrySet().stream().sorted(comparator_triple)
                                .limit(limit_url_map).collect(Collectors.toMap(Map.Entry::getKey,
                                entry -> new MutableTriple<>(entry.getValue().getLeft(), entry.getValue().getMiddle(), entry.getValue().getRight()),
                                (e1, e2) -> e1, LinkedHashMap::new));
                List<Double> list = extractSuspiciousNodesBasedOnURLs(map, malware_url_distribution_view, data, true);
                list.add(0, (double) limit);
                list.add(1, (double) limit_url_map);
                list.add((double) malicious_file_count_total);
                list.add((double) benign_file_count_total);
                list.add((double) malicious_file_count_distinct);
                list.add((double) benign_file_count_distinct);
                results.add(list);

                String str = list.stream().map(value -> Double.toString(value)).collect(Collectors.joining(", "));
                System.out.println(str);
            }
        }

        try (
                OutputStreamWriter stream_writer = new FileWriter(parameters.out_folder + "/domain_url_node_coverage.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            writer.write("top_domains, top_urls," +
                    "malicious_nodes_total_percentage, malicious_nodes_distinct_percentage," +
                    "malicious_nodes_total, benign_nodes_total, malicious_nodes_distinct, benign_nodes_distinct" +
                    "malicious_file_count_total, benign_file_count_total, " +
                    "malicious_file_count_distinct, benign_file_count_distinct\n");

            for (List<Double> list : results) {
                String str = list.stream().map(value -> Double.toString(value)).collect(Collectors.joining(", "));
                writer.write(str + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        System.exit(-1);
    }

    public static List<Double> extractSuspiciousNodesBasedOnURLs(Map<String, Pair<Double, Integer>> domain_name_suspiciousness,
                                                                 Map<String, MutableTriple<Double, Integer, Integer>> domain_url_suspiciousness,
                                                                 Map<Long, DefaultDirectedGraphWrapper> data, boolean print_flag) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        Supplier<Stream<Node>> stream_supplier = () -> data.values().parallelStream().flatMap(graph -> {
            boolean include_node;
            BreadthFirstIterator<Node, Edge> iterator;
            Set<Node> nodes = new HashSet<>();

            Set<Node> suspicious_nodes = graph.edgeSet().stream().filter(edge -> {
                if (domain_name_suspiciousness != null) {
                    Optional<Pair<Double, Integer>> score = Optional.ofNullable(
                            domain_name_suspiciousness.get(parameters.use_urls_as_nbd_seeds ? edge.url : edge.domain));
                    return score.isPresent() && score.get().getLeft() > 0.5;
                } else {
                    Optional<Triple<Double, Integer, Integer>> score = Optional.ofNullable(domain_url_suspiciousness.get(edge.url));
                    return score.isPresent() && score.get().getLeft() > 0.0;
                }
            }).flatMap(edge ->
                Stream.of(edge.source, edge.target)).collect(Collectors.toSet());

            if (domain_name_suspiciousness != null) {
                for (Node node : suspicious_nodes) {
                    iterator = new BreadthFirstIterator<>(graph, node);
                    while (iterator.hasNext()) {
                        Node current_node = iterator.next();
                        include_node = graph.edgesOf(current_node).stream()
                                .map(edge -> domain_url_suspiciousness.get(edge.url)).filter(Objects::nonNull)
                                .mapToDouble(Triple::getLeft).anyMatch(score -> Math.abs(score) > 0.0);

                        if (include_node)
                            nodes.add(current_node);
                    }
                }
            } else
                nodes = suspicious_nodes;
            return nodes.stream();
        });

        long malicious_nodes_total = stream_supplier.get()
                .filter(Node::isMalicious).map(node -> node.sha2_id).count();
        long benign_nodes_total = stream_supplier.get()
                .filter(Node::isBenign).map(node -> node.sha2_id).count();
        long malicious_nodes_distinct = stream_supplier.get()
                .filter(Node::isMalicious).map(node -> node.sha2_id).distinct().count();
        long benign_nodes_distinct = stream_supplier.get()
                .filter(Node::isBenign).map(node -> node.sha2_id).distinct().count();

        List<Double> result = new LinkedList<> (Arrays.asList(
                100.0*malicious_nodes_total/(benign_nodes_total + parameters.epsilon),
                100.0*malicious_nodes_distinct/(benign_nodes_distinct + parameters.epsilon),
                (double) malicious_nodes_total, (double) benign_nodes_total,
                (double) malicious_nodes_distinct, (double) benign_nodes_distinct));
        if (print_flag) {
            System.out.format("NBDs cover %,d/%,d (malicious_nodes_total/benign_nodes_total) %f%%\n",
                    malicious_nodes_total, benign_nodes_total, result.get(0));
            System.out.format("NBDs cover %,d/%,d (malicious_nodes_distinct/benign_nodes_distinct) %f%%\n",
                    malicious_nodes_distinct, benign_nodes_distinct, result.get(1));
        }

        GlobalParameters.getInstance().malware_1 = stream_supplier.get()
                .filter(Node::isMalicious).map(node -> node.sha2_id).collect(Collectors.toCollection(ConcurrentHashMap::newKeySet));
        GlobalParameters.getInstance().malware_2 = stream_supplier.get()
                .filter(Node::isMalicious).map(node -> node.sha2_id).collect(Collectors.toCollection(ConcurrentHashMap::newKeySet));
        GlobalParameters.getInstance().malware_3 = stream_supplier.get()
                .filter(Node::isMalicious).map(node -> node.sha2_id).collect(Collectors.toCollection(ConcurrentHashMap::newKeySet));
        GlobalParameters.getInstance().malware_4 = stream_supplier.get()
                .filter(Node::isMalicious).map(node -> node.sha2_id).collect(Collectors.toCollection(ConcurrentHashMap::newKeySet));
        return result;
    }

    public static void computeWine_TypeDistribution(Map<Long, DefaultDirectedGraphWrapper> data) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        Supplier<Stream<String>> supplier = () -> data.values().parallelStream()
                .flatMap(graph -> graph.vertexSet().stream()).map(node -> node.file_name);

        long file_count = supplier.get().count();
        long non_null_file_count = supplier.get().filter(Objects::nonNull).count();
        long null_file_count = supplier.get().filter(Objects::isNull).count();

        LinkedHashMap<String, Double> map = supplier.get().filter(Objects::nonNull)
                .map(file_name -> {
                    int index = file_name.indexOf('.');
                    index ++;
                    return file_name.substring(index);
                })
                .collect(Collectors.groupingByConcurrent(Function.identity(), Collectors.counting()))
                .entrySet().parallelStream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> 100.0*entry.getValue()/non_null_file_count, (e1, e2) -> e1, LinkedHashMap::new));
        System.out.format("[WINE dataset] file_count: %,d non_null_file_count: %,d null_file_count: %,d\n",
                file_count, non_null_file_count, null_file_count);

        try (
                OutputStreamWriter stream_writer = new FileWriter(parameters.out_folder + "/wine_file_extension_distribution.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {

            for (Map.Entry<String, Double> entry : map.entrySet()) {
                String str = String.format("%s, %.5f\n", entry.getKey(), entry.getValue());
                writer.write(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static void computeVT_TypeDistribution(Map<String, FileInfo> file_info) {
        Map<String, FileInfo> file_info_vt = file_info.entrySet().parallelStream()
                .filter(entry -> entry.getValue() == entry.getValue().redirect)
                .collect(Collectors.toConcurrentMap(Map.Entry::getKey, Map.Entry::getValue));

        long file_info_count = file_info_vt.size();
        long non_null_file_info_count = file_info_vt.values().parallelStream().map(file -> file.file_type_str).filter(Objects::nonNull).count();
        long null_file_info_count = file_info_vt.values().parallelStream().map(file -> file.file_type_str).filter(Objects::isNull).count();

        LinkedHashMap<String, Double> map = file_info_vt.values().parallelStream().map(file -> file.file_type_str).filter(Objects::nonNull)
                .collect(Collectors.groupingByConcurrent(Function.identity(), Collectors.counting()))
                .entrySet().parallelStream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> 100.0*entry.getValue()/non_null_file_info_count, (e1, e2) -> e1, LinkedHashMap::new));
        map.forEach((key, value) -> System.out.println(key + ": " + value + "%"));

        System.out.format("[VT dataset] file_info_count: %,d non_null_file_info_count: %,d null_file_info_count: %,d\n",
                file_info_count, non_null_file_info_count, null_file_info_count);
        System.out.println("percentage sum = " + map.values().stream().mapToDouble(Double::valueOf).sum());
    }

    public static void evaluateLD_Parameters(Map<String, FileInfo> file_info, Map<Long, DefaultDirectedGraphWrapper> data) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        LocalDetector local_detector = TrainML_Models.loadLD_Model(parameters.LF_implementation_type);
        List<double[]> results = new LinkedList<>();

        Set<String> loaded_files = data.values().parallelStream()
                .flatMap(graph -> graph.vertexSet().stream()).map(node -> node.file_info.sha2_id).collect(Collectors.toSet());
        List<FileInfo> benign_nodes = file_info.values().parallelStream().filter(file -> file.is_benign)
                .filter(file -> loaded_files.contains(file.sha2_id)).collect(Collectors.toList());
        List<FileInfo> benign_nodes_vt = benign_nodes.parallelStream().filter(FileInfo::hasVT_Info).collect(Collectors.toList());

        List<FileInfo> malicious_nodes = file_info.values().parallelStream().filter(file -> !file.is_benign)
                .filter(file -> loaded_files.contains(file.sha2_id)).collect(Collectors.toList());
        List<FileInfo> malicious_nodes_vt = malicious_nodes.parallelStream().filter(FileInfo::hasVT_Info).collect(Collectors.toList());

        Function<List<FileInfo>, Integer> ld_processing_function = (file_info_list) -> {
            return (int) file_info_list.parallelStream()
                    .map(file -> file.redirect.sha2)
                    .filter(sha256 -> local_detector.isMalware(sha256, null)).count();
        };
        System.out.println("\n================= evaluateLD_Parameters() =================");
        System.out.format("Benign file count: %d   Malicious file count: %d\n", benign_nodes.size(), malicious_nodes.size());

        for (double threshold = -0.01; threshold < 1.01; threshold += 0.001) {
            parameters.LD_threshold = threshold;

            double ld_false_positives = (double) ld_processing_function.apply(benign_nodes) / benign_nodes.size();
            double ld_false_positives_vt = (double) ld_processing_function.apply(benign_nodes_vt) / benign_nodes_vt.size();

            double ld_true_positives = (double) ld_processing_function.apply(malicious_nodes) / malicious_nodes.size();
            double ld_true_positives_vt = (double) ld_processing_function.apply(malicious_nodes_vt) / malicious_nodes_vt.size();
            results.add(new double[] {ld_false_positives, ld_true_positives, ld_false_positives_vt, ld_true_positives_vt});
            System.out.format("Threshold: %f   Overall FP:TP = %f/%f,   VT FP:TP = %f/%f\n",
                    threshold, ld_false_positives, ld_true_positives, ld_false_positives_vt, ld_true_positives_vt);
        }
        System.out.println("================= evaluateLD_Parameters() =================");

        try (
                OutputStreamWriter stream_writer = new FileWriter(parameters.out_folder + "/ld_roc.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            for (double entry[] : results)
                writer.write(String.format("%f,%f,%f,%f\n", entry[0],entry[1],entry[2],entry[3]));

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static void printNodeStatistics(Map<String, FileInfo> file_info,
                                           Map<Long, DefaultDirectedGraphWrapper> data, Map<Long, Map<String, DefaultDirectedGraphWrapper>> ig_data) {
        System.out.println("\n================= printNodeStatistics() =================");
        long benign_node_count = data.values().parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isBenign).count();
        long malicious_node_count = data.values().parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isMalicious).count();

        long benign_unique_node_count = data.values().parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isBenign).map(node -> node.sha2_id).distinct().count();
        long malicious_unique_node_count = data.values().parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isMalicious).map(node -> node.sha2_id).distinct().count();

        long benign_exec_node_count = data.values().parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isBenign).filter(Node::isExecutable).count();
        long benign_non_exec_node_count = data.values().parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isBenign).filter(node -> !node.isExecutable()).count();

        long malicious_exec_node_count = data.values().parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isMalicious).filter(Node::isExecutable).count();
        long malicious_non_exec_node_count = data.values().parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isMalicious).filter(node -> !node.isExecutable()).count();


        long benign_unique_exec_node_count = data.values().parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isBenign).filter(Node::isExecutable).map(node -> node.sha2_id).distinct().count();
        long benign_unique_non_exec_node_count = data.values().parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isBenign).filter(node -> !node.isExecutable()).map(node -> node.sha2_id).distinct().count();

        long malicious_unique_exec_node_count = data.values().parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isMalicious).filter(Node::isExecutable).map(node -> node.sha2_id).distinct().count();
        long malicious_unique_non_exec_node_count = data.values().parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isMalicious).filter(node -> !node.isExecutable()).map(node -> node.sha2_id).distinct().count();

        Set<Long> infected_machines = data.values().parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isMalicious).map(node -> node.machine_id).collect(Collectors.toSet());
        Set<Long> noninfected_machines = new HashSet<>(data.keySet());
        noninfected_machines.removeAll(infected_machines);

        long benign_node_exec = data.values().parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isBenign).filter(Node::isExecutable).count();
        long malicious_node_exec = data.values().parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isMalicious).filter(Node::isExecutable).count();

        List<String> benign_downloaders = ig_data.values().parallelStream().flatMap(map -> map.values().stream())
                .map(graph -> graph.root_node).filter(Node::isBenign).map(node -> node.sha2_id).collect(Collectors.toList());
        List<String> malicious_downloaders = ig_data.values().parallelStream().flatMap(map -> map.values().stream())
                .map(graph -> graph.root_node).filter(Node::isMalicious).map(node -> node.sha2_id).collect(Collectors.toList());

        Set<String> benign_unique_downloaders = new HashSet<>(benign_downloaders);
        Set<String> malicious_unique_downloaders = new HashSet<>(malicious_downloaders);


        System.out.format("benign_node_count: %,d malicious_node_count: %,d\n",
                benign_node_count, malicious_node_count);
        System.out.format("benign_unique_node_count: %,d malicious_unique_node_count: %,d\n",
                benign_unique_node_count, malicious_unique_node_count);
        System.out.format("benign_exec_node_count: %,d benign_non_exec_node_count: %,d malicious_exec_node_count: %,d malicious_non_exec_node_count: %,d\n",
                benign_exec_node_count, benign_non_exec_node_count, malicious_exec_node_count, malicious_non_exec_node_count);
        System.out.format("benign_unique_exec_node_count: %,d benign_unique_non_exec_node_count: %,d malicious_unique_exec_node_count: %,d malicious_unique_non_exec_node_count: %,d\n",
                benign_unique_exec_node_count, benign_unique_non_exec_node_count, malicious_unique_exec_node_count, malicious_unique_non_exec_node_count);
        System.out.format("Infected machines: %,d Noninfected machines: %,d\n", infected_machines.size(), noninfected_machines.size());
        System.out.format("benign unique downloaders %,d, malicious unique downloaders %,d\n", benign_unique_downloaders.size(), malicious_unique_downloaders.size());
        System.out.format("benign downloaders %,d, malicious downloaders %,d\n", benign_downloaders.size(), malicious_downloaders.size());
        System.out.format("benign exec %,d, malicious exec %,d\n", benign_node_exec, malicious_node_exec);
        System.out.println("================= ===================== =================\n");
    }
}
