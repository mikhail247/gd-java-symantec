package edu.ut.austin.NBD;

import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.DataInputOutput.GraphDataLoader;
import edu.ut.austin.FeatureExtractor.FeatureExtractor;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.GraphClasses.Edge;
import edu.ut.austin.GraphClasses.Node;
import edu.ut.austin.ML_Models.LocalDetector;
import edu.ut.austin.ML_Models.SparkRandomForestWrapper;
import edu.ut.austin.ML_Models.TrainML_Models;
import org.apache.commons.lang3.tuple.*;
import org.jgrapht.traverse.BreadthFirstIterator;
import org.nustaq.serialization.FSTConfiguration;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 3/14/2017.
 */
public class DomainBasedNBD implements NBD_Interface {

    public static class ClusterStorage implements Cluster {
        public int cluster_id;
        public String domain_or_url;
        //<machine id, list of selected nodes>
        public Map<Long, List<Node>> nodes;
        public Set<Long> machine_ids;
        public long time_stamp;

        public ClusterStorage(int cluster_id) {
            this.cluster_id = cluster_id;
            this.domain_or_url = "";
            this.nodes = new ConcurrentHashMap<>();
            this.machine_ids = Collections.newSetFromMap(new ConcurrentHashMap<Long, Boolean>());
        }

        public ClusterStorage(int cluster_id, String domain) {
            this(cluster_id);
            this.domain_or_url = domain;
        }

        @Override
        public int getClusterID() {
            return cluster_id;
        }

        public void setDomainName(String domain) {
            if (!domain.isEmpty())
                this.domain_or_url = domain;
        }

        public synchronized void addNodes(long machine_id, List<Node> new_nodes) {
            this.nodes.putIfAbsent(machine_id, new LinkedList<>());
            Set<Node> nodes = new HashSet<>(this.nodes.get(machine_id));
            nodes.addAll(new_nodes);
            this.nodes.put(machine_id, new LinkedList<>(nodes));
        }

        public Pair<Integer, Integer> getSize() {
            return new ImmutablePair<>(nodes.size(), machine_ids.size());
        }

        // <benign count, malicious count>, i.e. file count
        public Pair<Integer, Integer> getBenignMaliciousNodeCount() {
            MutablePair<Integer, Integer> pair = new MutablePair<>(0, 0);
            pair.setLeft((int) nodes.values().stream().flatMap(Collection::stream).filter(Node::isBenign).count());
            pair.setRight((int) nodes.values().stream().flatMap(Collection::stream).filter(Node::isMalicious).count());
            return pair;
        }

        // <benign count, malicious count>, i.e. file count
        public Pair<Integer, Integer> getBenignMaliciousMachineCount() {
            int infected_machine_count = (int) nodes.entrySet().stream()
                    .filter(entry -> entry.getValue().stream().anyMatch(Node::isMalicious)).count();
            int benign_machine_count = nodes.size() - infected_machine_count;
            MutablePair<Integer, Integer> pair = new MutablePair<>(benign_machine_count, infected_machine_count);
            return pair;
        }

        // <machine, <benign sha2, malicious sha2>>
        @Override
        public Map<Long, Pair<Set<String>, Set<String>>> getBenignMaliciousNodeMap() {
            Set<String> benign_set, malicious_set;
            Map<Long, Pair<Set<String>, Set<String>>> ret = new ConcurrentHashMap<>();

            for (Map.Entry<Long, List<Node>> entry : nodes.entrySet()) {
                benign_set = entry.getValue().stream().filter(Node::isBenign)
                        .map(node -> node.sha2_id).collect(Collectors.toSet());
                malicious_set = entry.getValue().stream().filter(Node::isMalicious)
                        .map(node -> node.sha2_id).collect(Collectors.toSet());
                ret.putIfAbsent(entry.getKey(), new ImmutablePair<>(benign_set, malicious_set));
            }
            return ret;
        }

        @Override
        public Set<Node> getUniqueCluster() {
            return nodes.values().stream().flatMap(Collection::stream).collect(Collectors.toSet());
        }

        @Override
        public List<Node> getAllClusterNodes() {
            return nodes.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
        }

        public void setVirtualTime(long time_stamp) {
            this.time_stamp = time_stamp;
        }

        private String getClusterParameters() {
            ConfigurationParameters parameters = ConfigurationParameters.getInstance();

            if (isEmpty())
                return null;

            if (!parameters.dump_cluster_stat)
                return null;
            Pair<Integer, Integer> node_stat = getBenignMaliciousNodeCount();
            Pair<Integer, Integer> machine_stat = getBenignMaliciousMachineCount();

            // ben_files, mal_files, ben_machines, mal_machines
            String str = node_stat.getLeft() + "," + node_stat.getRight()
                    + "," + machine_stat.getLeft() + "," + machine_stat.getRight();
            return str;
        }

        @Override
        public boolean isEmpty(){
            return nodes.isEmpty();
        }

        @Override
        public String getClusterName() {
            return domain_or_url;
        }
    }

    private FeatureExtractor feature_extractor;
    private final SparkRandomForestWrapper RF_model;
    private ArrayList<Pair<Double, Double>> feature_scaling;
    private Map<String, Pair<Double, Integer>> domain_name_suspiciousness;
    private Map<String, MutableTriple<Double, Integer, Integer>> domain_url_suspiciousness;
    private LocalDetector local_detector;

    // ThreadLocal
    public ThreadLocal<ArrayList<DomainBasedNBD.ClusterStorage>> storage = ThreadLocal.withInitial(() -> {
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        ArrayList<DomainBasedNBD.ClusterStorage> array = new ArrayList<>(global_parameters.suspicious_domain_count);
        IntStream.range(0, global_parameters.suspicious_domain_count)
                .forEach(i -> array.add(new DomainBasedNBD.ClusterStorage(i)));
        return array;
    });

    public DomainBasedNBD(GraphDataLoader loader) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        this.domain_name_suspiciousness = loader.getDomainNameSuspiciousness();
        this.domain_url_suspiciousness = loader.getURLSuspiciousness();
        RF_model = TrainML_Models.loadGraphRF_Model();

        long first_time_stamp = loader.getFirstTimeStamp() - 1;
        long last_time_stamp = loader.getLastTimeStamp() + parameters.time_window_size;
        long step_count = (last_time_stamp - first_time_stamp) / parameters.clustering_retraining_interval + 1;

        // load scaling parameters
        FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();
        Logger logger = GlobalParameters.getInstance().detection_logger;

        try {
            byte serialized_object[] = Files.readAllBytes(Paths.get(parameters.graph_scaling_parameters));
            feature_scaling = (ArrayList<Pair<Double, Double>>) conf.asObject(serialized_object);
            logger.info("graph_scaling_parameters object loaded");
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Cannot deserialize graph_scaling_parameters object.", e);
        }
        this.local_detector = TrainML_Models.loadLD_Model(parameters.LF_implementation_type);
    }

    // assumes complete graphs rather than IGs
    @Override
    public void augmentNBD_Storage(long time_stamp, Collection<DefaultDirectedGraphWrapper> graphs, FeatureExtractor feature_extractor) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        // init node RFC scores that will be used later to detect neighborhoods
        // graphs.parallelStream().forEach(graph -> graph.initNodeScores(feature_extractor));
        ArrayList<DomainBasedNBD.ClusterStorage> clusters = storage.get();

        Set<String> set = graphs.parallelStream().flatMap(graph -> graph.vertexSet().stream())
                .map(node -> node.sha2_id).collect(Collectors.toSet());
        GlobalParameters.getInstance().malware_1.removeAll(set);
        System.out.println("1. Malware uncovered yet: " + GlobalParameters.getInstance().malware_1.size());
        assert (parameters.use_urls_as_nbd_seeds);

        graphs.parallelStream().forEach(graph -> {

            Set<Node> suspicious_nodes = graph.vertexSet().stream().filter(node ->
                    Stream.concat(node.incoming_url_edges.stream(), node.outgoing_url_edges.stream())
                            .anyMatch(url -> domain_name_suspiciousness.keySet().contains(url))
            ).collect(Collectors.toSet());

            suspicious_nodes.stream().map(start_node -> {
                BreadthFirstIterator<Node, Edge> iterator;
                List<Node> nodes = new LinkedList<>();
                nodes.add(start_node);
                iterator = new BreadthFirstIterator<>(graph, start_node);

                while (iterator.hasNext()) {
                    Node node = iterator.next();
                    boolean include_node = graph.edgesOf(node).stream()
                            .anyMatch(edge_obj -> domain_name_suspiciousness.get(edge_obj.url) != null);
                    if (include_node)
                        nodes.add(node);
                }
                return new ImmutablePair<>(start_node, nodes);
            }).forEach(pair -> {
                Stream.concat(pair.getLeft().incoming_url_edges.stream(), pair.getLeft().outgoing_url_edges.stream())
                        .filter(url -> domain_name_suspiciousness.keySet().contains(url)).forEach(url -> {
                    int cluster_id = domain_name_suspiciousness.get(url).getRight();
                    ClusterStorage container = clusters.get(cluster_id);
                    container.setDomainName(url);
                    container.addNodes(graph.machine_id, pair.getRight());
                    container.setVirtualTime(time_stamp);
                });
            });
        });

        if (parameters.dump_cluster_stat) {
            String str = storage.get().stream().filter(cluster -> !cluster.isEmpty())
                    .map(ClusterStorage::getClusterParameters).collect(Collectors.joining("\n"));
            global_parameters.synchronizedDataDump(global_parameters.cluster_writer_before_merging, str);
        }

        Set<String> set_temp = storage.get().stream().flatMap(container -> container.nodes.values().stream())
                .flatMap(List::stream).map(node -> node.sha2_id).collect(Collectors.toSet());
        GlobalParameters.getInstance().malware_2.removeAll(set_temp);
        System.out.println("2. Malware uncovered yet: " + GlobalParameters.getInstance().malware_2.size());
        mergeClusters();

        if (parameters.dump_cluster_stat) {
            String str = storage.get().stream().filter(cluster -> !cluster.isEmpty())
                    .map(ClusterStorage::getClusterParameters).collect(Collectors.joining("\n"));
            global_parameters.synchronizedDataDump(global_parameters.cluster_writer_after_merging, str);
        }
    }

    private void mergeClusters() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        ArrayList<DomainBasedNBD.ClusterStorage> array = new ArrayList<>();
        int cluster_counter = 0;

        ConcurrentSkipListMap<Double, ConcurrentLinkedDeque<ClusterStorage>> sorted_clusters = new ConcurrentSkipListMap<>(Collections.reverseOrder());
        storage.get().parallelStream().filter(cluster -> !cluster.isEmpty()).forEach(cluster -> {
            double score = -1;

            switch (parameters.use_ground_thruth_for_merging_clusters) {
                case GROUND_TRUTH:
                    Pair<Integer, Integer> pair = cluster.getBenignMaliciousNodeCount();
                    score = (double) pair.getRight() / pair.getLeft();
                    break;

                case URL_SCORE:
                    int benign_url_count = (int) cluster.getAllClusterNodes().stream()
                            .flatMap(node -> Stream.concat(node.incoming_url_edges.stream(), node.outgoing_url_edges.stream()))
                            .map(url -> domain_name_suspiciousness.get(url)).filter(Objects::nonNull)
                            .filter(url_score -> url_score.getLeft() < parameters.domain_score_threshold)
                            .mapToDouble(Pair::getLeft).count();

                    int malicious_url_count = (int) cluster.getAllClusterNodes().stream()
                            .flatMap(node -> Stream.concat(node.incoming_url_edges.stream(), node.outgoing_url_edges.stream()))
                            .map(url -> domain_name_suspiciousness.get(url)).filter(Objects::nonNull)
                            .filter(url_score -> url_score.getLeft() >= parameters.domain_score_threshold)
                            .mapToDouble(Pair::getLeft).count();
                    score = (double) malicious_url_count / (benign_url_count + malicious_url_count);
                    break;

                case FV_PERCANTAGE:
                    int node_count = cluster.getSize().getLeft();
                    int alert_count = (int) cluster.nodes.values().stream().flatMap(Collection::stream)
                            .map(node -> node.file_info.redirect.sha2)
                            .filter(sha2 -> local_detector.isMalware(sha2, null)).count();
                    score = (double) alert_count / node_count;
                    break;
            }
            sorted_clusters.putIfAbsent(score, new ConcurrentLinkedDeque<>());
            sorted_clusters.get(score).add(cluster);
        });

        // <machine_id, nodes>
        Map<Long, List<Node>>  map_temp = new HashMap<>();
        ClusterStorage cluster_new = new ClusterStorage(cluster_counter ++);

        for (Map.Entry<Double, ConcurrentLinkedDeque<DomainBasedNBD.ClusterStorage>> entry_upper_level : sorted_clusters.entrySet()) {
            for (DomainBasedNBD.ClusterStorage cluster : entry_upper_level.getValue()) {
                if (cluster.nodes.isEmpty())
                    continue;
                cluster_new.domain_or_url += cluster.domain_or_url + "_---_";

                for (Map.Entry<Long, List<Node>> entry : cluster.nodes.entrySet()) {
                    map_temp.putIfAbsent(entry.getKey(), new LinkedList<>());
                    map_temp.get(entry.getKey()).addAll(entry.getValue());
                }
                int counter = map_temp.values().stream().mapToInt(List::size).sum();

                if (counter > parameters.GD_fvs_per_nbd_count) {
                    cluster_new.machine_ids = map_temp.keySet();
                    cluster_new.nodes = new ConcurrentHashMap<>(map_temp);
                    array.add(cluster_new);
                    cluster_new = new ClusterStorage(cluster_counter ++);
                    map_temp.clear();
                }
            }
        }

        if (!map_temp.isEmpty()) {
            cluster_new.machine_ids = map_temp.keySet();
            cluster_new.nodes = new ConcurrentHashMap<>(map_temp);
            array.add(cluster_new);
        }
        storage.set(array);
    }

    public int getDynamicParameter() {
        return storage.get().size();
    }

    @Override
    public void clear() {
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        ArrayList<DomainBasedNBD.ClusterStorage> array = new ArrayList<>(global_parameters.suspicious_domain_count);
        IntStream.range(0, global_parameters.suspicious_domain_count)
                .forEach(i -> array.add(new DomainBasedNBD.ClusterStorage(i)));
        storage.set(array);
    }

    public void printStatistics() {
        Logger logger = GlobalParameters.getInstance().logger;
        logger.info("Domains Count: " + storage.get().size());
        String bin_sizes = "<nodes.size, machine_ids.size>: " + storage.get().stream()
                .map(item -> item.getSize().toString()).collect(Collectors.joining(","));
        String benign_malicious_counts = "<benign files (count), malicious files (count)>: " + storage.get().stream()
                .map(item -> item.getBenignMaliciousNodeCount().toString()).collect(Collectors.joining(","));
        logger.info(bin_sizes);
        logger.info(benign_malicious_counts);
    }
}
