package edu.ut.austin.NBD;

import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.GraphClasses.Node;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Mikhail on 6/4/2017.
 */
public interface Cluster {
    public int getClusterID();
    public Pair<Integer, Integer> getSize();
    // <benign count, malicious count>, i.e. file count
    public Pair<Integer, Integer> getBenignMaliciousNodeCount();
    public Map<Long, Pair<Set<String>, Set<String>>> getBenignMaliciousNodeMap();
    public Set<Node> getUniqueCluster();
    public List<Node> getAllClusterNodes();
    public String getClusterName();
    public boolean isEmpty();
}
