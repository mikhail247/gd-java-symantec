package edu.ut.austin.NBD;

import edu.ut.austin.FeatureExtractor.FeatureExtractor;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;

import java.util.Collection;

/**
 * Created by Mikhail on 3/9/2017.
 */
public interface NBD_Interface {
    void augmentNBD_Storage(long time_stamp, Collection<DefaultDirectedGraphWrapper> graphs, FeatureExtractor feature_extractor);
    int getDynamicParameter();
    void printStatistics();

    void clear();
}
