package edu.ut.austin.NBD.PrefixGraph;

import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.GraphClasses.Node;

/**
 * Created by Mikhail on 2/1/2017.
 */
public class PrefixGraph_GraphBased extends PrefixGraph<DefaultDirectedGraphWrapper> {

    public PrefixGraph_GraphBased() {
        super();
    }

    @Override
    protected DefaultDirectedGraphWrapper convertGraphToTypeT(DefaultDirectedGraphWrapper graph) {
        return graph;
    }

    @Override
    protected boolean isSubGraph(DefaultDirectedGraphWrapper graph, DefaultDirectedGraphWrapper subgraph) {
        return graph.containsSubGraph(subgraph);
    }

    @Override
    protected void subtractGraph(DefaultDirectedGraphWrapper graph, DefaultDirectedGraphWrapper subgraph) {
        graph.subtractGraph(subgraph);
    }

    @Override
    protected boolean isGraphEmpty(DefaultDirectedGraphWrapper graph) {
        return graph.isGraphEmpty();
    }

    @Override
    protected DefaultDirectedGraphWrapper clone(DefaultDirectedGraphWrapper graph) {
        return (DefaultDirectedGraphWrapper) graph.clone();
    }

    @Override
    protected boolean isNodeInGraph(DefaultDirectedGraphWrapper graph, Node node) {
        return graph.containsVertex(node);
    }

    @Override
    protected int getBenignNodeCount(DefaultDirectedGraphWrapper graph) {
        return graph.getBenignNodeCount();
    }

    @Override
    protected int getMaliciousNodeCount(DefaultDirectedGraphWrapper graph) {
        return graph.getMaliciousNodeCount();
    }

    @Override
    protected long getLastTimeStamp(DefaultDirectedGraphWrapper graph) {
        return graph.getMaxTimeStamp();
    }

    @Override
    protected boolean isGraphGreater(DefaultDirectedGraphWrapper greater_graph, DefaultDirectedGraphWrapper smaller_graph) {
        if (greater_graph == null && smaller_graph == null)
            return false;
        else if (smaller_graph == null)
            return true;
        else {
            if (greater_graph.vertexSet().size() != smaller_graph.vertexSet().size())
                return greater_graph.vertexSet().size() > smaller_graph.vertexSet().size();
            else
                return greater_graph.edgeSet().size() > smaller_graph.edgeSet().size();
        }
    }

    @Override
    protected long getMachineID(DefaultDirectedGraphWrapper graph) {
        return graph.machine_id;
    }
}
