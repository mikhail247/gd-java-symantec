package edu.ut.austin.NBD.PrefixGraph;

import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.GraphClasses.Node;
import org.apache.commons.lang3.tuple.*;
import scala.collection.mutable.StringBuilder;

import java.io.*;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public abstract class PrefixGraph<T> implements PrefixGraph_ImmutableView<T> {
    private Map<Node, Integer> file_prevalence_map;
    private Map<Long, Pair<Integer, Integer>> machine_files; //<machine, <benign files, malicious files>>
    private Map<T, TreeNode<T>> merging_map; // maps a complete graph to a prefix graph node
    protected TreeNode<T> root;
    public volatile int node_count;

    protected abstract T convertGraphToTypeT(DefaultDirectedGraphWrapper graph);
    protected abstract boolean isSubGraph(T graph, T subgraph);
    protected abstract void subtractGraph(T graph, T subgraph);
    protected abstract boolean isGraphEmpty(T graph);
    protected abstract T clone(T graph);
    protected abstract boolean isNodeInGraph(T graph, Node node);
    protected abstract int getBenignNodeCount(T graph);
    protected abstract int getMaliciousNodeCount(T graph);
    protected abstract long getLastTimeStamp(T complete_graph);
    protected abstract boolean isGraphGreater(T greater_graph, T smaller_graph);
    protected abstract long getMachineID(T graph);

    protected PrefixGraph() {
        file_prevalence_map = new HashMap<>();
        machine_files = new HashMap<>();
        merging_map = null;
        root = new TreeNode();
        node_count = 1;
    }

    public static PrefixGraph createNBD_Storage() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        try {
            return (PrefixGraph) parameters.matching_type_class.newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return null;
    }

    public TreeNode<T> getRoot() {
        return root;
    }

    public void augmentPrefixTree(int thread_id, Map<Long, DefaultDirectedGraphWrapper> graphs) { // <machine_id, graph>
        int counter = 0;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        if (parameters.detection_mechanism == ConfigurationParameters.DetectionEnum.NONE)
            return;

        int vcpu_count =  GlobalParameters.getInstance().executor_size;
        ArrayList<List<DefaultDirectedGraphWrapper>> partitions = new ArrayList<>(vcpu_count);
        for (int i = 0; i < vcpu_count; i++)
            partitions.add(new LinkedList<>());

        for (Map.Entry<Long, DefaultDirectedGraphWrapper> entry : graphs.entrySet()) {
            DefaultDirectedGraphWrapper graph = entry.getValue();
            partitions.get(counter).add(graph);
            counter = (counter + 1) % vcpu_count;
        }

        List<Callable<Void>> callables = new LinkedList<>();
        for (int i = 0; i < vcpu_count; i ++) {
            Callable<Void> callable = new ParallelAugmentPrefixGraph(partitions.get(i));
            callables.add(callable);
        }
        try {
            System.out.println("Augmenting Prefix Graph");
            GlobalParameters.getInstance().executors.get(thread_id).invokeAll(callables);
            System.out.println("Feature Vectors extracted");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        fillOutFilePrevalenceMap(graphs);
        fillOutMachineFilesMap(graphs);
        assert(isDAG());
        System.out.format("augmentPrefixTree(): Processed %d graphs\n", counter);
    }

    private void fillOutFilePrevalenceMap(Map<Long, DefaultDirectedGraphWrapper> graphs) {
        System.out.println("Building File Prevalence Map");
        file_prevalence_map.clear();

        for (Map.Entry<Long, DefaultDirectedGraphWrapper> entry : graphs.entrySet()) {
            DefaultDirectedGraphWrapper graph = entry.getValue();
            Set<Node> unique_nodes = graph.vertexSet().stream().distinct().collect(Collectors.toSet());

            for (Node node : unique_nodes) {
                Integer counter = file_prevalence_map.get(node);
                if (counter == null)
                    counter = 0;
                file_prevalence_map.put(node, counter+1);
            }
        }
    }

    private void fillOutMachineFilesMap(Map<Long, DefaultDirectedGraphWrapper> graphs) {
        Pair<Integer, Integer> pair;

        for (Map.Entry<Long, DefaultDirectedGraphWrapper> entry : graphs.entrySet()) {
            long machine_id = entry.getKey();
            DefaultDirectedGraphWrapper graph = entry.getValue();
            pair = new ImmutablePair<>(graph.getBenignNodeCount(), graph.getMaliciousNodeCount());
            machine_files.put(machine_id, pair);
        }

        Deque<TreeNode<T>> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()) {
            TreeNode<T> node = queue.pollLast();
            node.benign_file_count = node.machine_ids.stream().mapToInt(machine_id -> machine_files
                        .get(machine_id).getLeft()).sum();
            node.malicious_file_count = node.machine_ids.stream().mapToInt(machine_id -> machine_files
                        .get(machine_id).getRight()).sum();
            queue.addAll(node.children.values());
        }
    }

    public int getNodeCount() {
        TreeNode<T> node;
        Set<TreeNode<T>> visited = new HashSet<>();
        Deque<TreeNode<T>> stack = new LinkedList<>();
        stack.add(root);

        while (!stack.isEmpty()) {
            node = stack.pollLast();
            if (visited.contains(node))
                continue;

            Set<TreeNode<T>> children = node.children.values()
                    .stream().collect(Collectors.toSet());
            stack.addAll(children);
            visited.add(node);
        }
        return visited.size();
    }

    /* We updating estimates of the root parameters because otherwise, we would need to
    * */
    private void updateRootParameters(Map<Long, DefaultDirectedGraphWrapper> graphs) {
        root.benign_file_count = 0;
        root.malicious_file_count = 0;

        for (Map.Entry<T, TreeNode<T>> entry : root.children.entrySet()) {
            root.benign_file_count += entry.getValue().benign_file_count;
            root.malicious_file_count += entry.getValue().malicious_file_count;
        }
    }

    @Deprecated
    private void augmentPrefixTreeGraph(T graph, long machine_id) {
        T graph_t = clone(graph);
        Pair<Integer, Integer> pair = augmentPrefixTreeGraph(graph, graph_t, root, machine_id);
        if (pair != null) {
            root.benign_file_count += pair.getLeft();
            root.malicious_file_count += pair.getRight();
        }
    }

    @Deprecated
    private Pair<Integer, Integer> augmentPrefixTreeGraph(T graph, T graph_t, TreeNode<T> node, long machine_id) {
        boolean is_graph_empty;
        boolean machine_added_flag = false;
        boolean match_id_found = false;
        Pair<Integer, Integer> pair;
        TreeNode<T> succ_t, succ = null;
        T max_prefix = null;

        // match by machine_id
        for (Map.Entry<T, TreeNode<T>> transition : node.children.entrySet()) {
            T subgraph = transition.getKey();
            succ = transition.getValue();

            if (succ.machine_ids.contains(machine_id)) {
                match_id_found = true;
                subtractGraph(graph_t, subgraph);
                break;
            }
        }
        if (isGraphEmpty(graph_t)) // the same graph that we had before -- nothing to update
            return null;

        if (match_id_found) {
            pair = augmentPrefixTreeGraph(graph, graph_t, succ, machine_id);
            if (pair != null) {
                succ.benign_file_count += pair.getLeft();
                succ.malicious_file_count += pair.getRight();
            }
            return pair;
        }

        // match by max prefix
        for (Map.Entry<T, TreeNode<T>> transition : node.children.entrySet()) {
            T subgraph = transition.getKey();
            succ_t = transition.getValue();

            if (isSubGraph(graph_t, subgraph)) {
                if (isGraphGreater(subgraph, max_prefix)) { // search for max prefix
                    max_prefix = subgraph;
                    succ = succ_t;
                }
            }
        }

        if (max_prefix != null) { // found non-null prefix
            node.machine_ids.add(machine_id);
            succ.machine_ids.add(machine_id);
            subtractGraph(graph_t, max_prefix);

            if (isGraphEmpty(graph_t)) {
                int benign_node_count = getBenignNodeCount(succ.complete_graph);
                int malicious_node_count = getMaliciousNodeCount(succ.complete_graph);
                pair = new ImmutablePair<>(benign_node_count, malicious_node_count);
            } else
                pair = augmentPrefixTreeGraph(graph, graph_t, succ, machine_id);

            if (pair != null) {
                succ.benign_file_count += pair.getLeft();
                succ.malicious_file_count += pair.getRight();
            }
            return pair;
        } else { // need to add a new node
            succ = merging_map.get(graph);
            boolean flag = succ == null;
            if (succ == null) {
                succ = new TreeNode(clone(graph), clone(graph_t));
                merging_map.put(graph, succ);
            }

            node.machine_ids.add(machine_id);
            node.children.put(clone(graph_t), succ);

            if (!isDAG())
                isDAG();

            succ.machine_ids.add(machine_id);
            int benign_node_count = getBenignNodeCount(succ.complete_graph);
            int malicious_node_count = getMaliciousNodeCount(succ.complete_graph);
            pair = new ImmutablePair<>(benign_node_count, malicious_node_count);
            return pair;
        }
    }

    private void augmentPrefixTree(T graph){
        long machine_id = getMachineID(graph);
        T graph_t = clone(graph);
        TreeNode<T> node = root;

        while (!isGraphEmpty(graph_t)) {
            TreeNode node_t = null;
            node.lock.readLock().lock();

            for (Map.Entry<T, TreeNode<T>> transition : node.children.entrySet()) {
                T subgraph = transition.getKey();

                if (isSubGraph(graph_t, subgraph)) {
                    node_t = transition.getValue();
                    node.machine_ids.add(machine_id);
                    subtractGraph(graph_t, subgraph);
                    break;
                }
            }
            if (node_t == null) {
                // we guarantee that at most one thread may be writing to the map, i.e.
                // there may be multiple readers in this section, but only one of them
                // will change the value of the AtomicBoolean -- the writer
                boolean success_flag = node.writer_flag.compareAndSet(false, true);
                node.lock.readLock().unlock();
                if (!success_flag)
                    continue;
            } else
                node.lock.readLock().unlock();

            if (node_t == null) {
                // acquire lock
                // only one thread may try to acquire the lock
                node_count ++;
                node.lock.writeLock().lock();
                node_t = new TreeNode(clone(graph), clone(graph_t));
                node_t.machine_ids.add(machine_id);

                node.machine_ids.add(machine_id);
                node.children.put(graph_t, node_t);

                node.writer_flag.set(false);
                node.lock.writeLock().unlock();
                break;
            }
            node = node_t;
        }
    }

    @Deprecated
    private TreeNode augmentPrefixTree(T graph, TreeNode<T> root, long machine_id) {
        TreeNode<T> node = null;

        for (Map.Entry<T, TreeNode<T>> transition : root.children.entrySet()) {
            T subgraph = transition.getKey();

            if (isSubGraph(graph, subgraph)) {
                node = transition.getValue();
                subtractGraph(graph, subgraph);
                root.machine_ids.add(machine_id);
                break;
            }
        }

        if (isGraphEmpty(graph)) {
            root.machine_ids.add(machine_id);
            return null;
        }

        if (node == null) {
            // node merging: try to find an existing node first
            TreeNode node_t = null;
            if (node_t == null) {
                node_t = new TreeNode();
            }
            node_t.machine_ids.add(machine_id);
            root.children.put(graph, node_t);
            return node_t;
        }
        return augmentPrefixTree(graph, node, machine_id);
    }

    public boolean isDAG() {
        TreeNode<T> node;
        Set<TreeNode<T>> visited = new HashSet<>();
        Deque<TreeNode<T>> stack = new LinkedList<>();
        Set<TreeNode<T>> set_stack = new HashSet<>();
        stack.add(root); set_stack.add(root);

        while (!stack.isEmpty()) {
            node = stack.pollLast();
            set_stack.remove(node);
            if (visited.contains(node))
                continue;

            List<TreeNode<T>> children = node.children.values().stream().collect(Collectors.toList());
            Set<TreeNode<T>> on_stack = children.stream().filter(n -> set_stack.contains(n))
                            .collect(Collectors.toSet());
            if (!on_stack.isEmpty())
                return false;
            stack.addAll(children);
            set_stack.addAll(children);
            visited.add(node);
        }
        return true;
    }


    public double getFilePrevalenceScore(Node node) {
        Integer file_prevalence = file_prevalence_map.get(node);
        if (file_prevalence == null)
            return 0;
        else
            return (double) file_prevalence / root.machine_ids.size();
    }
    /*
    * We store machine_ids in a set to handle the case when we add duplicate machine ids
    * because the current data structure is a "prefix graph", not a classical prefix tree.
    *
    * Returns relative FP score, i.e. relative to the number of machines in the prefix graph
    * */
    public double getFilePrevalenceScoreTreeBased(Node node) {
        Set<Long> hosts = new HashSet<>();
        getFilePrevalenceScoreTreeBased(root, node, hosts);
        return (double) hosts.size() / root.machine_ids.size();
    }

    private void getFilePrevalenceScoreTreeBased(TreeNode<T> root, Node node, Set<Long> hosts) {
        for (Map.Entry<T, TreeNode<T>> entry : root.children.entrySet()) {
            if (isNodeInGraph(entry.getKey(), node))
                hosts.addAll(entry.getValue().machine_ids);
            else
                getFilePrevalenceScoreTreeBased(entry.getValue(), node, hosts);
        }
    }

    /* Returns the number of benign and malicious files per machine.
    * One machine may have multiple corresponding nodes, only the most recent one
    * according to a time_stamp is considered.
    * */
    @Override
    public Map<Long, Pair<Integer, Integer>> getBenignMaliciousNodeCount(TreeNode<T> node) {
        Set<Long> machine_ids = node.machine_ids;
        Map<Long, Pair<Integer, Integer>> view = machine_files.entrySet().stream().filter(entry -> machine_ids
                .contains(entry.getKey())).collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()));
        return Collections.unmodifiableMap(view);
    }

    //<machine_id, <benign files, malicious files>>
    @Deprecated
    public Map<Long, MutablePair<Integer, Integer>> getBenignMaliciousNodeCountLoop(TreeNode<T> node) {
        Map<Long, MutablePair<Integer, Integer>> ret = new HashMap<>();
        Set<TreeNode<T>> visited = new HashSet<>();
        Deque<TreeNode<T>> queue = new LinkedList<>();
        queue.addLast(node);

        while (!queue.isEmpty()) {
            TreeNode<T> n = queue.pollFirst();
            visited.add(n);

            if (n.children.isEmpty()) {
                int benign_file_count = getBenignNodeCount(n.complete_graph);
                int malicious_file_count = getMaliciousNodeCount(n.complete_graph);

                for (long machine_id : n.machine_ids) {
                    MutablePair<Integer, Integer> pair = ret.get(machine_id);
                    if (pair == null)
                        pair = new MutablePair<>(0, 0);
                    pair.setLeft(pair.getLeft() + benign_file_count);
                    pair.setRight(pair.getRight() + malicious_file_count);
                    ret.put(machine_id, pair);
                }
            } else {
                Set<TreeNode<T>> nodes_to_visit = n.children.values().stream()
                        .filter(node_t -> !visited.contains(node_t)).collect(Collectors.toSet());
                queue.addAll(nodes_to_visit);
            }
        }
        return ret;
    }

    public void printStatistics() {
        if (!ConfigurationParameters.getInstance().debug_info)
            return;

        int layer_ctr = 0;
        Logger logger = GlobalParameters.getInstance().prefix_tree_logger;
        Set<TreeNode> visited = new HashSet<>();
        // <mal_ratio, <mal_files, ben_files, # of machines>
        TreeMap<Integer, List<Triple<Integer, Integer, Double>>> map = new TreeMap<>();
        // number of machines per node on each level of the tree
        List<List<Integer>> machine_clustering = new LinkedList<>();

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root); queue.add(null);
        logger.info("Prefix Graph IntrospectionModule: <# of machines, <mal_files, ben_files, mal_ratio>>");

        while (true) {
            TreeNode<T> node = queue.poll();

            if (node == null) {
                List<Integer> nodes = map.entrySet().stream().map(entry -> entry.getKey())
                        .sorted(Comparator.reverseOrder()).collect(Collectors.toList());
                machine_clustering.add(nodes);
                logger.info(layer_ctr++ + ": " + map.descendingMap());
                if (queue.isEmpty())
                    break;
                queue.add(null);
                map.clear();
                continue;
            }
            int graph_file_count = node.benign_file_count + node.malicious_file_count;
            double mal_ratio = graph_file_count != 0 ? (double) node.malicious_file_count / graph_file_count : 0;

            List<Triple<Integer, Integer, Double>> list = map.get(node.machine_ids.size());
            if (list == null)
                list = new LinkedList<>();
            list.add(new ImmutableTriple<>(node.malicious_file_count, node.benign_file_count, mal_ratio));
            map.put(node.machine_ids.size(), list);

            visited.add(node);
            Set<TreeNode<T>> new_nodes = node.children.values().stream().filter(n -> !visited.contains(n)).collect(Collectors.toSet());
            queue.addAll(new_nodes);
        }
        logger.info("");
        writeStatisticsToFile(machine_clustering);
    }

    private void writeStatisticsToFile(List<List<Integer>> machine_clustering) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        int max_length = machine_clustering.stream().mapToInt(list -> list.size()).max().getAsInt();

        try (
                Writer writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(parameters.prefix_graph_file_name), "ascii"));
        ) {
            for (List<Integer> list : machine_clustering) {
                Integer padding[] = new Integer[max_length - list.size()];
                Arrays.fill(padding, -1);
                list.addAll(Arrays.asList(padding));
                StringBuilder str = new StringBuilder(list.stream()
                        .map(item -> item.toString()).collect(Collectors.joining(",")));

                for (int i = 0; i < max_length - list.size(); i ++)
                    str.append(',');

                writer.write(str.toString() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class ParallelAugmentPrefixGraph implements Callable<Void> {
        List<DefaultDirectedGraphWrapper> working_set;

        private ParallelAugmentPrefixGraph(List<DefaultDirectedGraphWrapper> working_set) {
            this.working_set = working_set;
        }

        @Override
        public Void call() throws Exception {
            for (DefaultDirectedGraphWrapper graph : working_set) {
                T graph_representation = convertGraphToTypeT(graph);
                augmentPrefixTree(graph_representation);
            }
            return null;
        }
    }
}
