package edu.ut.austin.NBD.PrefixGraph;

import edu.ut.austin.GraphClasses.Node;
import org.jgrapht.DirectedGraph;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by Mikhail on 2/2/2017.
 */
public class TreeNode<T> {
    public ReentrantReadWriteLock lock;
    public AtomicBoolean writer_flag;
    public Map<T, TreeNode<T>> children;
    public T complete_graph, subgraph;
    public Set<Long> machine_ids;   // synchronized

    // parameters of the subgraph rooted at the current node (including the current node)
    // depends on the number of machines
    public int benign_file_count, malicious_file_count;

    public TreeNode(T complete_graph, T sbugraph) {
        this();
        this.complete_graph = complete_graph;
        this.subgraph = sbugraph;
    }

    public TreeNode() {
        lock = new ReentrantReadWriteLock();
        writer_flag = new AtomicBoolean(false);

        children = new LinkedHashMap<>();
        machine_ids = Collections.newSetFromMap(new ConcurrentHashMap());
        benign_file_count = -1;
        malicious_file_count = -1;
    }

    public int getSubgraphNodeCount() {
        return benign_file_count + malicious_file_count;
    }

    // a bit hacky method -- no time on properly defining generics hierarchy
    private Set<Node> getGraphNodes(T graph) {
        Set<Node> nodes;

        if (graph == null)
            return new HashSet<>();

        if (graph instanceof DirectedGraph)
            nodes = ((DirectedGraph) graph).vertexSet();
        else if (graph instanceof Set)
            nodes = ((Set) graph);
        else
            throw new RuntimeException("Unsupported type");
        return nodes;
    }

    public boolean isGraphEmpty() {
        Set<Node> nodes = getGraphNodes(complete_graph);
        return nodes.isEmpty();
    }

    // returns the number of benign nodes
    // in the complete graph
    private int getBenignNodeCount(T graph) {
        Set<Node> nodes = getGraphNodes(graph);
        long benign_nodes_count = nodes.stream().filter(node ->
                node.file_info.is_benign != null && node.file_info.is_benign).count();
        return (int) benign_nodes_count;
    }

    // returns the number of malicious nodes
    // in the complete graph
    private int getMaliciousNodeCount(T graph) {
        Set<Node> nodes = getGraphNodes(graph);
        return nodes.size() - getBenignNodeCount(graph);
    }

    // returns the total number of nodes
    // in the complete graph
    private int getTotalNodeCount(T graph) {
        Set<Node> nodes = getGraphNodes(graph);
        return nodes.size();
    }

    public String toString() {
        return "children: " + children.size() + " IDs: " + machine_ids.size();
    }
}