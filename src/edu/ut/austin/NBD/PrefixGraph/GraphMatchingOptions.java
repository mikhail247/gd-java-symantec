package edu.ut.austin.NBD.PrefixGraph;

/**
 * Created by Mikhail on 2/2/2017.
 */
public enum GraphMatchingOptions {
    GRAPH_BASED, VERTEX_BASED, FILE_NAME_BASED
}
