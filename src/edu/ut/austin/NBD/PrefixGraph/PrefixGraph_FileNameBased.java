package edu.ut.austin.NBD.PrefixGraph;

import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.GraphClasses.Node;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Mikhail on 2/1/2017.
 */
public class PrefixGraph_FileNameBased extends PrefixGraph<Set<String>> {

    public PrefixGraph_FileNameBased() {
        super();
    }

    @Override
    protected Set<String> convertGraphToTypeT(DefaultDirectedGraphWrapper graph) {
        Set<String> graph_node_labels = graph.vertexSet().stream()
                .map(n -> n.file_name).collect(Collectors.toSet());
        return graph_node_labels;
    }

    @Override
    protected boolean isSubGraph(Set<String> graph, Set<String> subgraph) {
        return graph.containsAll(subgraph);
    }

    @Override
    protected void subtractGraph(Set<String> graph, Set<String> subgraph) {
        graph.removeAll(subgraph);
    }

    @Override
    protected boolean isGraphEmpty(Set<String> graph) {
        return graph.isEmpty();
    }

    @Override
    protected Set<String> clone(Set<String> graph) {
        Set<String> graph_t = new HashSet<>();
        graph_t.addAll(graph);
        return graph_t;

    }

    @Override
    protected boolean isNodeInGraph(Set<String> graph, Node node) {
        return graph.contains(node.file_name);
    }

    @Override
    protected int getBenignNodeCount(Set<String> graph) {
        return -1;
    }

    @Override
    protected int getMaliciousNodeCount(Set<String> graph) {
        return -1;
    }

    @Override
    protected long getLastTimeStamp(Set<String> complete_graph) {
        return 0;
    }

    @Override
    protected boolean isGraphGreater(Set<String> greater_graph, Set<String> smaller_graph) {
        return false;
    }

    @Override
    protected long getMachineID(Set<String> graph) {
        return 0;
    }
}
