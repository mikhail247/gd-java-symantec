package edu.ut.austin.NBD.PrefixGraph;

import edu.ut.austin.GraphClasses.Node;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;

/**
 * Created by Mikhail on 2/13/2017.
 */
public interface PrefixGraph_ImmutableView<T> {
    public double getFilePrevalenceScore(Node node);
    //<machine_id, <benign files, malicious files>>
    public Map<Long, Pair<Integer, Integer>> getBenignMaliciousNodeCount(TreeNode<T> node);
}
