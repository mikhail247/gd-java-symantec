package edu.ut.austin.NBD;

import edu.ut.austin.DataInputOutput.GraphDataLoader;
import edu.ut.austin.FeatureExtractor.FeatureExtractor;
import edu.ut.austin.FeatureExtractor.ParallelNonGlobalFeatureExtractor;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.GraphClasses.Edge;
import edu.ut.austin.GraphClasses.Node;
import edu.ut.austin.GraphViz.GraphViz;
import edu.ut.austin.ML_Models.SparkRandomForestWrapper;
import edu.ut.austin.ML_Models.TrainML_Models;
import edu.ut.austin.Utils.Distance;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.clustering.BisectingKMeans;
import org.apache.spark.mllib.clustering.BisectingKMeansModel;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.jgrapht.DirectedGraph;
import org.jgrapht.traverse.BreadthFirstIterator;
import org.nustaq.serialization.FSTConfiguration;
import scala.tools.nsc.Global;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

/**
 * Created by Mikhail on 3/14/2017.
 */
public class ClusterLSH implements NBD_Interface {
    public static class ClusterStorage implements Cluster {
        public int cluster_id;
        public Set<DefaultDirectedGraphWrapper> graphlets;
        public Set<Long> machine_ids;

        public ClusterStorage(int cluster_id) {
            this.cluster_id = cluster_id;
            graphlets = Collections.newSetFromMap(new ConcurrentHashMap<DefaultDirectedGraphWrapper, Boolean>());
            machine_ids = Collections.newSetFromMap(new ConcurrentHashMap<Long, Boolean>());
        }

        @Override
        public int getClusterID() {
            return cluster_id;
        }

        public Pair<Integer, Integer> getSize() {
            return new ImmutablePair<>(graphlets.size(), machine_ids.size());
        }

        // <benign count, malicious count>, i.e. file count
        public Pair<Integer, Integer> getBenignMaliciousNodeCount() {
            MutablePair<Integer, Integer> pair = new MutablePair<>(0, 0);

            for (DefaultDirectedGraphWrapper graphlet : graphlets) {
                pair.setLeft(pair.getLeft() + graphlet.getBenignNodeCount());
                pair.setRight(pair.getRight() + graphlet.getMaliciousNodeCount());
            }
            return pair;
        }

        public Map<Long, Pair<Integer, Integer>> getBenignMaliciousNodeCountMap() {
            Integer count;
            Map<Long, Pair<Integer, Integer>> ret = new HashMap<>();
            Map<Long, Integer> benign_map = new HashMap<>();
            Map<Long, Integer> malicious_map = new HashMap<>();

            for (DefaultDirectedGraphWrapper graphlet : graphlets) {
                int benign_count = graphlet.getBenignNodeCount();
                int malicious_count = graphlet.getMaliciousNodeCount();

                count = benign_map.get(graphlet.machine_id);
                count = count == null ? 0 : count;
                count += benign_count;
                benign_map.put(graphlet.machine_id, count);

                count = malicious_map.get(graphlet.machine_id);
                count = count == null ? 0 : count;
                count += malicious_count;
                malicious_map.put(graphlet.machine_id, count);
            }

            for (Map.Entry<Long, Integer> entry : benign_map.entrySet()) {
                long machine_id = entry.getKey();
                int benign_count = entry.getValue();
                int malicious_count = malicious_map.get(machine_id);
                Pair<Integer, Integer> pair = new ImmutablePair<>(benign_count, malicious_count);
                ret.put(machine_id, pair);
            }
            return ret;
        }

        // <machine, <benign sha2, malicious sha2>>
        public Map<Long, Pair<Set<String>, Set<String>>> getBenignMaliciousNodeMap() {
            Set<String> benign_set, malicious_set;
            Map<Long, Pair<Set<String>, Set<String>>> ret = new HashMap<>();

            for (DefaultDirectedGraphWrapper graphlet : graphlets) {
                benign_set = graphlet.getBenignNodeIDs();
                malicious_set = graphlet.getMaliciousNodeIDs();
                Pair<Set<String>, Set<String>> pair = ret.get(graphlet.getMachineID());
                if (pair == null) {
                    ret.put(graphlet.getMachineID(), new ImmutablePair<>(benign_set, malicious_set));
                    continue;
                }
                pair.getLeft().addAll(benign_set);
                pair.getRight().addAll(malicious_set);
            }
            return ret;
        }

        public Set<Node> getUniqueCluster() {
            return graphlets.stream().flatMap(graphlet -> graphlet.vertexSet().stream()).collect(Collectors.toSet());
        }

        @Override
        public List<Node> getAllClusterNodes() {
            return null;
        }

        public boolean isEmpty(){
            return graphlets.isEmpty();
        }

        @Override
        public String getClusterName() {
            return null;
        }
    }

    private Distance dist_obj;
    private KMeansModel clustering_model_kmeans;
    private BisectingKMeansModel clustering_model_bisecting_kmeans;
    private FeatureExtractor feature_extractor;
    private final SparkRandomForestWrapper RF_model;
    private ConcurrentSkipListMap<Long, Object> clustering_models;
    private ArrayList<Pair<Double, Double>> feature_scaling;

    // ThreadLocal
    public ThreadLocal<ArrayList<ClusterStorage>> storage = ThreadLocal.withInitial(() -> {
        ArrayList<ClusterStorage> array = new ArrayList<>();
        IntStream.range(0, ConfigurationParameters.getInstance().max_cluster_count)
                .forEach(i -> array.add(new ClusterStorage(i)));
        return array;
    });

    public ClusterLSH(GraphDataLoader loader, Distance dist_obj) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        if (parameters.train_GraphRF) {
            switch (parameters.clustering_type) {
                case K_MEANS_OFFLINE:
                    TrainML_Models.trainKmeansModel();
                    break;

                case BISECTING_KMEANS_OFFLINE:
                    TrainML_Models.trainBisectingKMeansModel();
                    break;
            }
        }

        switch (parameters.clustering_type) {
            case K_MEANS_OFFLINE:
                clustering_model_kmeans = TrainML_Models.loadKmeansModel();
                assert (clustering_model_kmeans.k() <= parameters.max_cluster_count);
                break;

            case BISECTING_KMEANS_OFFLINE:
                clustering_model_bisecting_kmeans = TrainML_Models.loadBisectingKMeansModel();
                break;
        }

        this.dist_obj = dist_obj;
        RF_model = TrainML_Models.loadGraphRF_Model();

        long first_time_stamp = loader.getFirstTimeStamp() - 1;
        long last_time_stamp = loader.getLastTimeStamp() + parameters.time_window_size;
        long step_count = (last_time_stamp - first_time_stamp) / parameters.clustering_retraining_interval + 1;
        clustering_models = new ConcurrentSkipListMap<>();
        LongStream.range(0, step_count).forEach(i -> clustering_models
                .put(first_time_stamp + i*parameters.clustering_retraining_interval, new Object()));

        // load scaling parameters
        FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();
        Logger logger = GlobalParameters.getInstance().detection_logger;

        try {
            byte serialized_object[] = Files.readAllBytes(Paths.get(parameters.graph_scaling_parameters));
            feature_scaling = (ArrayList<Pair<Double, Double>>) conf.asObject(serialized_object);
            logger.info("graph_scaling_parameters object loaded");
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Cannot deserialize graph_scaling_parameters object.", e);
        }
    }

    @Override
    public void augmentNBD_Storage(long time_stamp, Collection<DefaultDirectedGraphWrapper> graphs, FeatureExtractor feature_extractor) {
        Collection<DefaultDirectedGraphWrapper> graphs_filtered = graphs;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        graphs_filtered = approximateSubgraphSearch(graphs);

        switch (parameters.clustering_type) {
            case K_MEANS_OFFLINE:
                augmentNBD_Storage_KMeansOffline(graphs_filtered, feature_extractor);
                break;
            case BISECTING_KMEANS_ONLINE:
                augmentNBD_Storage_BisectingKMeansOnline(time_stamp, graphs_filtered, feature_extractor);
                break;
            case BISECTING_KMEANS_OFFLINE:
                augmentNBD_Storage_BisectingKMeansOffline(graphs_filtered, feature_extractor);
                break;
            case IDEAL_SYMANTEC_FAMILY_CLUSTERING:
                augmentNBD_Storage_SymantecGroundTruth(graphs_filtered);
                break;
        }
    }

    // assume graphs contain ig_graphs
    private Collection<DefaultDirectedGraphWrapper> approximateSubgraphSearch(Collection<DefaultDirectedGraphWrapper> graphs) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        if (parameters.simulate_ig_detection)
            return graphs.parallelStream()
                    .filter(DefaultDirectedGraphWrapper::isGraphMalicious).collect(Collectors.toList());
        return graphs;
    }

    private void augmentNBD_Storage_KMeansOffline(Collection<DefaultDirectedGraphWrapper> graphs, FeatureExtractor feature_extractor) {
        System.out.println("Augmenting NBD Storage");
        this.feature_extractor = feature_extractor;
        graphs.forEach(this::augmentNBD_Storage_KMeansOffline);
        System.out.println("Augmented NBD Storage");
    }

    private void augmentNBD_Storage_BisectingKMeansOnline(long time_stamp, Collection<DefaultDirectedGraphWrapper> graphs, FeatureExtractor feature_extractor) {
        BisectingKMeansModel bisect_kmeans_model;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        this.feature_extractor = feature_extractor;
        global_parameters.logger.info("Augmenting NBD Storage. # of graphs: " + graphs.size());

        ArrayList<Vector> graph_fvs = graphs.parallelStream()
                .map(graph -> graph.getFeatureVector(feature_extractor))
                .map(this::normalizeGraphFV).map(fv -> Vectors.dense(fv))
                .collect(Collectors.toCollection(ArrayList::new));
        if (graph_fvs.isEmpty())
            return;

        JavaRDD<Vector> graph_fvs_spark = global_parameters.java_spark_context.parallelize(graph_fvs);
        if (clustering_models.floorEntry(time_stamp).getValue() instanceof BisectingKMeansModel)
            bisect_kmeans_model = (BisectingKMeansModel) clustering_models.floorEntry(time_stamp).getValue();
        else { // not yet initialized model
            global_parameters.logger.info("Building BisectingKMeansModel object");
            long key = clustering_models.floorKey(time_stamp);
            BisectingKMeans bisecting_kmeans = new BisectingKMeans().setK(parameters.max_cluster_count)
                    .setMinDivisibleClusterSize(parameters.min_divisible_cluster_size).setSeed(System.currentTimeMillis());
            bisect_kmeans_model = bisecting_kmeans.run(graph_fvs_spark);
            clustering_models.put(key, bisect_kmeans_model);
            global_parameters.logger.info("BisectingKMeansModel built");
        }

        BisectingKMeansModel final_bisect_kmeans_model = bisect_kmeans_model;
        ArrayList<ClusterStorage> storage_thread_local = storage.get();
        graphs.parallelStream()
                .forEach(graph -> {
                    double fv[] = normalizeGraphFV(graph.getFeatureVector(feature_extractor));
                    Vector graph_fv = Vectors.dense(fv);
                    int cluster_index = final_bisect_kmeans_model.predict(graph_fv);
                    ClusterStorage container = storage_thread_local.get(cluster_index);
                    container.graphlets.add(graph);
                    container.machine_ids.add(graph.machine_id);
                });
        int cluster_count = (int) storage.get().stream().filter(cluster -> !cluster.isEmpty()).count();
        global_parameters.logger.info("Augmented NBD Storage. # of clusters: " + cluster_count);
    }

    private void augmentNBD_Storage_BisectingKMeansOffline(Collection<DefaultDirectedGraphWrapper> graphs, FeatureExtractor feature_extractor) {
        System.out.println("Augmenting NBD Storage");
        this.feature_extractor = feature_extractor;
        graphs.forEach(this::augmentNBD_Storage_BisectingKMeansOffline);
        System.out.println("Augmented NBD Storage");
    }

    private void augmentNBD_Storage_SymantecGroundTruth(Collection<DefaultDirectedGraphWrapper> graphs) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        global_parameters.logger.info("Augmenting NBD Storage. # of graphs: " + graphs.size());

        ArrayList<String> family_name_dictionary = graphs.parallelStream()
                .flatMap(graph -> graph.vertexSet().stream()).map(node -> node.file_info.family_name)
                .map(family_name -> family_name == null ? "null" : family_name).distinct().collect(Collectors.toCollection(ArrayList::new));

        Map<String, Integer> map_family_name_to_cluster_id = IntStream.range(0, family_name_dictionary.size())
                .boxed().collect(Collectors.toMap(i -> family_name_dictionary.get(i), Function.identity()));

        ArrayList<ClusterStorage> storage_thread_local = IntStream.range(0, family_name_dictionary.size())
                .mapToObj(i -> new ClusterStorage(i)).collect(Collectors.toCollection(ArrayList::new));
        storage.set(storage_thread_local);

        graphs.parallelStream().forEach(graph -> {
            graph.vertexSet().stream().map(node -> node.file_info.family_name)
                    .filter(Objects::nonNull)
                    .map(map_family_name_to_cluster_id::get).distinct().forEach(cluster_index -> {
                        ClusterStorage container = storage_thread_local.get(cluster_index);
                        container.graphlets.add(graph);
                        container.machine_ids.add(graph.machine_id);
                    });
            });

        graphs.parallelStream().filter(DefaultDirectedGraphWrapper::isGraphBenign).forEach(graph -> {
                int cluster_index = map_family_name_to_cluster_id.get("null");
                ClusterStorage container = storage_thread_local.get(cluster_index);
                container.graphlets.add(graph);
                container.machine_ids.add(graph.machine_id);
        });

        // removes benign a cluster
        if (parameters.enable_ideal_shape_gd != null) {
            if (!map_family_name_to_cluster_id.isEmpty()) {
                int cluster_index = map_family_name_to_cluster_id.get("null");
                ClusterStorage container = storage_thread_local.get(cluster_index);
                container.graphlets.clear();
                container.machine_ids.clear();
            }
        }

        long machine_num = storage_thread_local.stream().flatMap(container -> container.graphlets.stream()).map(graph -> graph.machine_id).distinct().count();
        int cluster_count = family_name_dictionary.size();
        global_parameters.logger.info("Augmented NBD Storage. # of clusters: " + cluster_count);
    }

    private void computeGraphFeatureVectors(int thread_id, Map<Long, DefaultDirectedGraphWrapper> graphs, FeatureExtractor feature_extractor) {
        int counter = 0;
        int vcpu_count =  GlobalParameters.getInstance().executor_size;
        this.feature_extractor = feature_extractor;

        ArrayList<List<DefaultDirectedGraphWrapper>> partitions = new ArrayList<>(vcpu_count);
        for (int i = 0; i < vcpu_count; i++)
            partitions.add(new LinkedList<>());

        for (Map.Entry<Long, DefaultDirectedGraphWrapper> entry : graphs.entrySet()) {
            DefaultDirectedGraphWrapper graph = entry.getValue();
            partitions.get(counter).add(graph);
            counter = (counter + 1) % vcpu_count;
        }

        List<Callable<Void>> callables = new LinkedList<>();
        for (int i = 0; i < vcpu_count; i ++) {
            Callable<Void> callable = new ParallelNonGlobalFeatureExtractor(partitions.get(i), feature_extractor);
            callables.add(callable);
        }

        try {
            GlobalParameters.getInstance().executors.get(thread_id).invokeAll(callables);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    // <machine id, graph_suspicious>
    private Map<Long, Boolean> passGraphsThroughRF(Map<Long, DefaultDirectedGraphWrapper> graphs) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        return graphs.entrySet().parallelStream().collect(Collectors.toConcurrentMap(Map.Entry::getKey,
                entry -> {
                    Vector feature_vector = Vectors.dense(entry.getValue().getFeatureVector(feature_extractor));
                    double prediction = RF_model.predict(feature_vector);
                    return parameters.graphRF_threshold >= 0.5;
                }
        ));
    }

    private void augmentNBD_Storage_KMeansOffline(DefaultDirectedGraphWrapper graph) {
        double fv[] = graph.getFeatureVector(feature_extractor);
        fv = normalizeGraphFV(fv);
        Vector feature_vector = Vectors.dense(fv);

        int cluster_index = clustering_model_kmeans.predict(feature_vector);
        ClusterStorage container = storage.get().get(cluster_index);
        container.graphlets.add(graph);
        container.machine_ids.add(graph.machine_id);
    }

    private void augmentNBD_Storage_BisectingKMeansOffline(DefaultDirectedGraphWrapper graph) {
        double fv[] = normalizeGraphFV(graph.getFeatureVector(feature_extractor));
        Vector feature_vector = Vectors.dense(fv);

        int cluster_index = clustering_model_bisecting_kmeans.predict(feature_vector);
        ClusterStorage container = storage.get().get(cluster_index);
        container.graphlets.add(graph);
        container.machine_ids.add(graph.machine_id);
    }

    public int getDynamicParameter() {
        return storage.get().size();
    }

    private double[] normalizeGraphFV(double fv[]) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        for(int i = 0; i < fv.length; i ++) {
            if (Math.abs(feature_scaling.get(i).getRight() - feature_scaling.get(i).getLeft()) < parameters.epsilon) {
                fv[i] = 0;
                continue;
            }
            fv[i] = (fv[i] - feature_scaling.get(i).getLeft())
                    / (feature_scaling.get(i).getRight() - feature_scaling.get(i).getLeft());
        }
        double weights[] = feature_extractor.getFeatureWeights();
        IntStream.range(0, fv.length).forEach(i -> fv[i] *= weights[i]);
        return fv;
    }

    @Override
    public void clear() {
        storage.get().clear();
        IntStream.range(0, ConfigurationParameters.getInstance().max_cluster_count)
                .forEach(i -> storage.get().add(new ClusterStorage(i)));
    }

    public void printStatistics() {
        Logger logger = GlobalParameters.getInstance().logger;
        logger.info("LSH: Bin Sizes");
        String bin_sizes = "<graphlets.size, machine_ids.size>: " + storage.get().stream()
                .map(item -> item.getSize().toString()).collect(Collectors.joining(","));
        String benign_malicious_counts = "<benign files (count), malicious files (count)>: " + storage.get().stream()
                .map(item -> item.getBenignMaliciousNodeCount().toString()).collect(Collectors.joining(","));
        logger.info(bin_sizes);
        logger.info(benign_malicious_counts);
    }
}