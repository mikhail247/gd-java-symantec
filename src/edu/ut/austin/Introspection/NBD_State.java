package edu.ut.austin.Introspection;

import com.google.common.base.Supplier;
import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.DataInputOutput.GraphDataLoader;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.GraphClasses.Node;
import edu.ut.austin.ML_Models.TrainML_Models;
import edu.ut.austin.Utils.Utils;
import ml.dmlc.xgboost4j.java.Booster;
import ml.dmlc.xgboost4j.java.DMatrix;
import ml.dmlc.xgboost4j.java.XGBoostError;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 2/9/2017.
 */
public class NBD_State implements Serializable {
    // <trial, <machine_id, <<benign FVs>, <malicious FVs>>>>
    private ArrayList<Map<Long, Pair<List<Integer>, List<Integer>>>> fv_assignment;
    //<machine_id, <benign files, malicious files>>
    private ArrayList<Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>>> nbd_structure;

    private transient Map<String, Set<String>> subset_search_suspicious_files = new ConcurrentHashMap<>();
    private transient Map<String, Set<Long>> subset_search_suspicious_machines = new ConcurrentHashMap<>();
    private transient List<Future> futures = null;

    public Boolean graph_detector_label;
    public Boolean shape_detector_label;
    public Set<Long> machine_ids;
    // all files within infected neighborhoods
    public List<Node> infected_files;
    // all nbd files including duplicates
    public List<Node> nbd_files;
    public int ben_machine_count, mal_machine_count;
    public int ben_file_count, mal_file_count;
    public int ben_unique_file_count, mal_unique_file_count;
    public double mal_ratio;
    //<machine_id, graph_detection_time_stamp>
    public Map<Long, Long> graph_detection_time;
    public Double score;
    public double fv[][];
    public String cluster_hash_id;
    public String cluster_init_url;
    public long time_stamp;

    public static int url_dim_count = -1;
    public static int url_bin_count = -1;
    private static Map<String, Double> url_suspicious_score;
    private static AtomicInteger global_nbd_index = new AtomicInteger(0);
    private static transient Booster nbd_xgboost_classifier;

    static {
        url_suspicious_score = loadDomainNameScores();
        nbd_xgboost_classifier = TrainML_Models.loadXGBoostNBD_Classifier();
    }

    // loads all domain scores without threshold filtering
    private static  Map<String, Double> loadDomainNameScores() {
        String line;
        String file_name = "data/url_classification_score.csv";
        Map<String, Double> url_suspicious_score = new ConcurrentHashMap<>();

        try (
                BufferedReader br = new BufferedReader(new FileReader(file_name));
        ){
            while ((line = br.readLine()) != null) {
                String array[] = line.split(",");
                double score = Double.parseDouble(array[2]);
                url_suspicious_score.put(array[0], score);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return url_suspicious_score;
    }

    public NBD_State() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        graph_detection_time = null;
        fv_assignment = new ArrayList<>();
        nbd_structure = new ArrayList<>();
        subset_search_suspicious_files = new ConcurrentHashMap<>();
        subset_search_suspicious_machines = new ConcurrentHashMap<>();

        for (int i = 0; i < parameters.average_across_num_trials; i ++) {
            fv_assignment.add(new HashMap<>());
            nbd_structure.add(new HashMap<>());
        }
    }

    public void initNBD_State(List<Node> nbd_files) {
        this.nbd_files = nbd_files;
        this.infected_files = nbd_files.stream().filter(Node::isMalicious).collect(Collectors.toList());
        this.machine_ids = nbd_files.stream().map(node -> node.machine_id).collect(Collectors.toSet());
        this.ben_file_count = (int) nbd_files.stream().filter(Node::isBenign).count();
        this.mal_file_count = (int) nbd_files.stream().filter(Node::isMalicious).count();
        this.ben_unique_file_count = (int) nbd_files.stream().filter(Node::isBenign)
                .map(node -> node.sha2_id).distinct().count();
        this.mal_unique_file_count = (int) nbd_files.stream().filter(Node::isMalicious)
                .map(node -> node.sha2_id).distinct().count();
        this.mal_ratio = (double) mal_file_count / (mal_file_count + ben_file_count);

        Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>> map = new ConcurrentHashMap<>();
        nbd_files.forEach(node -> {
            map.putIfAbsent(node.machine_id, new ImmutablePair<>(new HashSet<>(), new HashSet<>()));
            if (node.isBenign())
                map.get(node.machine_id).getLeft().add(node.file_info);
            else
                map.get(node.machine_id).getRight().add(node.file_info);
        });
        nbd_structure.set(0, map);
    }

    // deallocating memory
    public void clear() {
        fv_assignment = null;
        nbd_structure = null;

        subset_search_suspicious_files = null;
        subset_search_suspicious_machines = null;
        futures = null;

        graph_detector_label = null;
        shape_detector_label = null;
        machine_ids = null;

        infected_files = null;
        graph_detection_time = null;
        score = null;
    }

    public boolean isMalicious() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance().getInstance();

        switch (parameters.detection_mechanism) {
            case SHAPE_GD:
                return shape_detector_label != null ? shape_detector_label : false;
            case GRAPH_DETECTOR:
                return graph_detector_label;
            default: // BOTH
                if (shape_detector_label == null)
                    return false;
                return graph_detector_label && shape_detector_label;
        }
    }

    public void setFV_Assignment(int trial, Map<Long, Pair<List<Integer>, List<Integer>>> fv_assignment) {
        this.fv_assignment.set(trial, fv_assignment);
    }

    // <machine_id, <benign files, malicious files>>
    @Deprecated
    public void setNBD_Structure(int trial, Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>> nbd_structure) {
        this.nbd_structure.set(trial, nbd_structure);
        this.ben_machine_count = getBenignMachines_GroundTruth(0).size();
        this.mal_machine_count = getUniqueMaliciousFiles_GroundTruth(0).size();
        this.ben_file_count = getUniqueBenignFiles_GroundTruth(0).size();
        this.mal_file_count = getUniqueMaliciousFiles_GroundTruth(0).size();
    }

    // <machine_id, <benign files, malicious files>>
    public Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>> getNBD_Structure(int trial) {
        return this.nbd_structure.get(trial);
    }

    public Map<Long, Pair<List<Integer>, List<Integer>>> getFV_Assignment(int trial) {
        return fv_assignment.get(trial);
    }

    public Set<String> getUniqueBenignFiles_GroundTruth(int trial) {
        return nbd_files.stream().filter(Node::isBenign)
                .map(node -> node.sha2_id).collect(Collectors.toSet());
    }

    public Set<String> getUniqueMaliciousFiles_GroundTruth(int trial) {
        return nbd_files.stream().filter(Node::isMalicious)
                .map(node -> node.sha2_id).collect(Collectors.toSet());
    }

    public Set<Long> getBenignMachines_GroundTruth(int trial) {
        Set<Long> infected_machines = getInfectedMachines_GroundTruth(0);
        Set<Long> machines = nbd_files.stream().map(node -> node.machine_id).collect(Collectors.toSet());
        machines.removeAll(infected_machines);
        return machines;
    }

    public Set<Long> getInfectedMachines_GroundTruth(int trial) {
        return nbd_files.stream().filter(Node::isMalicious)
                .map(node -> node.machine_id).collect(Collectors.toSet());
    }

    public double[] getLinearFV(double[][] fv) {
        if (fv == null)
            return null;

        int rows = fv.length;
        int columns = fv[0].length;
        int counter = 0;
        double fv_linear[] = new double[rows * columns];

        for (int i = 0; i < rows; i ++) {
            for (int j = 0; j < columns; j ++) {
                fv_linear[counter ++] = fv[i][j];
            }
        }
        return fv_linear;
    }

    public static String getNBD_Header() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        if (!parameters.dump_nbd_fvs)
            return null;
        assert(url_dim_count > 0 && url_bin_count > 0);

        String header = "label,benign_count,cluster_hash_id,total_file_count,total_unique_file_count,ben_machine_count,"
                + "mal_machine_count,"
                + IntStream.range(0, url_dim_count*url_bin_count).mapToObj(i -> "url_bin_" + i).collect(Collectors.joining(","))
                + ",url_nbd_density,unique_urls,"
                + IntStream.range(0, parameters.GD_pca_dim_count * parameters.GD_bin_count)
                        .mapToObj(i -> "gd_bin_" + i).collect(Collectors.joining(","));
        return header;
    }

    private List<Float> getURLs_FV(GraphDataLoader loader) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        if (parameters.use_shape_based_domain_features) {
            return getURLs_FV_DomainClassifierAsFilter(loader);
        } else {
            return getURLs_FV_DomainClassifierOutput();
        }
    }

    // graph density, unique URLS
    private List<Float> getURLs_FV_DomainClassifierOutput() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        Supplier<Stream<String>> supplier = () -> nbd_files.stream().flatMap(node ->
                Stream.concat(node.incoming_url_edges.stream(), node.outgoing_url_edges.stream()));
        double url_count = supplier.get().count() + parameters.epsilon;
        // <bin number, value>
        Map<Integer, Double> map = new ConcurrentHashMap<>();
        IntStream.range(0, url_bin_count).forEach(i -> map.putIfAbsent(i, 0.0));

        supplier.get().map(url -> url_suspicious_score.get(url)).filter(Objects::nonNull).map(score ->
                (int) ((url_bin_count-1)*score)).forEach(bin_index -> map.put(bin_index, map.get(bin_index)+1));
        double normalization_sum = map.values().stream().mapToDouble(Double::valueOf).sum();
        map.forEach((key, value) -> map.put(key, value / normalization_sum));

        List<Float> url_fv = new LinkedList<>();
        map.values().stream().map(item -> url_fv.add(item.floatValue()));
        url_fv.add((float) url_count / nbd_files.size());
        url_fv.add((float) supplier.get().distinct().count());
        return url_fv;
    }

    // graph density, unique URLS
    public List<Float> getURLs_FV_DomainClassifierAsFilter(GraphDataLoader loader) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        Map<String, int[]> domain_bucketed_scores = loader.getDomainBucketedScores();

        // only suspicious domains
        Supplier<Stream<String>> supplier = () -> nbd_files.stream().flatMap(node ->
                Stream.concat(node.incoming_url_edges.stream(), node.outgoing_url_edges.stream()))
                .filter(url -> {
                    Double value = url_suspicious_score.get(url);
                    if (value == null)
                        return false;
                    return value > parameters.domain_score_threshold;
                });

        double url_count = supplier.get().count() + parameters.epsilon;
        List<int[]> url_fvs = supplier.get().map(domain_bucketed_scores::get).collect(Collectors.toList());
        double[][] url_hist = buildNormalizedHistogramFromBucketIndices(url_fvs);
        double fv_linear[] = getLinearFV(url_hist);

        List<Float> url_fv = new LinkedList<>();
        Arrays.stream(fv_linear).forEach(item -> url_fv.add((float) item));
        url_fv.add((float) url_count / nbd_files.size());
        url_fv.add((float) supplier.get().distinct().count());
        return url_fv;
    }

    private double[][] buildNormalizedHistogramFromBucketIndices(List<int[]> fvs) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        double hist[][] = new double[url_dim_count][url_bin_count];

        for (int bucket_vector[] : fvs) {
            for (int dim = 0; dim < url_dim_count; dim ++) {
                int bin_index = bucket_vector[dim];
                hist[dim][bin_index]++;
            }
        }

        // normalize ref_hist
        for (int i = 0; i < url_dim_count ; i++) {
            double sum = 0;
            for (int j = 0; j < url_bin_count; j++)
                sum += hist[i][j];
            if (sum < parameters.epsilon)
                continue;
            for (int j = 0; j < url_bin_count; j++)
                hist[i][j] /= sum;
        }
        return hist;
    }

    public void relabelNBD_with_XGBoost(GraphDataLoader loader) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        if (parameters.shape_gd_implementation != ConfigurationParameters.ShapeGDImplementation.REAL_TIME_XGBOOST)
            return;

        shape_detector_label = false;
        score = -1.0;

        double fv_linear[] = getLinearFV(fv);
        if (fv_linear == null)
            return;

        int total_file_count = ben_file_count + mal_file_count;
        int total_unique_file_count = ben_unique_file_count + mal_unique_file_count;
        List<Float> fv_list = new LinkedList<>();
        fv_list.add((float) total_file_count);
        fv_list.add((float) total_unique_file_count);
        fv_list.addAll(getURLs_FV(loader));
        Arrays.stream(fv_linear).forEach(item -> fv_list.add((float) item));

        try {
            float vec[] = ArrayUtils.toPrimitive(fv_list.toArray(new Float[0]), 0.0F);
            DMatrix dmatrix = new DMatrix(vec, 1, vec.length);
            score = (double) nbd_xgboost_classifier.predict(dmatrix)[0][0];
            shape_detector_label = score >= parameters.real_time_nbd_xgboost_threshold;
        } catch (XGBoostError xgBoostError) {
            xgBoostError.printStackTrace();
            System.exit(-1);
        }
    }

    public String getFV_String(GraphDataLoader loader) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        double fv_linear[] = getLinearFV(fv);
        if (fv_linear == null)
            return null;

        int label = mal_ratio < parameters.nbd_threshold ? 0 : 1;
        int total_file_count = ben_file_count + mal_file_count;
        int total_unique_file_count = ben_unique_file_count + mal_unique_file_count;
        return  label + "," + ben_file_count + "," + cluster_hash_id + "," + total_file_count + ","
                + total_unique_file_count + "," + ben_machine_count + "," + mal_machine_count + ","
                + getURLs_FV(loader).stream().map(d -> Float.toString(d)).collect(Collectors.joining(","))
                + "," + Arrays.stream(fv_linear).boxed().map(d -> Double.toString(d)).collect(Collectors.joining(","));
    }

    public void process(int thread_id) {
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        global_parameters.subset_selector.forEach(subset_selector ->
                process(thread_id, subset_selector.toString()));
    }

    public void process(int thread_id, String subset_selector_name) {
        if (subset_search_suspicious_files == null)
            subset_search_suspicious_files = new ConcurrentHashMap<>();

        if (subset_search_suspicious_machines == null)
            subset_search_suspicious_machines = new ConcurrentHashMap<>();

        GlobalParameters global_parameters = GlobalParameters.getInstance();
        if (futures == null)
            futures = new LinkedList<>();

        global_parameters.subset_selector.stream()
                .filter(subset_selector -> subset_selector.toString().equals(subset_selector_name)).map(subset_selector ->
                new Runnable() {
                    @Override
                    public void run() {
                        Pair<Set<Long>, Set<String>> res = subset_selector.getSuspiciousEntities(NBD_State.this);
                        subset_search_suspicious_machines.put(subset_selector.toString(), res.getKey());
                        subset_search_suspicious_files.put(subset_selector.toString(), res.getValue());
                    }
                }).map(runnable -> global_parameters.executors.get(thread_id).submit(runnable)).forEach(future -> futures.add(future));
    }

    public Set<String> getSubsetSearchSuspiciousFiles(String subset_selector_name) {
        if (futures != null) {
            futures.forEach(future -> {
                try {
                    int nbd_count = IntrospectionModule.getInstance().processed_nbd_count.getAndIncrement();
                    if (!subset_selector_name.equals("edu.ut.austin.MachineSelectionStrategy.GroundTruthMachineSelection"))
                        if (nbd_count % 1000 == 0)
                            GlobalParameters.getInstance().logger.info("nbd processed: " + nbd_count
                                + " nbd_size: " + NBD_State.this.nbd_files.size());
                    future.get();
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
            });
        }

        if (subset_search_suspicious_files == null
                || subset_search_suspicious_files.get(subset_selector_name) == null) {
            // compute asynchronously, wait on the future, and then return the result;
            process(0, subset_selector_name);
            return getSubsetSearchSuspiciousFiles(subset_selector_name);
        }
        return subset_search_suspicious_files.get(subset_selector_name);
    }

    public Set<Long> getSubsetSearchSuspiciousMachines(String subset_selector_name) {

        if (futures != null) {
            futures.forEach(future -> {
                try {
                    future.get();
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            });
        }

        if (subset_search_suspicious_machines == null
                || subset_search_suspicious_machines.get(subset_selector_name) == null) {
            // compute asynchronously, wait on the future, and then return the result;
            process(0, subset_selector_name);
            return getSubsetSearchSuspiciousMachines(subset_selector_name);
        }
        return subset_search_suspicious_machines.get(subset_selector_name);
    }

    @Override
    public String toString() {
        int machine_count = machine_ids == null ? -1 : machine_ids.size();
        StringBuffer str = new StringBuffer();
        str.append("<graph_label: " + graph_detector_label);
        str.append(", shape_label: " + shape_detector_label);
        str.append(", <mal_ratio: " + mal_ratio);
        str.append(", <mal_files: " + mal_file_count);
        str.append(", <ben_files: " + ben_file_count);
        str.append(", <machine_count: " + machine_count);
        str.append(">>");
        return str.toString();
    }

    public void clearSubsetSelectorResutls() {
        if (subset_search_suspicious_files != null)
            subset_search_suspicious_files.clear();
        if (subset_search_suspicious_machines != null)
            subset_search_suspicious_machines.clear();
    }

    public void dumpNBDToFile(GraphDataLoader loader) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        if (!parameters.dump_nbd_instances)
            return;

        String dir_name = parameters.out_folder + "/nbd_dump";
        String header = "sha2_id,file_name,machine_id,time_stamp,file_info.is_benign,"
                + "file_info.family_name,file_info.prefix,file_info.suffix,file_info.sha2,file_info.earliest_wine_time,"
                + "file_info.av_count,file_info.vt_time_stamp,file_info.file_type_str,"
                + "geo_info.geo_id,geo_info.isp,geo_info.country,snapshot.time_stamp,cluster_hash_id,"
                + "cluster_label,incoming_url_edges,outgoing_url_edges,cluster_init_url\n";

        List<String> nbd_strs = nbd_files.stream().map(node -> {
            String geo_info_str = "None,None,None";
            Triple<Integer, String, String> geo_info = loader.getGeoLocationMap().get(node.machine_id);
            if (geo_info != null)
                geo_info_str = geo_info.getLeft() + "," + geo_info.getMiddle() + "," + geo_info.getRight();

            return node.sha2_id + "," + node.file_name.replaceAll(",", "_") + ","
                    + node.machine_id + "," + node.time_stamp + "," + node.file_info.is_benign  + ","
                    + node.file_info.family_name.replaceAll(",", "_") + ","
                    + node.file_info.prefix.replaceAll(",", "_") + ","
                    + node.file_info.suffix.replaceAll(",", "_")
                    + "," + node.file_info.sha2 + "," + node.file_info.earliest_wine_time
                    + "," + node.file_info.av_count + "," + node.file_info.vt_time_stamp + ","
                    + node.file_info.file_type_str.replaceAll(",", "_")
                    + "," + geo_info_str.replaceAll(",", "_") + "," +  time_stamp + ","
                    + cluster_hash_id.replaceAll(",", "_") + "," + shape_detector_label
                    + "," + Utils.encodeCollectionForPandasDataFrame(node.incoming_url_edges) + ","
                    + Utils.encodeCollectionForPandasDataFrame(node.outgoing_url_edges)
                    + "," + cluster_init_url.replaceAll(",", "_") + "\n";
        }).collect(Collectors.toList());

        try (
                OutputStreamWriter stream_writer = new FileWriter(dir_name + "/nbd_" + global_nbd_index.getAndIncrement() + ".csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            writer.write(header);
            for (String str : nbd_strs)
                writer.write(str);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
