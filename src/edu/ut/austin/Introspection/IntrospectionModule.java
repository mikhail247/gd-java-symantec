package edu.ut.austin.Introspection;

import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.DataInputOutput.GraphDataLoader;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.GraphClasses.Node;
import edu.ut.austin.ML_Models.LocalDetector;
import edu.ut.austin.ML_Models.TrainML_Models;
import edu.ut.austin.MachineSelectionStrategy.MachineSelectionStrategy;
import edu.ut.austin.Utils.Utils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.tuple.*;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import org.nustaq.serialization.FSTConfiguration;
import org.nustaq.serialization.serializers.FSTCollectionSerializer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 2/9/2017.
 */
public class IntrospectionModule implements Serializable {

    public enum ProcessNodeStatType {CCS_EVALUATION, FULL_EVALUATION}
    private static IntrospectionModule instance = null;
    private transient ConcurrentSkipListMap<Long, DetectionSnapshot> detection_snapshots;
    private ArrayList<String> subset_selector_names;

    private Set<String> possible_early_detection_ccs, dataset_infected_files_ccs;
    private Set<String> possible_early_detection_full, dataset_infected_files_full;

    private Set<Long> dataset_infected_machines_ccs;
    private Set<Long> dataset_infected_machines_full;
    // <file_id, file_info>
    private transient Map<String, FileInfo> file_info;
    private Set<String> file_info_ccs_evaluation, file_info_full_evaluation, all_downloaders;
    // <file_id, <machine_id, download time_stamp>>
    public transient Map<String, List<Pair<Long, Long>>>  file_to_machine_ccs_map, file_to_machine_full_map;
    private Map<String, Pair<Integer, Integer>> ccs_results;

    public AtomicInteger computed_fvs_count, existing_fvs_count, processed_nbd_count;

    private Set<String> data_set_benign_files, data_set_malicious_files;
    private transient Map<Long, Triple<Integer, String, String>> machine_geo_location;
    public transient LocalDetector local_detector;

    private transient ConfigurationParameters parameters;
    private transient GlobalParameters global_parameters;

    private class SnapshotStructure {
        public int nbd_count, infected_nbd_count;

        public int file_alerts_subset_selector, infected_file_ids_count;
        public int machine_alerts_subset_selector, machine_alerts_tp;
        public int file_alerts_tp, infected_machine_ids_ground_truth;
        public int total_unique_file_count_in_scope, total_unique_machine_count_in_scope;
        public int total_file_count_in_scope;

        public int total_unique_infected_file_count, total_unique_infected_machine_count;
        public int total_infected_file_count;

        // cumulative characteristics
        public int file_alerts_cum, infected_file_ids_count_cum;
        public int machine_count_cum, infected_machine_ids_count_cum;

        // precision
        public double file_precision, file_recall;
        public double machine_precision, machine_recall;
        public double file_specificity, file_accuracy;
        public double machine_specificity, machine_accuracy;
        public long time_stamp;

        @Override
        public String toString() {
            return nbd_count + "," + infected_nbd_count + "," + file_alerts_subset_selector + "," + file_alerts_tp + "," + total_unique_file_count_in_scope + "," + total_unique_infected_file_count + ","
                    + machine_alerts_subset_selector + "," + machine_alerts_tp + "," + total_unique_machine_count_in_scope + "," + total_unique_infected_machine_count + ","
                    + file_precision + "," + file_recall + "," + machine_precision + "," + machine_recall + ","
                    + total_file_count_in_scope + "," + total_infected_file_count + "," + file_specificity + "," + file_accuracy + ","
                    + machine_specificity + "," + machine_accuracy + "," + time_stamp + "\n";
        }
    }

    private class AggregatedResults {
        public int fp_files_ld, fp_files_nbd, fp_files_shape;
        public int tp_files_ld, tp_files_nbd, tp_files_shape;
        public int early_detection_files_nbd;
        public double early_detection_days_nbd;
        public double time_to_detection_days_nbd;

        public int fp_machines_ld, fp_machines_nbd, fp_machines_shape;
        public int tp_machines_ld, tp_machines_nbd, tp_machines_shape;

        public int possible_early_detection_files, benign_file_count, malicious_file_count;
        public double time_to_detection_machine_nbd, time_to_detection_machine_shape;
        public int benign_machine_count, malicious_machine_count;

        //pure LDs
        public double early_detection_days_pure_ld, time_to_detection_days_pure_ld;
        public int early_detected_files_pure_ld;
    }

    private IntrospectionModule() {
        init();
        computed_fvs_count = new AtomicInteger(0);
        existing_fvs_count = new AtomicInteger(0);
        processed_nbd_count = new AtomicInteger(0);
        detection_snapshots = new ConcurrentSkipListMap<>();
        subset_selector_names = GlobalParameters.getInstance().subset_selector
                .stream().map(MachineSelectionStrategy::toString).collect(Collectors.toCollection(ArrayList::new));
        local_detector = TrainML_Models.loadLD_Model(parameters.LF_implementation_type);
    }

    public void init() {
        parameters = ConfigurationParameters.getInstance();
        global_parameters = GlobalParameters.getInstance();
    }

    public static synchronized void init(GraphDataLoader loader) {
        instance = new IntrospectionModule();
        instance.possible_early_detection_ccs = loader.getPossibleEarlyDetectionShaIDs(ProcessNodeStatType.CCS_EVALUATION);
        instance.possible_early_detection_full = loader.getPossibleEarlyDetectionShaIDs(ProcessNodeStatType.FULL_EVALUATION);

        instance.dataset_infected_files_ccs = loader.getFileInfoCCSEvaluationMap(); // load malicious files that have been detected in the CCS paper
        instance.dataset_infected_machines_ccs = loader.getMachineIDsDownloadedFiles(instance.dataset_infected_files_ccs);

        instance.dataset_infected_files_full = loader.getLoadedFileShaIDs(ProcessNodeStatType.FULL_EVALUATION, false);
        instance.dataset_infected_machines_full = loader.getMachineIDsDownloadedFiles(instance.dataset_infected_files_full);

        instance.file_info = loader.getFileInfoMap();
        instance.machine_geo_location = loader.getGeoLocationMap();
        instance.file_info_ccs_evaluation = loader.getFileInfoCCSEvaluationMap();
        instance.file_info_full_evaluation = loader.getLoadedFileShaIDs(ProcessNodeStatType.FULL_EVALUATION);
        instance.all_downloaders = loader.getLoadedFileShaIDs(ProcessNodeStatType.CCS_EVALUATION);

        instance.data_set_benign_files = loader.getLoadedFileShaIDs(ProcessNodeStatType.FULL_EVALUATION, true);
        instance.data_set_malicious_files = loader.getLoadedFileShaIDs(ProcessNodeStatType.FULL_EVALUATION, false);
        instance.file_to_machine_ccs_map = loader.getFileToMachineMap(ProcessNodeStatType.CCS_EVALUATION);
        instance.file_to_machine_full_map = loader.getFileToMachineMap(ProcessNodeStatType.FULL_EVALUATION);
        instance.ccs_results = loader.getCCS_Results();
    }

    public static IntrospectionModule getInstance() {
        return instance;
    }

    public void processNBDs() {
        Random rand = new Random();
        // destroy the current set of executor pools and create a new one
        // with the number of executors equal to the number of virtual threads
        global_parameters.shutdownExecutors();
        global_parameters.executors = IntStream.range(0, 1).mapToObj(i ->
                Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors())).collect(Collectors.toCollection(ArrayList::new));

        detection_snapshots.values().parallelStream().flatMap(snapshot ->
                snapshot.nbd_states.values().stream()).forEach(nbd -> {
            int executor_id = rand.nextInt(global_parameters.executors.size());
            nbd.clearSubsetSelectorResutls();
            nbd.process(executor_id);
        });
        global_parameters.shutdownExecutors();

        // restore original executor pool
        global_parameters.executors = IntStream.range(0, parameters.engine_thread_count).mapToObj(i ->
                Executors.newFixedThreadPool(global_parameters.executor_size)).collect(Collectors.toCollection(ArrayList::new));
    }

    public static void clear() {
        instance = null;
    }

    public void addDetectionSnapshot(long time_stamp, Collection<DefaultDirectedGraphWrapper> data) {
        DetectionSnapshot snapshot = new DetectionSnapshot(time_stamp);
        Set<Long> machines = data.parallelStream().map(graph -> graph.machine_id).collect(Collectors.toSet());

        snapshot.total_malicious_unique_files_ground_truth = data.parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isMalicious).map(node -> node.sha2_id).collect(Collectors.toSet());
        snapshot.total_benign_unique_files_ground_truth = data.parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isBenign).map(node -> node.sha2_id).collect(Collectors.toSet());
        snapshot.total_malicious_files_ground_truth = (int) data.parallelStream().flatMap(graph ->
                graph.vertexSet().stream()).filter(Node::isMalicious).map(node -> node.sha2_id).count();
        snapshot.total_unique_file_count_in_scope = data.parallelStream().flatMap(graph -> graph.vertexSet().stream())
                .map(node -> node.sha2_id).distinct().collect(Collectors.toSet());
        snapshot.total_file_count_in_scope = (int) data.parallelStream().flatMap(graph -> graph.vertexSet().stream())
                .map(node -> node.sha2_id).count();

        snapshot.infected_machines_ground_truth = data.parallelStream().filter(graph ->
                graph.getMaliciousNodeCount() > 0).map(graph -> graph.machine_id).collect(Collectors.toSet());
        machines.removeAll(snapshot.infected_machines_ground_truth);
        snapshot.benign_machines_ground_truth = machines;
        snapshot.total_unique_machine_count_in_scope = machines.size() + snapshot.infected_machines_ground_truth.size();
        detection_snapshots.put(time_stamp, snapshot);
    }

    public String getStat() {
        StringBuffer str = new StringBuffer();
        if (subset_selector_names.isEmpty())
            return "";

        processed_nbd_count.set(0);
        String subset_selector_name = subset_selector_names.get(0);
        str.append(computeFileStat(ProcessNodeStatType.CCS_EVALUATION, true, subset_selector_name).getLeft());
        str.append(computeFileStat(ProcessNodeStatType.FULL_EVALUATION, false, subset_selector_name).getLeft());

        for (int i = 1; i < subset_selector_names.size(); i ++) {
            processed_nbd_count.set(0);
            subset_selector_name = subset_selector_names.get(i);
            str.append(computeFileStat(ProcessNodeStatType.CCS_EVALUATION, false, subset_selector_name).getLeft());
            str.append(computeFileStat(ProcessNodeStatType.FULL_EVALUATION, false, subset_selector_name).getLeft());
        }
        str.append(getCCS_Stat());
        return str.toString();
    }


    public void dumpAggreagtedResultsTable() {
        dumpAggreagtedResultsTable_Files();
        dumpAggreagtedResultsTable_Machines();
    }

    private String computeF_Score(double beta, double precision[], double recall[]) {
        double score[] = new double[precision.length];

        for (int i = 0; i < precision.length; i ++)
            score[i] = (1+beta*beta)*precision[i]*recall[i]/((beta*beta*precision[i]) + recall[i]);
        return String.format("%f%%,%f%%,%f%%", score[0], score[1], score[2]);
    }

    private void dumpAggreagtedResultsTable_Files() {
        StringBuffer str = new StringBuffer();
        // computes stat for NBDs
        AggregatedResults stat_nbd = computeFileStat(ProcessNodeStatType.FULL_EVALUATION, false, subset_selector_names.get(0)).getRight();
        // computes stat for NBDs+LDs
        AggregatedResults stat_shape = computeFileStat(ProcessNodeStatType.FULL_EVALUATION, false, subset_selector_names.get(1)).getRight();

        str.append(String.format("False Positives files,%d,%d,%d\n", stat_nbd.fp_files_ld, stat_nbd.fp_files_nbd, stat_nbd.fp_files_shape));
        str.append(String.format("False Positives files,%f%%,%f%%,%f%%\n", 100.0*stat_nbd.fp_files_ld/stat_nbd.benign_file_count,
                100.0*stat_nbd.fp_files_nbd/stat_nbd.benign_file_count, 100.0*stat_nbd.fp_files_shape/stat_nbd.benign_file_count));

        str.append(String.format("True Positives files,%d,%d,%d\n", stat_nbd.tp_files_ld, stat_nbd.tp_files_nbd, stat_nbd.tp_files_shape));
        str.append(String.format("True Positives files,%f%%,%f%%,%f%%\n", 100.0*stat_nbd.tp_files_ld/stat_nbd.malicious_file_count,
                100.0*stat_nbd.tp_files_nbd/stat_nbd.malicious_file_count, 100.0*stat_nbd.tp_files_shape/stat_nbd.malicious_file_count));

        double precision[] = {100.0*stat_nbd.tp_files_ld/(stat_nbd.tp_files_ld + stat_nbd.fp_files_ld),
                100.0*stat_nbd.tp_files_nbd/(stat_nbd.tp_files_nbd + stat_nbd.fp_files_nbd), 100.0*stat_nbd.tp_files_shape/(stat_nbd.tp_files_shape + stat_nbd.fp_files_shape)};
        double recall[] = {100.0*stat_nbd.tp_files_ld/stat_nbd.malicious_file_count,
                100.0*stat_nbd.tp_files_nbd/stat_nbd.malicious_file_count, 100.0*stat_nbd.tp_files_shape/stat_nbd.malicious_file_count};

        str.append(String.format("Precision files,%f%%,%f%%,%f%%\n", precision[0], precision[1], precision[2]));
        str.append(String.format("Recall files,%f%%,%f%%,%f%%\n", recall[0], recall[1], recall[2]));
        str.append(String.format("F_1.0 score files,%s\n", computeF_Score(1.0, precision, recall)));
        str.append(String.format("F_0.5 score files,%s\n", computeF_Score(0.5, precision, recall)));
        str.append(String.format("F_2.0 score files,%s\n", computeF_Score(2.0, precision, recall)));

        str.append(String.format("Early Detection days,%f,%f,%f\n", stat_nbd.early_detection_days_pure_ld, stat_nbd.early_detection_days_nbd, stat_shape.early_detection_days_nbd));
        str.append(String.format("Time to Detection days,%f,%f,%f\n", stat_nbd.time_to_detection_days_pure_ld, stat_nbd.time_to_detection_days_nbd, stat_shape.time_to_detection_days_nbd));
        str.append(String.format("Early Detection files,%d,%d,%d\n", stat_nbd.early_detected_files_pure_ld, stat_nbd.early_detection_files_nbd, stat_shape.early_detection_files_nbd));
        str.append(String.format("Early Detection files,%f%%,%f%%,%f%%\n", (double) 100*stat_nbd.early_detected_files_pure_ld/stat_nbd.possible_early_detection_files,
                (double) 100*stat_nbd.early_detection_files_nbd/stat_nbd.possible_early_detection_files, (double) 100*stat_shape.early_detection_files_nbd/stat_nbd.possible_early_detection_files));

        str.append(String.format("Possible Early Detection files,%d\n", stat_nbd.possible_early_detection_files));
        str.append(String.format("Benign files,%d\n", stat_nbd.benign_file_count));
        str.append(String.format("Malicious files,%d\n", stat_nbd.malicious_file_count));

        try (
                OutputStreamWriter stream_writer = new FileWriter(parameters.out_folder + "/aggregated_results_files.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            writer.write("Parameter,LDs,NBDs,Shape-GD + LDs\n");
            writer.write(str.toString() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private void dumpAggreagtedResultsTable_Machines() {
        StringBuffer str = new StringBuffer();
        // computes stat for NBDs
        AggregatedResults stat_nbd = computeFileStat(ProcessNodeStatType.FULL_EVALUATION, false, subset_selector_names.get(0)).getRight();
        // computes stat for NBDs+LDs
        AggregatedResults stat_shape = computeFileStat(ProcessNodeStatType.FULL_EVALUATION, false, subset_selector_names.get(1)).getRight();

        str.append(String.format("False Positives machines,%d,%d,%d\n", stat_nbd.fp_machines_ld, stat_nbd.fp_machines_nbd, stat_nbd.fp_machines_shape));
        str.append(String.format("False Positives machines,%f%%,%f%%,%f%%\n", 100.0*stat_nbd.fp_machines_ld/stat_nbd.benign_machine_count,
                100.0*stat_nbd.fp_machines_nbd/stat_nbd.benign_machine_count, 100.0*stat_nbd.fp_machines_shape/stat_nbd.benign_machine_count));

        str.append(String.format("True Positives machines,%d,%d,%d\n", stat_nbd.tp_machines_ld, stat_nbd.tp_machines_nbd, stat_nbd.tp_machines_shape));
        str.append(String.format("True Positives machines,%f%%,%f%%,%f%%\n", 100.0*stat_nbd.tp_machines_ld/stat_nbd.malicious_machine_count,
                100.0*stat_nbd.tp_machines_nbd/stat_nbd.malicious_machine_count, 100.0*stat_nbd.tp_machines_shape/stat_nbd.malicious_machine_count));

        double precision[] = {100.0*stat_nbd.tp_machines_ld/(stat_nbd.tp_machines_ld + stat_nbd.fp_machines_ld),
                100.0*stat_nbd.tp_machines_nbd/(stat_nbd.tp_machines_nbd + stat_nbd.fp_machines_nbd), 100.0*stat_nbd.tp_machines_shape/(stat_nbd.tp_machines_shape + stat_nbd.fp_machines_shape)};
        double recall[] = {100.0*stat_nbd.tp_machines_ld/stat_nbd.malicious_machine_count,
                100.0*stat_nbd.tp_machines_nbd/stat_nbd.malicious_machine_count, 100.0*stat_nbd.tp_machines_shape/stat_nbd.malicious_machine_count};

        str.append(String.format("Precision machines,%f%%,%f%%,%f%%\n", precision[0], precision[1], precision[2]));
        str.append(String.format("Recall machines,%f%%,%f%%,%f%%\n", recall[0], recall[1], recall[2]));
        str.append(String.format("F_1.0 score machines,%s\n", computeF_Score(1.0, precision, recall)));
        str.append(String.format("F_0.5 score machines,%s\n", computeF_Score(0.5, precision, recall)));
        str.append(String.format("F_2.0 score machines,%s\n", computeF_Score(2.0, precision, recall)));

        str.append(String.format("Time to Detection days,%f,%f,%f\n", 0.0, stat_nbd.time_to_detection_machine_nbd, stat_nbd.time_to_detection_machine_shape));
        str.append(String.format("Clean machines,%d\n", stat_nbd.benign_machine_count));
        str.append(String.format("Infected machines,%d\n", stat_nbd.malicious_machine_count));

        try (
                OutputStreamWriter stream_writer = new FileWriter(parameters.out_folder + "/aggregated_results_machines.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            writer.write("Parameter,LDs,NBDs,Shape-GD + LDs\n");
            writer.write(str.toString() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    // dump an ROC by varying NBD Classifier's threshold
    public void dumpShapeNBD_ROC() {
        if (!parameters.dump_shape_nbd_roc)
            return;
        String suffix = null;
        AggregatedResults stat;
        int point_count = 100;
        StringBuffer str = new StringBuffer();
        assert(subset_selector_names.get(1).equals("edu.ut.austin.MachineSelectionStrategy.LD_MachineSelection_only_mal_nbds"));

        for (int i = 0; i <= point_count; i ++) {
            double threshold = (double) i / point_count;
            parameters.nbd_xgboost_threshold = threshold;

            switch(parameters.shape_gd_implementation) {
                case POST_PROCESSING_XGBOOST:
                    suffix = "xgb";
                    parameters.real_time_nbd_xgboost_threshold = threshold;
                    parameters.nbd_xgboost_threshold = threshold;
                    break;

                case IDEAL:
                    suffix = "ideal";
                    parameters.enable_ideal_shape_gd = threshold;// must be 0.05 parameters.enable_ideal_shape_gd; //0.05;
                    break;
            }
            relabelNBDs();
            processNBDs();
            stat = computeFileStat(ProcessNodeStatType.FULL_EVALUATION, false, subset_selector_names.get(1)).getRight(); // "LD_MachineSelection" policy
            double fp_rate = (double) stat.fp_files_shape/stat.benign_file_count;
            double tp_rate = (double) stat.tp_files_shape/stat.malicious_file_count;
            str.append(threshold + "," + fp_rate + "," + tp_rate + "\n");
            global_parameters.logger.info(String.format("<<<dumpShapeNBD_ROC()>>> threshold: %f   FP: %f   TP: %f \n", threshold, fp_rate, tp_rate));
        }

        try (
                OutputStreamWriter stream_writer = new FileWriter(parameters.out_folder + "/shape_nbd_" + suffix + "_roc.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            writer.write(str.toString() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    // dump an ROC by varying LD's threshold
    public void dumpShapeLD_ROC() {
        if (!parameters.dump_shape_ld_roc)
            return;
        AggregatedResults stat;
        int point_count = 100;
        StringBuffer str = new StringBuffer();
        assert(subset_selector_names.get(1).equals("edu.ut.austin.MachineSelectionStrategy.LD_MachineSelection_only_mal_nbds"));

        for (int i = 0; i <= point_count; i ++) {
            double threshold = (double) i / point_count;
            parameters.LD_threshold = threshold;
            relabelNBDs();
            processNBDs();
            stat = computeFileStat(ProcessNodeStatType.FULL_EVALUATION, false, subset_selector_names.get(1)).getRight(); // "LD_MachineSelection" policy
            double fp_rate = (double) stat.fp_files_shape/stat.benign_file_count;
            double tp_rate = (double) stat.tp_files_shape/stat.malicious_file_count;
            str.append(threshold + "," + fp_rate + "," + tp_rate + "\n");
            global_parameters.logger.info(String.format("<<<dumpShapeLD_ROC()>>> threshold: %f   FP: %f   TP: %f \n", threshold, fp_rate, tp_rate));
        }

        try (
                OutputStreamWriter stream_writer = new FileWriter(parameters.out_folder + "/shape_ld_roc.csv");
                BufferedWriter writer = new BufferedWriter(stream_writer);
        ) {
            writer.write(str.toString() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    // avg. machine detection after getting infected,
    // detected/infected machines
    private Pair<String, AggregatedResults> computeFileStat(ProcessNodeStatType processing_type, boolean print_internal_stat, String subset_selector_name) {
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        AggregatedResults aggreagted_results = new AggregatedResults();
        Set<String> quarantined_files = new HashSet<>();
        Set<String> cum_false_positives = new HashSet<>();
        StringBuilder str = new StringBuilder();
        List<Map<String, Long>> results = new LinkedList<>();

        Map<String, FileInfo> file_info;
        Set<String> possible_early_detection = null;
        Set<String> dataset_infected_files = null;
        Set<Long> dataset_infected_machines = null;

        Map<String, List<Pair<Long, Long>>> file_to_machine_map_t = null;
        // <infected_machine_id, detection time stamp>
        Map<Long, Long> quarantined_machines_shape = new ConcurrentHashMap<>();
        Map<Long, Long> quarantined_machines_shape_ld = new ConcurrentHashMap<>();
        Map<Long, Long> infected_machine_time_stamp = new ConcurrentHashMap<>();
        Predicate<String> file_filter_t = null;

        switch (processing_type) {
            case CCS_EVALUATION:
                file_info = file_info_ccs_evaluation.parallelStream()
                        .collect(Collectors.toMap(Function.identity(), sha2_id -> this.file_info.get(sha2_id)));
                possible_early_detection = possible_early_detection_ccs;
                dataset_infected_files = dataset_infected_files_ccs;
                file_to_machine_map_t = file_to_machine_ccs_map;
                file_filter_t = file_id -> all_downloaders.contains(file_id);
                break;
            case FULL_EVALUATION:
                file_info = file_info_full_evaluation.parallelStream()
                        .collect(Collectors.toMap(Function.identity(), sha2_id -> this.file_info.get(sha2_id)));
                possible_early_detection = possible_early_detection_full;
                dataset_infected_files = dataset_infected_files_full;
                file_to_machine_map_t = file_to_machine_full_map;
                file_filter_t = file_id -> true;
                break;
            default:
                file_info = null;
        }

        Map<String, List<Pair<Long, Long>>> file_to_machine_map = file_to_machine_map_t;
        final Predicate<String> file_filter = file_filter_t;
        List<Long> appearance_in_wine_dataset_list = new LinkedList<>();

        for (DetectionSnapshot snapshot : detection_snapshots.values()) {
            // <sha2_id, time_difference>, i.e. early/late detection time
            Map<String, Long> file_map = snapshot.computeEarlyFileInfectionDetectionTime(quarantined_files, file_info, subset_selector_name);
            Supplier<LongStream> supplier =  () -> file_map.values().stream().mapToLong(Long::valueOf).filter(item -> item > 0);
            OptionalDouble opt = supplier.get().average();
            double avg_detection_time = opt.isPresent() ? opt.getAsDouble() : -1;
            results.add(file_map);

            long start_time_stamp = snapshot.time_stamp - ConfigurationParameters.getInstance().time_window_size;
            quarantined_files.parallelStream().filter(file_filter).flatMap(file_id -> file_to_machine_map.get(file_id).stream())
                    .filter(pair -> pair.getRight() >= start_time_stamp && pair.getRight() <= snapshot.time_stamp)
                    .forEach(pair -> quarantined_machines_shape.putIfAbsent(pair.getLeft(), snapshot.time_stamp));

            quarantined_files.parallelStream().filter(file_filter)
                    .filter(file_id -> local_detector.isMalware(file_info.get(file_id).redirect.sha2, null))
                    .flatMap(file_id -> file_to_machine_map.get(file_id).stream())
                    .filter(pair -> pair.getRight() >= start_time_stamp && pair.getRight() <= snapshot.time_stamp)
                    .forEach(pair -> quarantined_machines_shape_ld.putIfAbsent(pair.getLeft(), snapshot.time_stamp));

            file_map.entrySet().stream().filter(entry -> entry.getValue() > 0)
                    .map(entry -> snapshot.time_stamp - this.file_info.get(entry.getKey()).earliest_wine_time)
                    .forEach(appearance_in_wine_dataset_list::add);

            if (avg_detection_time > 0 && print_internal_stat)
                str.append(snapshot + " <early_detection: " +
                        Utils.convertUnixTimeStampToDays(avg_detection_time) +
                        " days, # of samples: " + supplier.get().count() + ">\n");

            cum_false_positives.addAll(snapshot.getBenignFileIDs_SubsetSelector(subset_selector_name));
        }
        Utils.dumpNBD_Stat(detection_snapshots.values());
        Supplier<LongStream> avg_supplier = () -> results.stream().flatMap(map -> map.values().stream())
                .mapToLong(Long::valueOf).filter(time_stamp -> time_stamp > 0);
        OptionalDouble opt = avg_supplier.get().average();
        double avg_early_detection = opt.isPresent() ? opt.getAsDouble() : -1;
        Utils.dumpCollectionToCSV(avg_supplier.get().boxed().collect(Collectors.toList()), parameters.out_folder + "/early_detection_time_average.csv");

        opt = appearance_in_wine_dataset_list.stream()
                .mapToLong(Long::valueOf).average();
        double appearance_in_wine_dataset = opt.isPresent() ? opt.getAsDouble() : -1;
        long downloaders_early_detected = results.stream().flatMap(map -> map.entrySet().stream())
                .filter(entry -> entry.getValue() > 0).map(Map.Entry::getKey).distinct().count();
        List<Long> early_detection_times = results.stream().flatMap(map ->
                map.values().stream()).filter(item -> item > 0).collect(Collectors.toList());

        double d[] = new double[early_detection_times.size()];
        int i = 0;
        for (long item : early_detection_times)
            d[i++] = (double) item;

        Set<String> data_set_benign_files_temp = new HashSet<>(data_set_benign_files);
        Set<String> data_set_malicious_files_temp = new HashSet<>(data_set_malicious_files);

        makeProjection(processing_type, quarantined_files);
        makeProjection(processing_type, dataset_infected_files);
        makeProjection(processing_type, cum_false_positives);
        makeProjection(processing_type, data_set_benign_files_temp);
        makeProjection(processing_type, data_set_malicious_files_temp);

        // machine-related stuff
        file_to_machine_map.entrySet().parallelStream().filter(entry -> data_set_malicious_files_temp.contains(entry.getKey()))
                .flatMap(entry -> entry.getValue().stream()).forEach(pair -> {
            infected_machine_time_stamp.putIfAbsent(pair.getLeft(), pair.getRight());
            if (infected_machine_time_stamp.get(pair.getLeft()) > pair.getRight())
                infected_machine_time_stamp.put(pair.getLeft(), pair.getRight());
        });

        // clean machines -- machines that have never been infected
        Set<Long> clean_machines = file_to_machine_map.values().parallelStream()
                .flatMap(Collection::stream).map(Pair::getLeft).collect(Collectors.toSet());
        clean_machines.removeAll(infected_machine_time_stamp.keySet());

        Double avg_machine_post_infection_time_shape = quarantined_machines_shape.entrySet().parallelStream()
                .mapToDouble(entry -> entry.getValue() - infected_machine_time_stamp.get(entry.getKey())).average().orElse(-1.0);

        Double avg_machine_post_infection_time_shape_ld = quarantined_machines_shape_ld.entrySet().parallelStream()
                .mapToDouble(entry -> entry.getValue() - infected_machine_time_stamp.get(entry.getKey())).average().orElse(-1.0);

        aggreagted_results.early_detection_days_nbd = Utils.convertUnixTimeStampToDays(avg_early_detection);
        aggreagted_results.time_to_detection_days_nbd = Utils.convertUnixTimeStampToDays(appearance_in_wine_dataset);
        aggreagted_results.early_detection_files_nbd = (int) downloaders_early_detected;

        // aggreagted_results.detected_file_count = quarantined_files.size();
        aggreagted_results.benign_file_count = data_set_benign_files_temp.size();
        aggreagted_results.malicious_file_count = data_set_malicious_files_temp.size();
        aggreagted_results.possible_early_detection_files = possible_early_detection.size();

        String prefix = processing_type == ProcessNodeStatType.CCS_EVALUATION ? "CCS_EVALUATION" : "FULL_EVALUATION";
        String res_str_files = String.format("[ %s ] [ %s ] %f/%f days (early detection/time to detection), " +
                        "%,d/%,d  (early detected/possible_early_detection) %,d/%,d (detected/infected)\n",
                subset_selector_name, prefix, Utils.convertUnixTimeStampToDays(avg_early_detection), Utils.convertUnixTimeStampToDays(appearance_in_wine_dataset),
                downloaders_early_detected, possible_early_detection.size(), quarantined_files.size(), data_set_malicious_files_temp.size());
        str.append(res_str_files);
        global_parameters.addLineToLogFile(res_str_files);

        List<Integer> shape_vs_ld_files = compareWithLD_FileLevel(cum_false_positives, quarantined_files);
        String shape_vs_ld_files_str = String.format("ShapeGD covers %,d/%,d (FP/TP) files, ShapeGD raises File-LD-level %,d/%,d (FP/TP)",
                shape_vs_ld_files.get(0), shape_vs_ld_files.get(1), shape_vs_ld_files.get(2), shape_vs_ld_files.get(3));
        List<Integer> pure_ld_stat = getPureLD_Stat(processing_type);
        aggreagted_results.fp_files_ld = pure_ld_stat.get(0);
        aggreagted_results.tp_files_ld = pure_ld_stat.get(1);
        aggreagted_results.fp_files_nbd = shape_vs_ld_files.get(0);
        aggreagted_results.tp_files_nbd = shape_vs_ld_files.get(1);
        aggreagted_results.fp_files_shape = shape_vs_ld_files.get(2);
        aggreagted_results.tp_files_shape = shape_vs_ld_files.get(3);

        String fp_stat_str = String.format("[ %s ] [ %s ] %s vs LDs %,d/%,d (FP/TP) vs Loaded Data set %,d/%,d (unique benign/malicious files)\n",
                subset_selector_name, prefix, shape_vs_ld_files_str, pure_ld_stat.get(0), pure_ld_stat.get(1), data_set_benign_files_temp.size(), data_set_malicious_files_temp.size());
        str.append(fp_stat_str);

        List<Integer> shape_vs_ld_machines = compareWithLD_MachineLevel(cum_false_positives, quarantined_files, file_to_machine_map, object -> true, file_info);
        String shape_vs_ld_machines_str = String.format("ShapeGD covers %,d/%,d (FP/TP) machines,  " +
                        "ShapeGD raises Machine-LD-level %,d/%,d (FP/TP) alerts  vs LD raises machine alerts %,d/%,d (FP/TP) alerts",
                shape_vs_ld_machines.get(0), shape_vs_ld_machines.get(1), shape_vs_ld_machines.get(2),
                shape_vs_ld_machines.get(3), shape_vs_ld_machines.get(4), shape_vs_ld_machines.get(5));
        aggreagted_results.fp_machines_nbd = shape_vs_ld_machines.get(0);
        aggreagted_results.tp_machines_nbd = shape_vs_ld_machines.get(1);
        aggreagted_results.fp_machines_shape = shape_vs_ld_machines.get(2);
        aggreagted_results.tp_machines_shape = shape_vs_ld_machines.get(3);
        aggreagted_results.fp_machines_ld = shape_vs_ld_machines.get(4);
        aggreagted_results.tp_machines_ld = shape_vs_ld_machines.get(5);
        aggreagted_results.benign_machine_count = clean_machines.size();
        aggreagted_results.malicious_machine_count = infected_machine_time_stamp.size();
        aggreagted_results.time_to_detection_machine_nbd = Utils.convertUnixTimeStampToDays(avg_machine_post_infection_time_shape);
        aggreagted_results.time_to_detection_machine_shape = Utils.convertUnixTimeStampToDays(avg_machine_post_infection_time_shape_ld);

        String res_str_machines = String.format("[ %s ] [ %s ] Time to detection time: %f/%f days (shape/shape-LDs), %s vs %,d/%,d (infected/clean) machines in the dataset\n",
                subset_selector_name, prefix, Utils.convertUnixTimeStampToDays(avg_machine_post_infection_time_shape),
                Utils.convertUnixTimeStampToDays(avg_machine_post_infection_time_shape_ld), shape_vs_ld_machines_str, infected_machine_time_stamp.size(), clean_machines.size());
        str.append(res_str_machines);

        List<Double> ld_early_detection = computeLD_EarlyDetectionParameters(possible_early_detection);
        String.format("LD early detection stat: %f/%f (early detection/detection after appearance in WINE), %,d/%,d (early detected/possible_early_detection)\n\n",
                ld_early_detection.get(0), ld_early_detection.get(1), ld_early_detection.get(2).intValue(), ld_early_detection.get(3).intValue());
        str.append(ld_early_detection);
        aggreagted_results.early_detection_days_pure_ld = ld_early_detection.get(0);
        aggreagted_results.time_to_detection_days_pure_ld = ld_early_detection.get(1);
        aggreagted_results.early_detected_files_pure_ld = ld_early_detection.get(2).intValue();

        global_parameters.addLineToLogFile(res_str_machines);
        return new ImmutablePair<>(str.toString(), aggreagted_results);
    }

    private String getCCS_Stat() {
        Pair<Integer, Integer> pair;
        StringBuffer str = new StringBuffer("CCS'15 Results ");

        pair = ccs_results.get("benignware");
        str.append(String.format("%,d/%,d (FP/all benign downloaders) ", pair.getLeft(), pair.getRight()));

        pair = ccs_results.get("malware");
        str.append(String.format("%,d/%,d (TP/all malicious downloaders)\n", pair.getLeft(), pair.getRight()));
        return str.toString();
    }

    private List<Double> computeLD_EarlyDetectionParameters(Set<String> possible_early_detection) {
        LocalDetector local_detector = TrainML_Models.loadLD_Model(parameters.LF_implementation_type);
        List<Double> results = new LinkedList<>();

        Set<FileInfo> early_detected_samples = possible_early_detection.parallelStream()
                .filter(file_id -> local_detector.isMalware(file_info.get(file_id).redirect.sha2, null))
                .map(file_id -> file_info.get(file_id)).collect(Collectors.toSet());

        double early_detection_time = early_detected_samples.parallelStream()
                .mapToDouble(file -> file.vt_time_stamp - file.earliest_wine_time).average().orElse(-1);

        results.add(Utils.convertUnixTimeStampToDays(early_detection_time));
        results.add(0.0);
        results.add((double) early_detected_samples.size());
        results.add((double) possible_early_detection.size());
        return results;
    }

    private Pair<Integer, Integer> getNBD_Count() {
        int nbd_count = (int) detection_snapshots.values().parallelStream()
                .mapToLong(snapshot -> snapshot.nbd_states.values().size()).sum();

        int malicious_nbd_count = (int) detection_snapshots.values().parallelStream()
                .flatMap(snapshot -> snapshot.nbd_states.values().stream()).filter(NBD_State::isMalicious).count();
        return new ImmutablePair<>(nbd_count, malicious_nbd_count);
    }

    private void makeProjection(ProcessNodeStatType processing_type, Set<String> set) {
        if (processing_type != ProcessNodeStatType.CCS_EVALUATION)
            return;
        set.retainAll(all_downloaders);
    }

    private List<Integer> getPureLD_Stat(ProcessNodeStatType processing_type) {
        Set<String> dataset;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        switch (processing_type) {
            case CCS_EVALUATION:
                dataset = all_downloaders;
                break;
            case FULL_EVALUATION:
                dataset = file_info_full_evaluation;
                break;
            default:
                dataset = null;
        }

        LocalDetector local_detector = TrainML_Models.loadLD_Model(parameters.LF_implementation_type);
        List<String> benign_files = dataset.parallelStream()
                .map(file_id -> file_info.get(file_id)).filter(file_info -> file_info.is_benign)
                .map(file_info -> file_info.redirect.sha2).collect(Collectors.toList());

        List<String> malicious_files = dataset.parallelStream()
                .map(file_id -> file_info.get(file_id)).filter(file_info -> !file_info.is_benign)
                .map(file_info -> file_info.redirect.sha2).collect(Collectors.toList());

        int ld_false_positives = (int) benign_files.parallelStream()
                .filter(sha256 -> local_detector.isMalware(sha256, null)).count();

        int ld_true_positives = (int) malicious_files.parallelStream()
                .filter(sha256 -> local_detector.isMalware(sha256, null)).count();

        List<Integer> results = new LinkedList<>();
        results.add(ld_false_positives);
        results.add(ld_true_positives);
        results.add(benign_files.size());
        results.add(malicious_files.size());
        return results;
    }

    private List<Integer> compareWithLD_FileLevel(Set<String> cum_false_positives, Set<String> quarantined_files) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        List<Integer> results = new LinkedList<>();
        LocalDetector local_detector = TrainML_Models.loadLD_Model(parameters.LF_implementation_type);
        results.add(cum_false_positives.size());
        results.add(quarantined_files.size());

        int ld_fp_count = (int) cum_false_positives.parallelStream().map(file_id ->
                file_info.get(file_id).redirect.sha2).filter(sha256 -> local_detector.isMalware(sha256, null)).count();
        int ld_tp_count = (int) quarantined_files.parallelStream().map(file_id ->
                file_info.get(file_id).redirect.sha2).filter(sha256 -> local_detector.isMalware(sha256, null)).count();
        results.add(ld_fp_count);
        results.add(ld_tp_count);
        return results;
    }

    private List<Integer> compareWithLD_MachineLevel(Set<String> cum_false_positives, Set<String> quarantined_files,
                                              Map<String, List<Pair<Long, Long>>> file_to_machine_map, Predicate<String> file_filter, Map<String, FileInfo> file_info_view) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        List<Integer> results = new LinkedList<>();
        LocalDetector local_detector = TrainML_Models.loadLD_Model(parameters.LF_implementation_type);

        Set<Long> machine_tp = quarantined_files.parallelStream().filter(file_filter)
                .flatMap(file_id -> file_to_machine_map.get(file_id).stream())
                .map(Pair::getLeft).collect(Collectors.toSet());

        Set<Long> machine_fp = cum_false_positives.parallelStream().filter(file_filter)
                .flatMap(file_id -> file_to_machine_map.get(file_id).stream()).map(Pair::getLeft)
                .filter(machine_id -> !machine_tp.contains(machine_id)).collect(Collectors.toSet());
        results.add(machine_fp.size());
        results.add(machine_tp.size());

        Set<Long> ld_machine_tp = quarantined_files.parallelStream().filter(file_filter)
                .filter(file_id -> local_detector.isMalware(file_info.get(file_id).redirect.sha2, null))
                .flatMap(file_id -> file_to_machine_map.get(file_id).stream()).map(Pair::getLeft).collect(Collectors.toSet());

        Set<Long> ld_machine_fp = cum_false_positives.parallelStream().filter(file_filter)
                .filter(file_id -> local_detector.isMalware(file_info.get(file_id).redirect.sha2, null))
                .flatMap(file_id -> file_to_machine_map.get(file_id).stream()).map(Pair::getLeft)
                .filter(machine_id -> !ld_machine_tp.contains(machine_id)).collect(Collectors.toSet());
        results.add(ld_machine_fp.size());
        results.add(ld_machine_tp.size());

        Set<Long> pure_ld_machine_tp = file_info_view.values().parallelStream().filter(file_info_t -> !file_info_t.is_benign)
                .filter(file_info_t -> local_detector.isMalware(file_info_t.redirect.sha2, null))
                .map(file_info_t -> file_info_t.sha2_id).filter(file_to_machine_map::containsKey)
                .flatMap(file_id -> file_to_machine_map.get(file_id).stream()).map(Pair::getLeft).collect(Collectors.toSet());

        Set<Long> pure_ld_machine_fp = file_info_view.values().parallelStream().filter(file_info_t -> file_info_t.is_benign)
                .filter(file_info_t -> local_detector.isMalware(file_info_t.redirect.sha2, null))
                .map(file_info_t -> file_info_t.sha2_id).filter(file_to_machine_map::containsKey)
                .flatMap(file_id -> file_to_machine_map.get(file_id).stream()).map(Pair::getLeft)
                .filter(machine_id -> !pure_ld_machine_tp.contains(machine_id)).collect(Collectors.toSet());
        results.add(pure_ld_machine_fp.size());
        results.add(pure_ld_machine_tp.size());
        return results;
    }

    public void dumpAdvancedStat() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        global_parameters.logger.info("Total # of processed neighborhoods: " + processed_nbd_count.get());
        dumpNBDstat();

        global_parameters.subset_selector.forEach(subset_selector -> {
                processed_nbd_count.set(0);
                dumpAdvancedStat(parameters.introspection_ground_truth_stat_file_name
                        + "_" + subset_selector.toString() + "_" +subset_selector.getArgs() + ".csv", subset_selector.toString());
                });
        dumpAdvancedPureLDStat(parameters.introspection_ground_truth_stat_file_name + "_pure_LD.csv");
    }

    private void dumpNBDstat() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        try {
            FileUtils.deleteDirectory(new File(parameters.nbd_stat_folder_name));
            Files.createDirectories(Paths.get(parameters.nbd_stat_folder_name));
        } catch (Exception e) {
            e.printStackTrace();
        }

        detection_snapshots.values().parallelStream().forEach(snapshot -> {
            Map<Double, List<Triple<Integer, Integer, Integer>>> nbd_stat_machines = new TreeMap<>(Collections.reverseOrder());
            String file_name = parameters.nbd_stat_folder_name + File.separator + "machines_" + snapshot.time_stamp;

            try (
                OutputStreamWriter stream_writer = new FileWriter(file_name);
                BufferedWriter writer = new BufferedWriter(stream_writer);
            ) {
                for (NBD_State nbd : snapshot.nbd_states.values()) {
                    int label = nbd.isMalicious() ? 1 : 0;
                    Double key = (double) nbd.mal_file_count / nbd.ben_file_count;
                    if (key.isInfinite())
                        continue;
                    nbd_stat_machines.putIfAbsent(key, new LinkedList<>());
                    nbd_stat_machines.get(key).add(new ImmutableTriple<>(label, nbd.ben_machine_count, nbd.mal_machine_count));
                    double score = nbd.score != null ? nbd.score : -1;
                    writer.write(key + "," + label + "," + nbd.ben_machine_count
                            + "," + nbd.mal_machine_count + "," + score +"\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        });

        detection_snapshots.values().parallelStream().forEach(snapshot -> {
            Map<Double, Queue<Triple<Integer, Integer, Integer>>> nbd_stat_files = new TreeMap<>(Collections.reverseOrder());
            String file_name = parameters.nbd_stat_folder_name + File.separator + "files_" + snapshot.time_stamp;

            try (
                    OutputStreamWriter stream_writer = new FileWriter(file_name);
                    BufferedWriter writer = new BufferedWriter(stream_writer);
            ) {
                for (NBD_State nbd : snapshot.nbd_states.values()) {
                    int label = nbd.isMalicious() ? 1 : 0;
                    Double key = (double) nbd.mal_file_count / nbd.ben_file_count;
                    if (key.isInfinite())
                        continue;
                    nbd_stat_files.putIfAbsent(key, new ConcurrentLinkedQueue<>());
                    nbd_stat_files.get(key).add(new ImmutableTriple<>(label, nbd.ben_file_count, nbd.mal_file_count));
                    double score = nbd.score != null ? nbd.score : -1;
                    writer.write(key + "," + label + "," + nbd.ben_file_count + ","
                            + nbd.mal_file_count + "," + score + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        });
    }

    private void dumpAdvancedStat(String file_name, String subset_selector_name) {
        Set<String> file_alerts = new HashSet<>();
        Set<Long> machine_alerts = new HashSet<>();
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();

        List<SnapshotStructure> stat = new LinkedList<>();
        stat.add(new SnapshotStructure());

        for (DetectionSnapshot snapshot : detection_snapshots.values()) {
            SnapshotStructure struct = new SnapshotStructure();
            stat.add(struct);
            struct.nbd_count = snapshot.getNBD_Count();
            struct.infected_nbd_count = snapshot.getMaliciousNBD_Count();

            Set<String> file_tps = snapshot.getInfectedFileIDs_SubsetSelector(subset_selector_name);
            Set<String> file_fns = new HashSet<>(snapshot.total_malicious_unique_files_ground_truth);
            file_fns.removeAll(file_tps);
            Set<String> file_fps = snapshot.getFileIDs_SubsetSelector(subset_selector_name);
            file_fps.removeAll(snapshot.total_malicious_unique_files_ground_truth);
            Set<String> file_tns = new HashSet<>(snapshot.total_benign_unique_files_ground_truth);
            file_tns.removeAll(snapshot.getFileIDs_SubsetSelector(subset_selector_name));

            struct.file_alerts_subset_selector = snapshot.getFileIDs_SubsetSelector(subset_selector_name).size();
            struct.file_alerts_tp = snapshot.getInfectedFileIDs_SubsetSelector(subset_selector_name).size();
            struct.total_unique_file_count_in_scope = snapshot.total_unique_file_count_in_scope.size();
            struct.total_unique_infected_file_count = snapshot.total_malicious_unique_files_ground_truth.size();
            struct.total_file_count_in_scope = snapshot.total_file_count_in_scope;
            struct.total_infected_file_count = snapshot.total_malicious_files_ground_truth;
            file_alerts.addAll(snapshot.getFileIDs_SubsetSelector(subset_selector_name));
            struct.file_alerts_cum = file_alerts.size();

            // precision & recall
            struct.file_precision = 1.0 * file_tps.size() / (file_tps.size() + file_fps.size() + parameters.epsilon);
            struct.file_recall = 1.0 * file_tps.size() / (file_tps.size() + file_fns.size() + parameters.epsilon);
            struct.file_specificity = 1.0 * file_tns.size() / (file_tns.size() + file_fps.size() + parameters.epsilon);
            struct.file_accuracy = 1.0 * (file_tps.size() + file_tns.size()) /
                    (snapshot.total_benign_unique_files_ground_truth.size() + snapshot.total_malicious_unique_files_ground_truth.size() + parameters.epsilon);

            // machine-related stat
            Set<Long> machine_tps = snapshot.getInfectedMachineIDs_SubsetSelector(subset_selector_name);
            Set<Long> machine_fns = new HashSet<>(snapshot.infected_machines_ground_truth);
            machine_fns.removeAll(snapshot.getMachineIDs_SubsetSelector(subset_selector_name));
            Set<Long> machine_fps = snapshot.getMachineIDs_SubsetSelector(subset_selector_name);
            machine_fps.removeAll(snapshot.infected_machines_ground_truth);
            Set<Long> machine_tns = new HashSet<>(snapshot.benign_machines_ground_truth);
            machine_tns.removeAll(snapshot.getMachineIDs_SubsetSelector(subset_selector_name));


            struct.machine_alerts_subset_selector = snapshot.getMachineIDs_SubsetSelector(subset_selector_name).size();
            struct.machine_alerts_tp =  snapshot.getInfectedMachineIDs_SubsetSelector(subset_selector_name).size();
            struct.total_unique_machine_count_in_scope = (int) Stream.concat(snapshot.infected_machines_ground_truth.stream(), snapshot.benign_machines_ground_truth.stream()).distinct().count();
            struct.total_unique_infected_machine_count = snapshot.infected_machines_ground_truth.size();
            machine_alerts.addAll(snapshot.getMachineAlerts());
            struct.machine_count_cum = machine_alerts.size();

            // precision & recall
            struct.machine_precision = 1.0 * machine_tps.size() / (machine_tps.size() + machine_fps.size() + parameters.epsilon);
            struct.machine_recall = 1.0 * machine_tps.size() / (machine_tps.size() + machine_fns.size() + parameters.epsilon);
            struct.machine_specificity = 1.0 * machine_tns.size() / (machine_tns.size() + machine_fps.size() + parameters.epsilon);
            struct.machine_accuracy = 1.0 * (machine_tps.size() + machine_tns.size()) /
                    (snapshot.infected_machines_ground_truth.size() + snapshot.benign_machines_ground_truth.size() + parameters.epsilon);
            struct.time_stamp = snapshot.time_stamp;
        }

        try(
                BufferedWriter w = new BufferedWriter(new FileWriter(file_name))
        )
        {
            for (SnapshotStructure snapshot : stat) {
                w.write(snapshot.toString());
            }
        }
        catch(IOException e) {
            e.printStackTrace();
        }

        long execution_time = System.currentTimeMillis() - GlobalParameters.getInstance().start_execution;
        System.out.format("Advanced statistics: Exectuion Time: %02d hours %02d minutes %02d seconds\n", TimeUnit.MILLISECONDS.toHours(execution_time),
                TimeUnit.MILLISECONDS.toMinutes(execution_time) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(execution_time)),
                TimeUnit.MILLISECONDS.toSeconds(execution_time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(execution_time)));
    }

    // run an LD on each snapshot
    private void dumpAdvancedPureLDStat(String file_name) {
        Set<String> file_alerts = new HashSet<>();
        Set<Long> machine_alerts = new HashSet<>();
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        LocalDetector local_detector = TrainML_Models.loadLD_Model(parameters.LF_implementation_type);

        List<SnapshotStructure> stat = new LinkedList<>();
        stat.add(new SnapshotStructure());

        for (DetectionSnapshot snapshot : detection_snapshots.values()) {
            SnapshotStructure struct = new SnapshotStructure();
            stat.add(struct);
            struct.nbd_count = 1;
            struct.infected_nbd_count = 1;

            Supplier<Stream<String>> supplier = () -> snapshot.total_unique_file_count_in_scope.stream()
                    .filter(sha_id -> local_detector.isMalware(file_info.get(sha_id).redirect.sha2, null));
            Set<String> file_tps = supplier.get().filter(sha_id -> !file_info.get(sha_id).is_benign).collect(Collectors.toSet());
            Set<String> file_fns = new HashSet<>(snapshot.total_malicious_unique_files_ground_truth);
            file_fns.removeAll(file_tps);
            Set<String> file_fps = supplier.get().filter(sha_id -> file_info.get(sha_id).is_benign).collect(Collectors.toSet());
            Set<String> file_tns = new HashSet<>(snapshot.total_benign_unique_files_ground_truth);
            file_tns.removeAll(supplier.get().collect(Collectors.toSet()));
            struct.file_alerts_subset_selector = (int) supplier.get().count();

            struct.file_alerts_tp = file_tps.size();
            struct.total_unique_file_count_in_scope = snapshot.total_unique_file_count_in_scope.size();
            struct.total_unique_infected_file_count = snapshot.total_malicious_unique_files_ground_truth.size();
            struct.total_file_count_in_scope = snapshot.total_file_count_in_scope;
            struct.total_infected_file_count = snapshot.total_malicious_files_ground_truth;
            file_alerts.addAll(supplier.get().collect(Collectors.toSet()));
            struct.file_alerts_cum = file_alerts.size();

            // precision & recall
            struct.file_precision = 1.0 * file_tps.size() / (file_tps.size() + file_fps.size() + parameters.epsilon);
            struct.file_recall = 1.0 * file_tps.size() / (file_tps.size() + file_fns.size() + parameters.epsilon);
            struct.file_specificity = 1.0 * file_tns.size() / (file_tns.size() + file_fps.size() + parameters.epsilon);
            struct.file_accuracy = 1.0 * (file_tps.size() + file_tns.size()) /
                    (snapshot.total_benign_unique_files_ground_truth.size() + snapshot.total_malicious_unique_files_ground_truth.size() + parameters.epsilon);

            // machine-related stat
            long start_time_stamp = snapshot.time_stamp - ConfigurationParameters.getInstance().time_window_size;
            Set<Long> snapshot_machine_alerts = supplier.get().flatMap(file_id -> file_to_machine_full_map.get(file_id).stream())
                    .filter(pair -> pair.getRight() >= start_time_stamp && pair.getRight() <= snapshot.time_stamp).map(Pair::getLeft).collect(Collectors.toSet());
            Set<Long> machine_tns = new HashSet<>(snapshot.benign_machines_ground_truth);
            machine_tns.removeAll(snapshot_machine_alerts);

            Set<Long> machine_tps = new HashSet<>(snapshot_machine_alerts);
            machine_tps.retainAll(snapshot.infected_machines_ground_truth);

            Set<Long> machine_fps = new HashSet<>(snapshot_machine_alerts);
            machine_fps.removeAll(machine_tps);
            Set<Long> machine_fns = new HashSet<>(snapshot.infected_machines_ground_truth);
            machine_fns.removeAll(machine_tps);

            struct.machine_alerts_subset_selector = snapshot_machine_alerts.size();
            struct.machine_alerts_tp =  machine_tps.size();
            struct.total_unique_machine_count_in_scope = (int) Stream.concat(snapshot.infected_machines_ground_truth.stream(), snapshot.benign_machines_ground_truth.stream()).distinct().count();//snapshot.total_unique_machine_count_in_scope;
            struct.total_unique_infected_machine_count = snapshot.infected_machines_ground_truth.size();
            machine_alerts.addAll(snapshot_machine_alerts);
            struct.machine_count_cum = machine_alerts.size();

            // precision & recall
            struct.machine_precision = 1.0 * machine_tps.size() / (machine_tps.size() + machine_fps.size() + parameters.epsilon);
            struct.machine_recall = 1.0 * machine_tps.size() / (machine_tps.size() + machine_fns.size() + parameters.epsilon);
            struct.machine_specificity = 1.0 * machine_tns.size() / (machine_tns.size() + machine_fps.size() + parameters.epsilon);
            struct.machine_accuracy = 1.0 * (machine_tps.size() + machine_tns.size()) /
                    (snapshot.infected_machines_ground_truth.size() + snapshot.benign_machines_ground_truth.size() + parameters.epsilon);
        }

        try(
                BufferedWriter w = new BufferedWriter(new FileWriter(file_name))
        )
        {
            for (SnapshotStructure snapshot : stat) {
                w.write(snapshot.toString());
            }
        }
        catch(IOException e) {
            e.printStackTrace();
        }

        long execution_time = System.currentTimeMillis() - GlobalParameters.getInstance().start_execution;
        System.out.format("Advanced statistics: Exectuion Time: %02d hours %02d minutes %02d seconds\n", TimeUnit.MILLISECONDS.toHours(execution_time),
                TimeUnit.MILLISECONDS.toMinutes(execution_time) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(execution_time)),
                TimeUnit.MILLISECONDS.toSeconds(execution_time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(execution_time)));
    }

    public void printResults() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        Pair<Integer, Integer> pair = getNBD_Count();
        global_parameters.logger.info("Introspection results. " + parameters.out_folder);
        global_parameters.logger.info("Total NBD count: " + pair.getLeft() + " Malicious NBD Count: " + pair.getRight());
        global_parameters.logger.info(getStat());
        global_parameters.logger.info(String.format("computed_fvs_count: %d existing_fvs_count: %d\n", computed_fvs_count.get(), existing_fvs_count.get()));

        long execution_time = System.currentTimeMillis() - GlobalParameters.getInstance().start_execution;
        global_parameters.logger.info(String.format("Exectuion Time: %02d hours %02d minutes %02d seconds\n", TimeUnit.MILLISECONDS.toHours(execution_time),
                TimeUnit.MILLISECONDS.toMinutes(execution_time) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(execution_time)),
                TimeUnit.MILLISECONDS.toSeconds(execution_time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(execution_time))));

        dumpAggreagtedResultsTable();
    }

    public DetectionSnapshot getDetectionSnapshot(long time_step) {
        return detection_snapshots.get(time_step);
    }

    public DetectionSnapshot removeDetectionSnapshot(long time_step) {
        return detection_snapshots.remove(time_step);
    }

    public NBD_State getNBD(long time_step, int index) {
        return detection_snapshots.get(time_step).nbd_states.get(index);
    }

    public void removeNBD(long time_step, int index) {
        detection_snapshots.get(time_step).nbd_states.remove(index);
    }

    private void clearSubsetSelectorResutls() {
        if (ConfigurationParameters.getInstance().recompute_stat)
            detection_snapshots.values().forEach(DetectionSnapshot::clearSubsetSelectorResutls);
    }

    // Do not use it because NBDs are dumped incrementally due to serialization issues
    public void dumpAllNBDsToFile() {
        if (!parameters.dump_nbd_instances)
            return;

        String dir_name = "results/nbd_dump";
        AtomicInteger nbd_index = new AtomicInteger(0);

        File dir = new File(dir_name);
        if (dir.exists())
            try {
                FileUtils.deleteDirectory(dir);
            } catch (IOException e) {
                e.printStackTrace();
            }
        dir.mkdir();

        String header = "sha2_id,file_name,machine_id,time_stamp,file_info.is_benign,"
                + "file_info.family_name,file_info.prefix,file_info.suffix,file_info.sha2,file_info.earliest_wine_time,"
                + "file_info.av_count,file_info.vt_time_stamp,file_info.file_type_str,"
                + "geo_info.geo_id,geo_info.isp,geo_info.country,snapshot.time_stamp,incoming_url_edges,outgoing_url_edges,cluster_init_url\n";
        Map<Long, Queue<NBD_State>> nbds = new ConcurrentHashMap<>();

        for (DetectionSnapshot snapshot : detection_snapshots.values()) {
            snapshot.nbd_states.values().stream().filter(NBD_State::isMalicious)
                    .filter(nbd -> nbd.mal_ratio > 0.5).filter(nbd -> nbd.ben_file_count + nbd.mal_file_count > 500)
                    .forEach(nbd -> {
                        nbds.putIfAbsent(snapshot.time_stamp, new ConcurrentLinkedQueue<>());
                        nbds.get(snapshot.time_stamp).add(nbd);
                    });
        }
        System.out.println("dumpAllNBDsToFile() selects " + nbds.size() + " nbds");

        for (Map.Entry<Long, Queue<NBD_State>> entry : nbds.entrySet()) {
            for (NBD_State nbd : entry.getValue()) {
                List<String> nbd_strs = nbd.nbd_files.stream().map(node -> {
                    String geo_info_str = "None,None,None";
                    Triple<Integer, String, String> geo_info = machine_geo_location.get(node.machine_id);
                    if (geo_info != null)
                        geo_info_str = geo_info.getLeft() + "," + geo_info.getMiddle() + "," + geo_info.getRight();

                    return node.sha2_id + "," + node.file_name + ","
                            + node.machine_id + "," + node.time_stamp + "," + node.file_info.is_benign  + ","
                            + node.file_info.family_name + "," + node.file_info.prefix + "," + node.file_info.suffix
                            + "," + node.file_info.sha2 + "," + node.file_info.earliest_wine_time
                            + "," + node.file_info.av_count + "," + node.file_info.vt_time_stamp + "," + node.file_info.file_type_str
                            + "," + geo_info_str + "," +  entry.getKey()
                            + "," + Utils.encodeCollectionForPandasDataFrame(node.incoming_url_edges) + "," + Utils.encodeCollectionForPandasDataFrame(node.outgoing_url_edges)
                            + "," + nbd.cluster_init_url + "\n";
                }).collect(Collectors.toList());

                try (
                        OutputStreamWriter stream_writer = new FileWriter(dir_name + "/nbd_" + nbd_index.getAndIncrement() + ".csv");
                        BufferedWriter writer = new BufferedWriter(stream_writer);
                ) {
                    writer.write(header);
                    for (String str : nbd_strs)
                        writer.write(str);
                } catch (IOException e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
            }
        }
    }

    public synchronized void saveStateToDriveFST() {
        byte serialized_object[];

        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        Logger logger = GlobalParameters.getInstance().logger;
        FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();
        conf.setShareReferences(true);
        conf.registerSerializer(HashSet.class, new FSTCollectionSerializer(),true);
        logger.info("Serializing Introspection module. " + detection_snapshots.size() + " snapshots");

        Map<String, Long> earliest_detection_time = file_info.entrySet().parallelStream().filter(entry ->
                entry.getValue().earliest_wine_time != null).collect(Collectors.toConcurrentMap(Map.Entry::getKey,
                entry -> entry.getValue().earliest_wine_time));

        try (
                OutputStream file = new FileOutputStream(parameters.introspection_folder_name + "/introspection.object");
                OutputStream output = new BufferedOutputStream(file);
        ) {
            serialized_object = conf.asByteArray(this);
            output.write(serialized_object);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Cannot Serialize introspection.object");
            ex.printStackTrace();
        }

        try (
                OutputStream file = new FileOutputStream(parameters.introspection_folder_name + "/file_to_machine_ccs_map.object");
                OutputStream output = new BufferedOutputStream(file);
        ) {
            serialized_object = conf.asByteArray(file_to_machine_ccs_map);
            output.write(serialized_object);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Cannot Serialize introspection.object");
            ex.printStackTrace();
        }

        try (
                OutputStream file = new FileOutputStream(parameters.introspection_folder_name + "/file_to_machine_full_map.object");
                OutputStream output = new BufferedOutputStream(file);
        ) {
            serialized_object = conf.asByteArray(file_to_machine_full_map);
            output.write(serialized_object);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Cannot Serialize introspection.object");
            ex.printStackTrace();
        }

        try (
                OutputStream file = new FileOutputStream(parameters.introspection_folder_name + "/earliest_detection_time.object");
                OutputStream output = new BufferedOutputStream(file);
        ) {
            serialized_object = conf.asByteArray(earliest_detection_time);
            output.write(serialized_object);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Cannot Serialize introspection.object");
            ex.printStackTrace();
        }
        detection_snapshots.values().forEach(DetectionSnapshot::saveSnapshotToDrive);

        try (
                OutputStream file = new FileOutputStream(parameters.out_folder + "/configuration_parameters.txt");
                Writer writer = new BufferedWriter(new OutputStreamWriter(file, "ascii"));
        ) {
            String str_parameters = ToStringBuilder.reflectionToString(parameters);
            writer.write(str_parameters);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Cannot write configuration parameters");
            ex.printStackTrace();
        }
        logger.info("Introspection module serialized");
    }

    public synchronized void saveStateToDriveSTD() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        Logger logger = GlobalParameters.getInstance().detection_logger;

        try (
                OutputStream file = new FileOutputStream(parameters.introspection_folder_name);
                OutputStream buffer = new BufferedOutputStream(file);
                ObjectOutput output = new ObjectOutputStream(buffer);
        ) {
            output.writeObject(this);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Cannot serialize IntrospectionModule", ex);
            System.exit(-1);
        }
    }

    public static synchronized void loadStateFromDriveFST() {
        Map<String, Long> earliest_detection_time = null;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();
        Logger logger = GlobalParameters.getInstance().detection_logger;

        try {
            byte serialized_object[] = Files.readAllBytes(Paths.get(parameters.introspection_folder_name + "/introspection.object"));
            instance = (IntrospectionModule) conf.asObject(serialized_object);
            instance.file_info = new GraphDataLoader().getFileInfoMap();
            instance.machine_geo_location = new GraphDataLoader().getGeoLocationMap();
            instance.local_detector = TrainML_Models.loadLD_Model(parameters.LF_implementation_type);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Cannot deserialize introspection.object", e);
            System.exit(-1);
        }

        try {
            byte serialized_object[] = Files.readAllBytes(Paths.get(parameters.introspection_folder_name + "/file_to_machine_ccs_map.object"));
            instance.file_to_machine_ccs_map = (Map<String, List<Pair<Long, Long>>> ) conf.asObject(serialized_object);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Cannot deserialize earliest_detection_time.object", e);
            System.exit(-1);
        }

        try {
            byte serialized_object[] = Files.readAllBytes(Paths.get(parameters.introspection_folder_name + "/file_to_machine_full_map.object"));
            instance.file_to_machine_full_map = (Map<String, List<Pair<Long, Long>>> ) conf.asObject(serialized_object);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Cannot deserialize earliest_detection_time.object", e);
            System.exit(-1);
        }

        try {
            byte serialized_object[] = Files.readAllBytes(Paths.get(parameters.introspection_folder_name + "/earliest_detection_time.object"));
            earliest_detection_time = (Map<String, Long>) conf.asObject(serialized_object);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Cannot deserialize earliest_detection_time.object", e);
            System.exit(-1);
        }

        if (earliest_detection_time != null) {
            earliest_detection_time.entrySet().stream().forEach(entry ->
                    instance.file_info.get(entry.getKey()).earliest_wine_time = entry.getValue());
        }

        loadDetectionSnapshots(instance);
        instance.subset_selector_names = global_parameters.subset_selector
                .stream().map(MachineSelectionStrategy::toString).collect(Collectors.toCollection(ArrayList::new));
        instance.clearSubsetSelectorResutls();
        instance.init();
        global_parameters.logger.info("Serialized introspection module loaded\n");
    }

    public static void loadDetectionSnapshots(IntrospectionModule instance) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();
        conf.setShareReferences(true);
        conf.registerSerializer(HashSet.class, new FSTCollectionSerializer(),true);

        instance.detection_snapshots = new ConcurrentSkipListMap<>();
        List<String> introspection_files = Stream.of(new File(parameters.introspection_folder_name).listFiles())
                .map(File::getName).filter(file_name -> !file_name.contains("object")).sorted().collect(Collectors.toList());

        for (String file_name : introspection_files) {
            try {
                byte serialized_object[] = Files.readAllBytes(Paths.get(parameters.introspection_folder_name + File.separator + file_name));
                DetectionSnapshot snapshot = (DetectionSnapshot) conf.asObject(serialized_object);
                instance.detection_snapshots.put(snapshot.time_stamp, snapshot);
            } catch (IOException ex) {
                global_parameters.logger.log(Level.SEVERE, "Cannot deserialize %. Class not found.", file_name);
                System.exit(-1);
            }
        }
    }

    public void relabelNBDs() {
        int counter = 0;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();
        MutablePair<Integer, Integer> stat = new MutablePair<>(0, 0);

        // load XGBoost prediction
        switch(parameters.shape_gd_implementation) {
            case POST_PROCESSING_XGBOOST:
                Map<String, Double> xgboost_predictor = TrainML_Models.loadXGBoostNBD_Predictions();

                for (DetectionSnapshot snapshot : detection_snapshots.values()) {
                    snapshot.nbd_states.values().stream().filter(nbd -> nbd.cluster_hash_id != null).forEach(nbd -> {
                        double prediction = xgboost_predictor.get(nbd.cluster_hash_id);
                        nbd.shape_detector_label = prediction >= parameters.nbd_xgboost_threshold;
                        nbd.score = prediction;
                        if (nbd.shape_detector_label)
                            stat.setLeft(stat.getLeft()+1);
                        else
                            stat.setRight(stat.getRight()+1);
                    });
                    counter += snapshot.nbd_states.values().stream().filter(nbd -> nbd.cluster_hash_id != null).count();
                }
                break;

            case IDEAL:
                for (DetectionSnapshot snapshot : detection_snapshots.values()) {
                    snapshot.nbd_states.values().stream().filter(nbd -> nbd.cluster_hash_id != null).forEach(nbd -> {
                        nbd.shape_detector_label = nbd.mal_ratio >= parameters.enable_ideal_shape_gd;
                        nbd.score = nbd.mal_ratio;
                        if (nbd.shape_detector_label)
                            stat.setLeft(stat.getLeft()+1);
                        else
                            stat.setRight(stat.getRight()+1);
                    });
                    counter += snapshot.nbd_states.values().stream().filter(nbd -> nbd.cluster_hash_id != null).count();
                }
                break;
        }
        global_parameters.logger.info(String.format("relabelNBDs(): malicious nbds: %d, benign nbds: %d", stat.getLeft(), stat.getRight()));
    }

    public synchronized IntrospectionModule loadStateFromDriveStd() {
        IntrospectionModule statistics = null;
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        Logger logger = GlobalParameters.getInstance().detection_logger;

        try (
                InputStream file = new FileInputStream(parameters.introspection_folder_name);
                InputStream buffer = new BufferedInputStream(file);
                ObjectInput input = new ObjectInputStream(buffer);
        ) {
            statistics = (IntrospectionModule) input.readObject();
        } catch (ClassNotFoundException ex) {
            logger.log(Level.SEVERE, "Cannot deserialize IntrospectionModule. Class not found.", ex);
            System.exit(-1);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Cannot deserialize IntrospectionModule.", ex);
            System.exit(-1);
        }
        return statistics;
    }
}
