package edu.ut.austin.Introspection;

import edu.ut.austin.DataInputOutput.FileInfo;
import edu.ut.austin.DataInputOutput.GraphDataLoader;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.hadoop.hdfs.DFSClient;
import org.apache.ivy.core.module.descriptor.Configuration;
import org.nustaq.serialization.FSTConfiguration;
import org.nustaq.serialization.serializers.FSTCollectionSerializer;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Mikhail on 2/9/2017.
 */
public class DetectionSnapshot implements Serializable {
    public long time_stamp = -1;    // the timestamp of this snapshot, i.e. the time stamp of the end of the current sliding time window
    public int snapshot_data_size = -1;
    public int suspicious_nbd_count = -1;
    public int total_distinct_suspicious_graphs_count = -1;

    public int graph_detector_tp = -1;
    public int shape_detector_nbd_alerts = -1;

    public Set<String> total_malicious_unique_files_ground_truth = null;
    public Set<String> total_benign_unique_files_ground_truth = null;
    public Set<String> total_unique_file_count_in_scope = null;

    public Set<Long> infected_machines_ground_truth = null;
    public Set<Long> benign_machines_ground_truth = null;

    public int total_malicious_files_ground_truth, total_file_count_in_scope, total_unique_machine_count_in_scope;
    // node_count for a prefix graph, and bin_count for a LSH
    public int dynamic_parameter;
    // <cluster_id, nbd_stat>
    public ConcurrentHashMap<Integer, NBD_State> nbd_states;

    // early detection
    public DetectionSnapshot(long time_stamp) {
        this.time_stamp = time_stamp;
        nbd_states = new ConcurrentHashMap<>();
    }

    // save info about all valid NBDs
    public void setFV_Allocation(int nbd_index, int trial, Map<Long, Pair<List<Integer>, List<Integer>>> fv_assignment) {
        NBD_State nbd_state = nbd_states.get(nbd_index);
        nbd_state.setFV_Assignment(trial, fv_assignment);
    }

    public NBD_State addNBD(int nbd_index, int trial) {
        NBD_State nbd_state = new NBD_State();
        nbd_states.putIfAbsent(nbd_index, nbd_state);
        return nbd_state;
    }

    // save info about all valid NBDs
    @Deprecated
    public void setNBD_Structure(int nbd_index, int trial, Map<Long, Pair<Set<FileInfo>, Set<FileInfo>>> nbd_structure) {
        NBD_State nbd_state = new NBD_State();
        nbd_state.setNBD_Structure(trial, nbd_structure);
        nbd_states.putIfAbsent(nbd_index, nbd_state);
    }

    public int getMaliciousNBD_Count() {
        return (int) nbd_states.values().stream().filter(NBD_State::isMalicious).count();
    }

    public int getNBD_Count() {
        return nbd_states.size();
    }

    @Override
    public String toString() {
        return String.format("<snapshot_data_size (# of machines): %d, suspicious_nbd_count: %d, total_distinct_suspicious_graphs_count: %d>, " +
                "<graph_detector_tp: %d, shape_detector_nbd_alerts: %d>, <dynamic_parameter: %d>",
                snapshot_data_size, suspicious_nbd_count, total_distinct_suspicious_graphs_count,
                graph_detector_tp, shape_detector_nbd_alerts, dynamic_parameter);
    }

    public Set<String> getFileIDs_SubsetSelector(String subset_selector_name) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        return nbd_states.values().stream().filter(nbd -> (parameters.mal_check && nbd.isMalicious()) || !parameters.mal_check) //TODO malware
                .flatMap(nbd -> nbd.getSubsetSearchSuspiciousFiles(subset_selector_name).stream()).collect(Collectors.toSet());
    }

    public Set<String> getInfectedFileIDs_SubsetSelector(String subset_selector_name) {
        Set<String> set = getFileIDs_SubsetSelector(subset_selector_name);
        set.retainAll(getInfectedFileIDs_GroundTruth());
        return set;
    }

    public Set<String> getBenignFileIDs_SubsetSelector(String subset_selector_name) {
        Set<String> set = getFileIDs_SubsetSelector(subset_selector_name);
        set.retainAll(getBenignFileIDs_GroundTruth());
        return set;
    }

    public Set<String> getInfectedFileIDs_GroundTruth() {
        return nbd_states.values().stream().flatMap(nbd ->
                nbd.getUniqueMaliciousFiles_GroundTruth(0).stream()).collect(Collectors.toSet());
    }

    public Set<String> getBenignFileIDs_GroundTruth() {
        return nbd_states.values().stream().flatMap(nbd ->
                nbd.getUniqueBenignFiles_GroundTruth(0).stream()).collect(Collectors.toSet());
    }

    public Set<String> getFileIDs() {
        return nbd_states.values().stream().filter(NBD_State::isMalicious)
                .flatMap(nbd -> nbd.getNBD_Structure(0).values().stream())
                .flatMap(pair -> Stream.concat(pair.getLeft().stream(), pair.getRight().stream()))
                .map(file_info -> file_info.sha2_id).collect(Collectors.toSet());
    }

    public Set<Long> getMachineIDs_SubsetSelector(String subset_selector_name) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        return nbd_states.values().stream().filter(nbd -> (parameters.mal_check && nbd.isMalicious()) || !parameters.mal_check)
                .flatMap(nbd -> nbd.getSubsetSearchSuspiciousMachines(subset_selector_name).stream()).collect(Collectors.toSet());
    }

    public Set<Long> getInfectedMachineIDs_SubsetSelector(String subset_selector_name) {
        Set<Long> set = getMachineIDs_SubsetSelector(subset_selector_name);
        set.retainAll(getInfectedMachineIDs_GroundTruth());
        return set;
    }

    public Set<Long> getInfectedMachineIDs_GroundTruth() {
        return nbd_states.values().stream().flatMap(nbd ->
                nbd.getInfectedMachines_GroundTruth(0).stream()).collect(Collectors.toSet());
    }

    public Set<Long> getMachineAlerts() {
        return nbd_states.values().stream().filter(NBD_State::isMalicious)
                .flatMap(nbd -> nbd.machine_ids.stream()).collect(Collectors.toSet());
    }

    public void cleanUpNBDs(Set<Integer> indexes_to_keep) {
        nbd_states.keySet().retainAll(indexes_to_keep);
    }

    // <sha2_id, time_difference>, i.e. early/late detection time
    public Map<String, Long> computeEarlyFileInfectionDetectionTime(Set<String> quarantined_files,
                                                                    Map<String, FileInfo> file_info, String subset_selector_name) {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        Set<String> nodes = nbd_states.values().stream().filter(nbd -> (parameters.mal_check && nbd.isMalicious()) || !parameters.mal_check)
                .flatMap(nbd -> nbd.getSubsetSearchSuspiciousFiles(subset_selector_name).stream())
                .filter(sha2_id -> !quarantined_files.contains(sha2_id)).collect(Collectors.toSet());

        Map<String, Optional<Long>> ret = nodes.stream().filter(sha2_id -> getEarlyDetectionTime(file_info, sha2_id, time_stamp) != null)
                .collect(Collectors.groupingBy(Function.identity(),
                        Collectors.mapping(sha2_id -> getEarlyDetectionTime(file_info, sha2_id, time_stamp), Collectors.maxBy(Comparator.<Long>naturalOrder()))
                ));

        nbd_states.values().stream().filter(nbd -> (parameters.mal_check && nbd.isMalicious()) || !parameters.mal_check)
                .flatMap(nbd -> nbd.getSubsetSearchSuspiciousFiles(subset_selector_name).stream())
                .filter(sha2_id -> getEarlyDetectionTime(file_info, sha2_id, time_stamp) != null).forEach(quarantined_files::add);
        return ret.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().get()));
    }

    public Long getEarlyDetectionTime(Map<String, FileInfo> file_info_map, String file_name, long current_time_stamp) {
        FileInfo file_info = file_info_map.get(file_name);
        if (file_info == null || file_info.is_benign)
            return null;
        return file_info.vt_time_stamp - current_time_stamp;
    }

    @Deprecated
    // <avg. time, # of infected machines, early_detection_tiems>
    public Triple<Double, Integer, List<Double>> computeEarlyInfectionDetectionTimeOld(Set<Long> quarantined_machines, GraphDataLoader loader) {
        List<Double> list;
        Supplier<Stream<Long>> stream_supplier = () -> nbd_states.values().stream().filter(nbd_state -> nbd_state.isMalicious())
                .flatMap(nbd_state -> nbd_state.machine_ids.stream()).filter(machine_id -> !quarantined_machines.contains(machine_id)).distinct();

        Supplier<Stream<Double>> early_detection_time_supplier = () -> stream_supplier.get().map(machine_id ->
                (double) computeEarlyInfectionDetectionTimeHelper(machine_id, loader)).filter(Objects::nonNull).filter(time_stamp -> time_stamp > 0);

        list = early_detection_time_supplier.get().collect(Collectors.toList());
        OptionalDouble ret_optional = early_detection_time_supplier.get().mapToDouble(Double::valueOf).average();
        Double ret = ret_optional.isPresent() ? ret_optional.getAsDouble() : null;
        int count = (int) stream_supplier.get().count();
        nbd_states.values().forEach(nbd_state -> quarantined_machines.addAll(nbd_state.machine_ids));
        return new ImmutableTriple<>(ret, count, list);
    }

    private Long computeEarlyInfectionDetectionTimeHelper(Long machine_id, GraphDataLoader loader) {
        Long detection_time_stamp = loader.getGraphDetectionTime(machine_id);
        if (detection_time_stamp == null)
            return null;
        return detection_time_stamp - time_stamp;
    }

    private Double computeEarlyInfectionDetectionTimeHelper(Set<Long> machine_ids, GraphDataLoader loader) {
        OptionalDouble optional = machine_ids.stream().map(loader::getGraphDetectionTime)
                .filter(Objects::nonNull).mapToLong(time -> time - time_stamp).average();
        if (optional.isPresent())
            return optional.getAsDouble();
        else
            return null;
    }

    public void clearSubsetSelectorResutls() {
        nbd_states.values().forEach(NBD_State::clearSubsetSelectorResutls);
    }

    public void saveSnapshotToDrive() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();
        conf.setShareReferences(true);
        conf.registerSerializer(HashSet.class, new FSTCollectionSerializer(),true);

        try (
                OutputStream file = new FileOutputStream(parameters.introspection_folder_name + "/snapshot_" + time_stamp);
                OutputStream output = new BufferedOutputStream(file);
        ) {
            byte serialized_object[] = conf.asByteArray(this);
            output.write(serialized_object);
        } catch (Exception ex) {
            global_parameters.logger.log(Level.SEVERE, "Cannot Serialize introspection_%s serialized", time_stamp);
            ex.printStackTrace();
        }
    }
}
