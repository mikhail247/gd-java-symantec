package edu.ut.austin.NodeSelectionPolicy;

import com.google.common.base.Functions;
import edu.ut.austin.NBD.Cluster;
import edu.ut.austin.NBD.ClusterLSH;
import edu.ut.austin.NBD.DomainBasedNBD;
import edu.ut.austin.NBD.PrefixGraph.PrefixGraph;
import edu.ut.austin.NBD.PrefixGraph.TreeNode;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Mikhail on 2/2/2017.
 */
public class ConservativeNodeSelection<T> implements NodeSelectionPolicy<T>{
    /**
     * Select all the nodes whose children have less than min_num_machines machines
     * and those nodes taht have some unselected children
     * <TreeNode, machine_id>
     */
    // <complete_downloader_graph, TreeNode>
    @Override
    public ArrayList<Pair<TreeNode<T>, T>> getNodesForDetection(PrefixGraph prefix_graph, int min_num_files_per_nbd) {
        Set<TreeNode<T>> visited = new HashSet<>();
        ArrayList<Pair<TreeNode<T>, T>> ret = new ArrayList<>(prefix_graph.node_count);

        getNodesForDetection(prefix_graph.getRoot(), visited, ret, min_num_files_per_nbd);
        ret.trimToSize();
        return ret;
    }

    @Override
    public Map<Integer, Cluster> getNodesForDetection(ClusterLSH nbd_storage, int min_num_files_per_nbd) {
        Pair<Integer, Integer> pair;
        Map<Integer, Cluster> ret;
        ret = new HashMap<>();

        for (ClusterLSH.ClusterStorage cluster : nbd_storage.storage.get()) {
            pair = cluster.getBenignMaliciousNodeCount();

            if (pair.getLeft() + pair.getRight() > min_num_files_per_nbd)
                ret.put(cluster.cluster_id, cluster);
        }
        return ret;
    }

    @Override
    public Map<Integer, Cluster> getNodesForDetection(DomainBasedNBD nbd_storage, int min_num_files_per_nbd) {
        return nbd_storage.storage.get().parallelStream().filter(cluster -> {
            Pair<Integer, Integer> pair = cluster.getBenignMaliciousNodeCount();
            return pair.getLeft() + pair.getRight() > min_num_files_per_nbd;
        }).collect(Collectors.toMap(cluster -> ((DomainBasedNBD.ClusterStorage) cluster).cluster_id, cluster -> cluster));
    }

    private void getNodesForDetection(TreeNode<T> node, Set<TreeNode<T>> visited, ArrayList<Pair<TreeNode<T>, T>> ret,
                                      int min_num_files_per_nbd) {
        if (visited.contains(node))
            return;
        visited.add(node);
        int min_child_nodes = node.children.values().stream().mapToInt(n -> ((TreeNode) n)
                                .getSubgraphNodeCount()).min().orElse(0);

        if (node.getSubgraphNodeCount() > min_num_files_per_nbd
                && min_child_nodes < min_num_files_per_nbd && !node.isGraphEmpty()) {
            ret.add(new ImmutablePair<>(node, node.complete_graph));
        }

        for (Map.Entry<T, TreeNode<T>> entry : node.children.entrySet()) {
            int file_count = entry.getValue().getSubgraphNodeCount();
            if (file_count > min_num_files_per_nbd) {
                getNodesForDetection(entry.getValue(), visited, ret, min_num_files_per_nbd);
            }
        }
    }
}
