package edu.ut.austin.NodeSelectionPolicy;

import edu.ut.austin.NBD.Cluster;
import edu.ut.austin.NBD.ClusterLSH;
import edu.ut.austin.NBD.DomainBasedNBD;
import edu.ut.austin.NBD.PrefixGraph.PrefixGraph;
import edu.ut.austin.NBD.PrefixGraph.TreeNode;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Mikhail on 2/2/2017.
 */
public interface NodeSelectionPolicy<T> {
    public ArrayList<Pair<TreeNode<T>, T>> getNodesForDetection(PrefixGraph nbd_storage, int min_num_files_per_nbd);
    public Map<Integer, Cluster> getNodesForDetection(ClusterLSH nbd_storage, int min_num_files_per_nbd);
    public Map<Integer, Cluster> getNodesForDetection(DomainBasedNBD nbd_storage, int min_num_files_per_nbd);
}
