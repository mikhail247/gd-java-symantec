package edu.ut.austin;

import edu.ut.austin.AV_Strategy.KasperskyResponseTeam;
import edu.ut.austin.DataInputOutput.GraphDataLoader;
import edu.ut.austin.DataInputOutput.RF_TrainingProcedure;
import edu.ut.austin.FeatureExtractor.FeatureExtractor;
import edu.ut.austin.GlobalDataStructures.ConfigurationParameters;
import edu.ut.austin.GlobalDataStructures.GlobalParameters;
import edu.ut.austin.GraphClasses.DefaultDirectedGraphWrapper;
import edu.ut.austin.Introspection.DetectionSnapshot;
import edu.ut.austin.Introspection.IntrospectionModule;
import edu.ut.austin.ML_Models.TrainML_Models;
import edu.ut.austin.ML_Models.TrainShapeGD;
import edu.ut.austin.NBD.DomainBasedNBD;
import edu.ut.austin.NBD.NBD_Interface;
import edu.ut.austin.RealTimeSimulation.RealTimeDetection;
import edu.ut.austin.RealTimeSimulation.RealTimeSimulation;
import edu.ut.austin.RealTimeSimulation.ShapeGD_Generic;
import edu.ut.austin.Utils.Utils;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Engine {
    private void runDetectionParallel(GraphDataLoader loader, ShapeGD_Generic shape_detector) {
        RealTimeSimulation data_producer;
        RealTimeDetection detector;
        NBD_Interface nbd_storage;
        AtomicInteger epoch_processed = new AtomicInteger(0);
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        data_producer = new RealTimeSimulation(loader);
        detector = new RealTimeDetection(shape_detector, loader);
        nbd_storage = new DomainBasedNBD(loader);
        int epoch_total_number = data_producer.getEpochCount();
        GlobalParameters.shutdownSparkContext();

        class DataProcessingKernel extends Thread {
            private final int thread_id;
            private long first_time_stamp, last_time_stamp;

            public DataProcessingKernel (int thread_id, long first_time_stamp, long last_time_stamp) {
                this.thread_id = thread_id;
                this.first_time_stamp = first_time_stamp;
                this.last_time_stamp = last_time_stamp;
            }

            @Override
            public void run() {
                long current_virtual_time = first_time_stamp + parameters.shift_rolling_time_window;
                FeatureExtractor feature_extractor;
                Pair<Long, Collection<DefaultDirectedGraphWrapper>> pair;
                IntrospectionModule introspection = IntrospectionModule.getInstance();

                while (current_virtual_time < last_time_stamp
                        && (pair = data_producer.getRealTimeData(current_virtual_time, thread_id)) != null) {
                    Collection<DefaultDirectedGraphWrapper> data = pair.getRight();
                    introspection.addDetectionSnapshot(current_virtual_time, data);
                    feature_extractor = new FeatureExtractor(data);
                    nbd_storage.clear();
                    nbd_storage.augmentNBD_Storage(current_virtual_time, data, feature_extractor);
                    introspection.getDetectionSnapshot(current_virtual_time).dynamic_parameter = nbd_storage.getDynamicParameter();
                    detector.process(current_virtual_time, thread_id, nbd_storage, feature_extractor);
                    int epochs = epoch_processed.incrementAndGet();

                    // cleaning up the memory to avoid OutOfMemoryExceptions
                    if (parameters.store_detection_snapshots) {
                        introspection.getDetectionSnapshot(current_virtual_time).saveSnapshotToDrive();
                        introspection.removeDetectionSnapshot(current_virtual_time);
                    }
                    current_virtual_time += parameters.shift_rolling_time_window;
                    global_parameters.logger.info(() -> String.format("Completed: %d%%", (int) 100.0 * epochs / epoch_total_number));
                }
            }
        }
        Random random = new Random(12345);
        long time_step = (loader.getLastTimeStamp() + parameters.time_window_size
                - (loader.getFirstTimeStamp() - 1)) / parameters.engine_thread_count;
        List<DataProcessingKernel> threads = IntStream.range(0, parameters.engine_thread_count)
                .mapToObj(i -> {
                    long start_time_stamp = loader.getFirstTimeStamp() - 1 + i*time_step;
                    long end_time_stamp = loader.getFirstTimeStamp() - 1 + (i+1)*time_step;
                    return new DataProcessingKernel(i, start_time_stamp, end_time_stamp);
                }).collect(Collectors.toList());

        threads.forEach(thread -> {
            int delay = thread.thread_id * random.nextInt(5*1000);
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            thread.start();
        });

        threads.forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        detector.destroyExecutorPool();
    }

    private void runDetection(GraphDataLoader loader, ShapeGD_Generic shape_detector) {
        FeatureExtractor feature_extractor;
        Pair<Long, Collection<DefaultDirectedGraphWrapper>> pair;
        IntrospectionModule introspection = IntrospectionModule.getInstance();
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        RealTimeSimulation data_producer = new RealTimeSimulation(loader);
        RealTimeDetection detector = new RealTimeDetection(shape_detector, loader);
        NBD_Interface nbd_storage = new DomainBasedNBD(loader);
        GlobalParameters.shutdownSparkContext();

        while ((pair = data_producer.getRealTimeDataSpeculative()) != null) {
            long current_virtual_time = pair.getLeft();
            Collection<DefaultDirectedGraphWrapper> data = pair.getRight();
            global_parameters.av_strategy.removeKnownMalware(data);
            introspection.addDetectionSnapshot(current_virtual_time, data);
            feature_extractor = new FeatureExtractor(data);
            nbd_storage.clear();
            nbd_storage.augmentNBD_Storage(current_virtual_time, data, feature_extractor);
            introspection.getDetectionSnapshot(current_virtual_time).dynamic_parameter = nbd_storage.getDynamicParameter();
            detector.process(current_virtual_time, 0, nbd_storage, feature_extractor);
            DetectionSnapshot snapshot = introspection.getDetectionSnapshot(current_virtual_time);
            global_parameters.av_strategy.processAlerts(snapshot, data);
            global_parameters.av_strategy.printStatistics();
            if (parameters.store_detection_snapshots) {
                introspection.getDetectionSnapshot(current_virtual_time).saveSnapshotToDrive();
                introspection.removeDetectionSnapshot(current_virtual_time);
            }
        }
        detector.destroyExecutorPool();
    }

    private static boolean runXGBoostClassiifer() {
        String line = null;
        BufferedReader buffered_stream;

        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        Runtime rt = Runtime.getRuntime();

        try {
            Process proc = rt.exec("python NBD_XGBoostClassifier.py " + parameters.data_file_count + " " + parameters.out_folder);
            buffered_stream = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            while ( (line = buffered_stream.readLine()) != null)
                System.out.println(line);

            buffered_stream = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
            System.out.println("<ERROR>");
            while ( (line = buffered_stream.readLine()) != null)
                System.out.println(line);
            System.out.println("</ERROR>");
            int exit_value = proc.waitFor();
            return exit_value == 0;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void runEngine() {
        ConfigurationParameters parameters;
        GlobalParameters global_parameters;
        GraphDataLoader loader;
        RF_TrainingProcedure rf_trainer;
        ShapeGD_Generic shape_detector;

        parameters = ConfigurationParameters.getInstance();
        global_parameters = GlobalParameters.getInstance();

        loader = new GraphDataLoader();
        loader.loadData();
        loader.printStatistics();
        IntrospectionModule.init(loader);

        shape_detector = new ShapeGD_Generic(false);

        if (shape_detector.is_invalid) {
            global_parameters.logger.info("ShapeGD_Generic not found");
            System.exit(-1);
        }
        global_parameters.av_strategy = new KasperskyResponseTeam(loader.getFileInfoMap());
        rf_trainer = new RF_TrainingProcedure(loader);
        rf_trainer.trainGraphLevelRF_Detector();
        TrainML_Models.trainBisectingKMeansModel();
        TrainShapeGD.trainMultipleConditionalShapeGDs(loader);

        global_parameters.logger.info("engine_thread_count: " + parameters.engine_thread_count);
        Engine engine = new Engine();
        if (parameters.enable_av_strategy)
            // sequential execution
            engine.runDetection(loader, shape_detector);
        else
            // parallel execution
            engine.runDetectionParallel(loader, shape_detector);

        if (parameters.store_detection_snapshots)
            IntrospectionModule.loadDetectionSnapshots(IntrospectionModule.getInstance());
        // select some value for nbd_xgboost_threshold
        parameters.automatically_set_nbd_xgboost_threshold = true;

        if (parameters.shape_gd_implementation == ConfigurationParameters.ShapeGDImplementation.POST_PROCESSING_XGBOOST) {
            global_parameters.closeNBD_Writer();
            boolean success = runXGBoostClassiifer();
            if (success) {
                IntrospectionModule.getInstance().relabelNBDs();
            }
        }

        IntrospectionModule.getInstance().processNBDs();
        IntrospectionModule.getInstance().printResults();
        IntrospectionModule.getInstance().dumpAdvancedStat();
        IntrospectionModule.getInstance().dumpAggreagtedResultsTable();

        if (!ConfigurationParameters.brute_force_domain_urls) {
            GlobalParameters.getInstance().freeResources();
            IntrospectionModule.getInstance().saveStateToDriveFST();
        }
    }

    private static void runStatistics() {
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters global_parameters = GlobalParameters.getInstance();

        IntrospectionModule.loadStateFromDriveFST();
        IntrospectionModule introspection_module = IntrospectionModule.getInstance();
        introspection_module.relabelNBDs();
        introspection_module.dumpAllNBDsToFile();
        introspection_module.printResults();
        introspection_module.dumpAdvancedStat();
        IntrospectionModule.getInstance().dumpShapeLD_ROC();
        IntrospectionModule.getInstance().dumpShapeNBD_ROC();

        if (!ConfigurationParameters.brute_force_domain_urls)
            GlobalParameters.getInstance().freeResources();
    }

    public static void bruteForceDomainURLs(String[] args) {
        ConfigurationParameters.init(args, "empty");
        ConfigurationParameters parameters = ConfigurationParameters.getInstance();
        GlobalParameters.getInstance().loadSubsetSelectors();

        String folder_path = "results/results_" + parameters.output_folder_suffix;
        Set<String> processed_cases = Arrays.stream(new File(folder_path).listFiles())
                .filter(Objects::nonNull).filter(folder -> folder.isDirectory()).map(folder ->
                folder.getName()).collect(Collectors.toSet());

        // for 8 files: top domains < 8K, top urls < 11K
        // for 32 files: top domains < 20K, top urls < 26K
        Map<Integer, Triple<Integer, Integer, Integer>> configuration = new HashMap<>();
        configuration.put(8, new ImmutableTriple<>(8000, 11000, 500));
        configuration.put(32, new ImmutableTriple<>(20000, 26000, 2500));
        int top_N_domains_limit = configuration.get(parameters.data_file_count).getLeft();
        int top_N_urls_limit = configuration.get(parameters.data_file_count).getMiddle();
        int url_increment = configuration.get(parameters.data_file_count).getRight();

        for (int top_N_domains = 1000; top_N_domains <= top_N_domains_limit; top_N_domains += 1000) {
            for (int top_N_urls = 500; top_N_urls <= top_N_urls_limit; top_N_urls += url_increment) {
                String folder_name = "domains_" + top_N_domains + "_urls_" + top_N_urls;
                if (processed_cases.contains(folder_name))
                    continue;
                ConfigurationParameters.init(args, folder_name);
                parameters = ConfigurationParameters.getInstance();
                parameters.top_N_domains = top_N_domains;
                parameters.top_N_urls = top_N_urls;
                GlobalParameters.getInstance().logger.info("BruteForce: top_N_domains: " + top_N_domains
                        + " top_N_urls: " + top_N_urls);

                switch(ConfigurationParameters.getInstance().mode) {
                    case RUN_ANALISYS:
                        GlobalParameters.getInstance().logger.info("Run statistics");
                        runStatistics();
                        break;
                    case RUN_ENGINE:
                        GlobalParameters.getInstance().logger.info("Run engine");
                        runEngine();
                        break;
                }
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("Process PID: " + Utils.getPid());
        if (ConfigurationParameters.brute_force_domain_urls) {
            bruteForceDomainURLs(args);
            GlobalParameters.getInstance().freeResources();
            System.exit(-1);
        }

        ConfigurationParameters.init(args, null);
        GlobalParameters.getInstance().loadSubsetSelectors();

        switch(ConfigurationParameters.getInstance().mode) {
            case RUN_ENGINE:
                GlobalParameters.getInstance().logger.info("Run engine");
                runEngine();
                break;
            case RUN_ANALISYS:
                GlobalParameters.getInstance().logger.info("Run statistics");
                runStatistics();
                break;
        }
    }
}